<?php

return array(
	# Account credentials from developer portal
	'Account' => array(
		//'ClientId' => 'ASJq82eJITsnSVL_e_JIlIwYKRLeNVaKIL5I5OEUk29Rica-DtFS-AboR3H4_MDKcc9r7L2dOEDuKiI4',
		'ClientId' => 'ATps0pCsaHPi0w1v86WyJfu-sm8_XwnIiHYLrQMjyUgO1YXP9ZPWZdd5ngQ2SaEiiWMI4kn1MLxpE-19',
		'ClientSecret' => 'EEn6gXWAGMC4JqwMqEY5cJsejdr4xIm5jmOBn2rZsalfnWzfGZ4OaoWpPbvyjpW32h0fkzEv9mgG6UFe'
		//'ClientSecret' => 'EMYxUr26wBSWiQnst4UzLpAZCFKNfAnNj62JG5JQr9m6tgbtV5tB17pgovSEm8XN-4pSrR0DUulvMkgh',
	),

	# Connection Information
	'Http' => array(
		'ConnectionTimeOut' => 30,
		'Retry' => 1,
		//'Proxy' => 'http://[username:password]@hostname[:port][/path]',
	),

	# Service Configuration
	'Service' => array(
		# For integrating with the live endpoint,
		# change the URL to https://api.paypal.com!
		'EndPoint' => 'https://api.sandbox.paypal.com',
	),


	# Logging Information
	'Log' => array(
		'LogEnabled' => true,

		# When using a relative path, the log file is created
		# relative to the .php file that is the entry point
		# for this request. You can also provide an absolute
		# path here
		'FileName' => '../PayPal.log',

		# Logging level can be one of FINE, INFO, WARN or ERROR
		# Logging is most verbose in the 'FINE' level and
		# decreases as you proceed towards ERROR
		'LogLevel' => 'FINE',
	),
);
