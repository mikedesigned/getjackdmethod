<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CanGetAppDashboardsTest extends TestCase
{
    use DatabaseTransactions;

    public function testCanGetAppDashboard() {
        $user = factory(App\User::class)->create();
        $this->actingAs($user)
            ->get('/dashboard')
            ->see("My Stuff"); // the first tile of the dashboard
    }

    public function testCanGetMakeItHappenDashboard() {
        $user = factory(App\User::class)->create();
        $package = App\Package::find(1);
        $user->packages()->attach($package);

        $this->actingAs($user)
            ->get('/dashboard/business')
            ->see("Make It Happen"); // the dashboard for the business plan.
    }

    public function testCanGetMarketItDashboard() {
        $user = factory(App\User::class)->create();
        $package = App\Package::find(2);
        $user->packages()->attach($package);

        $this->actingAs($user)
            ->get('/dashboard/market')
            ->see("Brief Us"); // the first tile on the market it dashboard
    }

    public function testCanGetJustTellIt() {
        $user = factory(App\User::class)->create();
        $package = App\Package::find(3);
        $user->packages()->attach($package);

        $this->actingAs($user)
            ->get('/dashboard/research')
            ->see("Just Tell It"); // the just tell it dashboard
    }
}
