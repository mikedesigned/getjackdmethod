<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PackagePermissionsTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * The stub user does not have a package attached, so he
     * should receive 403 error at the dashboard for that
     * particular package.
     *
     * @return void
     */
    public function testUserCannotAccessUnownedPackage()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->get('/dashboard/business')
            ->assertResponseStatus(403); // error message
    }
}
