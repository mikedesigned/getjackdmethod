<?php

use Illuminate\Database\Seeder;


class BusinessPlanContentTableSeeder extends Seeder
{
    public function run()
    {
        factory(App\PlanContent::class, 50)->create()->each(function($bp){
           $bp->business_plan()->save(factory(App\BusinessPlan::class)->make());
        });
    }
}
