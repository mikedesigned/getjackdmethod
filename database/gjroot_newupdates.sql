-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 25, 2015 at 06:03 PM
-- Server version: 5.6.27-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gjroot`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `availability` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `type`, `start_time`, `end_time`, `status`, `availability`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 0, '2015-11-25 21:48:26', '2015-11-25 23:48:32', 1, 0, 1, '0000-00-00 00:00:00', '2015-11-03 02:19:09'),
(2, 0, '2015-11-23 08:00:00', '2015-11-23 10:00:00', 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, '2015-11-30 21:50:29', '2015-11-30 23:00:00', 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 0, '2015-11-01 08:00:00', '2015-11-01 10:00:00', 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 0, '2015-10-01 08:00:00', '2015-10-01 10:00:00', 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, '2015-12-23 15:01:29', '2015-12-23 17:01:29', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(7, 1, '2016-01-15 17:15:38', '2016-01-15 19:15:38', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(8, 1, '2016-02-25 04:07:39', '2016-02-25 06:07:39', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(9, 1, '2015-12-10 04:08:47', '2015-12-10 06:08:47', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(10, 2, '2016-02-19 04:40:12', '2016-02-19 06:40:12', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(11, 1, '2016-01-31 13:21:10', '2016-01-31 15:21:10', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(12, 1, '2015-12-21 04:46:31', '2015-12-21 06:46:31', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(13, 2, '2016-02-19 10:26:13', '2016-02-19 12:26:13', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(14, 1, '2015-12-16 21:46:47', '2015-12-16 23:46:47', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(15, 2, '2015-12-29 11:09:52', '2015-12-29 13:09:52', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(16, 1, '2016-02-15 21:14:59', '2016-02-15 23:14:59', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(17, 1, '2016-02-17 05:00:09', '2016-02-17 07:00:09', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(18, 1, '2016-01-20 03:01:50', '2016-01-20 05:01:50', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(19, 2, '2015-12-31 10:30:48', '2015-12-31 12:30:48', 0, 1, 0, '2015-11-03 02:10:57', '2015-11-03 02:10:57'),
(20, 0, '2015-11-11 01:26:58', '2015-11-11 03:26:58', 0, 0, 1, '2015-11-03 02:10:57', '2015-11-03 02:16:38'),
(21, 2, '2016-01-26 21:10:46', '2016-01-26 23:10:46', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(22, 0, '2015-11-20 06:32:06', '2015-11-20 08:32:06', 0, 0, 1, '2015-11-03 02:10:58', '2015-11-03 02:16:26'),
(23, 2, '2015-12-14 21:20:57', '2015-12-14 23:20:57', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(24, 2, '2016-02-03 09:07:12', '2016-02-03 11:07:12', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(25, 2, '2016-02-16 01:41:36', '2016-02-16 03:41:36', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(26, 0, '2015-11-20 09:36:02', '2015-11-20 11:36:02', 0, 0, 1, '2015-11-03 02:10:58', '2015-11-03 02:15:46'),
(27, 1, '2015-12-08 11:35:32', '2015-12-08 13:35:32', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(28, 2, '2016-02-18 21:34:39', '2016-02-18 23:34:39', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(29, 2, '2016-02-29 09:37:00', '2016-02-29 11:37:00', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(30, 2, '2015-11-08 20:27:56', '2015-11-08 22:27:56', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(31, 1, '2015-12-23 10:27:58', '2015-12-23 12:27:58', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(32, 2, '2016-02-18 17:29:13', '2016-02-18 19:29:13', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(33, 2, '2015-11-07 01:28:08', '2015-11-07 03:28:08', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(34, 1, '2015-11-10 13:58:41', '2015-11-10 15:58:41', 0, 1, 0, '2015-11-03 02:10:58', '2015-11-03 02:10:58'),
(35, 0, '2015-11-13 04:10:55', '2015-11-13 06:10:55', 0, 0, 1, '2015-11-03 02:10:59', '2015-11-03 02:18:18'),
(36, 0, '2015-11-12 12:33:50', '2015-11-12 14:33:50', 0, 0, 1, '2015-11-03 02:10:59', '2015-11-03 02:16:59'),
(37, 2, '2015-12-14 04:03:15', '2015-12-14 06:03:15', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(38, 1, '2015-12-21 01:14:05', '2015-12-21 03:14:05', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(39, 2, '2016-03-02 10:02:19', '2016-03-02 12:02:19', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(40, 2, '2016-01-11 23:00:14', '2016-01-12 01:00:14', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(41, 0, '2015-11-21 09:35:03', '2015-11-21 11:35:03', 0, 0, 1, '2015-11-03 02:10:59', '2015-11-03 02:30:43'),
(42, 2, '2015-11-29 21:09:15', '2015-11-29 23:09:15', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(43, 2, '2016-01-19 19:11:22', '2016-01-19 21:11:22', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(44, 0, '2015-11-11 21:16:41', '2015-11-11 23:16:41', 0, 0, 1, '2015-11-03 02:10:59', '2015-11-17 02:52:00'),
(45, 1, '2015-11-23 11:42:07', '2015-11-23 13:42:07', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(46, 1, '2015-12-07 00:25:40', '2015-12-07 02:25:40', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(47, 0, '2015-12-16 19:14:08', '2015-12-16 21:14:08', 0, 0, 1, '2015-11-03 02:10:59', '2015-11-16 01:05:25'),
(48, 1, '2015-12-14 00:31:08', '2015-12-14 02:31:08', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(49, 2, '2015-12-22 06:36:06', '2015-12-22 08:36:06', 0, 1, 0, '2015-11-03 02:10:59', '2015-11-03 02:10:59'),
(50, 2, '2015-12-15 04:08:14', '2015-12-15 06:08:14', 0, 1, 0, '2015-11-03 02:11:00', '2015-11-03 02:11:00'),
(51, 2, '2015-12-16 07:49:24', '2015-12-16 09:49:24', 0, 1, 0, '2015-11-03 02:11:00', '2015-11-03 02:11:00'),
(52, 0, '2015-11-11 09:58:55', '2015-11-11 11:58:55', 0, 0, 1, '2015-11-03 02:11:00', '2015-11-21 04:14:30'),
(53, 1, '2016-03-02 22:58:31', '2016-03-03 00:58:31', 0, 1, 0, '2015-11-03 02:11:00', '2015-11-03 02:11:00'),
(54, 1, '2016-01-15 06:39:58', '2016-01-15 08:39:58', 0, 1, 0, '2015-11-03 02:11:00', '2015-11-03 02:11:00'),
(55, 2, '2015-11-04 17:00:11', '2015-11-04 19:00:11', 0, 1, 0, '2015-11-03 02:11:00', '2015-11-03 02:11:00');

-- --------------------------------------------------------

--
-- Table structure for table `briefing_form_documents`
--

CREATE TABLE `briefing_form_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `briefing_form_submission_id` int(11) NOT NULL,
  `briefing_question_answer_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `doc_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `briefing_form_questions`
--

CREATE TABLE `briefing_form_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `briefing_form_questions`
--

INSERT INTO `briefing_form_questions` (`id`, `question_text`, `question_name`, `question_type`, `created_at`, `updated_at`) VALUES
(1, 'Business name', 'business_name', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Business contact name and title', 'contact_title', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Email Address', 'email', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Tell us about your business (what do you sell? How long have you been in buisiness? Just some background information on your business)', 'about_your_business', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'What’s your annual turnover?', 'turnover', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'How do you sell your product or service? (wholesale, retail, online, offline). And for how much (price range)?', 'selling_method', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Who is your target audience? Your customer, their age, income and occupation etc', 'target_audience', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Who is your ideal customer?  What do you know about them?', 'ideal_customer', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'What are your vision and mission statements (core messages)', 'core_messages', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'What’s your current brand position? Look and feel. What do you stand for?', 'current_brand_position', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Please provide your logo, slogan, website address and social media accounts (handles and hashtags you use)', 'logo', 'file', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Do you know who your biggest competitors are?\rIf yes, please list them and what you know about them', 'competitor', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Unique selling position (what are you currently offering that competitors aren’t). \r\rHow do you differentiate yourself from your competitors?', 'unique_selling_position', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Have you conducted any market research before (i.e. surveys, focus groups, your industry research). What were the results?', 'market_research', 'file', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'How do you manage your customers feedback? Positive or negative', 'customer_feedback', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'How do your customers hear about you, what you offer and where you’re located?', 'hear_about_you', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'How do you stay in contact with your customers? i.e. newsletters etc', 'contact_method', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Have you done any other marketing, PR or advertising in the past? \r\rIf yes please describe and show us.', 'marketing_material', 'file', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Tell us what you want to market? Is it the business in general or a specific product/service, launch, event or campaign?\r\rPlease give us a description of what you’d like to focus on.', 'what_to_market', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'What are your media and objectives? Do you have clear marketing KPI’s?\r\r(Do you want to generate inquiries, create awareness (brand), provide information, build your image,  sell a product or announce a new service)?', 'media_and_objectives', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'What is your media and marketing budget? And how did you come to this number?', 'budget', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'What are your campaign timings for the budget outlined?', 'campaign_timings', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'What markets would you like to focus your marketing activity on?', 'market_focus', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'What media platforms are you interested in exploring? And how much do you know about each? \r\rFor example; social media, TV, radio, digital, print, outdoor, PR, sponsorships, events or direct mail.', 'media_platforms', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'Do you have a social media plan for your business?\r\rAnd who maintains your website and social media accounts', 'social_media_plan', 'textarea', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'Do you have a loyalty program in place?', 'loyalty_program', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'How will you be measuring the success of the marketing activity you select?', 'measuring_criteria', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'Are there any factors that could Influence the success of your marketing effort (ie holidays, cost or availability of products and services across the year, peak – off peak times or staffing issues?)', 'influential_factors', 'text', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `briefing_form_submissions`
--

CREATE TABLE `briefing_form_submissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `briefing_question_answers`
--

CREATE TABLE `briefing_question_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `briefing_form_submission_id` int(11) NOT NULL,
  `briefing_question_id` int(11) NOT NULL,
  `briefing_question_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `buckets`
--

CREATE TABLE `buckets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mailchimp_list_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `buckets`
--

INSERT INTO `buckets` (`id`, `name`, `created_at`, `updated_at`, `mailchimp_list_id`) VALUES
(1, 'New Signups', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(2, 'Business Package Users', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(3, 'Signed Up For Over Six Months', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(4, 'Hasn''t Upgraded Any Packages', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `bucket_user`
--

CREATE TABLE `bucket_user` (
  `bucket_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bucket_user`
--

INSERT INTO `bucket_user` (`bucket_id`, `user_id`) VALUES
(1, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `business_plan_documents`
--

CREATE TABLE `business_plan_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `doc_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `business_plan_id` int(11) NOT NULL,
  `business_plan_category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `filepath` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_contents`
--

CREATE TABLE `default_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_contents`
--

INSERT INTO `default_contents` (`id`, `content`, `cat_id`, `created_at`, `updated_at`) VALUES
(9, '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th>\r\n			<p>Goals</p>\r\n			</th>\r\n			<th>\r\n			<p>What steps will you take to achieve your goals/objectives</p>\r\n			</th>\r\n			<th>\r\n			<p>When do you expect to achieve your set goals/objectives?</p>\r\n			</th>\r\n			<th>\r\n			<p>Person responsible</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>What are the business goals/objectives?</p>\r\n			</td>\r\n			<td>\r\n			<p>Action steps. Step 1,2,3, etc.</p>\r\n			</td>\r\n			<td>\r\n			<p>Add dates</p>\r\n			</td>\r\n			<td>\r\n			<p>Who&rsquo;ll be responsible to make sure your goal is achieved?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 33, '2015-10-18 18:10:38', '2015-10-18 18:10:38'),
(10, '<table border="1" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>General Business Details</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Description</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business name:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Trading name:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Registered date:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Registered location:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business structure:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Your business number/s:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Location of premises:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Buy/lease agreements</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Relevant business taxes: are you registered? Yes or no?</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Licences and permits:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Domain/website names:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Memberships and affiliations:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business trading hours:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Add more&hellip;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Products/Services</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Products/services:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Add more&hellip;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Owner Details</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business owner(s):</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Relevant owner experience:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Add more&hellip;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:14:07', '2015-10-18 18:14:07'),
(11, '<table border="1" cellpadding="0" cellspacing="0" style="width:99%" summary="A table with space to enter details of current staff under the headings: Job Title (e.g. Marketing/ Sales Manager), Name (e.g. Mr Chris Brantley), Expected staff turnover (e.g. 12-18 months) and Skills or strengths (e.g. Relevant qualifications in Sales/Marketing. At least 5 years experience in the industry. Award in marketing excellence).">\r\n	<thead>\r\n		<tr>\r\n			<th style="width:135px">\r\n			<p>Name</p>\r\n			</th>\r\n			<th style="width:184px">\r\n			<p>Job Title</p>\r\n			</th>\r\n			<th style="width:293px">\r\n			<p>Skills/Qualifications or Strengths/Responsibilities</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:135px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:184px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:293px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:14:51', '2015-10-18 18:14:51'),
(12, '<table border="1" cellpadding="0" cellspacing="0" style="width:99%" summary="A table with space to enter details of required staff under the headings: Job Title (e.g. Office Manager), Quantity (e.g. 1), Expected staff turnover (e.g. 2-3 years), Skills necessary (e.g. Relevant qualifications in Office Management. At least 2 years experience.), and date required (e.g. Month/Year).">\r\n	<thead>\r\n		<tr>\r\n			<th style="width:97px">\r\n			<p>Job Title</p>\r\n			</th>\r\n			<th style="width:97px">\r\n			<p>Quantity</p>\r\n			</th>\r\n			<th style="width:267px">\r\n			<p>Necessary Skills</p>\r\n			</th>\r\n			<th style="width:151px">\r\n			<p>Date Required</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:97px">\r\n			<p>What is the position you&rsquo;ll be looking to fill?</p>\r\n			</td>\r\n			<td style="width:97px">\r\n			<p>How many of these positions will you need?</p>\r\n			</td>\r\n			<td style="width:267px">\r\n			<p>List the experience or qualifications required</p>\r\n			</td>\r\n			<td style="width:151px">\r\n			<p>When do you expect to need this position?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:15:07', '2015-10-18 18:15:07'),
(13, '<table align="left" border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th scope="col">\r\n			<p>Product/Service</p>\r\n			</th>\r\n			<th scope="col">\r\n			<p>Description</p>\r\n			</th>\r\n			<th scope="col">\r\n			<p>Market Demand</p>\r\n			</th>\r\n			<th scope="col">\r\n			<p>Distributor/Supplier</p>\r\n			</th>\r\n			<th scope="col">\r\n			<p>Cost for Distributor/Supplier</p>\r\n			</th>\r\n			<th scope="col">\r\n			<p>Price</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>Product/Service &ndash; include all necessary codes</p>\r\n			</td>\r\n			<td>\r\n			<p>Brief description</p>\r\n			</td>\r\n			<td>\r\n			<p>Anticipated demand from the market</p>\r\n			</td>\r\n			<td>\r\n			<p>Supplier/distributor details</p>\r\n			</td>\r\n			<td>\r\n			<p>Costs associated from Distributor/supplier</p>\r\n			</td>\r\n			<td>\r\n			<p>Include final retail price and all taxes</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:16:42', '2015-10-18 18:25:02'),
(14, '<table align="left" border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th scope="col" style="width:250px">\r\n			<p>Inventory Item</p>\r\n			</th>\r\n			<th scope="col" style="width:250px">\r\n			<p>Cost Per Unit</p>\r\n			</th>\r\n			<th scope="col" style="width:250px">\r\n			<p>Quantity on Hand</p>\r\n			</th>\r\n			<th scope="col" style="width:250px">\r\n			<p>Total</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:250px">\r\n			<p>List your inventory &ndash; include any necessary internal codes</p>\r\n			</td>\r\n			<td style="width:250px">\r\n			<p>How much does each unit cost?</p>\r\n			</td>\r\n			<td style="width:250px">\r\n			<p>How many units do you have on hand?</p>\r\n			</td>\r\n			<td style="width:250px">\r\n			<p>What is the total cost?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:17:03', '2015-10-18 18:25:25'),
(15, '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th scope="col" style="width: 160px;">\r\n			<p>Product/Service</p>\r\n			</th>\r\n			<th scope="col" style="width: 160px;">\r\n			<p>Distribution Channel</p>\r\n			</th>\r\n			<th scope="col" style="width: 160px;">\r\n			<p>Reason for Chosen Channel</p>\r\n			</th>\r\n			<th scope="col" style="width: 160px;">\r\n			<p>Method of Sale</p>\r\n			</th>\r\n			<th scope="col" style="width: 160px;">\r\n			<p>Strengths</p>\r\n			</th>\r\n			<th scope="col" style="width: 160px;">\r\n			<p>Weaknesses</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:160px">\r\n			<p>List the product/service</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Where your customers will be able to purchase your product/service, .e.g., shopfront, online</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Why you chose this channel, &nbsp;e.g., because it&rsquo;s where your target market is based</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>How you&rsquo;ll reach customers, e.g., salespeople actively selling door to door</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>List all strengths</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>List all weaknesses</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:17:16', '2015-10-18 18:25:55'),
(16, '<table align="left" border="1" cellpadding="0" cellspacing="0" style="width:1031px" summary="A table with space to enter details of products and services, including the product/service name, a brief description, and price (including GST).">\r\n	<thead>\r\n		<tr>\r\n			<th style="width:217px">\r\n			<p>Legal Requirement</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</th>\r\n			<th style="width:345px">\r\n			<p>Description</p>\r\n			</th>\r\n			<th style="width:266px">\r\n			<p>Date Completed</p>\r\n			</th>\r\n			<th style="width:203px">\r\n			<p>Cost</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:217px">\r\n			<p>What is the legal requirement, e.g., trademark application</p>\r\n			</td>\r\n			<td style="width:345px">\r\n			<p>Give a brief description of your legal requirement. For example, do you need assistance from a lawyer to officially trademark your business name?</p>\r\n			</td>\r\n			<td style="width:266px">\r\n			<p>What date do you intend to have this completed?</p>\r\n			</td>\r\n			<td style="width:203px">\r\n			<p>Detail any costs associated, such as lawyer fees</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:17:34', '2015-10-18 18:17:34'),
(17, '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th scope="col" style="width: 200px;">\r\n			<p>Insurance Type</p>\r\n			</th>\r\n			<th scope="col" style="width: 200px;">\r\n			<p>Description/Coverage</p>\r\n			</th>\r\n			<th scope="col" style="width: 200px;">\r\n			<p>Insurance Broker/Company/Contact Details</p>\r\n			</th>\r\n			<th scope="col" style="width: 200px;">\r\n			<p>Premiums/Cost</p>\r\n			</th>\r\n			<th scope="col" style="width: 200px;">\r\n			<p>Renewal Date</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:200px">\r\n			<p>Example: professional indemnity insurance</p>\r\n			</td>\r\n			<td style="width:200px">\r\n			<p>Give a brief description of the cover</p>\r\n			</td>\r\n			<td style="width:200px">\r\n			<p>Who is your policy with?</p>\r\n			</td>\r\n			<td style="width:200px">\r\n			<p>What are the costs associated with the policy?</p>\r\n			</td>\r\n			<td style="width:200px">\r\n			<p>When is your policy due for renewal?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:17:50', '2015-10-18 18:26:19'),
(18, '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th>\r\n			<p>Potential Risk</p>\r\n			</th>\r\n			<th>\r\n			<p>Level of Severity</p>\r\n			</th>\r\n			<th>\r\n			<p>Likelihood</p>\r\n			</th>\r\n			<th>\r\n			<p>Impact</p>\r\n			</th>\r\n			<th>\r\n			<p>Strategy to Avoid/Manage</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>What is the potential risk to your business?</p>\r\n			</td>\r\n			<td>\r\n			<p>How severe could the damage be (minor, moderate, significant)?</p>\r\n			</td>\r\n			<td>\r\n			<p>What is the likelihood this risk could occur (low, medium, high)?</p>\r\n			</td>\r\n			<td>\r\n			<p>What is the impact of the risk to your business (minor, moderate, significant)?</p>\r\n			</td>\r\n			<td>\r\n			<p>What strategies will you put in place to try and avoid the risk?</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:18:46', '2015-10-18 18:24:00'),
(19, '<table border="1" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th style="width:277px">\r\n			<p>Software and Programs</p>\r\n			</th>\r\n			<th style="width:126px">\r\n			<p>Purchase Date</p>\r\n			</th>\r\n			<th style="width:119px">\r\n			<p>Purchase Price</p>\r\n			</th>\r\n			<th style="width:97px">\r\n			<p>Updating or Running Costs</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:277px">\r\n			<p>File sharing programs/email systems</p>\r\n			</td>\r\n			<td style="width:126px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:119px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:97px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:277px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:126px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:119px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:97px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th style="width:267px">\r\n			<p>Potential IT Issues/Required Updates or Maintenance</p>\r\n			</th>\r\n			<th style="width:158px">\r\n			<p>Person Responsible</p>\r\n			</th>\r\n			<th style="width:176px">\r\n			<p>Dates of Scheduled Updates/Maintenance</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:267px">\r\n			<p>Example: update all anti-virus software</p>\r\n			</td>\r\n			<td style="width:158px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:176px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:267px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:158px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:176px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:19:40', '2015-10-18 18:26:54'),
(20, '<table border="1" cellpadding="0" cellspacing="0" style="width:100%" summary="A table with space to enter details of plant and equipment purchases under the headings: Equipment (e.g Personal Computer), purchase date (e.g. 20/03/2010), purchase price (e.g $2100) and running cost (e.g $100 a month).">\r\n	<thead>\r\n		<tr>\r\n			<th style="width:269px">\r\n			<p>Equipment</p>\r\n			</th>\r\n			<th style="width:122px">\r\n			<p>Purchase Date</p>\r\n			</th>\r\n			<th style="width:115px">\r\n			<p>Purchase Price</p>\r\n			</th>\r\n			<th style="width:95px">\r\n			<p>Running Cost</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:269px">\r\n			<p>Examples: machinery/printers/computers</p>\r\n			</td>\r\n			<td style="width:122px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:115px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:95px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:19:56', '2015-10-18 18:19:56'),
(21, '<table align="left" border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Name</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Date Established</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Business Size </strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Estimated Turnover/Market Share</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Value to Customer</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Business Strengths</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Business Weaknesses</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Competitor name</p>\r\n			</td>\r\n			<td>\r\n			<p>When did they enter the market?</p>\r\n			</td>\r\n			<td>\r\n			<p>Staff numbers, sites, offices, stores etc.</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>Estimated revenue and market share of competitor</p>\r\n			</td>\r\n			<td>\r\n			<p>Niche product/service, time in market etc.</p>\r\n			</td>\r\n			<td>\r\n			<p>Example: convenience to customer</p>\r\n			</td>\r\n			<td>\r\n			<p>Limited offering, cost of product/service etc.</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:22:31', '2015-10-18 18:22:31'),
(22, '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="height:30px; width:497px">\r\n			<p><strong>STRENGTHS</strong></p>\r\n			</td>\r\n			<td style="height:30px; width:497px">\r\n			<p><strong>WEAKNESSES</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="height:34px; width:497px">\r\n			<p>What you&rsquo;re good at</p>\r\n			</td>\r\n			<td style="height:34px; width:497px">\r\n			<p>What you&rsquo;re not so good at</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="height:29px; width:497px">\r\n			<p><strong>OPPORTUNITIES</strong></p>\r\n			</td>\r\n			<td style="height:29px; width:497px">\r\n			<p><strong>THREATS</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="height:36px; width:497px">\r\n			<p>Areas of growth for your business that will give you an edge over competitors</p>\r\n			</td>\r\n			<td style="height:36px; width:497px">\r\n			<p>What will influence your business growth/success?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:23:09', '2015-10-18 18:23:09'),
(23, '<table border="1" cellpadding="1" cellspacing="1" style="width:100%">\r\n	<thead>\r\n		<tr>\r\n			<th scope="col" style="width:140px">\r\n			<p>Marketing and Media Activity</p>\r\n			</th>\r\n			<th scope="col" style="width:140px">\r\n			<p>Target Product/Service</p>\r\n			</th>\r\n			<th scope="col" style="width:140px">\r\n			<p>Reason for Chosen Channel</p>\r\n			</th>\r\n			<th scope="col" style="width:140px">\r\n			<p>Measurement of Success</p>\r\n			</th>\r\n			<th scope="col" style="width:140px">\r\n			<p>Total Budget Allocation ($)</p>\r\n			</th>\r\n			<th scope="col" style="width:140px">\r\n			<p>When</p>\r\n			</th>\r\n			<th scope="col" style="width:140px">\r\n			<p>Person Responsible</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:140px">\r\n			<p>AdWords, radio, giveaways, launch, media release etc.</p>\r\n			</td>\r\n			<td style="width:140px">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style="width:140px">\r\n			<p>E.g. product launch, sales, brand awareness</p>\r\n			</td>\r\n			<td style="width:140px">\r\n			<p>E.g. Increase sales by 20%</p>\r\n			</td>\r\n			<td style="width:140px">\r\n			<p>$</p>\r\n			</td>\r\n			<td style="width:140px">\r\n			<p>Insert campaign dates</p>\r\n			</td>\r\n			<td style="width:140px">\r\n			<p>Who will be responsible for carrying out the campaign?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, '2015-10-18 18:23:38', '2015-10-18 18:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_15_045303_create_business_plan_categories_table', 2),
('2015_10_15_054230_create_business_plan_contents_table', 3),
('2015_10_15_054849_CreateBusinessContentCategoriesTable', 4),
('2015_10_15_064134_AddStatusColumnToBusinessPlanContentTable', 4),
('2015_10_15_070308_AddUserIdToBusinessPlanContentsTable', 5),
('2015_10_15_071014_create_business_plan_tips_table', 6),
('2015_10_15_071140_AddTipIdToBusinessPlanCategoryTable', 7),
('2015_10_15_114545_AddCatIdToTipsTable', 8),
('2015_10_16_010434_create_business_plans_table', 9),
('2015_10_16_022752_AddCatIdToContentsTable', 9),
('2015_10_16_134046_AddNameToBusinessPlansTable', 9),
('2015_10_16_134234_AddBusinessPlanIdToContentTable', 9),
('2015_10_16_160141_create_business_plan_documents_table', 10),
('2015_10_17_135105_AddTypeColumnToBusinessCategoriesTable', 11),
('2015_10_17_144519_AddUserIdToFileUpload', 12),
('2015_10_17_154239_create_business_plan_default_contents_table', 13),
('2015_10_19_033916_AddDefaultContentIdToCategoryTable', 14),
('2015_10_19_040156_ChangeContentColumnOnDefaultContentTable', 15),
('2015_10_23_054503_create_packages_table', 16),
('2015_10_24_091052_create_user_addresses_table', 16),
('2015_10_24_092210_AddAddressColumnToUserTable', 17),
('2015_10_25_065824_create_package_user_pivot_table', 18),
('2015_10_25_081301_create_buckets_table', 19),
('2015_10_25_081701_create_bucket_user_pivot_table', 20),
('2015_10_26_123813_ChangePackagesDescriptionColumn', 21),
('2013_04_09_062329_create_revision_table', 22),
('2015_10_26_132115_create_appointments_table', 22),
('2015_10_30_043243_add_availability_to_appointments_table', 23),
('2015_10_30_135044_add_appointment_type_to_appointments', 24),
('2015_11_04_011121_create_marketing_plans_table', 25),
('2015_11_04_011704_AddPolyRelationToContentsTable', 25),
('2015_11_04_021504_RenameKeyTables', 25),
('2015_11_04_023611_ClearBPIDFromContentsTable', 25),
('2015_11_13_043957_AddFieldMailchimpListId', 26),
('2015_11_18_015701_create_jobs_table', 26),
('2015_11_18_033424_create_failed_jobs_table', 26),
('2015_11_18_094057_AddPlanTypeToCategoryTable', 26),
('2015_11_18_114438_create_pages_table', 27),
('2015_11_18_114439_create_page_closures_table', 27),
('2015_11_18_120022_add_slug_to_pages_table', 28),
('2015_11_18_144754_AddFilepathToMarketingPlansTable', 29),
('2015_11_19_010213_AddColumnUpgradedAtToUsersTable', 30),
('2015_11_19_051630_AddColumnMailchimpMemberHashToUsers', 30),
('2015_11_19_190313_AddFilePathToBPDocs', 30),
('2015_11_19_164912_CreateTableBriefingFormQuestions', 31),
('2015_11_19_182209_AddFieldBriefedToUsersTable', 31),
('2015_11_20_045655_CreateTableBriefingFormSubmission', 32),
('2015_11_20_045729_CreateTableBriefingQuestionAnswers', 32),
('2015_11_20_045759_CreateTableBriefingFormDocuments', 32),
('2015_11_23_015057_AddFileFieldsToBriefingFormDocumentsTable', 33),
('2014_02_10_145728_notification_categories', 34),
('2014_08_01_210813_create_notification_groups_table', 34),
('2014_08_01_211045_create_notification_category_notification_group_table', 34),
('2015_05_05_212549_create_notifications_table', 34),
('2015_06_06_211555_add_expire_time_column_to_notification_table', 34),
('2015_06_06_211555_change_type_to_extra_in_notifications_table', 34),
('2015_06_07_211555_alter_category_name_to_unique', 34),
('2015_11_23_061823_CreatePlansTable', 35),
('2015_11_23_062003_AddPlanTypeToPlansTable', 35),
('2015_11_23_071046_AddFilepathToPlansTable', 35),
('2015_11_24_033012_DropTableBusinessPlan', 35),
('2015_11_24_033034_DropTableMarketingPlan', 35),
('2015_11_24_034531_AddPlanIDToPlanContentsTable', 35),
('2015_11_24_053301_DropColumnsContentablesFromPlanContentsTable', 35);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_id` bigint(20) UNSIGNED NOT NULL,
  `from_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_id` bigint(20) UNSIGNED NOT NULL,
  `to_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expire_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications_categories_in_groups`
--

CREATE TABLE `notifications_categories_in_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_categories`
--

CREATE TABLE `notification_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_groups`
--

CREATE TABLE `notification_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `price`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Make It Happen!', 49.00, '<ul class="feature-list">\r\n      <li>Unlimited Private Pens</li>\r\n      <li>Live View</li>\r\n      <li>Asset Hosting <span class="feature-amount">1 GB</span></li>\r\n      <li>Collab Mode <span class="feature-amount">2 people</span></li>\r\n      <li>Professor Mode <span class="feature-amount">10 students</span></li>\r\n      <li><span class="feature-amount">100</span> Send-to-Phone Messages</li>\r\n      <li>Unlimited Embed Themes w/ Custom CSS</li>\r\n      <li>Custom CSS on your Profile, Blog, and Blog posts</li>\r\n    </ul>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Plan It, Market It, Now Sell It!', 2500.00, '<ul class="feature-list">\r\n      <li>Unlimited Private Pens</li>\r\n      <li>Live View</li>\r\n      <li>Asset Hosting <span class="feature-amount">1 GB</span></li>\r\n      <li>Collab Mode <span class="feature-amount">2 people</span></li>\r\n      <li>Professor Mode <span class="feature-amount">10 students</span></li>\r\n      <li><span class="feature-amount">100</span> Send-to-Phone Messages</li>\r\n      <li>Unlimited Embed Themes w/ Custom CSS</li>\r\n      <li>Custom CSS on your Profile, Blog, and Blog posts</li>\r\n    </ul>', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Just Tell It', 750.00, '<ul class="feature-list">\r\n      <li>Unlimited Private Pens</li>\r\n      <li>Live View</li>\r\n      <li>Asset Hosting <span class="feature-amount">1 GB</span></li>\r\n      <li>Collab Mode <span class="feature-amount">2 people</span></li>\r\n      <li>Professor Mode <span class="feature-amount">10 students</span></li>\r\n      <li><span class="feature-amount">100</span> Send-to-Phone Messages</li>\r\n      <li>Unlimited Embed Themes w/ Custom CSS</li>\r\n      <li>Custom CSS on your Profile, Blog, and Blog posts</li>\r\n    </ul>', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `package_user`
--

CREATE TABLE `package_user` (
  `package_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `package_user`
--

INSERT INTO `package_user` (`package_id`, `user_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `real_depth` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `package_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `position`, `real_depth`, `title`, `content`, `package_id`, `deleted_at`, `slug`) VALUES
(1, NULL, 0, 0, 'TV', 'Some page content!', 3, NULL, 'tv'),
(8, NULL, 2, 0, 'Radio', 'Content for Radio Page', 3, NULL, 'radio'),
(9, 8, 0, 1, 'What is Radio?', '<p>Radio was invented in 1895 by Marconi&hellip; ha, only joking. Don&rsquo;t worry, we won&rsquo;t bore you with this unnecessary info. At the end of the day, who cares!?</p>\r\n\r\n<p>What we <em>do</em> care about is making sure we arm you with enough information about radio so you can determine if it should form part of your marketing. So lets put the headphones on, open the mic and chat about what radio has to offer&hellip;</p>\r\n\r\n<p>Radio is one of the world&rsquo;s most widespread mass mediums because it&rsquo;s:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Affordable</p>\r\n	</li>\r\n	<li>\r\n	<p>Portable</p>\r\n	</li>\r\n	<li>\r\n	<p>Immediate</p>\r\n	</li>\r\n	<li>\r\n	<p>Live and local (relevant to the broadcast market and targeted at specific audiences)</p>\r\n	</li>\r\n	<li>\r\n	<p>Personal</p>\r\n	</li>\r\n	<li>\r\n	<p>Airs music, news, information, entertainment, services and perhaps most importantly, audience interaction</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Globally, radio comprises of commercial (music stations and talk stations) and community networks. Its authenticity and ability to create &lsquo;theatre of the mind&rsquo; helps listeners generate ideas and prompts call to action for your business.</p>\r\n\r\n<p>Lets take a look at why radio may be an important element in your marketing.</p>\r\n', 3, NULL, 'what-is-radio'),
(10, 8, 1, 1, 'Why is radio important?', '<p>Radio continues to play a large part in the daily listening activities of most people.</p>\r\n\r\n<p>They either listen Live On Air, stream it via their computers/phones, through digital radios, in cars or via podcasts (pre-recorded segments of their favourite stations and DJs/Announcers listened to at a time that suits them).</p>\r\n\r\n<p>Radio continues to be a strong and effective advertising channel. Here&rsquo;s why:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>It can be accessed anywhere, anytime and by anybody</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s engaging, highly social and has a powerful connection to its audience, which results in word of mouth and friend recommendations (don&#39;t discount the power On Air personalities who loyal listeners consider &#39;friends&rsquo;)</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s live and local, reflecting the community of its listeners</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s perfect for building brand awareness to stimulate activity via a direct call to action (e.g. buy, subscribe, attend, call, share)</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>The benefits are:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Cost effective (to buy and produce)</p>\r\n	</li>\r\n	<li>\r\n	<p>Has high reach (reaches a lot of listeners at one time and you have the ability to time messages to reach your right market)</p>\r\n	</li>\r\n	<li>\r\n	<p>Has high frequency (can reach listeners several times over a period of time)</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><em><strong>Hot Tip</strong></em>: It can take a listener up to five times to recognise a business and what they&rsquo;re trying to sell in their radio campaign, so keep this in mind when you start to build your campaign.</p>\r\n', 3, NULL, 'why-is-radio-important'),
(11, 8, 2, 1, 'Types of Radio', '<p>Radio comprises of &lsquo;commercial&rsquo; and &lsquo;community&rsquo; networks. Each radio station targets a different audience, with different formats appealing to different age groups and interests.</p>\r\n\r\n<p>Your business and target market will dictate the station you choose. So lets take a look at the most popular radio station formats and the markets they traditionally target.</p>\r\n\r\n<h2><strong>Music Stations</strong></h2>\r\n\r\n<p>Music stations main content is, you guessed it, music!</p>\r\n\r\n<p>DJs/Announcers help bring the content together, integrating: competitions, news, traffic, weather and gossip around music playlists.</p>\r\n\r\n<p>Examples of radio stations&rsquo; music format and their target markets include:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Current/Top 40 Music targeted at 18-39 (skewed towards females)</p>\r\n	</li>\r\n	<li>\r\n	<p>Rock Music targeted at 25-54 (skewed towards males)</p>\r\n	</li>\r\n	<li>\r\n	<p>Variety Music (oldies/easy listening) targeted at 40+ (males and females)</p>\r\n	</li>\r\n	<li>\r\n	<p>Urban/Hip-Hop/R&amp;B targeted at 18-39 (skewed towards males)</p>\r\n	</li>\r\n	<li>\r\n	<p>Dance Music targeted at those aged 13-24 (skewed towards males &amp; females)</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2><strong>Talk Stations</strong></h2>\r\n\r\n<p>Talk radio&rsquo;s content is all about discussion around current events and issues. The format features live interviews with guests as well as live listener interaction. The format plays less music and is more &lsquo;hard hitting&rsquo; on important news events, sometimes even controversial &ndash; you may have moments of &lsquo;did they really just say that?!&rsquo;</p>\r\n\r\n<p>Talk stations are traditionally targeted to an audience of influencers because they have far more influence over business, community, politics, education, health, the arts and entertainment.</p>\r\n\r\n<p>These stations traditionally target the older end of the market: 35-64, males and females.</p>\r\n\r\n<h2><strong>Community Stations </strong></h2>\r\n\r\n<p>Community stations serve geographic communities and communities of interest, broadcasting content that&rsquo;s popular and relevant to a local and specific audience. Community radio stations are owned, operated and influenced by the communities they live in. They&rsquo;re generally not-for-profit and enable individuals, groups, and communities to tell their own stories.</p>\r\n\r\n<p>The main targets are niche communities (based on culture and religion) or targeted at families, e.g. Christian radio stations.</p>\r\n\r\n<p><em><strong>Hot Tip: </strong></em>all formats allow you to market your business, so make sure you do your research to understand what format and audience will work for you.&nbsp;</p>\r\n', 3, NULL, 'types-of-radio'),
(12, 8, 3, 1, 'Radio Time Periods', '<p>Your local radio station is broken into what they call &lsquo;day parts&rsquo; or &lsquo;timeslots&rsquo;. This is how a radio station breaks up their content, music and commercial airtime. The traditional day parts/timeslots include:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Breakfast: 5am-9am, Mon-Fri</p>\r\n	</li>\r\n	<li>\r\n	<p>Mornings: 9am-12noon, Mon-Fri</p>\r\n	</li>\r\n	<li>\r\n	<p>Afternoons: 12noon:4pm, Mon-Fri</p>\r\n	</li>\r\n	<li>\r\n	<p>Drive: 4pm-7pm, Mon-Fri</p>\r\n	</li>\r\n	<li>\r\n	<p>Nights: 7pm-12pm, Mon-Fri</p>\r\n	</li>\r\n	<li>\r\n	<p>Midnight to Dawns: 12pm-5am, Mon-Fri</p>\r\n	</li>\r\n	<li>\r\n	<p>Weekends: 9am-5pm, Sat-Sun</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Your most listened to &lsquo;day parts&rsquo; are while you&rsquo;re downing your first morning coffee (Breakfast) or sitting in traffic on your drive home from work (Drive). You&rsquo;ll now notice the advertisers in these timeslots and understand why they&rsquo;re there.</p>\r\n\r\n<p>During these times you&rsquo;ll also notice your favourite announcers, along with your favourite songs. Coincidence? Absolutely not! It&rsquo;s all an evil plan to keep you tuning in and listening for longer! Important client and radio station giveaways also appear during these popular timeslots. And you guessed it, they&rsquo;re the most expensive to advertise in because your message will be reaching the greatest amount of people at the same time&mdash;so cha-ching!</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>just because Breakfast and Drive have the most amount of people listening, doesn&rsquo;t necessarily mean they&rsquo;re the perfect slots for <em>you</em>. If your budget is tight and your target market is for example &lsquo;mums,&rsquo; you could place your commercials in the cheaper timeslot of afternoons, targeting the school pick up 2pm-4pm, or if you&rsquo;re targeting shift workers/truck drivers 12pm-5am (Midnight to Dawns) might be perfect, and generally dirt cheap! Go where your target market is!</div>\r\n', 3, NULL, 'radio-time-periods'),
(13, 8, 4, 1, 'Advertising on Radio', '<p>Before you start to build your radio campaign, it&rsquo;s time to arm you with the info you need to create the ultimate campaign to drive your business.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Here are some questions to ask yourself before you get started:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>What is the objective of your campaign (drive sales, increase brand awareness, drive enquiries to a website)?</p>\r\n	</li>\r\n	<li>\r\n	<p>Where can people get what you&rsquo;re promoting, i.e. in-store/website?</p>\r\n	</li>\r\n	<li>\r\n	<p>What is your business&rsquo; point of difference? Always highlight what makes you stand out and how you&rsquo;re different to your competitors.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>When building your radio campaign, make sure to:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Include your key message such as your business&rsquo; brand position, e.g. Nike&rsquo;s &lsquo;Just Do It&rsquo;</p>\r\n	</li>\r\n	<li>\r\n	<p>Have clean simple messaging, and remember the famous acronym: KISS (Keep It Simple Stupid)</p>\r\n	</li>\r\n	<li>\r\n	<p>Ensure one call to action that lets listeners know exactly what action you expect from them (buy, call, register etc.)</p>\r\n	</li>\r\n	<li>\r\n	<p>Highlight no more than two or three points in one commercial</p>\r\n	</li>\r\n	<li>\r\n	<p>Keep the product or service at the forefront throughout, and repeat key information such as your company name and product or service</p>\r\n	</li>\r\n	<li>\r\n	<p>Don&rsquo;t include phone numbers unless it&rsquo;s the main focus of the commercial, and you have the opportunity to highlight it</p>\r\n	</li>\r\n	<li>\r\n	<p>Avoid clutter and keep it clean, including music/sound effects/words, and make sure you include proper pauses</p>\r\n	</li>\r\n	<li>\r\n	<p>Make sure to use informal language and write the way you talk (speak as if your talking directly to your customers)</p>\r\n	</li>\r\n	<li>\r\n	<p>Jingles/signature tunes may work for your business&mdash;let your creative writer guide you on what will and won&rsquo;t work</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Now you&rsquo;ve thought about what you want to say, we&rsquo;re about to tell you about the bits and pieces a radio station&rsquo;s account manager will try to sell you. Our aim here is to arm you with some info on each element so you know how to use them.</p>\r\n\r\n<p>The radio station will normally write and produce your radio material for you. Your account manager will work with their assigned copywriter to write your radio scripts. The better the relationship between you and your account manager, the better the brief, resulting in the most successful radio scripts for YOU.</p>\r\n\r\n<p>Remember they&rsquo;re your scripts; so once a copywriter provides them to you, make sure you <em>love them</em>. You need to tell your story in the best way possible so once they head into production and your final piece of audio brilliance is created, you see results.</p>\r\n\r\n<div style="background:#eee; border:1px solid #ccc; padding:5px 10px"><em><strong>Hot Tip: </strong></em>your scriptwriting and production should be included in your commercial plan from your account manager. Try and get them to throw in these elements for FREE. If you can&rsquo;t, make sure you get your best possible price (I&rsquo;d pay no more than $200-$500). Make sure you clarify these fees before you sign a contract. You don&rsquo;t want any unwanted surprises!</div>\r\n', 3, NULL, 'advertising-on-radio'),
(14, 8, 5, 1, 'Your Radio Elements', '<p><strong>Commercials </strong>are the most common form of advertising on a radio station. They&rsquo;re pre-recorded and sold as 15, 30, 45 or 60 second pieces of content that are 100% dedicated to selling your business to listeners. They include words, music beds and sound effects, aired during all timeslots, from Breakfast to Midnight-Dawns.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>radio commercial breaks are around five minutes long, so you can have several clients in one break, i.e. 10 clients at 30 seconds per commercial per break.</div>\r\n\r\n<p><strong>Live reads </strong>are read by a DJ/Announcer. They range from 30-60 seconds in length and are scripted by your copywriter. The DJ/Announcer will put their own &lsquo;Flava&rsquo; into the read to try and drive sales, but they are &lsquo;live&rsquo; in a radio show. Most popular times are Breakfast, Mornings and Afternoons.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>live reads are perfect for long messaging and to give your business a personal touch. They&rsquo;re normally more expensive than commercials, so make sure they&rsquo;re what you need.</div>\r\n\r\n<p><strong>News &amp; Weather or Traffic Sponsorships </strong>are very, very short but offer great reach and frequency. They air from Breakfast to Drive slots and are normally ten seconds long (pre-recorded or live) and should be around 15 words in length. They air several times an hour throughout prime periods and during the day.</p>\r\n\r\n<p><strong>Outside Broadcasts </strong>(OB) consist of live coverage of an event/client and take place outside the studio. Usually a whole radio show is broadcast from a venue, with a major onsite set-up. This set-up encourages people to come in and see the radio station at the client&rsquo;s location.</p>\r\n\r\n<p>Radio stations usually run OBs for their own station promotions, however, you can buy them for your business. They are most popular for store openings, major sales or big entertainment events, e.g. concerts</p>\r\n\r\n<p><em><strong>Hot Tip: </strong></em>OBs aren&rsquo;t cheap and usually require a commercial spend to happen! Unless you have min $10,000-$20,000 we wouldn&rsquo;t recommend you even consider them.</p>\r\n\r\n<p><strong>Live Crosses </strong>are usually 30-60 seconds long and recorded &lsquo;live&rsquo; out of a studio from an event or client location. These crosses are recorded by a member of the promotions or On Air team and include giveaways, encouraging listeners to come down to the location to see them and the event/client.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>This is a cheaper way to get the radio station to your store and drive some onsite traffic. We&rsquo;d recommend considering this first over an OB.</div>\r\n\r\n<p><strong>Show/Segment Sponsorships </strong>align clients as &lsquo;sponsors&rsquo; of their programs. These clients/sponsors are offered live and pre-recorded messaging in the show. These messages range from 5-15 seconds and are played throughout the show. For example, if you&rsquo;re the sponsor of your local radio station breakfast program, you will hear your messages between 6am-9am.</p>\r\n\r\n<p>You may even want to sponsor a segment, e.g., maybe you&rsquo;re a local surf shop, so you might want to sponsor the local radio station&rsquo;s surf report. It fits your business and allows you to talk to your customers in the relevant On Air environment.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>we love this type of commercial messaging. It&rsquo;s short, sharp, 100% targeted, has frequency and uses the talent of the radio station to drive your brand message. In our opinion, this is the best way to stand out and sell your message! Don&rsquo;t forget to ask your account manager about what you can sponsor. It might be exactly what you&rsquo;re after.</div>\r\n\r\n<p><strong>Sales Promotions and </strong><strong>Contests/Giveaways </strong>are great ways to get customers to sample your product and generate word of mouth promotion. If your product/service is desirable to listeners of your target radio station, ask your account manager about whether the station will be interested in giving away your products/services to listeners.</p>\r\n\r\n<p>Sales promotions and contests can be giveaways in just one or several radio shows and/or on the radio station&rsquo;s website. You might want to give away (for FREE) tickets to a concert you&rsquo;re holding at a venue, vouchers for a massage/pamper session or a free mechanical service, as long as the value is great for the radio station (this will differ depending on the radio station). Everyone loves a freebie! They appeal to the listeners and there&rsquo;s a good chance your account manager will be open to the conversation and include it in the campaign.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip</strong></em>: if you bundle the giveaways with a commercial spend you can often get these for FREE across your radio station. Also offer some vouchers to the DJs/Announcers on the radio station&mdash;nice little feel-good gestures are always a great way to show your business in a good light, and you never know; the announcer may love it so much they spruik it on air for free!</div>\r\n', 3, NULL, 'your-radio-elements'),
(15, 8, 6, 1, 'Example of a Radio Schedule', '<p>Here&rsquo;s an example of a radio schedule you might see from an account manager.</p>\r\n\r\n<p>It will detail what they&rsquo;ve proposed to drive your business forward and will be broken into timeframe, airtime elements and cost for each element. We thought this might give you a heads up, if radio is for you!</p>\r\n\r\n<p><a name="_GoBack"></a> MICHAEL: PLEASE INSERT RADIO SCHEDULE EXAMPLE AS AN IMAGE (Provided as separate doco)</p>\r\n', 3, NULL, 'example-of-a-radio-schedule'),
(16, 8, 7, 1, 'A Few Last Points', '<p>Here are a few handy questions to ask your account manager before you get started:</p>\r\n\r\n<h2><strong>Survey Results</strong></h2>\r\n\r\n<p>Ask to see them&mdash;the latest raw results&mdash;not the spin version</p>\r\n\r\n<h2><strong>Listener Numbers</strong></h2>\r\n\r\n<p>Ask to see the listener numbers relevant to your target market. The station you choose needs to be right for your business. There&rsquo;s no point buying commercials on a radio station that talks to over 60s when your product/service is targeted at 18 year-olds!</p>\r\n\r\n<h2><strong>Special Offers</strong></h2>\r\n\r\n<p>Ask for their current special offer. They always have one! You just need to ask. <em><strong>Hot Tip</strong></em>: contact your account manager towards the end of the month. They&rsquo;re more likely to do a deal!</p>\r\n\r\n<h2><strong>Detail your Objectives</strong></h2>\r\n\r\n<p>Make sure you clearly detail your objectives in your meeting&mdash;what you want the advertising to do for your business. Set clear KPIs around the activity, so you and your account manager know what success looks like, and you can keep the radio station accountable.</p>\r\n\r\n<h2><strong>Tweak the Plan until you</strong><strong>&rsquo;</strong><strong>re Happy </strong></h2>\r\n\r\n<p>If you don&rsquo;t feel 100% comfortable with the plan, tell them and tweak it until you are. If you go in with doubt, you won&rsquo;t feel like your advertising dollar is going to go far enough for you</p>\r\n\r\n<h2><strong>PROS of Radio Advertising</strong></h2>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Cost effective</p>\r\n	</li>\r\n	<li>\r\n	<p>Local and live</p>\r\n	</li>\r\n	<li>\r\n	<p>Mass media that can target niches through stations and time zones</p>\r\n	</li>\r\n	<li>\r\n	<p>Immediate&mdash;you can be on the radio pretty quickly</p>\r\n	</li>\r\n	<li>\r\n	<p>Works really well with digital advertising (integrated campaigns)</p>\r\n	</li>\r\n	<li>\r\n	<p>Provides information/news/gossip in &frac12;-1 hour blocks, so you can get the headlines quickly</p>\r\n	</li>\r\n	<li>\r\n	<p>Ability to time/place messages to reach the right audience</p>\r\n	</li>\r\n	<li>\r\n	<p>Access anywhere, anytime by anybody</p>\r\n	</li>\r\n	<li>\r\n	<p>High reach and frequency, meaning you can have in impact in a short period of time</p>\r\n	</li>\r\n	<li>\r\n	<p>Announcers can talk about your business live on the radio. This gives you an element of personal endorsement</p>\r\n	</li>\r\n	<li>\r\n	<p>Great for brand awareness</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2><strong>CONS of Radio Advertising</strong></h2>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Streaming services/downloads provide competition</p>\r\n	</li>\r\n	<li>\r\n	<p>Clutter&mdash;lots of different businesses placed in the same commercial break</p>\r\n	</li>\r\n	<li>\r\n	<p>Some commercials can sound the same</p>\r\n	</li>\r\n	<li>\r\n	<p>Can be quite ridged (need to fit into the radio station formats)</p>\r\n	</li>\r\n	<li>\r\n	<p>Hard to judge success of the campaign&mdash;unlike digital, there are no clear measurements of outcomes</p>\r\n	</li>\r\n	<li>\r\n	<p>People are usually doing something else while they&rsquo;re listening to the radio</p>\r\n	</li>\r\n	<li>\r\n	<p>Live messages are subject to human error and create flexibility</p>\r\n	</li>\r\n</ul>\r\n', 3, NULL, 'a-few-last-points'),
(21, NULL, 3, 0, 'Digital', '<p>Test</p>\r\n', 3, NULL, 'digital'),
(22, NULL, 4, 0, 'Print', '', 3, NULL, 'print'),
(23, NULL, 5, 0, 'Outdoor', '', 3, NULL, 'outdoor'),
(24, NULL, 6, 0, 'Social', '', 3, NULL, 'social'),
(25, 1, 0, 1, 'What is TV?', '<p>It has pictures, sound, was born in the 1920s and&mdash;lets be honest&mdash;hasn&rsquo;t changed much since then. However, TV to this day&nbsp;remains an essential communications tool, capturing the attention of target audiences like no other medium, and on a mass scale.</p>\r\n\r\n<p>Lets pour a cup of tea, grab the best position on the couch, reach for the remote and start flicking through what TV has to offer your business.</p>\r\n\r\n<p>Despite new media options, TV is still the world&rsquo;s most watched mass medium and here&rsquo;s why:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>TV reflects what&rsquo;s happening in people&rsquo;s day to day lives</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s one of the most talked about mediums in the world (word-of-mouth and social media)</p>\r\n	</li>\r\n	<li>\r\n	<p>The combination of vision and sound creates the greatest impact, recall and brand fame</p>\r\n	</li>\r\n	<li>\r\n	<p>It has huge scale, reaching the masses instantly and simultaneously</p>\r\n	</li>\r\n	<li>\r\n	<p>Different programs and networks target different audiences, allowing you to place your messages in the right environment</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s the most effective in driving the biggest profit for businesses</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>So let&rsquo;s take a look at why TV may be important for your marketing.</p>\r\n', 3, NULL, 'what-is-tv'),
(26, 1, 1, 1, 'Why is TV Important?', '<p>As the most consumed medium across the globe, TV continues to be a strong and effective platform for businesses to market themselves. It builds momentum, creates emotion and encourages brand search and purchase.</p>\r\n\r\n<p>Here&rsquo;s why:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>It&rsquo;s an integral part of&nbsp;our culture and society (people love talking about what happens on TV and it emotionally connects us)</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s a great way to kick-start a campaign and build momentum, drive a call to action, or introduce a new product</p>\r\n	</li>\r\n	<li>\r\n	<p>The combination of pictures and sound brings a business&rsquo; brand to life</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>The benefits are:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>It has the greatest scale and reach</p>\r\n	</li>\r\n	<li>\r\n	<p>It provides up-to-date audience numbers (literally overnight) so you&rsquo;ll know how many people watched the program you bought into</p>\r\n	</li>\r\n	<li>\r\n	<p>It makes people respond (the combination of pictures and sound drives action)</p>\r\n	</li>\r\n	<li>\r\n	<p>People use second screens while watching TV (mobiles, laptops and tablets) so it closes the gap between viewer reactions through social media and search for products, shortening the path to purchase</p>\r\n	</li>\r\n	<li>\r\n	<p>It builds brand fame for your business (e.g. &lsquo;I saw your TV commercial the other day&rsquo;)</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s the catalyst for other media because an idea created for TV can be re-worked for other mediums (e.g. social media and billboards)</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Now we know a bit more about what&rsquo;s on the box, lets find out if TV is right for your business.</p>\r\n', 3, NULL, 'why-is-tv-important'),
(27, 1, 2, 1, 'Types of TV Network Audiences', '<p>In most markets, TV is comprised of:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Free-to-Air and Satellite</p>\r\n	</li>\r\n	<li>\r\n	<p>Pay TV (or &lsquo;cable&rsquo;)</p>\r\n	</li>\r\n	<li>\r\n	<p>Catch-Up TV</p>\r\n	</li>\r\n	<li>\r\n	<p>Streaming Services like Netflix and Apple TV</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>As a whole, TV is the most mainstream of all media, targeting a vast section of the population, but each TV network targets a different audience, with different formats appealing to different age groups and interests.</p>\r\n\r\n<p>When it comes to telling people about your business on TV, you can buy commercials on free-to-air/satellite or cable/pay TV. They have the largest audiences and drive the greatest result for your business both on a local and national level.</p>\r\n\r\n<p>Your business and target market will dictate the specific network you choose. So let&rsquo;s take a look at the different TV networks and formats, the advertising opportunities, and the markets they traditionally target.</p>\r\n\r\n<h2><strong>Free-to-Air and Satellite</strong></h2>\r\n\r\n<p>You guessed it; free-to-air/satellite TV provides content for <strong>FREE</strong> to its viewers. You literally only require a TV to watch it. Free-to-air channels in Australia include the 7, 9 and 10 Networks, the ABC, SBS and their respective digital channels (examples include One, Eleven, Gem, Go, 7Mate, 7Two and ABC Kids).</p>\r\n\r\n<p>Free-to-air/satellite is the most popular and widely recognised platform with most networks traditionally targeting audiences aged 25-54. Designed to reach the greatest amount of people at one time, programming will be of general appeal to the masses, not too controversial, and top rating. Essentially, every marketer&rsquo;s dream!</p>\r\n\r\n<h2><strong>Pay TV Networks. </strong></h2>\r\n\r\n<p>You got it; it&rsquo;s the opposite to free-to-air. People have to pay to access these networks and the programs they broadcast (e.g. Foxtel in Australia)</p>\r\n\r\n<p>Traditionally these networks operate on a subscription basis, offering more specialised programs and channels. Unlike free-to-air networks, which are concerned with ratings in specific times (e.g. peak times 6:00pm &ndash; 10:30pm), these networks air programs that make people subscribe to and target a more niche market with their content.</p>\r\n\r\n<p>Cable/Pay TV networks are traditionally cheaper to purchase due to the very niche nature of the programs and you can buy commercials in packages across likeminded channels, e.g., sports channels (of which there may be several). That way you&rsquo;re buying commercials based on your target market&rsquo;s interests rather than the larger but more generic audience free-to-air offers.</p>\r\n\r\n<h2><strong>Catch-Up TV</strong></h2>\r\n\r\n<p>With catch-up TV services and mobile TV, viewers never need to miss an episode of their favourite program. Online, viewers can get access to exclusive content, behind-the-scenes footage and chat in live forums with other fans.</p>\r\n\r\n<p>Catch-up TV services provide advertising opportunities before each episode begins. Generally one or multiple ads are shown before each program, are 15-second spots, and are usually set-aside for clients that spend nationally (across every state and territory).</p>\r\n\r\n<h2><strong>Streaming/Internet TV Services</strong></h2>\r\n\r\n<p>This type of media is growing and growing. It&rsquo;s a great service for programs, but you can&rsquo;t advertise on or near it. Well, not yet anyway.</p>\r\n\r\n<p>This service allows audiences to stream live from the internet, programs to watch when they want. It too is based on a subscription model, however it&rsquo;s completely commercial free. Netflix and Stan are examples of these services and I&rsquo;m sure you&rsquo;ve heard many a friend talk about staying in and watching full seasons of their favourite shows, back to back, without any commercials on a Saturday night (wine and chocolate in hand!)</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip:</strong></em> Keep up-to-date with possible changes here though. It may not be long before you start to see some before and after video advertising sneaking into these services for third party advertisers including YOU!</div>\r\n', 3, NULL, 'types-of-tv-network-audiences'),
(28, 1, 3, 1, 'Television Time Periods', '<p>Your TV channels are broken into two main advertising time periods or day parts: off-peak (breakfast TV, daytime, weekends or overnight) and peak (nights).</p>\r\n\r\n<p>Below is a summary of the times and how your account manager may start to structure your commercial schedule:</p>\r\n\r\n<h2><strong>Off-Peak</strong></h2>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Breakfast TV (5am-9am, Mon-Sun)</p>\r\n	</li>\r\n	<li>\r\n	<p>Daytime TV (9am-5pm, Mon-Sun)</p>\r\n	</li>\r\n	<li>\r\n	<p>Weekends (9am-5pm, Sat-Sun)</p>\r\n	</li>\r\n	<li>\r\n	<p>Overnights (10:30pm-9am, Mon-Sun)</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2><strong>Peak</strong></h2>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Fringe nights (5pm-6pm, Mon-Sun)</p>\r\n	</li>\r\n	<li>\r\n	<p>Nights (6pm-10:30pm, Sun-Thurs) but traditionally, Friday and Saturday nights have lower audiences</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Viewership of TV programming, as measured by companies such as Nielsen Media Research is often used as a metric for TV commercial placement, and consequently, the rates you&rsquo;re charged to air your commercial within a given network, television program, or time of day. Traditionally TV starts to gain audiences during the Mon-Fri nightly news (usually around 6pm or 7pm).</p>\r\n\r\n<p>You&rsquo;ll start to notice the most popular programs hitting your screens at night. The highest rating programs usually air between 7:30pm-10:30pm and are the most expensive timeslots. You&rsquo;ll notice big global brands with BIG budgets advertising in these timeslots. That&rsquo;s because it ain&rsquo;t cheap!</p>\r\n', 3, NULL, 'television-time-periods'),
(29, 1, 4, 1, 'Advertising on TV', '<p>The most efficient way to plan and book a commercial schedule is to speak directly with your local channel or network&rsquo;s sales department. Once you&rsquo;ve determined what channel/network works best to reach your target customers, your account manager for the given channel or network can help you plan and book your commercial schedule. They can also assist with your production requirements.</p>\r\n\r\n<p>The cost of running television commercials varies widely, depending on whether your commercials run on local stations or national networks. Rates vary depending on the time of day and year, as well as your location. To cut costs, make sure you know the programs your target market is most likely to watch.</p>\r\n\r\n<p>Once you know your target market, your account manager may talk to you about TARPS or cost per TARP.</p>\r\n\r\n<p>Here&rsquo;s what they mean:</p>\r\n\r\n<p><strong>A TARP</strong> is the percentage of a specific target audience viewing a program at the time. For example, your target market might be men aged 18-39 and you want to know how a particular program (e.g. NCIS) performs for this target. If 23% of men 18-39 in your market watch NCIS, your TARP for the program in this market is 23.</p>\r\n\r\n<p><strong>A</strong><strong> cost per TARP</strong> is the dollar cost of advertising within a program divided by the number of TARPs it will achieve. If NCIS costs $2,000 for a 30 sec commercial and achieves 23 TARPs against the specified target audience, then the cost per TARP (CPT) is $86.95</p>\r\n\r\n<p>Here&rsquo;s what a schedule might look like for you:</p>\r\n\r\n<p><strong>MICHAEL: INSERT TV SCHEDULE (per provided excel spreadhseet)</strong></p>\r\n\r\n<p>An average half hour&nbsp;TV&nbsp;program has around eight minutes of commercials (national clients like Coca Cola and local clients such as plumbers), divided into two or three breaks of 2-3mins. For local clients, most television channels/networks offer two minutes of local advertising per hour, giving small businesses a more affordable way to use TV as part of their marketing strategy.</p>\r\n\r\n<p>When you buy TV advertising, you buy based on the program and who the program is targeted at. As a camping shop, you might find you want to advertise in a lifestyle-based program. Often these programs air over the weekend and are therefore much cheaper to buy, plus it talks directly to your current and future customers as they&rsquo;re emotionally connected to the content.</p>\r\n\r\n<p>This works on two levels: better for your wallet and targeted at the people you want walking through your door. WIN WIN! &nbsp;</p>\r\n', 3, NULL, 'advertising-on-tv'),
(30, 1, 5, 1, 'Building Your TV Campaign', '<p>Okay, so you now know more about TV and who to talk to about advertising on it. It&rsquo;s now time to arm you with the info you need to create your ultimate campaign and really drive your business objectives.</p>\r\n\r\n<p>Here are the components and descriptions on how to use TV ad campaigns.</p>\r\n\r\n<h2><strong>Commercials </strong></h2>\r\n\r\n<p>This is the most common form of advertising on TV, and available to businesses of all sizes. They&rsquo;re pre-recorded and sold as 15, 30, 45 or 60-second pieces of content and are 100% dedicated to selling your business or product to viewers. They include sound, talent, moving pictures or graphics and can air at anytime, day or night.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>TV commercial breaks are around 2-3 minutes, so you can have several clients in one break (i.e. 4-6 clients at 30 seconds per commercial break).</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>Infomercial</strong></h2>\r\n\r\n<p>These are direct response television commercials of varying lengths (i.e. 30/60/120 seconds or 5-28 minutes). They can sit in a commercial break or be part of a program like a morning show or a dedicated shopping channel. The commercials offer products for direct sale, encouraging viewers to call or email to purchase the product via credit card, with the product sent directly to the viewer&rsquo;s home. They are usually bought on a national basis. So&hellip; how is your Nutri-Bullet going?</p>\r\n\r\n<h2><strong>Billboards</strong></h2>\r\n\r\n<p>Billboards are 10-second commercials placed at the beginning and end of a program or leading into a news or weather segment. They are NOT for call to action messages, but rather a branding exercise. They normally highlight that a program is &lsquo;brought to you by&rsquo; a business, bought at a local, state and national level.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>These commercials are a great way to drive brand awareness for your business as well as being a cheaper option to spread your money further.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>News And Weather Sponsorships </strong></h2>\r\n\r\n<p>Much like billboards, you can sponsor your local news or weather segments in your nightly news programs each night. These commercials are ten seconds as well, and are used for branding your business, not a call to action. They can be bought on a local, state and national level.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>This is a very popular timeslot for local businesses so make sure you get in early. The nightly news timeslot has strong viewership numbers often without the prime time price tag. So get on it!</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2><strong>Branded Entertainment </strong></h2>\r\n\r\n<p>If you have a larger budget, this may be an option for your business. We love this stuff! Essentially it&rsquo;s entertaining content or programs funded by your business. Its purpose is to give your business the opportunity to communicate its image to its target audience in an original way, creating positive links between the brand and the program. These projects are often the result of deals&nbsp;between businesses, producers and networks.</p>\r\n\r\n<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;"><em><strong>Hot Tip: </strong></em>branded content is traditionally created by big brands with big budgets, so keep this in mind and watch for great examples that you might be able to create with a smaller budget. This content may be used online versus broadcast on TV if you have a small budget. The cost to produce a program that runs 22 minutes can be high, depending on the network. The show concept must also fit the network&rsquo;s programming, so there are a few hurdles to get over to produce and air a program.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Here are a few considerations to raise with your account manager when looking at advertising on TV:</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Ask to see the audience broken down by program for only the market/s you want to advertise in (make sure they&rsquo;re right for your business)</p>\r\n	</li>\r\n	<li>\r\n	<p>Try to advertise during TV survey periods (they run on a 13-week cycle and the most popular programs are aired during these periods, reaching the greatest number of people)</p>\r\n	</li>\r\n	<li>\r\n	<p>Ask for their current special offer (they always have one, so you just need to ask) and we recommend you contact your account manager towards the end of the month when they&rsquo;re more likely do a deal</p>\r\n	</li>\r\n	<li>\r\n	<p>Splice your advertising schedule with 10, 15 or 30-second commercials, ensuring there&rsquo;s a branding element and a clear call to action (the different time lengths will allow you to spread your dollar further, which is preferable to scheduling only 30-second commercials)</p>\r\n	</li>\r\n	<li>\r\n	<p>Make sure you clearly detail your objectives in your meeting by deciding what you want the advertising to achieve for your business. Set clear KPIs around the activity so you and your account manager know what success looks like, and you can keep the television station accountable</p>\r\n	</li>\r\n	<li>\r\n	<p>If you don&rsquo;t feel 100% comfortable with the plan, tell them. Then tweak the plan until you&rsquo;re satisfied. If you go in with doubt, you won&rsquo;t feel like your advertising dollar is going to go far enough&nbsp;</p>\r\n	</li>\r\n</ul>\r\n', 3, NULL, 'building-your-tv-campaign'),
(31, 1, 6, 1, 'Producing Your TV Commercial', '<p><strong>It&rsquo;s time to get creative!</strong></p>\r\n\r\n<p>The TV networks may offer to write and produce your TV material for you, but you can also choose to work with an independent production house to produce your commercials and send them to the network for airing.</p>\r\n\r\n<p>Before you start to build your TV commercial, think about the points below before you even get to the script:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Set clear objectives for what you want your advertising to achieve (drive sales, increase brand awareness, drive enquiries to a website, or trial a product/service)</p>\r\n	</li>\r\n	<li>\r\n	<p>Tell people exactly where and how to get the product (e.g. in-store, website, phone number)</p>\r\n	</li>\r\n	<li>\r\n	<p>Highlight your business&rsquo; point of difference (what makes you stand out from your competitors?)</p>\r\n	</li>\r\n	<li>\r\n	<p>Include your key message (what&rsquo;s your business&rsquo; brand position; i.e. &lsquo;Nike &ndash; Just Do It&rsquo;)</p>\r\n	</li>\r\n	<li>\r\n	<p>Have clean simple messaging, and remember to KISS (Keep It Simple Stupid)</p>\r\n	</li>\r\n	<li>\r\n	<p>Ensure one call to action (what do you want people to do or buy, and what is their action?)</p>\r\n	</li>\r\n	<li>\r\n	<p>Highlight no more than 2-3 points in one commercial</p>\r\n	</li>\r\n	<li>\r\n	<p>Keep the product or service at the forefront throughout (repeat key information such as your company name, product/service or phone numbers)</p>\r\n	</li>\r\n	<li>\r\n	<p>Remove clutter and keep it clean (that includes music and sound effects, pictures and graphics)</p>\r\n	</li>\r\n	<li>\r\n	<p>Find a way to emotionally connect your target with your message (don&rsquo;t just make them watch and listen; make them <em>feel</em>)</p>\r\n	</li>\r\n	<li>\r\n	<p>Make sure to use informal language and write the way you talk (act as though you&rsquo;re talking directly to your customers)</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>Once you know your direction, here are a few things you need to think about when you move onto producing a TV commercial.</p>\r\n\r\n<p>There are 3 clear processes:</p>\r\n\r\n<h2><strong>1. Pre-Production</strong></h2>\r\n\r\n<h3><strong>Consultation</strong></h3>\r\n\r\n<p>This process should include a meeting with your assigned producer to determine what you need. Give them an idea of a creative direction and they&rsquo;ll provide a quote.</p>\r\n\r\n<h3><strong>The Script</strong></h3>\r\n\r\n<p>Once you&rsquo;ve chosen your production company, the script is the most important part of the process. Your writer should continue to re-write versions of the script with audio and visual notes until you&rsquo;re 100% happy</p>\r\n\r\n<h3><strong>Casting</strong></h3>\r\n\r\n<p>This is a critical consideration because the actor or narrator will become the face or voice of your brand. Producers must source and hire talent that perfectly represents your business and communicates your message.</p>\r\n\r\n<h3><strong>Rehearsals</strong></h3>\r\n\r\n<p>Don&rsquo;t leave anything to chance. By practising before the day of the shoot, the whole team (talent, crew and producers) can minimise the time it will take to film or video the content. Remember that shoots can involve a lot of detail including wardrobe, hair, make up, sets and props. One missing element can lead to a delayed (or even cancelled!) production.</p>\r\n\r\n<h2><strong>2. Shoot</strong></h2>\r\n\r\n<h3><strong>Resources</strong></h3>\r\n\r\n<p>Your producers are responsible for any agreed resources that should be on set, as per the script. That not only means camera crews, audio and lighting teams, but also furniture, props and backgrounds.</p>\r\n\r\n<h3><strong>Scheduling</strong></h3>\r\n\r\n<p>There&rsquo;s a reason why good producers run productions according to strict schedules. The tiniest detail can make the shoot lag. The last thing you want is a rushed performance, compromised script or increased budget. Ensure enough time has been allocated for wardrobe, hair and make up, prior to the shoot.</p>\r\n\r\n<h2><strong>3. Post Production</strong></h2>\r\n\r\n<h3><strong>Editing </strong></h3>\r\n\r\n<p>After the shoot, your commercial goes into post-production. This is where all of the footage is taken into an editing studio and combined to produce your final commercial. During this phase, music, graphics and takes are selected to be edited together based on your approved script (this is why the script is soooo important!). You will get to approve the commercial before it is 100% completed and may be able to make slight changes to ensure it speaks perfectly to your customers.</p>\r\n\r\n<h3><strong>Delivery </strong></h3>\r\n\r\n<p>The production company or TV network should deliver the final version of your commercial to your chosen TV network. It&rsquo;s their final (and most important) task to ensure all legal requirements are correct and nothing stops your commercial from being aired according to the agreed schedule.</p>\r\n', 3, NULL, 'producing-your-tv-commercial'),
(32, 1, 7, 1, 'Pros and Cons of TV', '<h2><strong>PROS of Television:</strong></h2>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Reaches the largest audience in the shortest period of time</p>\r\n	</li>\r\n	<li>\r\n	<p>Reaches viewers when they&rsquo;re most attentive</p>\r\n	</li>\r\n	<li>\r\n	<p>Allows you to convey your message with pictures, sound and motion</p>\r\n	</li>\r\n	<li>\r\n	<p>Allows for you to create brand personality and credibility</p>\r\n	</li>\r\n	<li>\r\n	<p>Ability to time and place messages that reach the right audience</p>\r\n	</li>\r\n	<li>\r\n	<p>Have a strong Impact and create brand fame</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2><strong>CONS of Television:</strong></h2>\r\n\r\n<ul>\r\n	<li>\r\n	<p>It&rsquo;s expensive compared to other media, both to produce and air</p>\r\n	</li>\r\n	<li>\r\n	<p>Can create clutter, with many different businesses placed in the same commercial break</p>\r\n	</li>\r\n	<li>\r\n	<p>Production takes a long time and it&rsquo;s hard to make changes once it&rsquo;s been produced</p>\r\n	</li>\r\n	<li>\r\n	<p>TV is heavily governed, so commercials can take a long time to approve and be costly to finalise</p>\r\n	</li>\r\n	<li>\r\n	<p>It&rsquo;s hard to judge the success of a campaign due to no clear measurement of outcomes</p>\r\n	</li>\r\n	<li>\r\n	<p>TV needs repetition to work, so can be costly and timely</p>\r\n	</li>\r\n	<li>\r\n	<p>Hard to target niche audiences</p>\r\n	</li>\r\n	<li>\r\n	<p>Free-to-air and cable/Pay TV viewership is reducing due to competition from streaming services (Netflix and STan TV), which currently don&rsquo;t offer commercial opportunities</p>\r\n	</li>\r\n</ul>\r\n', 3, NULL, 'pros-and-cons-of-tv');

-- --------------------------------------------------------

--
-- Table structure for table `page_closure`
--

CREATE TABLE `page_closure` (
  `closure_id` int(10) UNSIGNED NOT NULL,
  `ancestor` int(10) UNSIGNED NOT NULL,
  `descendant` int(10) UNSIGNED NOT NULL,
  `depth` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_closure`
--

INSERT INTO `page_closure` (`closure_id`, `ancestor`, `descendant`, `depth`) VALUES
(1, 1, 1, 0),
(13, 8, 8, 0),
(14, 9, 9, 0),
(15, 8, 9, 1),
(16, 10, 10, 0),
(17, 8, 10, 1),
(18, 11, 11, 0),
(19, 8, 11, 1),
(20, 12, 12, 0),
(21, 8, 12, 1),
(22, 13, 13, 0),
(23, 8, 13, 1),
(24, 14, 14, 0),
(25, 8, 14, 1),
(26, 15, 15, 0),
(27, 8, 15, 1),
(28, 16, 16, 0),
(29, 8, 16, 1),
(34, 21, 21, 0),
(35, 22, 22, 0),
(36, 23, 23, 0),
(37, 24, 24, 0),
(38, 25, 25, 0),
(39, 1, 25, 1),
(40, 26, 26, 0),
(41, 1, 26, 1),
(42, 27, 27, 0),
(43, 1, 27, 1),
(44, 28, 28, 0),
(45, 1, 28, 1),
(46, 29, 29, 0),
(47, 1, 29, 1),
(48, 30, 30, 0),
(49, 1, 30, 1),
(50, 31, 31, 0),
(51, 1, 31, 1),
(52, 32, 32, 0),
(53, 1, 32, 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `user_id`, `created_at`, `updated_at`, `type`, `filepath`) VALUES
(1, 'My Business Plan', 1, '2015-10-18 03:57:45', '2015-11-19 01:07:18', 'business', ''),
(2, 'Kirsty', 1, '2015-11-23 19:15:13', '2015-11-24 20:37:37', 'business', '__final-1448433450--2-business-plan.pdf'),
(3, 'test', 1, '2015-11-23 19:29:26', '2015-11-23 19:29:26', 'business', ''),
(4, 'Get Jack''D Marketing Plan', 1, '2015-09-22 02:00:00', '2015-11-23 23:56:44', 'marketing', ''),
(6, 'Testtestetst', 1, '2015-11-24 17:44:43', '2015-11-24 17:44:43', 'marketing', '');

-- --------------------------------------------------------

--
-- Table structure for table `plan_categories`
--

CREATE TABLE `plan_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tip_id` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_content_id` int(11) NOT NULL,
  `plan_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_categories`
--

INSERT INTO `plan_categories` (`id`, `parent_id`, `lft`, `rgt`, `depth`, `name`, `created_at`, `updated_at`, `tip_id`, `content_type`, `default_content_id`, `plan_type`) VALUES
(9, NULL, 1, 98, 0, 'Business Plan Root', '2015-10-14 19:07:53', '2015-10-18 17:55:40', 0, 'text', 5, 'business'),
(10, 9, 2, 3, 1, 'Executive Summary', '2015-10-14 19:09:22', '2015-10-17 01:15:13', 1, 'text', 1, 'business'),
(11, 9, 4, 27, 1, 'The Business Summary', '2015-10-14 19:10:42', '2015-10-15 02:11:59', 0, 'text', 0, 'business'),
(12, 9, 28, 67, 1, 'The Business Operations', '2015-10-14 19:10:47', '2015-10-15 02:38:14', 6, 'text', 0, 'business'),
(13, 9, 68, 83, 1, 'The Market', '2015-10-14 19:10:57', '2015-10-15 00:32:59', 0, 'text', 0, 'business'),
(14, 9, 84, 93, 1, 'The Financials', '2015-10-14 19:11:03', '2015-10-15 00:32:59', 0, 'text', 0, 'business'),
(15, 9, 94, 95, 1, 'Research and Development(R&D)/Innovation Activities', '2015-10-14 19:11:16', '2015-10-18 05:57:09', 33, 'text', 0, 'business'),
(16, 9, 96, 97, 1, 'Appendix/Supporting Documents', '2015-10-14 19:11:26', '2015-10-15 00:32:59', 0, 'document', 0, 'business'),
(24, 12, 29, 30, 2, 'Business and Registration Details', '2015-10-14 23:22:19', '2015-10-18 18:14:07', 6, 'text', 10, 'business'),
(26, 12, 39, 48, 2, 'Product and Service', '2015-10-14 23:22:37', '2015-10-18 05:50:00', 11, 'text', 0, 'business'),
(27, 12, 49, 58, 2, 'Legal and Accounting', '2015-10-14 23:23:09', '2015-10-15 00:32:59', 0, 'text', 0, 'business'),
(29, 12, 61, 66, 2, 'Technology and Equipment', '2015-10-14 23:23:22', '2015-10-15 00:32:59', 0, 'text', 0, 'business'),
(30, 11, 19, 20, 2, 'What is your Business?', '2015-10-14 23:25:02', '2015-10-18 05:47:27', 2, 'text', 0, 'business'),
(31, 11, 21, 22, 2, 'Vision', '2015-10-14 23:25:08', '2015-10-18 05:47:50', 3, 'text', 0, 'business'),
(32, 11, 23, 24, 2, 'Mission', '2015-10-14 23:25:13', '2015-10-18 05:48:06', 4, 'text', 0, 'business'),
(33, 11, 25, 26, 2, 'Goals and Action Plan', '2015-10-14 23:25:17', '2015-10-18 18:10:38', 5, 'text', 9, 'business'),
(34, 13, 69, 70, 2, 'Market Summary', '2015-10-14 23:26:10', '2015-10-18 05:53:54', 22, 'text', 0, 'business'),
(35, 13, 71, 78, 2, 'Target Market', '2015-10-14 23:26:16', '2015-10-18 05:54:09', 23, 'text', 0, 'business'),
(36, 13, 79, 80, 2, 'SWOT Analysis', '2015-10-14 23:26:22', '2015-10-18 18:23:10', 26, 'text', 22, 'business'),
(37, 13, 81, 82, 2, 'Marketing and Media Plan', '2015-10-14 23:26:27', '2015-10-18 18:23:39', 28, 'text', 23, 'business'),
(38, 25, 32, 33, 3, 'Key Personnel', '2015-10-14 23:27:08', '2015-10-18 18:14:51', 8, 'text', 11, 'business'),
(39, 25, 34, 35, 3, 'Required Personnel', '2015-10-14 23:27:16', '2015-10-18 18:15:07', 9, 'text', 12, 'business'),
(40, 25, 36, 37, 3, 'Recruitment and Training', '2015-10-14 23:27:24', '2015-10-18 19:03:53', 10, 'text', 0, 'business'),
(41, 14, 85, 92, 2, 'Key Objectives and Financial Review', '2015-10-15 00:09:33', '2015-10-18 05:55:51', 29, 'text', 0, 'business'),
(42, 41, 86, 87, 3, 'Profit and Loss Forecast', '2015-10-15 00:09:59', '2015-10-18 05:56:11', 30, 'spreadsheet', 0, 'business'),
(43, 41, 88, 89, 3, 'Balance Sheet Forecast', '2015-10-15 00:10:07', '2015-10-18 05:56:29', 31, 'spreadsheet', 0, 'business'),
(44, 41, 90, 91, 3, 'Expected Cash Flow', '2015-10-15 00:10:14', '2015-10-18 05:56:53', 32, 'spreadsheet', 0, 'business'),
(45, 35, 72, 73, 3, 'Market Research', '2015-10-15 00:11:00', '2015-10-18 05:54:24', 24, 'text', 0, 'business'),
(46, 35, 74, 75, 3, 'Competitors', '2015-10-15 00:11:07', '2015-10-18 18:22:31', 25, 'text', 21, 'business'),
(47, 35, 76, 77, 3, 'Unique Selling Position', '2015-10-15 00:11:14', '2015-10-18 05:55:02', 27, 'text', 0, 'business'),
(48, 27, 50, 51, 3, 'Legal', '2015-10-15 00:28:20', '2015-10-18 18:17:35', 16, 'text', 16, 'business'),
(49, 27, 52, 53, 3, 'Accounting', '2015-10-15 00:31:41', '2015-10-18 05:52:08', 17, 'text', 0, 'business'),
(50, 27, 54, 55, 3, 'Insurance', '2015-10-15 00:31:49', '2015-10-18 18:17:50', 18, 'text', 17, 'business'),
(51, 27, 56, 57, 3, 'Risk Management and Security', '2015-10-15 00:32:05', '2015-10-18 18:18:46', 19, 'text', 18, 'business'),
(52, 29, 62, 63, 3, 'IT Infrastructure', '2015-10-15 00:32:14', '2015-10-18 18:19:40', 20, 'text', 19, 'business'),
(53, 29, 64, 65, 3, 'Equipment', '2015-10-15 00:32:21', '2015-10-18 18:19:56', 21, 'text', 20, 'business'),
(54, 26, 40, 41, 3, 'Product/Service and Pricing', '2015-10-15 00:32:32', '2015-10-18 18:16:42', 12, 'text', 13, 'business'),
(55, 26, 42, 43, 3, 'Inventory', '2015-10-15 00:32:46', '2015-10-18 18:17:03', 13, 'text', 14, 'business'),
(56, 26, 44, 45, 3, 'Distribution', '2015-10-15 00:32:53', '2015-10-18 18:17:16', 14, 'text', 15, 'business'),
(57, 26, 46, 47, 3, 'Location', '2015-10-15 00:32:59', '2015-10-18 05:51:05', 15, 'text', 0, 'business'),
(58, NULL, 99, 140, 0, 'Marketing Plan Root', '2015-11-17 23:04:09', '2015-11-18 00:44:27', 0, 'text', 0, 'marketing'),
(59, 58, 100, 101, 1, 'Executive Summary', '2015-11-17 23:36:55', '2015-11-18 00:27:27', 0, 'text', 0, 'marketing'),
(61, 58, 104, 105, 1, 'Vision Statement', '2015-11-18 00:40:39', '2015-11-18 00:40:40', 0, 'text', 0, 'marketing'),
(62, 58, 106, 107, 1, 'Mission Statement', '2015-11-18 00:40:48', '2015-11-18 00:40:48', 0, 'text', 0, 'marketing'),
(63, 58, 108, 123, 1, 'The Market', '2015-11-18 00:40:54', '2015-11-18 00:43:24', 0, 'text', 0, 'marketing'),
(64, 58, 124, 125, 1, 'Sales and Distribution Channels', '2015-11-18 00:41:02', '2015-11-18 00:43:24', 0, 'text', 0, 'marketing'),
(65, 58, 126, 127, 1, 'Marketing and Media Goals & Objectives', '2015-11-18 00:41:13', '2015-11-18 00:43:24', 0, 'text', 0, 'marketing'),
(66, 58, 128, 129, 1, 'Marketing and Media Plans', '2015-11-18 00:41:21', '2015-11-18 00:43:24', 0, 'text', 0, 'marketing'),
(67, 58, 130, 131, 1, 'Marketing and Media Budget', '2015-11-18 00:41:28', '2015-11-18 00:43:24', 0, 'text', 0, 'marketing'),
(68, 58, 132, 137, 1, 'Timeline of Activity', '2015-11-18 00:41:55', '2015-11-18 00:44:27', 0, 'text', 0, 'marketing'),
(69, 58, 138, 139, 1, 'Monitoring and Measurement of Marketing and Media Activities', '2015-11-18 00:42:11', '2015-11-18 00:44:27', 0, 'text', 0, 'marketing'),
(70, 63, 109, 110, 2, 'Market Summary', '2015-11-18 00:42:41', '2015-11-18 00:42:42', 0, 'text', 0, 'marketing'),
(71, 63, 111, 112, 2, 'Target Market', '2015-11-18 00:42:47', '2015-11-18 00:42:48', 0, 'text', 0, 'marketing'),
(72, 63, 113, 114, 2, 'Market Research', '2015-11-18 00:42:55', '2015-11-18 00:42:56', 0, 'text', 0, 'marketing'),
(73, 63, 115, 116, 2, 'Competitors', '2015-11-18 00:43:02', '2015-11-18 00:43:03', 0, 'text', 0, 'marketing'),
(74, 63, 117, 118, 2, 'SWOT Analysis', '2015-11-18 00:43:08', '2015-11-18 00:43:09', 0, 'text', 0, 'marketing'),
(75, 63, 119, 120, 2, 'Unique Selling Position', '2015-11-18 00:43:16', '2015-11-18 00:43:16', 0, 'text', 0, 'marketing'),
(76, 63, 121, 122, 2, 'Brand Position/Statement', '2015-11-18 00:43:24', '2015-11-18 00:43:25', 0, 'text', 0, 'marketing'),
(77, 68, 133, 134, 2, 'Marketing and Media Plan', '2015-11-18 00:44:18', '2015-11-18 00:44:18', 0, 'text', 0, 'marketing'),
(78, 68, 135, 136, 2, 'Digital/Social Media Content Plan', '2015-11-18 00:44:27', '2015-11-18 00:44:28', 0, 'text', 0, 'marketing');

-- --------------------------------------------------------

--
-- Table structure for table `plan_contents`
--

CREATE TABLE `plan_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_contents`
--

INSERT INTO `plan_contents` (`id`, `content`, `created_at`, `updated_at`, `status`, `user_id`, `cat_id`, `plan_id`) VALUES
(1, '<p>Is it broken?</p>\r\n', '2015-11-17 14:25:10', '2015-11-17 14:25:10', 1, 1, 10, 1),
(2, '<p>Testing endponit.</p>\r\n', '2015-11-17 14:33:57', '2015-11-17 14:33:57', 1, 1, 30, 1),
(5, '<p>GetJackD! Method aims to provide businesses with the opportunity to engage our services | expertise via a dedicated portal across our fields of expertise: marketing, media, sponsorships and project | event management.</p>\n\n<p>With a combined 17 years media experience – across radio, television, events, digital and record labels we specialise in the entertainment space.</p>\n\n<p>Our portal is dedicated to empowering, educating and growing new | emerging and small businesses, focusing on areas where there are knowledge gaps. GetJacKD! Method’s method include:</p>\n\n<p> </p>\n\n<ul><li><strong>Make It Happen:</strong> We provide a full business plan with help | tips and tricks inside a dedicated portal. Businesses have access to the portal for a three-year period, so they can capture how their business grows in the important first few years of set up. 100% Passive</li>\n</ul>\n\n<p> </p>\n\n<ul><li><strong>Brand It:</strong> We provide businesses with the tips and tricks to creating their business brand. From text, colours to logo and website creation, we give businesses advice and examples of how to position themselves to stand out. This also lives in the dedicated portal, live for three years. 100% Passive</li>\n</ul>\n\n<p> </p>\n\n<ul><li><strong>Plan it and Shout it:</strong> Businesses will be provided with media/marketing 101 documents with pros &amp; cons of each platform and case studies of businesses using each. This is for businesses ready to start telling people about what they have to sell, but don’t need helping planning the activity. 100% Passive</li>\n</ul>\n\n<p> </p>\n\n<ul><li><strong>Plan It, Market It, Sell It:</strong> We provide start ups/small businesses with templates to find their position in market. Their USP, SWOT, competitors and customers (so they can think about how they market). They will have access to us for 2 hours to brief us on their business and start to think about how they can start to tell and market it what they have to sell. A marketing plan will be provided for them to complete and send back, with a 2-hour follow up session provided to offer advice on the plan before they take action. Businesses will also be provided with media/marketing 101 documents with pros &amp; cons of each platform and case studies to help them build their plans. 75% Passive</li>\n</ul>\n\n<p> </p>\n\n<p>GetJackD! Method aims to provide businesses with easy to access advice and documentation, offering a clear path for a new business or existing business/campaign to come to life.</p>\n\n<p> </p>\n\n<p>We will have a dedicated online | digital marketing strategy to help grow the Method community and offer advice and support via blog and EDM content that lives across our website, social media channels and inside the portal. </p>\n\n<p> </p>\n\n<p>The marketing objectives of GetJackD! Method over the next 12 months is to:</p>\n\n<p> </p>\n\n<ul><li>Introduce the Australian market to the GetJackD! Method (driving brand awareness)</li>\n	<li>Turnover $xxxx in year 1, selling a combination of packages</li>\n	<li>Use client testimonials to help tell our story to drive sale</li>\n</ul>', '2015-11-17 19:02:07', '2015-11-23 23:36:40', 0, 1, 59, 1),
(6, '<p>Testing executive summary!</p>\r\n', '2015-11-17 19:02:12', '2015-11-17 19:02:12', 1, 1, 59, 1),
(8, 's:1372:"{"data":[["FORECAST BALANCE SHEET",null,null,null],[null,"Year One","Year Two","Year Three"],[null,null,null,null],["Current Assets",null,null,null],["Accounts Receivable",null,null,null],["Petty Cash",null,null,null],["Cash",null,null,null],["Stock",null,null,null],["Inventory",null,null,null],["Add More…",null,null,null],[null,null,null,null],["Long Term Assets",null,null,null],["Vehicles",null,null,null],["Property|Building",null,null,null],["Land",null,null,null],["Equipment & Tools",null,null,null],["Furniture & Office fitout",null,null,null],["Renovations",null,null,null],["Computer Equipment",null,null,null],["Depreciation",null,null,null],["Add More…",null,null,null],[null,null,null,null],["Total Assets","=SUM(B3:B22)","=SUM(C3:C22)","=SUM(D3:D22)"],[null,null,null,null],["Current Liabilities",null,null,null],["Accounts Payable",null,null,null],["Interest Payable",null,null,null],["Income Tax",null,null,null],["Credit Cards Payable",null,null,null],["Salaries|Wages Payable",null,null,null],[null,null,null,null],["Long Term Liabilities",null,null,null],["Business Loans",null,null,null],[null,null,null,null],[null,null,null,null],["Total Liabilities","=SUM(B25:B35)","=SUM(C25:C35)","=SUM(D25:D35)"],[null,null,null,null],["Net Assets","=B23-B36","=C23-C36","=D23-D36"],["Owners Equity","=B24-B37","=C24-C37","=D24-D37"],[null,null,null,null]]}";', '2015-11-18 16:15:46', '2015-11-18 16:15:46', 1, 1, 43, 1),
(9, 's:2423:"{"data":[["PROJECTED CASHFLOW",null,null,null,null,null,null,null,null,null,null,null,null],[null,"JAN","FEB","MAR","APRIL","MAY","JUNE","JULY","AUG","SEPT","OCT","NOV","DEC"],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Beginning Cash  Balance",null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Incoming ",null,null,null,null,null,null,null,null,null,null,null,null],["Income | Sales",null,null,null,null,null,null,null,null,null,null,null,null],["Add More…",null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Total Incoming Cash ",null,"=SUM(C6:C9)","=SUM(D6:D9)","=SUM(E6:E9)","=SUM(F6:F9)","=SUM(G6:G9)","=SUM(H6:H9)","=SUM(I6:I9)","=SUM(J6:J9)","=SUM(K6:K9)","=SUM(L6:L9)","=SUM(M6:M9)"],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Total Cash Available","=SUM(B4+B10)","=SUM(C4+C10)","=SUM(D4+D10)","=SUM(E4+E10)","=SUM(F4+F10)","=SUM(G4+G10)","=SUM(H4+H10)","=SUM(I4+I10)","=SUM(J4+J10)","=SUM(K4+K10)","=SUM(L4+L10)","=SUM(M4+M10)"],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Outgoing ",null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Insert all expenses",null,null,null,null,null,null,null,null,null,null,null,null],["Accountant Fees",null,null,null,null,null,null,null,null,null,null,null,null],["Bank Fees",null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Add More…",null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null],["Total Outgoing Expenses","=SUM(B13:B21)","=SUM(C13:C21)","=SUM(D13:D21)","=SUM(E13:E21)","=SUM(F13:F21)","=SUM(G13:G21)","=SUM(H13:H21)","=SUM(I13:I21)","=SUM(J13:J21)","=SUM(K13:K21)","=SUM(L13:L21)","=SUM(M13:M21)"],["Cash Balance","=B12-B22","=C12-C22","=D12-D22","=E12-E22","=F12-F22","=G12-G22","=H12-H22","=I12-I22","=J12-J22","=K12-K22","=L12-L22","=M12-M22"],["Closing","=B4+B12-B22","=C4+C12-C22","=D4+D12-D22","=E4+E12-E22","=F4+F12-F22","=G4+G12-G22","=H4+H12-H22","=I4+I12-I22","=J4+J12-J22","=K4+K12-K22","=L4+L12-L22","=M4+M12-M22"],[null,null,null,null,null,null,null,null,null,null,null,null,null]]}";', '2015-11-18 16:16:00', '2015-11-18 16:16:00', 1, 1, 44, 1),
(11, '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>', '2015-11-18 17:39:47', '2015-11-18 17:39:47', 1, 1, 59, 0),
(13, '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', '2015-11-18 17:39:58', '2015-11-18 17:39:58', 1, 1, 61, 0),
(15, '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', '2015-11-18 17:40:11', '2015-11-18 17:40:11', 1, 1, 62, 0),
(17, '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', '2015-11-18 17:40:24', '2015-11-18 17:40:24', 1, 1, 63, 0),
(19, 's:3226:"{"data":[["","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC","Total"],["Total Incomes","=SUM(B7:B12)","=SUM(C7:C12)","=SUM(D7:D12)","=SUM(E7:E12)","=SUM(F7:F12)","=SUM(G7:G12)","=SUM(H7:H12)","=SUM(I7:I12)","=SUM(J7:J12)","=SUM(K7:K12)","=SUM(L7:L12)","=SUM(M7:M12)","=SUM(B2:M2)"],["Total Expenses","=SUM(B17:B43)","=SUM(C17:C43)","=SUM(D17:D43)","=SUM(E17:E43)","=SUM(F17:F43)","=SUM(G17:G43)","=SUM(H17:H43)","=SUM(I17:I43)","=SUM(J17:J43)","=SUM(K17:K43)","=SUM(L17:L43)","=SUM(M17:M43)","=SUM(B3:M3)"],["NET (Income - Expenses)","=B2-B3","=C2-C3","=D2-D3","=E2-E3","=F2-F3","=G2-G3","=H2-H3","=I2-I3","=J2-J3","=K2-K3","=L2-L3","=M2-M3","=N2-N3"],["","","","","","","","","","","","","",""],["Income","","","","","","","","","","","","",""],["Salary",11370,11370,11370,11370,11370,11370,11370,11370,11370,11370,11370,11370,""],["Interest income",56,56,56,56,56,56,56,56,56,56,56,56,""],["Public assistance","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ",""],["Dividends","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ",""],["Gifts",300,300,300,300,300,300,300,300,300,300,300,300,""],["Other",1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,""],["","","","","","","","","","","","","",""],["Expenses","","","","","","","","","","","","",""],["Living","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Rent/Mortgage",3200,3200,3200,3200,3200,3200,3200,3200,3200,3200,3200,3200,""],["Electricity",160,160,160,160,160,160,160,160,160,160,160,160,""],["Water/Gas/Sewer",80,80,80,80,80,80,80,80,80,80,80,80,""],["TV/Internet/Phone",50,50,50,50,50,50,50,50,50,50,50,50,""],["Maintenance",260,260,260,260,260,260,260,260,260,260,260,260,""],["Obligations","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Loans",1500,1500,1500,1500,1500,1500,1500,1500,1500,1500,1500,1500,""],["Credit cards",120,120,120,120,120,120,120,120,120,120,120,120,""],["Taxes","450","450","450","450","450","450","450","450","450","450","450","450",""],["Insurance","140","140","140","140","140","140","140","140","140","140","140","140",""],["Daily expenses","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Food",1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,""],["Clothing",350,350,350,350,350,350,350,350,350,350,350,350,""],["Personal supplies",120,120,120,120,120,120,120,120,120,120,120,120,""],["Health care",320,320,320,320,320,320,320,320,320,320,320,320,""],["Education",540,540,540,540,540,540,540,540,540,540,540,540,""],["Entertainment",210,210,210,210,210,210,210,210,210,210,210,210,""],["Transportation",220,220,220,220,220,220,220,220,220,220,220,220,""],["Other","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Donations",80,80,80,80,80,80,80,80,80,80,80,80,""],["Savings",500,500,500,500,500,500,500,500,500,500,500,500,""],["Gifts",200,200,200,200,200,200,200,200,200,200,200,200,""],["Retirement",800,800,800,800,800,800,800,800,800,800,800,800,""],["Other",150,150,150,150,150,150,150,150,150,150,150,150,""],[null,null,null,null,null,null,null,null,null,null,null,null,null,null]]}";', '2015-11-18 20:55:13', '2015-11-18 20:55:13', 1, 1, 42, 1),
(20, 's:3226:"{"data":[["","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC","Total"],["Total Incomes","=SUM(B7:B12)","=SUM(C7:C12)","=SUM(D7:D12)","=SUM(E7:E12)","=SUM(F7:F12)","=SUM(G7:G12)","=SUM(H7:H12)","=SUM(I7:I12)","=SUM(J7:J12)","=SUM(K7:K12)","=SUM(L7:L12)","=SUM(M7:M12)","=SUM(B2:M2)"],["Total Expenses","=SUM(B17:B43)","=SUM(C17:C43)","=SUM(D17:D43)","=SUM(E17:E43)","=SUM(F17:F43)","=SUM(G17:G43)","=SUM(H17:H43)","=SUM(I17:I43)","=SUM(J17:J43)","=SUM(K17:K43)","=SUM(L17:L43)","=SUM(M17:M43)","=SUM(B3:M3)"],["NET (Income - Expenses)","=B2-B3","=C2-C3","=D2-D3","=E2-E3","=F2-F3","=G2-G3","=H2-H3","=I2-I3","=J2-J3","=K2-K3","=L2-L3","=M2-M3","=N2-N3"],["","","","","","","","","","","","","",""],["Income","","","","","","","","","","","","",""],["Salary",11370,11370,11370,11370,11370,11370,11370,11370,11370,11370,11370,11370,""],["Interest income",56,56,56,56,56,56,56,56,56,56,56,56,""],["Public assistance","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ",""],["Dividends","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ","-  ",""],["Gifts",300,300,300,300,300,300,300,300,300,300,300,300,""],["Other",1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,""],["","","","","","","","","","","","","",""],["Expenses","","","","","","","","","","","","",""],["Living","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Rent/Mortgage",3200,3200,3200,3200,3200,3200,3200,3200,3200,3200,3200,3200,""],["Electricity",160,160,160,160,160,160,160,160,160,160,160,160,""],["Water/Gas/Sewer",80,80,80,80,80,80,80,80,80,80,80,80,""],["TV/Internet/Phone",50,50,50,50,50,50,50,50,50,50,50,50,""],["Maintenance",260,260,260,260,260,260,260,260,260,260,260,260,""],["Obligations","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Loans",1500,1500,1500,1500,1500,1500,1500,1500,1500,1500,1500,1500,""],["Credit cards",120,120,120,120,120,120,120,120,120,120,120,120,""],["Taxes","450","450","450","450","450","450","450","450","450","450","450","450",""],["Insurance","140","140","140","140","140","140","140","140","140","140","140","140",""],["Daily expenses","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Food",1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,1200,""],["Clothing",350,350,350,350,350,350,350,350,350,350,350,350,""],["Personal supplies",120,120,120,120,120,120,120,120,120,120,120,120,""],["Health care",320,320,320,320,320,320,320,320,320,320,320,320,""],["Education",540,540,540,540,540,540,540,540,540,540,540,540,""],["Entertainment",210,210,210,210,210,210,210,210,210,210,210,210,""],["Transportation",220,220,220,220,220,220,220,220,220,220,220,220,""],["Other","","","","","","","","","","","","",""],["","","","","","","","","","","","","",""],["Donations",80,80,80,80,80,80,80,80,80,80,80,80,""],["Savings",500,500,500,500,500,500,500,500,500,500,500,500,""],["Gifts",200,200,200,200,200,200,200,200,200,200,200,200,""],["Retirement",800,800,800,800,800,800,800,800,800,800,800,800,""],["Other",150,150,150,150,150,150,150,150,150,150,150,150,""],[null,null,null,null,null,null,null,null,null,null,null,null,null,null]]}";', '2015-11-18 21:13:50', '2015-11-18 21:13:50', 1, 1, 42, 1),
(21, '<table><thead><tr><th>\r\n			<p>Goals</p>\r\n			</th>\r\n			<th>\r\n			<p>What steps will you take to achieve your goals/objectives</p>\r\n			</th>\r\n			<th>\r\n			<p>When do you expect to achieve your set goals/objectives?</p>\r\n			</th>\r\n			<th>\r\n			<p>Person responsible</p>\r\n			</th>\r\n		</tr></thead><tbody><tr><td>\r\n			<p>My objectives are something like this where I want to </p>\r\n			</td>\r\n			<td>\r\n			<p>Thioasdfnklasdnfasdlknfasdlknfasdlkfnasd</p>\r\n			</td>\r\n			<td>\r\n			<p>This is somet stufasdasdfasdf This is somet stufasdasdfasdfThis is somet stufasdasdfasdfThis is somet stufasdasdfasdfThis is somet stufasdasdfasdfThis is somet stufasdasdfasdfThis is somet stufasdasdfasdf</p>\r\n			</td>\r\n			<td>\r\n			<p>asdoifnasdflkasdnflkasdnflkasdnflkasdnflaskdnf</p>\r\n			</td>\r\n		</tr></tbody></table>', '2015-11-18 21:45:31', '2015-11-18 22:39:52', 1, 1, 33, 1),
(22, '<p>New content...</p>\r\n', '2015-11-18 21:55:02', '2015-11-18 21:55:02', 1, 1, 31, 1),
(24, '<p>newer content</p>\r\n', '2015-11-18 22:01:48', '2015-11-18 22:04:06', 1, 1, 32, 1),
(26, '<p>GetJackD! Method is a portal customised to our needs and the needs of our customers. There is also a consumer facing website, promoting the packages and processing all payments of the package.   </p>\n\n<p> </p>\n\n<p>Consumer Website Structure:</p>\n\n<ul><li>Home Page</li>\n	<li>What is GetJackD! Method?</li>\n	<li>Why GetJackD! Method?</li>\n	<li>Who are GetJackD! Method?</li>\n	<li>The Method (with info/buy now buttons)</li>\n	<li>Blogs | case studies | industry profiles</li>\n	<li>Contact Details</li>\n	<li>Log In/Account Details</li>\n	<li>Social Media Plug Ins</li>\n</ul>\n\n<p> </p>\n\n<p>GetJackD! Method Portal:</p>\n\n<ul><li>Each user has their own log in and password</li>\n	<li>Each user opens to their own home screen with the method they’ve purchased (personalised to them). Including news/blogs relevant to their method</li>\n	<li>Users will have access to their content for an agreed period of time, with all documentations | templates loaded for them. They have the ability to export their plans into PDFs for use with banks etc. We have ensured we’ve protected our IP as much as possible, by making a lot of the content web based</li>\n	<li>We have ensured that the portal;\n	<ul><li>Highlights other methods and allows purchase through the main website</li>\n		<li>Host all content (no need to click away ie for blogs/case studies etc)</li>\n		<li>Schedule Meetings via a calendar and scheduling | call program</li>\n		<li>Track where users are at in each method (via weekly reports). We can send reminder emails | notes if they’re not progressing or if they’re at the end, upselling them to the next suitable method</li>\n	</ul></li>\n</ul>\n\n<p> </p>\n\n<p>Potential Portal Structure:</p>\n\n<ul><li>Individual dashboard with their;\n	<ul><li>Method purchased (all the content)</li>\n		<li>Blogs | case studies | industry experts</li>\n		<li>Upgrade to new methods</li>\n		<li>User details</li>\n		<li>Social media links</li>\n		<li>FAQ</li>\n	</ul></li>\n</ul>\n\n<p> </p>\n\n<p>Required Assets (<strong>all have been implemented TD</strong>):</p>\n\n<ul><li>E-Commerce Platform</li>\n	<li>CRM. Want to track leads &amp; push people through the sales funnel</li>\n	<li>EDMs</li>\n	<li>Skype/Go to Meeting/Webnair/calendar</li>\n	<li>All content written for google adwords etc</li>\n	<li>All mobile responsive</li>\n</ul>\n\n<p> </p>\n\n<p>Technical Considerations (<strong>all have or are being implemented</strong>):</p>\n\n<ul><li>Secure sign in</li>\n	<li>Enough server space to house thousands of user profiles and documents. That doesn’t break</li>\n	<li>Secure Payment portal</li>\n	<li>Pop-up windows with tips | tricks | advice (or maybe special sections?)</li>\n	<li>We want the platform to talk to our Xero accounting system</li>\n	<li>All systems to work together. Ie CRM/EDM/XERO/ECOMMERCE/SCHEDULING</li>\n	<li>Long term: Option to create multiple language portals depending on where the user is from (e.g. Japanese)</li>\n</ul>', '2015-11-23 19:31:52', '2015-11-24 20:48:34', 0, 1, 30, 2),
(27, '<p>GetJackD! Method is a leading online communications portal that; educates, supports and empowers start-ups and small businesses to run their own race across media and marketing channels.  </p>\n\n<p> </p>\n\n<p>The Method provides the tools and advice to help businesses grow. Testerrrrr</p>\n', '2015-11-23 19:37:55', '2015-11-24 20:35:11', 1, 1, 31, 2),
(28, '', '2015-11-23 19:42:04', '2015-11-24 19:18:31', 1, 1, 57, 2),
(29, '<p>GetJackD! Method aims to provide businesses with the opportunity to engage our services | expertise via a dedicated portal across our fields of expertise: marketing, media, sponsorships and project | event management.</p>\n\n<p>With a combined 17 years media experience – across radio, television, events, digital and record labels we specialise in the entertainment space.</p>\n\n<p> </p>\n\n<p>Our portal is dedicated to empowering, educating and growing new | emerging and small businesses, focusing on areas where there are knowledge gaps. GetJacKD! Method’s method include:</p>\n\n<p> </p>\n\n<ul><li><strong>Make It Happen:</strong> We provide a full business plan with help | tips and tricks inside a dedicated portal. Businesses have access to the portal for a three-year period, so they can capture how their business grows in the important first few years of set up. 100% Passive</li>\n</ul>\n\n<p> </p>\n\n<ul><li><strong>Brand It:</strong> We provide businesses with the tips and tricks to creating their business brand. From text, colours to logo and website creation, we give businesses advice and examples of how to position themselves to stand out. This also lives in the dedicated portal, live for three years. 100% Passive</li>\n</ul>\n\n<p> </p>\n\n<ul><li><strong>Plan it and Shout it:</strong> Businesses will be provided with media/marketing 101 documents with pros &amp; cons of each platform and case studies of businesses using each. This is for businesses ready to start telling people about what they have to sell, but don’t need helping planning the activity. 100% Passive</li>\n</ul>\n\n<p> </p>\n\n<ul><li><strong>Plan It, Market It, Sell It:</strong> We provide start ups/small businesses with templates to find their position in market. Their USP, SWOT, competitors and customers (so they can think about how they market). They will have access to us for 2 hours to brief us on their business and start to think about how they can start to tell and market it what they have to sell. A marketing plan will be provided for them to complete and send back, with a 2-hour follow up session provided to offer advice on the plan before they take action. Businesses will also be provided with media/marketing 101 documents with pros &amp; cons of each platform and case studies to help them build their plans. 75% Passive</li>\n</ul>\n\n<p> </p>\n\n<p>GetJackD! Method aims to provide businesses with easy to access advice and documentation, offering a clear path for a new business or existing business/campaign to come to life.</p>\n\n<p> </p>\n\n<p>We will have a dedicated online | digital marketing strategy to help grow the Method community and offer advice and support via blog and EDM content that lives across our website, social media channels and inside the portal. </p>\n\n<p> </p>\n\n<p>The business objectives of GetJackD! Method over the next 12 months is to:</p>\n\n<ul><li>Be as passive as possible. Offer a service that doesn’t require a lot of man hours</li>\n	<li>Help grow businesses. Empower them to learn more around our area of expertise</li>\n	<li>Work internationally</li>\n	<li>Bring in project managers, community manager, digital marketer and copywriter</li>\n</ul>', '2015-11-23 20:46:15', '2015-11-23 20:46:27', 1, 1, 10, 2),
(30, '<p>GetJackD! Method is a leading online communications portal that; educates, supports and empowers start-ups and small businesses to run their own race across media and marketing channels.  </p>\n\n<p> </p>\n\n<p>The Method provides the tools and advice to help businesses grow.</p>\n', '2015-11-23 20:48:38', '2015-11-24 20:31:22', 1, 1, 32, 2),
(31, '<table summary="A table with space to enter details of current staff under the headings: Job Title (e.g. Marketing/ Sales Manager), Name (e.g. Mr Chris Brantley), Expected staff turnover (e.g. 12-18 months) and Skills or strengths (e.g. Relevant qualifications in Sales/Marketing. At least 5 years experience in the industry. Award in marketing excellence)."><thead><tr><th>\n			<p>Name</p>\n			</th>\n			<th>\n			<p>Job Title</p>\n			</th>\n			<th>\n			<p>Skills/Qualifications or Strengths/Responsibilities</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p><strong>Amy Warren </strong></p>\n			</td>\n			<td>\n			<p><strong>Director and Co-Founder</strong></p>\n			</td>\n			<td>\n			<p>Amy Warren has worked with global brands, been at the helm of million dollar campaigns, built a successful business and become a mentor to the next generation of marketers.</p>\n\n			<p>From major music labels like Sony Music, Universal Music, EMI and Warner Music to market giants such as Optus, Swisse, McDonalds and Samsung, Amy’s extensive experience has informed the GetJackD! Method, making it an invaluable resource for emerging businesses.</p>\n\n			<p>Her ability to conceptualise, plan, present, budget and manage has been tested during hundreds of successful campaigns, earning Amy <a href="http://www.commercialradio.com.au/">Commercial Radio Australia Awards</a> for Best Multimedia Execution. Her value as an educator was officially recognised recently when Amy was invited to join the Queensland University of Technology’s Entertainment Industry faculty as a mentor. </p>\n\n			<p>As a founder of GetJackD! Media, Amy knows how to grow a successful business, and with the creation of the GetJackD! Method, she is not only sharing well-tested marketing techniques but also her personal experience as an entrepreneur. From strategy to media buying, Amy’s unique understanding of the media landscape makes the GetJackD! Method a critical advantage for new, emerging and expanding businesses.</p>\n			</td>\n		</tr><tr><td><strong>Kirsty Warren </strong></td>\n			<td><strong>Co-Founder</strong></td>\n			<td>\n			<p>Kirsty Warren has worked with one of the world’s leading record companies, Sony Music, developing a unique and diverse skill set across sales, artist public relations and event management.</p>\n\n			<p>Kirsty consistently and significantly exceeding her sales targets, all while overseeing artist publicity across Queensland. She has worked with leading brands like JB HI-FI, Southern Cross Austereo, Nova Entertainment, ARN, Fairfax Media and News Limited. Kirsty has maintained relationships with some of Queensland’s leading independent music retailers, showing a deep understanding of small business needs in a big corporate world.</p>\n\n			<p>As Business Manager at GetJackD! Media, Kirsty uses her invaluable knowledge of the Australian media and retail landscapes to give clients a critical edge in the market. Her diverse skill set across, sales, artist and public relations, and event management continues to provide the business’ many clients and partners with outstanding results.</p>\n\n			<p>Kirsty’s passion for helping small business makes her a driving force in the development of the highly effective GetJackD! Method</p>\n			</td>\n		</tr><tr><td>Glenn Mitchell</td>\n			<td>Copywriter</td>\n			<td>Freelance and experience with digital copywriting</td>\n		</tr><tr><td>Sunny Interactive</td>\n			<td>Portal Build and Website</td>\n			<td>Manage all elements of the portal (design, build and updgrades as needed)</td>\n		</tr><tr><td>TBA</td>\n			<td>Graphic Designer</td>\n			<td>Brief in on all design elements required for the business</td>\n		</tr><tr><td>QUT program</td>\n			<td>Interns</td>\n			<td>2-4 students to assist in creating content and managing social media content. Also basic admin.</td>\n		</tr></tbody></table>', '2015-11-23 20:50:15', '2015-11-23 21:04:21', 1, 1, 38, 2),
(32, '<table><tbody><tr><td>\n			<p><strong>General Business Details</strong></p>\n			</td>\n			<td>\n			<p><strong>Description</strong></p>\n			</td>\n		</tr><tr><td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Business name:</p>\n			</td>\n			<td>\n			<p>GetJackD! Method Test</p>\n			</td>\n		</tr><tr><td>\n			<p>Trading name:</p>\n			</td>\n			<td>\n			<p>GetJackD! Method</p>\n			</td>\n		</tr><tr><td>\n			<p>Registered date:</p>\n			</td>\n			<td>\n			<p>2015</p>\n			</td>\n		</tr><tr><td>\n			<p>Registered location:</p>\n			</td>\n			<td>\n			<p>Queensland</p>\n			</td>\n		</tr><tr><td>\n			<p>Business structure:</p>\n			</td>\n			<td>\n			<p>Trust</p>\n			</td>\n		</tr><tr><td>\n			<p>Your business number/s:</p>\n			</td>\n			<td>\n			<p>4829547494039</p>\n			</td>\n		</tr><tr><td>\n			<p>Location of premises:</p>\n			</td>\n			<td>\n			<p>Australia first than International. Online store/portal</p>\n			</td>\n		</tr><tr><td>\n			<p>Buy/lease agreements</p>\n			</td>\n			<td>\n			<p>None</p>\n			</td>\n		</tr><tr><td>\n			<p>Relevant business taxes: are you registered? Yes or no?</p>\n			</td>\n			<td>\n			<p>Yes – GST</p>\n			</td>\n		</tr><tr><td>\n			<p>Licences and permits:</p>\n			</td>\n			<td>\n			<p>None</p>\n			</td>\n		</tr><tr><td>\n			<p>Domain/website names:</p>\n			</td>\n			<td>\n			<p>getjackdmethod.com</p>\n			</td>\n		</tr><tr><td>\n			<p>Memberships and affiliations:</p>\n			</td>\n			<td>\n			<p>None</p>\n			</td>\n		</tr><tr><td>\n			<p>Business trading hours:</p>\n			</td>\n			<td>\n			<p>All the time</p>\n			</td>\n		</tr><tr><td>\n			<p>Add more…</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr></tbody></table>\n\n<p> </p>\n\n<table><tbody><tr><td>\n			<p><strong>Products/Services</strong></p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Products/services:</p>\n			</td>\n			<td>\n			<p>Marketing and media online communications portal to help small businesses grow and run their own marketing race, at their own pace. We’re here to educate, encourage and empower.</p>\n			</td>\n		</tr><tr><td>\n			<p>Add more…</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr></tbody></table>\n\n<p> </p>\n\n<table><tbody><tr><td>\n			<p><strong>Owner Details</strong></p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Business owner(s):</p>\n			</td>\n			<td>\n			<p>Amy Warren &amp; Kirsty Warren</p>\n			</td>\n		</tr><tr><td>\n			<p>Relevant owner experience:</p>\n			</td>\n			<td>\n			<p>11 years at SCA (radio/TV/digital/events) &amp; 5 years at Sony Music (artists relations, promo/publicity &amp; retail sales). 2 years running GetJackD! Media an media and marketing business for entertainment |lifestyle based clients</p>\n			</td>\n		</tr><tr><td>\n			<p>Add more…</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr></tbody></table>', '2015-11-23 20:53:34', '2015-11-24 20:12:13', 1, 1, 24, 2),
(33, '<table summary="A table with space to enter details of required staff under the headings: Job Title (e.g. Office Manager), Quantity (e.g. 1), Expected staff turnover (e.g. 2-3 years), Skills necessary (e.g. Relevant qualifications in Office Management. At least 2 years experience.), and date required (e.g. Month/Year)."><thead><tr><th>\n			<p>Job Title</p>\n			</th>\n			<th>\n			<p>Quantity</p>\n			</th>\n			<th>\n			<p>Necessary Skills</p>\n			</th>\n			<th>\n			<p>Date Required</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Coordinator Admin</p>\n			</td>\n			<td>\n			<p>1</p>\n			</td>\n			<td>\n			<p>Relevant qualifications in Office Management. At least 2 years experience.</p>\n			</td>\n			<td>\n			<p>2016</p>\n			</td>\n		</tr><tr><td>Project Managers</td>\n			<td>based on methods purchased</td>\n			<td>Relevant qualifications in Office Management. At least 2 years experience.</td>\n			<td>2016</td>\n		</tr><tr><td>Community Manager</td>\n			<td>1</td>\n			<td>\n			<p>Freelance basis</p>\n\n			<p>Relevant qualifications in social media and content strategy to community manager and develop appropriate content.</p>\n			</td>\n			<td>2016</td>\n		</tr><tr><td>Digital Marketer</td>\n			<td>1</td>\n			<td>\n			<p>Consultation basis</p>\n\n			<p>BCM. Expertise in Facebook | LinkedIn and Search</p>\n			</td>\n			<td>2016</td>\n		</tr></tbody></table>', '2015-11-23 21:07:04', '2015-11-23 21:07:07', 1, 1, 39, 2),
(34, '<p><strong>Recruitment options</strong></p>\n\n<ul><li>Friends in the industry looking for additional work as project managers</li>\n	<li>QUT for the Business Coordinator | Community Manager | Admin</li>\n	<li>Using existing partners. Glenn and BCM</li>\n</ul>\n\n<p><strong>Training programs</strong></p>\n\n<ul><li>Develop a training program for project managers that they must follow. This will be provided through the portal (multiple log-ins required). Not ready on launch</li>\n	<li>Ensure GJMethod (Amy and Kirsty) are unskilled on content and digital marketing strategies to start to manage own digital marketing spend, and train a QUT graduate on managing the Community moving forward</li>\n</ul>\n\n<p><strong>Skill retention strategies</strong></p>\n\n<ul><li>Train staff on the methods via the portal</li>\n	<li>Conduct sessions with experts in the field to continue to up skill and teach</li>\n	<li>Offer incentives for bringing business into the method. % to be agreed</li>\n	<li>Highlight the benefits of working remotely | flexibility</li>\n	<li>Work with other small business in partnership and as needed basis</li>\n</ul>', '2015-11-23 21:14:16', '2015-11-23 21:18:06', 1, 1, 40, 2),
(35, '<table><thead><tr><th>\n			<p>Product/Service</p>\n			</th>\n			<th>\n			<p>Description</p>\n			</th>\n			<th>\n			<p>Market Demand</p>\n			</th>\n			<th>\n			<p>Distributor/Supplier</p>\n			</th>\n			<th>\n			<p>Cost for Distributor/Supplier</p>\n			</th>\n			<th>\n			<p>Price</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p><strong>Blogs | emails</strong></p>\n			</td>\n			<td>\n			<p>Advice based on personal experience email series to get you thinking about business and our areas of expertise.</p>\n\n			<p>All a taster for our methods, along with industry news and tips and tricks on our area of expertise. Written in a fun and cheeky way that gets attention.</p>\n\n			<p>Once people are on our database they receive this communication</p>\n			</td>\n			<td>\n			<p>N/A</p>\n			</td>\n			<td>\n			<p>online portal</p>\n			</td>\n			<td>\n			<p>$0</p>\n			</td>\n			<td>\n			<p><strong>$0/pm</strong></p>\n			</td>\n		</tr><tr><td><strong>Make It Happen</strong></td>\n			<td>\n			<p>Access to the portal</p>\n\n			<p> </p>\n\n			<p>Business Plan (a full editable and personalised plan with tips and tricks)</p>\n\n			<p> </p>\n\n			<p>Access to the portal for 2 years</p>\n\n			<p>Access to helpful links/bios/blogs</p>\n			</td>\n			<td>N/A</td>\n			<td>online portal</td>\n			<td>$0</td>\n			<td>\n			<p><strong>$299</strong></p>\n			</td>\n		</tr><tr><td><strong>Brand It</strong></td>\n			<td>\n			<p>Access to the portal</p>\n\n			<p> </p>\n\n			<p>Information provided on branding your business. Tips and Tricks</p>\n\n			<p> </p>\n\n			<p>Access to the portal for 2 years</p>\n\n			<p>Additional access to helpful links/bios/blogs</p>\n			</td>\n			<td>N/A</td>\n			<td>online</td>\n			<td>$0</td>\n			<td><strong>$99</strong></td>\n		</tr><tr><td>\n			<p><strong>Plan it and Shout it</strong></p>\n			</td>\n			<td>\n			<p>Access to the portal</p>\n\n			<p> </p>\n\n			<p>Media platform documents, case studies, dos and don’ts</p>\n\n			<p> </p>\n\n			<p>Access to all content for 2 years. Content will be refreshed over this time, to ensure information is relevant especially in the digital | social space</p>\n			Access to helpful links/bios/blogs</td>\n			<td>N/A</td>\n			<td>online</td>\n			<td>$0</td>\n			<td><strong>$499</strong></td>\n		</tr><tr><td><strong>Plan It, Market It, Now Sell It</strong></td>\n			<td>\n			<p>Access to the portal</p>\n\n			<p>Briefing form and access to booking a session with the GJMethod team (2hrs)</p>\n\n			<p> </p>\n\n			<p>Media platform documents, case studies, dos and don’ts</p>\n\n			<p> </p>\n\n			<p>Delivery of a Marketing/Media Plan inside the portal that can be saved and updated over the period</p>\n\n			<p> </p>\n\n			<p>Access to all content for 2 years. Content will be refreshed over this time, to ensure information is relevant especially in the digital | social space</p>\n\n			<p>Access to booking a session with the GJMethod team (2hrs) to go over the marketing plan and suggest any amendments before implementation</p>\n\n			<p>Access to helpful links/bios/blogs</p>\n			</td>\n			<td>N/A</td>\n			<td>online</td>\n			<td>$0</td>\n			<td><strong>$3,499</strong></td>\n		</tr></tbody></table>', '2015-11-23 21:26:49', '2015-11-23 21:27:10', 1, 1, 54, 2),
(36, '<table><thead><tr><th>\n			<p>Product/Service</p>\n			</th>\n			<th>\n			<p>Distribution Channel</p>\n			</th>\n			<th>\n			<p>Reason for Chosen Channel</p>\n			</th>\n			<th>\n			<p>Method of Sale</p>\n			</th>\n			<th>\n			<p>Strengths</p>\n			</th>\n			<th>\n			<p>Weaknesses</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Make it Happen</p>\n			</td>\n			<td>\n			<p>online store</p>\n			</td>\n			<td>\n			<p>It’s where the target market is </p>\n			</td>\n			<td>\n			<p>online store with a digital marketing and content strategy</p>\n			</td>\n			<td>\n			<p>Ease of access for people</p>\n\n			<p>cheap to promote</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>cBrand It</td>\n			<td>online store</td>\n			<td>It’s where the target market is </td>\n			<td>online store with a digital marketing and content strategy</td>\n			<td>\n			<p>Ease of access for people</p>\n\n			<p>cheap to promote</p>\n			</td>\n			<td>getting people to understand the platform and use it the right way</td>\n		</tr><tr><td>Plan It and Shout In</td>\n			<td>online store</td>\n			<td>It’s where the target market is </td>\n			<td>online store with a digital marketing and content strategy</td>\n			<td>\n			<p>Ease of access for people</p>\n\n			<p>cheap to promote</p>\n			</td>\n			<td>getting people to understand the platform and use it the right way</td>\n		</tr><tr><td>Plan It, Market It, Now Sell It </td>\n			<td>online store</td>\n			<td>It’s where the target market is </td>\n			<td>online store with a digital marketing and content strategy</td>\n			<td>\n			<p>Ease of access for people</p>\n\n			<p>cheap to promote</p>\n			</td>\n			<td>getting people to understand the platform and use it the right way</td>\n		</tr></tbody></table>', '2015-11-23 21:32:56', '2015-11-23 21:35:29', 1, 1, 56, 2),
(37, '<table summary="A table with space to enter details of products and services, including the product/service name, a brief description, and price (including GST)."><thead><tr><th>\n			<p>Legal Requirement</p>\n\n			<p> </p>\n			</th>\n			<th>\n			<p>Description</p>\n			</th>\n			<th>\n			<p>Date Completed</p>\n			</th>\n			<th>\n			<p>Cost</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Applied for a trademark</p>\n			</td>\n			<td>\n			<p>Have engaged a Lawyer and applied ourselves via the Australian Government IP website</p>\n			</td>\n			<td>\n			<p>January 2016</p>\n			</td>\n			<td>\n			<p>$430</p>\n			</td>\n		</tr><tr><td>Customer Terms</td>\n			<td>Have written a brief to the lawyers </td>\n			<td>November 2015</td>\n			<td>$750</td>\n		</tr><tr><td>Privacy Terms</td>\n			<td>Have written a brief to the lawyers </td>\n			<td>November 2015</td>\n			<td>$500</td>\n		</tr><tr><td>IP – Contributor Deed</td>\n			<td>All written and provided to contributors</td>\n			<td>September 2015</td>\n			<td>$500</td>\n		</tr></tbody></table>\n\n<p> </p>\n\n<p>Have engaged the following lawyers for agreement/employment contracts with clients/staff:</p>\n\n<p> </p>\n\n<p><strong>David Kelly</strong></p>\n\n<p>Director – KellyHazellQuill</p>\n\n<p>Find our blog at <a href="http://www.khq.com.au/blog">www.khq.com.au/blog</a></p>\n\n<p>Level 15, 440 Collins Street</p>\n\n<p>Melbourne, Victoria 3000</p>\n\n<p>+ 61 3 9663 9877 | 0409 018 436 | <a href="mailto:dkelly@khq.com.au">dkelly@khq.com.au</a> | <a href="http://www.kellyhazellquill.com.au/">www.khq.com.au</a></p>\n\n<p> </p>\n\n<p>IP/Trademark Engagement:</p>\n\n<p><strong>Fiona Gilbert </strong>| Partner</p>\n\n<p>Level 4, 2 Ebenezer Place,</p>\n\n<p>Adelaide, South Australia 5000</p>\n\n<p><strong>Tel: + 61 8 8311 9003</strong> | <strong>Fax:</strong> + 61 3 9009 5494 | <strong>Mob:</strong> 0449 118 113| <strong>Email:</strong> <a href="mailto:fgilbert@khq.com.au">fgilbert@khq.com.au</a> | <strong>Website:</strong> <a href="http://www.kellyhazellquill.com.au/">www.kellyhazellquill.com.au</a></p>\n\n<p> </p>\n', '2015-11-23 21:49:52', '2015-11-23 22:30:43', 1, 1, 48, 2),
(38, '<table><thead><tr><th>\n			<p>Insurance Type</p>\n			</th>\n			<th>\n			<p>Description/Coverage</p>\n			</th>\n			<th>\n			<p>Insurance Broker/Company/Contact Details</p>\n			</th>\n			<th>\n			<p>Premiums/Cost</p>\n			</th>\n			<th>\n			<p>Renewal Date</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p><strong>Workers compensation</strong></p>\n			</td>\n			<td>\n			<p>Not required at this point, as Kirsty and Amy are both Directors and Co-Founders. Staff will all be contractors/have their own business name</p>\n			</td>\n			<td>\n			<p>n/a</p>\n			</td>\n			<td>\n			<p>n/a</p>\n			</td>\n			<td>\n			<p>n/a</p>\n			</td>\n		</tr><tr><td><strong>Public liability insurance</strong></td>\n			<td>\n			<p>Policy Number: ESD00085725</p>\n\n			<p>CFC Underwriting Limited Cover: $10 million</p>\n\n			<p>P/A Location: Worldwide</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>Fee: $1448.49 P/A</td>\n			<td>Date: Jul 015-Jun016 </td>\n		</tr><tr><td><strong>Professional indemnity</strong></td>\n			<td>\n			<p>Policy Number: ESD00085725</p>\n\n			<p>CFC Underwriting Limited Cover: $10 million</p>\n\n			<p>P/A Location: Worldwide</p>\n			</td>\n			<td> </td>\n			<td>Fee: $1448.49 P/A</td>\n			<td>Date: Jul 015-Jun016 </td>\n		</tr></tbody></table>', '2015-11-23 22:00:39', '2015-11-23 22:01:04', 1, 1, 50, 2),
(39, '<table><thead><tr><th>\n			<p>Potential Risk</p>\n			</th>\n			<th>\n			<p>Level of Severity</p>\n			</th>\n			<th>\n			<p>Likelihood</p>\n			</th>\n			<th>\n			<p>Impact</p>\n			</th>\n			<th>\n			<p>Strategy to Avoid/Manage</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>No Clients</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>Highly Unlikely</p>\n			</td>\n			<td>\n			<p>Significant</p>\n			</td>\n			<td>\n			<p>Put together a detailed content and digital marketing strategy to drive people to the website and purchase a method</p>\n\n			<p> </p>\n\n			<p>Ensure this strategy pushes people through a clear sales funnel (that isn’t overly sales-ie) via database communication</p>\n			</td>\n		</tr><tr><td>Competitors</td>\n			<td> </td>\n			<td>High</td>\n			<td>Significant</td>\n			<td>Do what we do better and offer services that differ to there’s. Keep a competitive watch on the landscape</td>\n		</tr><tr><td>IP Protection</td>\n			<td> </td>\n			<td>High</td>\n			<td>Signficant</td>\n			<td>Need to make sure we protect or portal as much as possible. Keep an eye on the market. Make sure all staff employed | engaged have signed a contributor agreement to protect our interests</td>\n		</tr><tr><td>\n			<p>Portal | Technical Issues</p>\n			</td>\n			<td> </td>\n			<td>High</td>\n			<td>Signficant</td>\n			<td>\n			<p>Work closely with the IT and web team to make sure all functions of the portal are working and consumers are having the best possible experience.</p>\n\n			<p> </p>\n\n			<p>Jump on any issues ASAP and rectify. Ensure the customer is kept informed of the issues and when they will be rectified.</p>\n\n			<p> </p>\n			Need to put together a procedure manual and log, to ensure the issues are centralised and fixed appropriately</td>\n		</tr><tr><td>\n			<p>Customer Information Privacy and Security</p>\n			</td>\n			<td> </td>\n			<td>High</td>\n			<td>Signficant</td>\n			<td>\n			<p>Ensure all of the privacy policies, terms of use and portal manuals are easily accessed for customers</p>\n\n			<p> </p>\n\n			<p>Ensure the payment gateway is secure, along with the business portal. Want customers to know their information is safe from threats</p>\n\n			<p> </p>\n			Ensure antivirus software is included across the website and portal. All of these programs need to updated on a regular basis to ensure they’re current and work for our systems</td>\n		</tr></tbody></table>', '2015-11-23 22:05:38', '2015-11-23 22:05:46', 1, 1, 51, 2),
(40, '<table><thead><tr><th>\n			<p>Software and Programs</p>\n			</th>\n			<th>\n			<p>Purchase Date</p>\n			</th>\n			<th>\n			<p>Purchase Price</p>\n			</th>\n			<th>\n			<p>Updating or Running Costs</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Wordpress</p>\n			</td>\n			<td>\n			<p>2015</p>\n			</td>\n			<td>\n			<p>$59.00</p>\n			</td>\n			<td>\n			<p>$14.99 p/y</p>\n			</td>\n		</tr><tr><td>\n			<p>Paypal </p>\n			</td>\n			<td>\n			<p>2015</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>2% fee for all transactions</p>\n			</td>\n		</tr><tr><td>Portal &amp; CRM platform</td>\n			<td>2015</td>\n			<td>$8,350</td>\n			<td>TBA</td>\n		</tr><tr><td>Xero Accounting software</td>\n			<td>2015</td>\n			<td> </td>\n			<td>$50 p/m</td>\n		</tr><tr><td>SSL certificate</td>\n			<td>2015</td>\n			<td> </td>\n			<td>$150 p/y</td>\n		</tr><tr><td>Backup Server</td>\n			<td>2015</td>\n			<td> </td>\n			<td>$24 p/m</td>\n		</tr><tr><td>Office 365 accounts (2 in total)</td>\n			<td>2015</td>\n			<td> </td>\n			<td>$54 p/m</td>\n		</tr></tbody></table><p> </p>\n\n<table><thead><tr><th>\n			<p>Potential IT Issues/Required Updates or Maintenance</p>\n			</th>\n			<th>\n			<p>Person Responsible</p>\n			</th>\n			<th>\n			<p>Dates of Scheduled Updates/Maintenance</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Backup server for lost data</p>\n			</td>\n			<td>\n			<p>Sunny Interactive</p>\n			</td>\n			<td>\n			<p>Monthly</p>\n			</td>\n		</tr><tr><td>\n			<p>SSL certificate to protect user data</p>\n			</td>\n			<td>\n			<p>Sunny Interactive</p>\n			</td>\n			<td>\n			<p>Annually</p>\n			</td>\n		</tr></tbody></table>', '2015-11-23 22:15:03', '2015-11-23 22:15:11', 1, 1, 52, 2),
(41, '<table><thead><tr><th>\n			<p>Inventory Item</p>\n			</th>\n			<th>\n			<p>Cost Per Unit</p>\n			</th>\n			<th>\n			<p>Quantity on Hand</p>\n			</th>\n			<th>\n			<p>Total</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Basic Blog | Content Comms</p>\n			</td>\n			<td>\n			<p>$200</p>\n			</td>\n			<td>\n			<p>Unlimited</p>\n			</td>\n			<td>\n			<p>$200 per week</p>\n			</td>\n		</tr><tr><td>Make It Happen</td>\n			<td> </td>\n			<td>Unlimited</td>\n			<td> </td>\n		</tr><tr><td>Brand It</td>\n			<td> </td>\n			<td>Unlimited</td>\n			<td> </td>\n		</tr><tr><td>Plan It and Shout It</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td>Plan It, Market I and Now Sell It</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr></tbody></table>', '2015-11-23 22:18:19', '2015-11-23 22:18:24', 1, 1, 55, 2),
(42, '<table summary="A table with space to enter details of plant and equipment purchases under the headings: Equipment (e.g Personal Computer), purchase date (e.g. 20/03/2010), purchase price (e.g $2100) and running cost (e.g $100 a month)."><thead><tr><th>\n			<p>Equipment</p>\n			</th>\n			<th>\n			<p>Purchase Date</p>\n			</th>\n			<th>\n			<p>Purchase Price</p>\n			</th>\n			<th>\n			<p>Running Cost</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Consumer Website</p>\n			</td>\n			<td>\n			<p>2015</p>\n			</td>\n			<td>\n			<p>$59.00</p>\n			</td>\n			<td>\n			<p>$14.99</p>\n			</td>\n		</tr><tr><td>Portal</td>\n			<td>2015</td>\n			<td>$8,350</td>\n			<td>TBA</td>\n		</tr></tbody></table>\n\n<p> </p>\n\n<p> </p>\n', '2015-11-23 22:28:35', '2015-11-23 22:32:05', 1, 1, 53, 2);
INSERT INTO `plan_contents` (`id`, `content`, `created_at`, `updated_at`, `status`, `user_id`, `cat_id`, `plan_id`) VALUES
(43, '<table><thead><tr><th>\n			<p>Goals</p>\n			</th>\n			<th>\n			<p>What steps will you take to achieve your goals/objectives</p>\n			</th>\n			<th>\n			<p>When do you expect to achieve your set goals/objectives?</p>\n			</th>\n			<th>\n			<p>Person responsible</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Help grow businesses. Empower them to learn more around our area of expertise</p>\n			</td>\n			<td>\n			<p>Tester</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>Bring in project managers, community manager, digital marketer and copywriter</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr></tbody></table>', '2015-11-23 22:39:52', '2015-11-24 20:47:35', 1, 1, 33, 2),
(44, '<p><strong>Position in market</strong></p>\n\n<p>Tailored and structured methods in areas of media | business | partnerships. It’s taking a look at business and marketing plan, providing business and entertainment solutions | advice that are platform agnostic.</p>\n\n<p> </p>\n\n<p><strong>Business message:</strong></p>\n\n<ul><li>GetJackD! Method. Educating and empowering businesses with easy to use branding and marketing tools </li>\n</ul>\n\n<p> </p>\n\n<p><strong>Business slogan:</strong></p>\n\n<ul><li>Your business. Your market. Your way</li>\n</ul>', '2015-11-23 22:45:29', '2015-11-23 22:45:32', 1, 1, 34, 2),
(45, '<p><strong>Key customers:</strong></p>\n\n<ul><li>Start Ups    \n	<ul><li>No Idea: Looking for guidance about how to start a business.</li>\n	</ul></li>\n	<li>Small Sole Operators: less than 5 employees\n	<ul><li>In business for less than 2 years. Have no marketing support</li>\n	</ul></li>\n	<li>Small Sole Operators: 5-10 employees\n	<ul><li>In business for more than 2 years. No marketing support</li>\n	</ul></li>\n</ul>\n\n<p><strong>Value to customers</strong>: Ability to learn about marketing | media as they build out their business, ability to tap into a network of experts to help set up or grow their business. They can move through methods at their own pace and when they have time. There is also the ability to work on their own with templates a guidance/case studies.</p>\n\n<p><strong>Customer management:</strong></p>\n\n<p>Portal</p>\n\n<ul><li>Keeps track of the methods</li>\n	<li>Client integration with the methods/links and other content</li>\n	<li>Ensure customers stay within the portal and engage with the content</li>\n	<li>Encourage users to share GJM written content amongst their peers</li>\n	<li>Upsell across method via EDM comms and digital marketing (new purchases)</li>\n	<li>Follow ups and thank you’s</li>\n</ul>\n\n<p> </p>\n\n<p>Exceptional Service | Delivery</p>\n\n<ul><li>Deliver on the needs of the market</li>\n	<li>Engage in a fun and creative manner. Talking in real terms to drive response</li>\n	<li>Ensure measurability and accountability on methods. Listen to feedback | revise</li>\n	<li>Add new packages as the demand increases or we see gaps in the market</li>\n</ul>\n\n<p> </p>\n\n<p>Work Referral</p>\n\n<ul><li>Refer larger projects to GetJackD! Media and quote when needed</li>\n	<li>Work with our partners and refer services for businesses that can afford them</li>\n	<li>Ensure referred service fees (20%) are added</li>\n</ul>', '2015-11-23 22:47:04', '2015-11-23 22:47:08', 1, 1, 35, 2),
(46, '<table><tbody><tr><td>\n			<p><strong>Name</strong></p>\n			</td>\n			<td>\n			<p><strong>Date Established</strong></p>\n			</td>\n			<td>\n			<p><strong>Business Size </strong></p>\n			</td>\n			<td>\n			<p><strong>Estimated Turnover/Market Share</strong></p>\n			</td>\n			<td>\n			<p><strong>Value to Customer</strong></p>\n			</td>\n			<td>\n			<p><strong>Business Strengths</strong></p>\n			</td>\n			<td>\n			<p><strong>Business Weaknesses</strong></p>\n			</td>\n		</tr><tr><td>\n			<p>Basic Bananas</p>\n			</td>\n			<td>\n			<p>2000s</p>\n			</td>\n			<td>\n			<p>small</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<p>n/a</p>\n			</td>\n			<td>\n			<p>Cheap group workshops $27</p>\n\n			<p> </p>\n\n			<p>Face to face programs (based on a retainer for 12 months)</p>\n\n			<p>$1200-1800 per month</p>\n\n			<p> </p>\n\n			<p>VIP consulting days $15,000 per day, $9,000 half a day</p>\n\n			<p> </p>\n\n			<p>Online videos | courses</p>\n\n			<p>$147-$2999</p>\n			</td>\n			<td>\n			<p>Cheap</p>\n\n			<p> </p>\n\n			<p>Varying levels for businesses to tap into</p>\n\n			<p> </p>\n\n			<p>Suck people in with the workshop to sell them on a bigger product</p>\n\n			<p> </p>\n\n			<p>Is established in the market</p>\n\n			<p> </p>\n\n			<p>Very targeted at small businesses</p>\n			</td>\n			<td>\n			<p>Group workshops</p>\n\n			<p> </p>\n\n			<p>Step through a weekly program</p>\n\n			<p> </p>\n\n			<p>You need to attend on specific dates</p>\n\n			<p> </p>\n\n			<p>To be apart of the face to face, businesses need to apply and be accepted</p>\n			</td>\n		</tr><tr><td>The Middle Finger Project</td>\n			<td>2014</td>\n			<td>small</td>\n			<td> </td>\n			<td>\n			<p>More of a copyrighting | branding skew</p>\n\n			<p> </p>\n\n			<p>Packages range from $39- $1999</p>\n\n			<p> </p>\n			You can also buy one on one time charged out on a minute basis</td>\n			<td>\n			<p>Completely online</p>\n\n			<p> </p>\n			People can do it at home</td>\n			<td>\n			<p>It’s a very different communication strategy may offend people</p>\n\n			<p> </p>\n\n			<p>Step through a weekly program</p>\n\n			<p> </p>\n\n			<p>Limited to certain times of the year</p>\n\n			<p> </p>\n			US based</td>\n		</tr><tr><td>Collabasaurs</td>\n			<td>2015</td>\n			<td>small</td>\n			<td> </td>\n			<td>\n			<p>Connects businesses via a match making app, focused on finding brand partnerships</p>\n\n			<p> </p>\n			Opens up leads and ideas amongst businesses</td>\n			<td>\n			<p>Partners business with likeminded businesses for free</p>\n\n			<p> </p>\n			You can select who you want to partner with</td>\n			<td>\n			<p>Subscription model</p>\n\n			<p> </p>\n\n			<p>You need business to say yes to working with you</p>\n\n			<p> </p>\n\n			<p>Looks for more product or service sharing activity not so much a cash transaction</p>\n\n			<p> </p>\n\n			<p>You may have to give up your product or time (which may cost you money) to be apart of the program</p>\n			</td>\n		</tr><tr><td>Owners Collective</td>\n			<td>2010s</td>\n			<td>small</td>\n			<td> </td>\n			<td>\n			<p>Peer academy helping businesses across various areas of marketing and coaching</p>\n\n			<p> </p>\n			Engages owners of other small businesses to take components of the 3-month mastermind program. So not a consistent person over the period</td>\n			<td>\n			<p>Group of experts brought together to assist businesses</p>\n\n			<p> </p>\n\n			<p>Targeted at the professional market</p>\n\n			<p> </p>\n\n			<p>Corporate offer. Different to small business</p>\n			</td>\n			<td>\n			<p>Step through a weekly program</p>\n\n			<p> </p>\n\n			<p>3 month program</p>\n\n			<p> </p>\n\n			<p>Only has a certain number of programs a year, with a limited in take</p>\n\n			<p> </p>\n\n			<p>No consistency on who you work with</p>\n			</td>\n		</tr><tr><td>Sprkd</td>\n			<td>2000s</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td>US based</td>\n		</tr><tr><td>Marketing Agencies</td>\n			<td>Various</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td>Business Coaches</td>\n			<td>Various</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td>Indusrty Courses </td>\n			<td>Various</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td>Individual Consultations</td>\n			<td>Various</td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td><a href="http://www.macinnismarketing.com.au/our-services/small-business-marketing-planning-package/">http://www.macinnismarketing.com.au/our-services/small-business-marketing-planning-package/</a></td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td>\n			<p><a href="http://www.marketingeye.com.au/buy-now.html">http://www.marketingeye.com.au/buy-now.html</a></p>\n			<a href="http://motherlode.com.au/small-business-marketing-packages-brisbane/">http://motherlode.com.au/small-business-marketing-packages-brisbane/</a></td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td><a href="http://www.brightowlmarketing.com.au/">http://www.brightowlmarketing.com.au/</a></td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr><tr><td><a href="http://www.talkaboutcreative.com.au/marketing-branding/">http://www.talkaboutcreative.com.au/marketing-branding/</a></td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n			<td> </td>\n		</tr></tbody></table>', '2015-11-23 22:51:11', '2015-11-23 22:56:02', 1, 1, 46, 2),
(47, '<ul><li>Niche in market (media | partnership focused) with no bias on channels</li>\n	<li>Different priced packages to entice people (we’re affordable. You don’t have to commit)</li>\n	<li>We don’t want retainers, we want to empower so businesses are educated to run their own race, forever</li>\n	<li>Get information when they want. Access anytime. All content will remain up to date and current</li>\n	<li>One on One contact and guidance when required (most competitors offer groups | workshops or huge fees)</li>\n	<li>An exclusive portal accessed anywhere, anytime that’s personalised to you and your business</li>\n	<li>Ability to move through packages to a full solution @ various sizes | campaigns</li>\n	<li>Wide network of contacts and partners to tap into if needed</li>\n	<li>Real world insights into media and marketing, through us and our industry partners</li>\n</ul>', '2015-11-23 22:56:39', '2015-11-23 22:56:43', 1, 1, 47, 2),
(48, '<table><thead><tr><th>\n			<p><strong>STRENGTHS</strong></p>\n			</th>\n			<th>\n			<p><strong>WEAKNESSES</strong></p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<ul><li><strong>Niche Product Offering</strong>\n				<ul><li>Clearly communicate the benefits of the business via methods</li>\n					<li>Provide a service that businesses can execute and understand themselves rather than having someone ‘do it’ for them</li>\n				</ul></li>\n				<li><strong>Flexibility and Delivery</strong>\n				<ul><li>The flexibility of our offering and service delivery is key. Highlight the flexibility of packages and how they all roll into each other and they are personalised to your business to implement ASAP</li>\n				</ul></li>\n				<li><strong>Cost | Price</strong>\n				<ul><li>Low cost for a business when compared to full time staff or Agency services</li>\n					<li>Offer multiple methods depending on a businesses budget – tailoring to all</li>\n					<li>Special introductory offers for new packages</li>\n				</ul></li>\n				<li><strong>Passive</strong>\n				<ul><li>The aim is to make this as passive as possible to provide a personal service without the need for all of our time</li>\n					<li>Bring in contractors as necessary minimising the need for our time</li>\n				</ul></li>\n				<li><strong>Portal</strong>\n				<ul><li>Offers an area to house the methods as well as offer additional resources to get underway</li>\n					<li>Everything housed in one area, minimising confusion or need for additional services/platforms</li>\n					<li>Personalised experience for users without any bugs</li>\n					<li>Updated content is loaded as media platforms change</li>\n					<li>Safety of user information and ability to have all content for 3 years</li>\n				</ul></li>\n			</ul></td>\n			<td>\n			<ul><li><strong>Niche Product Offering</strong>\n				<ul><li>Might be too hard for traditionalists to understand the services on offer or use the portal</li>\n				</ul></li>\n				<li><strong>Cost to Build</strong>\n				<ul><li>Its been an expensive process to build the website and the portal to house the content, along with creating all of the packages. There is a bit of work for us to do to recoup costs</li>\n				</ul></li>\n				<li><strong>Service Based</strong>\n				<ul><li>We need to try and make the services as passive as possible. There are still man hours involved in the offer</li>\n					<li>We want people to believe its personalised, even through we know its automated and package driven</li>\n				</ul></li>\n				<li><strong>Need for Project Managers</strong>\n				<ul><li>Need to bring Project Managers to oversee campaigns for clients. Higher staff costs</li>\n					<li>Need to work with a digital marketer, community manager, designers and IT people to deliver the service</li>\n				</ul></li>\n				<li><strong>Time to Build</strong>\n				<ul><li>Has take a significant amount time to create the methods, content to market the methods, the structure of the platform and liaison with all parties</li>\n				</ul></li>\n				<li><strong>Updated Content</strong>\n				<ul><li>This is very content heavy and needs constant management and updating to stay relevant and offer information that’s on trend</li>\n				</ul></li>\n				<li><strong>Cost To Market</strong>\n				<ul><li>Expensive to market the packages to each of the niche markets across desired platforms to drive sale</li>\n				</ul></li>\n				<li><strong>Need to Update the Tech</strong>\n				<ul><li>We’ll have to stay on track with new technology for the platform and any bugs with people being able to access/use the content</li>\n				</ul></li>\n				<li><strong>High Maintenance Small Businesses</strong></li>\n			</ul><p>Knowing how small businesses operate, they may end up being too high maintenance and time consuming</p>\n			</td>\n		</tr><tr><td>\n			<p><strong>OPPORTUNITIES</strong></p>\n			</td>\n			<td>\n			<p><strong>THREATS</strong></p>\n			</td>\n		</tr><tr><td>\n			<ul><li><strong>Niche Product Offering</strong>\n				<ul><li>Really highlighting the services and business potential for new and existing clients</li>\n					<li>There is nothing like this in market at the moment, highlighting media platforms and a dedicated personal portal to guide success</li>\n				</ul></li>\n				<li><strong>New Business – Unique Offering</strong>\n				<ul><li>A new offer in market, that offers clients another fresh opportunity that may have been lacking in market</li>\n					<li>No need for expensive agencies or consultants. Learn and do it at your own pace</li>\n					<li>We can also add new methods to the offer to ensure were reaching new markets or growing businesses | moving them into new methods</li>\n				</ul></li>\n				<li><strong>Leads for Bigger Business</strong>\n				<ul><li>Move clients to GetJackD! Media clients</li>\n					<li>Potential to adapt methods to large businesses needing marketing guidance or to go back to the ‘basics’</li>\n				</ul></li>\n				<li><strong>Passive Income</strong>\n				<ul><li>Ways to have money come in without the need to actively be involved in every campaign</li>\n				</ul></li>\n				<li><strong>Grow the Team</strong>\n				<ul><li>Ability to grow a team of people for use across all areas of the GetJackD! Media/Method Businesses</li>\n				</ul></li>\n				<li><strong>Product based delivery system</strong>\n				<ul><li>We’ve created a platform that allows for the build of other products ie Amy’s Book etc</li>\n				</ul></li>\n				<li><strong>Global </strong>\n				<ul><li>Opportunities to work globally</li>\n				</ul></li>\n			</ul></td>\n			<td>\n			<ul><li><strong>Competitors</strong>\n				<ul><li>Competitors in big | small existing companies that have a reputation and case studies of existing work</li>\n					<li>Agencies competing for the same business</li>\n				</ul></li>\n				<li><strong>Companies – Hiring Internal Staff</strong>\n				<ul><li>Hiring internal staff to facilitate marketing and media plans</li>\n				</ul></li>\n			</ul><p> </p>\n\n			<ul><li>New Businesses entering the market</li>\n			</ul><p> </p>\n\n			<ul><li>IT &amp; User Issues with the platform</li>\n			</ul><p> </p>\n\n			<ul><li>Cashflow &amp; Repeat Business</li>\n			</ul><p> </p>\n\n			<ul><li>Lack of a big team to tap into, relying on external partners to deliver elements of service or to project manage campaigns</li>\n			</ul><p> </p>\n\n			<ul><li>Issues with IP protection and security of user data and information</li>\n			</ul><p> </p>\n\n			<ul><li>No one buys a package</li>\n			</ul><p> </p>\n			</td>\n		</tr></tbody></table>', '2015-11-23 22:58:23', '2015-11-24 19:51:17', 1, 1, 36, 2),
(49, '<table><thead><tr><th>\n			<p>Marketing and Media Activity</p>\n			</th>\n			<th>\n			<p>Target Product/Service</p>\n			</th>\n			<th>\n			<p>Reason for Chosen Channel</p>\n			</th>\n			<th>\n			<p>Measurement of Success</p>\n			</th>\n			<th>\n			<p>Total Budget Allocation ($)</p>\n			</th>\n			<th>\n			<p>When</p>\n			</th>\n			<th>\n			<p>Person Responsible</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Facebook</p>\n			</td>\n			<td>\n			<p>All</p>\n			</td>\n			<td>\n			<p>launch, brand awareness and sales of packages</p>\n\n			<p> </p>\n\n			<p>Blogs, wins, case studies, profiles, events and any other relevant information on the business and industry</p>\n			</td>\n			<td>\n			<p>TBA</p>\n			</td>\n			<td>\n			<p>$TBA</p>\n			</td>\n			<td>\n			<p>From Jan 2016</p>\n			</td>\n			<td>\n			<p>Kirsty</p>\n			</td>\n		</tr><tr><td>\n			<p>Google Adwords</p>\n			</td>\n			<td>All</td>\n			<td>\n			<p>launch, brand awareness and sales of packages</p>\n\n			<p> </p>\n\n			<p>Search/optimisation. Website copy has been written to reflect this</p>\n			</td>\n			<td>TBA</td>\n			<td>TBA</td>\n			<td>From Jan 2016</td>\n			<td>\n			<p>Kirsty</p>\n			</td>\n		</tr><tr><td>LinkedIn</td>\n			<td>\n			<p>All</p>\n			</td>\n			<td>Blogs, wins, case studies, profiles, events and any other relevant information on the business and industry</td>\n			<td>TBA</td>\n			<td>TBA</td>\n			<td>From Jan 2016</td>\n			<td>Kirsty</td>\n		</tr><tr><td>\n			<p>EDM | Database</p>\n			</td>\n			<td>All</td>\n			<td>Content platform to create blogs/EDM comms/automation for messages.  </td>\n			<td>TBA</td>\n			<td>TBA</td>\n			<td>From Jan 2016</td>\n			<td>Kirsty</td>\n		</tr><tr><td>Twitter</td>\n			<td>All</td>\n			<td>Content Based. Videos/Photos/Links/Profiles</td>\n			<td>TBA</td>\n			<td>TBA</td>\n			<td>From Jan 2016</td>\n			<td>Kirsty</td>\n		</tr><tr><td>\n			<p>Industry Publications</p>\n			</td>\n			<td>All</td>\n			<td>\n			<p>Try and be PR’ed in appropriate industry press. Ie B&amp;T/AdNews</p>\n			</td>\n			<td>TBA</td>\n			<td>TBA</td>\n			<td>From Jan 2016</td>\n			<td>Kirsty</td>\n		</tr></tbody></table>\n\n<p> </p>\n\n<p><strong>Business objectives:</strong></p>\n\n<ul><li>Be as passive as possible. Offer a service that doesn’t require a lot of man hours</li>\n	<li>Help grow businesses. Empower them to learn more around our area of expertise</li>\n	<li>Work internationally</li>\n	<li>Bring in project managers, community manager, digital marketer and copywriter</li>\n</ul>\n\n<p><strong>Marketing and media strategy: </strong>The following methods will be used to market our services:</p>\n\n<ul><li>Communication via:\n	<ul><li>Website</li>\n		<li>Blogs</li>\n		<li>EDM</li>\n		<li>Client testimonials and case studies</li>\n	</ul></li>\n	<li>Content Plan. Social Media Networks (Facebook and LinkedIn primarily)</li>\n	<li>Digital Marketing Plan. Facebook, LinkedIn and Google Adwords</li>\n	<li>PR/Publicity via industry publications and small networking functions | groups</li>\n</ul>\n\n<p> </p>\n\n<p><strong>Growth potential:</strong></p>\n\n<ul><li>Move clients through a sales funnel across all methods</li>\n	<li>Start in Australia and move to international markets</li>\n	<li>Introduce more project managers to service more businesses</li>\n	<li>Move larger clients to GetJackD! Media on a project basis</li>\n	<li>Add new methods to the offering to expand services</li>\n	<li>Offer a consultancy model to tap into the need for clients to have one on one time</li>\n</ul>\n\n<p> </p>\n', '2015-11-23 23:07:11', '2015-11-23 23:08:34', 1, 1, 37, 2),
(50, '<p><strong>Research &amp; development (R&amp;D)/innovation activities: </strong>A portal to house all of the clients and content created. The portal becomes a hub for users allowing them to not only receive information but to communicate with GJMethod team members and arrange one on one session.  </p>\n\n<p><strong>Intellectual property strategy: </strong>Have applied for trademark in Australia. Need to look at international trademarks/IP. Need to trademark the process/portal/name. All contributors have signed an agreement so GJMethod own the IP around the platform and content created.</p>\n', '2015-11-23 23:11:23', '2015-11-23 23:11:26', 1, 1, 15, 2),
(51, '<p>GetJackD! Method is a leading online communications portal that; educates, supports and empowers start ups and small businesses to run their own race across media and marketing channels.  </p>\n\n<p> </p>\n\n<p>The Method provides the tools and advice to help businesses grow.</p>\n', '2015-11-23 23:40:28', '2015-11-23 23:40:28', 0, 1, 61, 4),
(52, '<p>GetJackD! Method empowers businesses to ‘run their own race’ across: media and marketing plans, business plans, event sponsorships, project management &amp; business partnerships.</p>\n\n<p> </p>\n\n<p>The Method is a dedicated self managed online portal, arming start ups and small businesses with the tools to get started or grow. The portal houses all tiered packages and additional advice and links, enabling owners to ''hit the ground running'' and build their own 360<strong>° </strong>plan based on their business marketing objectives. </p>\n', '2015-11-23 23:48:17', '2015-11-23 23:48:20', 1, 1, 62, 4),
(53, '<ul><li>Start Ups    \n	<ul><li>No Idea: Looking for guidance about how to start a business.</li>\n	</ul></li>\n	<li>Small Sole Operators: less than 5 employees\n	<ul><li>In business for less than 2 years. Have no marketing support</li>\n	</ul></li>\n	<li>Small Sole Operators: 5-10 employees\n	<ul><li>In business for more than 2 years. No marketing support</li>\n	</ul></li>\n</ul>\n\n<p><strong>Value to customers</strong>: Ability to learn about marketing | media as they build out their business, ability to tap into a network of experts to help set up or grow their business. They can move through methods at their own pace and when they have time. There is also the ability to work on their own with templates a guidance/case studies.</p>\n', '2015-11-23 23:49:24', '2015-11-23 23:49:27', 1, 1, 71, 4),
(54, '<table><thead><tr><th>\n			<p>Competitor</p>\n			</th>\n			<th>\n			<p>Size</p>\n			</th>\n			<th>\n			<p>Market share (%)</p>\n			</th>\n			<th>\n			<p>Value to customers</p>\n			</th>\n			<th>\n			<p>Strengths</p>\n			</th>\n			<th>\n			<p>Weaknesses</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p>Basic Bananas</p>\n			</td>\n			<td>\n			<p>Small</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>Cheap group workshops $27</p>\n\n			<p> </p>\n\n			<p>Face to face programs (based on a retainer for 12 months)</p>\n\n			<p>$1200-1800 per month</p>\n\n			<p> </p>\n\n			<p>VIP consulting days $15,000 per day, $9,000 half a day</p>\n\n			<p> </p>\n\n			<p>Online videos | courses</p>\n\n			<p>$147-$2999</p>\n			</td>\n			<td>\n			<p>Cheap</p>\n\n			<p> </p>\n\n			<p>Varying levels for businesses to tap into</p>\n\n			<p> </p>\n\n			<p>Suck people in with the workshop to sell them on a bigger product</p>\n\n			<p> </p>\n\n			<p>Is established in the market</p>\n\n			<p> </p>\n\n			<p>Very targeted at small businesses</p>\n			</td>\n			<td>\n			<p>Group workshops</p>\n\n			<p> </p>\n\n			<p>Step through a weekly program</p>\n\n			<p> </p>\n\n			<p>You need to attend on specific dates</p>\n\n			<p> </p>\n\n			<p>To be apart of the face to face, businesses need to apply and be accepted</p>\n\n			<p> </p>\n\n			<p> </p>\n\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>The Middle Finger Project</p>\n			</td>\n			<td>\n			<p>Small</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>More of a copyrighting | branding skew</p>\n\n			<p> </p>\n\n			<p>Packages range from $39- $1999</p>\n\n			<p> </p>\n\n			<p>You can also buy one on one time charged out on a minute basis</p>\n			</td>\n			<td>\n			<p>Completely online</p>\n\n			<p> </p>\n\n			<p>People can do it at home</p>\n			</td>\n			<td>\n			<p>It’s a very different communication strategy may offend people</p>\n\n			<p> </p>\n\n			<p>Step through a weekly program</p>\n\n			<p> </p>\n\n			<p>Limited to certain times of the year</p>\n\n			<p> </p>\n\n			<p>US based</p>\n			</td>\n		</tr><tr><td>\n			<p>Collabasaurs</p>\n			</td>\n			<td>\n			<p>small</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>Connects businesses via a match making app, focused on finding brand partnerships</p>\n\n			<p> </p>\n\n			<p>Opens up leads and ideas amongst businesses</p>\n			</td>\n			<td>\n			<p>Partners business with likeminded businesses for free</p>\n\n			<p> </p>\n\n			<p>You can select who you want to partner with</p>\n			</td>\n			<td>\n			<p>Subscription model</p>\n\n			<p> </p>\n\n			<p>You need business to say yes to working with you</p>\n\n			<p> </p>\n\n			<p>Looks for more product or service sharing activity not so much a cash transaction</p>\n\n			<p> </p>\n\n			<p>You may have to give up your product or time (which may cost you money) to be apart of the program</p>\n\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Owners Collective</p>\n			</td>\n			<td>\n			<p>small</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>Peer academy helping businesses across various areas of marketing and coaching</p>\n\n			<p> </p>\n\n			<p>Engages owners of other small businesses to take components of the 3-month mastermind program. So not a consistent person over the period</p>\n			</td>\n			<td>\n			<p>Group of experts brought together to assist businesses</p>\n\n			<p> </p>\n\n			<p>Targeted at the professional market</p>\n\n			<p> </p>\n\n			<p>Corporate offer. Different to small business</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<p>Step through a weekly program</p>\n\n			<p> </p>\n\n			<p>3 month program</p>\n\n			<p> </p>\n\n			<p>Only has a certain number of programs a year, with a limited in take</p>\n\n			<p> </p>\n\n			<p>No consistency on who you work with</p>\n\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Sprkd</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p>US based</p>\n			</td>\n		</tr><tr><td>\n			<p>Marketing Agencies</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p><a href="http://www.macinnismarketing.com.au/our-services/small-business-marketing-planning-package/">http://www.macinnismarketing.com.au/our-services/small-business-marketing-planning-package/</a></p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p><a href="http://www.brightowlmarketing.com.au/">http://www.brightowlmarketing.com.au/</a></p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p><a href="http://www.marketsmartly.com/service/strategy-planning/">http://www.marketsmartly.com/service/strategy-planning/</a></p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p><a href="http://www.talkaboutcreative.com.au/marketing-branding/">http://www.talkaboutcreative.com.au/marketing-branding/</a></p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Business Coaches</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Industry Courses i.e. Business School</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr><tr><td>\n			<p>Individual Consultants</p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n		</tr></tbody></table>', '2015-11-23 23:49:58', '2015-11-23 23:50:01', 1, 1, 73, 4),
(55, '<table><tbody><tr><td>\n			<p><strong>Strengths</strong></p>\n			</td>\n			<td>\n			<p><strong>Weaknesses</strong></p>\n			</td>\n			<td>\n			<p><strong>Opportunities</strong></p>\n			</td>\n			<td>\n			<p><strong>Threats</strong></p>\n			</td>\n		</tr><tr><td>\n			<ul><li><strong>Niche Product Offering</strong>\n				<ul><li>Clearly communicate the benefits of the business via methods</li>\n					<li>Provide a service that businesses can execute and understand themselves rather than having someone ‘do it’ for them</li>\n				</ul></li>\n				<li><strong>Flexibility and Delivery</strong>\n				<ul><li>The flexibility of our offering and service delivery is key. Highlight the flexibility of packages and how they all roll into each other and they are personalised to your business to implement ASAP</li>\n				</ul></li>\n				<li><strong>Cost | Price</strong>\n				<ul><li>Low cost for a business when compared to full time staff or Agency services</li>\n					<li>Offer multiple methods depending on a businesses budget – tailoring to all</li>\n					<li>Special introductory offers for new packages</li>\n				</ul></li>\n				<li><strong>Passive</strong>\n				<ul><li>The aim is to make this as passive as possible to provide a personal service without the need for all of our time</li>\n					<li>Bring in contractors as necessary minimising the need for our time</li>\n				</ul></li>\n				<li><strong>Portal</strong>\n				<ul><li>Offers an area to house the methods as well as offer additional resources to get underway</li>\n					<li>Everything housed in one area, minimising confusion or need for additional services/platforms</li>\n					<li>Personalised experience for users without any bugs</li>\n					<li>Updated content is loaded as media platforms change</li>\n					<li>Safety of user information and ability to have all content for 3 years</li>\n				</ul></li>\n			</ul><p> </p>\n			</td>\n			<td>\n			<ul><li><strong>Niche Product Offering</strong>\n				<ul><li>Might be too hard for traditionalists to understand the services on offer or use the portal</li>\n				</ul></li>\n				<li><strong>Cost to Build</strong>\n				<ul><li>Its been an expensive process to build the website and the portal to house the content, along with creating all of the packages. There is a bit of work for us to do to recoup costs</li>\n				</ul></li>\n				<li><strong>Service Based</strong>\n				<ul><li>We need to try and make the services as passive as possible. There are still man hours involved in the offer</li>\n					<li>We want people to believe its personalised, even through we know its automated and package driven</li>\n				</ul></li>\n				<li><strong>Need for Project Managers</strong>\n				<ul><li>Need to bring Project Managers to oversee campaigns for clients. Higher staff costs</li>\n					<li>Need to work with a digital marketer, community manager, designers and IT people to deliver the service</li>\n				</ul></li>\n				<li><strong>Time to Build</strong>\n				<ul><li>Has take a significant amount time to create the methods, content to market the methods, the structure of the platform and liaison with all parties</li>\n				</ul></li>\n				<li><strong>Updated Content</strong>\n				<ul><li>This is very content heavy and needs constant management and updating to stay relevant and offer information that’s on trend</li>\n				</ul></li>\n				<li><strong>Cost To Market</strong>\n				<ul><li>Expensive to market the packages to each of the niche markets across desired platforms to drive sale</li>\n				</ul></li>\n				<li><strong>Need to Update the Tech</strong>\n				<ul><li>We’ll have to stay on track with new technology for the platform and any bugs with people being able to access/use the content</li>\n				</ul></li>\n				<li><strong>High Maintenance Small Businesses</strong></li>\n			</ul><p>Knowing how small businesses operate, they may end up being too high maintenance and time consuming</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<ul><li><strong>Niche Product Offering</strong>\n				<ul><li>Really highlighting the services and business potential for new and existing clients</li>\n					<li>There is nothing like this in market at the moment, highlighting media platforms and a dedicated personal portal to guide success</li>\n				</ul></li>\n				<li><strong>New Business – Unique Offering</strong>\n				<ul><li>A new offer in market, that offers clients another fresh opportunity that may have been lacking in market</li>\n					<li>No need for expensive agencies or consultants. Learn and do it at your own pace</li>\n					<li>We can also add new methods to the offer to ensure were reaching new markets or growing businesses | moving them into new methods</li>\n				</ul></li>\n				<li><strong>Leads for Bigger Business</strong>\n				<ul><li>Move clients to GetJackD! Media clients</li>\n					<li>Potential to adapt methods to large businesses needing marketing guidance or to go back to the ‘basics’</li>\n				</ul></li>\n				<li><strong>Passive Income</strong>\n				<ul><li>Ways to have money come in without the need to actively be involved in every campaign</li>\n				</ul></li>\n				<li><strong>Grow the Team</strong>\n				<ul><li>Ability to grow a team of people for use across all areas of the GetJackD! Media/Method Businesses</li>\n				</ul></li>\n				<li><strong>Product based delivery system</strong>\n				<ul><li>We’ve created a platform that allows for the build of other products ie Amy’s Book etc</li>\n				</ul></li>\n				<li><strong>Global </strong>\n				<ul><li>Opportunities to work globally</li>\n				</ul></li>\n			</ul><p> </p>\n			</td>\n			<td>\n			<ul><li><strong>Competitors</strong>\n				<ul><li>Competitors in big | small existing companies that have a reputation and case studies of existing work</li>\n					<li>Agencies competing for the same business</li>\n				</ul></li>\n				<li><strong>Companies – Hiring Internal Staff</strong>\n				<ul><li>Hiring internal staff to facilitate marketing and media plans</li>\n				</ul></li>\n			</ul><p> </p>\n\n			<ul><li>New Businesses entering the market</li>\n			</ul><p> </p>\n\n			<ul><li>IT &amp; User Issues with the platform</li>\n			</ul><p> </p>\n\n			<ul><li>Cashflow &amp; Repeat Business</li>\n			</ul><p> </p>\n\n			<ul><li>Lack of a big team to tap into, relying on external partners to deliver elements of service or to project manage campaigns</li>\n			</ul><p> </p>\n\n			<ul><li>Issues with IP protection and security of user data and information</li>\n			</ul><p> </p>\n\n			<ul><li>No one buys a package</li>\n			</ul><p> </p>\n			</td>\n		</tr></tbody></table>', '2015-11-23 23:51:17', '2015-11-23 23:51:20', 1, 1, 74, 4),
(56, '<p> </p>\n\n<ul><li>Niche in market (media | partnership focused) with no bias on channels</li>\n	<li>Different priced packages to entice people (we’re affordable. You don’t have to commit)</li>\n	<li>We don’t want retainers, we want to empower so businesses are educated to run their own race, forever</li>\n	<li>Get information when they want. Access anytime. All content will remain up to date and current</li>\n	<li>One on One contact and guidance when required (most competitors offer groups | workshops or huge fees)</li>\n	<li>An exclusive portal accessed anywhere, anytime that’s personalised to you and your business</li>\n	<li>Ability to move through packages to a full solution @ various sizes | campaigns</li>\n	<li>Wide network of contacts and partners to tap into if needed</li>\n	<li>Real world insights into media and marketing, through us and our industry partners</li>\n</ul>', '2015-11-23 23:51:50', '2015-11-23 23:51:54', 1, 1, 75, 4),
(57, '<p><strong>Business message: </strong>GetJackD! Method. Educating and empowering businesses with easy to use branding and marketing tools </p>\n\n<p><strong>Business slogan: </strong>Your Business. Your Market. Your Way</p>\n', '2015-11-23 23:52:45', '2015-11-23 23:52:48', 1, 1, 76, 4),
(58, '<table><thead><tr><th>\n			<p>Channel type</p>\n			</th>\n			<th>\n			<p>Products/services</p>\n			</th>\n			<th>\n			<p>Percentage of sales (%)</p>\n			</th>\n			<th>\n			<p>Distribution strategy</p>\n			</th>\n		</tr></thead><tbody><tr><td>\n			<p> </p>\n\n			<p>E-commerce</p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p><strong>Make It Happen - $299</strong></p>\n\n			<p>Access to the portal</p>\n\n			<p> </p>\n\n			<p>Business Plan (a full editable and personalised plan with tips and tricks)</p>\n\n			<p> </p>\n\n			<p>Access to the portal for 2 years</p>\n\n			<p>Access to helpful links/bios/blogs</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p>Ease of access</p>\n\n			<p> </p>\n\n			<p>Accessed anytime and anywhere</p>\n\n			<p> </p>\n\n			<p>Live for either 1-3 years</p>\n			</td>\n		</tr><tr><td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p><strong>Brand It - $99</strong></p>\n\n			<p>Access to the portal</p>\n\n			<p> </p>\n\n			<p>Information provided on branding your business. Tips and Tricks</p>\n\n			<p> </p>\n\n			<p>Access to the portal for 2 years</p>\n\n			<p>Additional access to helpful links/bios/blogs</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p>Ease of access</p>\n\n			<p> </p>\n\n			<p>Accessed anytime and anywhere</p>\n\n			<p> </p>\n\n			<p>Live for either 1-3 years</p>\n			</td>\n		</tr><tr><td>\n			<p> </p>\n			</td>\n			<td>\n			<p><strong>Plan it and Shout it - $499</strong></p>\n\n			<p>Access to the portal</p>\n\n			<p> </p>\n\n			<p>Media platform documents, case studies, dos and don’ts</p>\n\n			<p> </p>\n\n			<p>Access to all content for 2 years. Content will be refreshed over this time, to ensure information is relevant especially in the digital | social space</p>\n\n			<p>Access to helpful links/bios/blogs</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p>Ease of access</p>\n\n			<p> </p>\n\n			<p>Accessed anytime and anywhere</p>\n\n			<p> </p>\n\n			<p>Live for either 1-3 years</p>\n			</td>\n		</tr><tr><td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p><strong>Plan It, Market It, Sell It</strong></p>\n\n			<p><strong>$3,499</strong></p>\n\n			<p>Access to the portal</p>\n\n			<p>Briefing form and access to booking a session with the GJMethod team (2hrs)</p>\n\n			<p> </p>\n\n			<p>Media platform documents, case studies, dos and don’ts</p>\n\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n			</td>\n			<td>\n			<p> </p>\n\n			<p>Ease of access</p>\n\n			<p> </p>\n\n			<p>Accessed anytime and anywhere</p>\n\n			<p> </p>\n\n			<p>Live for either 1-3 years</p>\n			</td>\n		</tr></tbody></table>', '2015-11-23 23:53:30', '2015-11-23 23:53:33', 1, 1, 64, 4),
(59, '<p>asdf</p>\n', '2015-11-24 14:32:09', '2015-11-24 14:32:09', 0, 1, 49, 1),
(60, '<p>Tester</p>\n', '2015-11-24 20:13:30', '2015-11-24 20:13:40', 1, 1, 26, 2);

-- --------------------------------------------------------

--
-- Table structure for table `plan_tips`
--

CREATE TABLE `plan_tips` (
  `id` int(10) UNSIGNED NOT NULL,
  `tip` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_tips`
--

INSERT INTO `plan_tips` (`id`, `tip`, `created_at`, `updated_at`, `cat_id`) VALUES
(1, '<p>Don&rsquo;t write this now. The Executive Summary should be written last as it&rsquo;s the most important part of your plan. It should summarise your business and is designed to capture the reader&rsquo;s attention in a few sharp, pointed and perfectly shaped sentences, getting them excited about your business.</p>\r\n\r\n<p>Think about:</p>\r\n\r\n<ul>\r\n	<li>Who you are</li>\r\n	<li>What you do</li>\r\n	<li>What your business aims to achieve and the steps you&rsquo;ll take to get there</li>\r\n</ul>\r\n\r\n<p>If you&rsquo;re seeking financing or an investor, this is your first opportunity to grab interest. It should highlight the strengths of your business. If you&rsquo;re just starting out, you won&rsquo;t have as much info as an established company, so focus on your own experience.</p>\r\n\r\n<p>You want to prove how and why your business will be successful and how you&rsquo;ll get there. Remember to think of location; the current market and opportunities; your customer base; and who&rsquo;s involved in your business. The simple who, what, when, where, why and how come into play here.&nbsp;</p>\r\n', '2015-10-17 01:15:13', '2015-10-17 01:15:13', 10),
(2, '<p>Okay, lets describe what your business is.</p>\r\n\r\n<p>Look to provide:</p>\r\n\r\n<ul>\r\n	<li>An overview of your business</li>\r\n	<li>What you&rsquo;re selling, i.e., product, service, or combination of the two?</li>\r\n	<li>Will you have a physical store/online store or wholesale?</li>\r\n	<li>Will you have an office/warehouse?</li>\r\n	<li>Will you need staff/contractors/consultants?</li>\r\n</ul>\r\n\r\n<p>Provide a description of what your idea of success is. Will your product/service be offering your customers/clients with:</p>\r\n\r\n<ul>\r\n	<li>A niche product/service not currently in your market/industry?</li>\r\n	<li>Something price driven, i.e., cheaper than your competitors?</li>\r\n	<li>A premium product or service that isn&rsquo;t currently available?</li>\r\n</ul>\r\n', '2015-10-18 05:47:27', '2015-10-18 19:31:56', 30),
(3, '<p>Now it&rsquo;s time to describe your business potential and your goals for now and the future. It must be clear, concise and leave everyone inspired, wanting to know more. It doesn&rsquo;t matter how many words you use; it&rsquo;s the quality of the words that count. &nbsp;</p>\r\n\r\n<p>Think about how you want others to perceive your business and the reasons why you&rsquo;re creating the business. Look to include your customers, suppliers, competitors and employees.</p>\r\n\r\n<p>The vision must be a long-term view&mdash;you should never have to change your vision statement, no matter how much you grow.</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Keep it short and concise</li>\r\n	<li>Make sure it&rsquo;s specific to your business</li>\r\n	<li>Make sure people internally and externally understand it</li>\r\n	<li>Use passion and emotion&mdash;it should excite the reader</li>\r\n</ul>\r\n\r\n<p><strong>Some examples for you:</strong></p>\r\n\r\n<ul>\r\n	<li>Nike: (source: <a href="http://about.nike.com/">http://about.nike.com/</a>)</li>\r\n	<li>Avon (source: <a href="http://www.avon.com.ph/PRSuite/our_vision.page">http://www.avon.com.ph/PRSuite/our_vision.page</a>)</li>\r\n</ul>\r\n', '2015-10-18 05:47:49', '2015-10-18 05:47:49', 31),
(4, '<p>How are you going to accomplish your vision? This is where the Mission Statement comes in.</p>\r\n\r\n<p>You should describe the company&rsquo;s function, the markets you&rsquo;re targeting and your competitive advantage. The mission should describe your business, its customers, employees and the community. How will you achieve your vision statement? It&rsquo;s time to clearly state your objectives.</p>\r\n\r\n<p><strong>Tip: </strong>Your vision and mission statements will often be visible to your customers, potential customers, suppliers and investors (e.g., published on your website) so they need to stand out and give people an insight into your company&rsquo;s ethos.</p>\r\n\r\n<p><strong>Some examples for you:</strong></p>\r\n\r\n<ul>\r\n	<li>Disney (source: <a href="http://disneycompanyprofile.weebly.com/">http://disneycompanyprofile.weebly.com/</a>)</li>\r\n	<li>Amazon (source: <a href="https://www.facebook.com/Amazon/info?tab=page_info">https://www.facebook.com/Amazon/info?tab=page_info</a>)</li>\r\n</ul>\r\n', '2015-10-18 05:48:06', '2015-10-18 05:48:06', 32),
(5, '<p>Setting clear goals/objectives and including the necessary steps to make them happen is A MUST, both when starting your business and to ensure future success.</p>\r\n\r\n<p>List your business goals. Detail how, when and who&rsquo;ll be responsible to achieve them.</p>\r\n\r\n<p><strong>Examples of goals/objectives:</strong></p>\r\n\r\n<ul>\r\n	<li>Finding a good product supplier</li>\r\n	<li>Looking for a good distribution deal</li>\r\n	<li>Making your first sale</li>\r\n	<li>Hiring your first staff member</li>\r\n</ul>\r\n\r\n<p>Each goal/objective should be specific to your business and will change and grow as your business does.</p>\r\n\r\n<p><strong>Tip</strong>: Make them measurable and achievable.&nbsp;</p>\r\n', '2015-10-18 05:48:21', '2015-10-18 05:48:21', 33),
(6, '<p>Time to collate! This section should house all major business details. Keeping all key information in the one place will ensure you and the reader (i.e. investor) can quickly access your essential business information.</p>\r\n\r\n<p>When looking at the basics of setting up a business (your business/trading names and overall business structure) a great starting point is to speak with your local government authorities and your accountant. They&rsquo;ll be able to guide you through the right business structure for you, i.e., sole trader, a trust, partnership, business or company. Each structure has its own advantages and disadvantages, so choosing the right structure for your business is key. Making the wrong choice at the start may lead to large and costly changes down the track.</p>\r\n\r\n<p><strong>Tip: </strong>Do your research. Even scrolling through your government websites will offer advice/information you may not previously have known. It&rsquo;s also a good idea to do a website domain search before settling on names to make sure the website address you want is available. It&rsquo;s as simple as typing your ideal website address into Google to see if it&rsquo;s free, or you can head to specific websites where you&rsquo;ll be able to run a search for your ideal website address, e.g., <a href="https://au.godaddy.com/?ci">https://au.godaddy.com/?ci</a>=&nbsp;</p>\r\n', '2015-10-18 05:48:49', '2015-10-18 05:48:49', 24),
(7, '<p>An organisational chart is a great way to show how you see your business operating on a daily basis, and it also makes future planning really easy and efficient.</p>\r\n\r\n<p>A good organisational chart can:</p>\r\n\r\n<ul>\r\n	<li>Guide employees in understanding their own responsibilities</li>\r\n	<li>Help manage the workloads of all employees</li>\r\n	<li>Clearly show employees responsibilities and promotion opportunities</li>\r\n	<li>Identify potential gaps and what you may need to plan for</li>\r\n</ul>\r\n', '2015-10-18 05:49:02', '2015-10-18 05:49:02', 25),
(8, '<p>It&rsquo;s always a good idea to detail your key personnel. That way you can highlight who you&rsquo;ve currently got in your business and what skills/strengths they bring to the table.</p>\r\n\r\n<p>While you may not even be considering staff at this point, detailing your key personnel is also a great way to see what possible gaps or skills you may need in the future.</p>\r\n\r\n<p><strong>Tip:</strong> If you&rsquo;re looking for funding/investment, highlighting the skills and strengths of your personnel will not only showcase the potential of your business but also back-up why they should invest in your future.</p>\r\n\r\n<p>List your current staff in the table, including all owners.</p>\r\n', '2015-10-18 05:49:20', '2015-10-18 05:49:20', 38),
(9, '<p>Hopefully you see your business getting bigger and bigger over time. But in getting bigger, you may find you need more employees/staff to operate your business and achieve your goals. It&rsquo;s time to future-proof your business and think about the positions you may need.</p>\r\n\r\n<p>Detail the positions, what skills are required and the date you think you&rsquo;ll need someone by. This section will change as your business grows.</p>\r\n', '2015-10-18 05:49:30', '2015-10-18 05:49:30', 39),
(10, '<p>People can make or break a business, so specific recruiting and training systems for your business are a MUST.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The hiring and firing process can be a timely and costly exercise so ensuring your team has the right qualifications/experience from the start is key. Not only that, your business should have the appropriate systems and training manuals/processes in place to ensure your staff have clarity in their positions and align their daily activities with your company&rsquo;s vision and &lsquo;tone of voice&rsquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Investing in the right people not only improves productivity but also boosts team morale.</p>\r\n\r\n<p>All costs associated need to be considered during the recruitment process (monetary and otherwise). Things to think about include advertising for the required position, current staff covering the vacant role, or purely the time it takes to hire someone new (screening, interviewing, testing etc.). For instance, it&rsquo;s a standard procedure to not only interview web developers, but also design specific tests to prove their proficiency in languages and systems.</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Online Advertising services (i.e., Seek, CareerOne, LinkedIn) and even credible referrals are a great way to find staff</li>\r\n	<li>Create clear procedural manuals/documents that can be given to staff</li>\r\n	<li>Put checklists/important information in high traffic areas for staff so they can read them</li>\r\n	<li>Make sure you have written job descriptions for each staff member so there&rsquo;s no confusion on tasks</li>\r\n</ul>\r\n\r\n<p>Put in place review processes to make sure staff are held accountable</p>\r\n', '2015-10-18 05:49:47', '2015-10-18 05:49:47', 40),
(11, '<p>Here&rsquo;s where you detail each product/service you&rsquo;ll be providing to your clients/customers.</p>\r\n\r\n<p>Consider how you&rsquo;ll be selling them, where they&rsquo;re coming from and all costs associated with each product/service.</p>\r\n', '2015-10-18 05:50:00', '2015-10-18 05:50:00', 26),
(12, '<p>Describe how your products/services will be sold, and the payment method you&rsquo;ll have available, e.g., direct debit/credit card/merchant facilities./gift cards/PayPal.</p>\r\n\r\n<p>Will you have credit card, refund and warranty policies in place? We recommend you check with your local government authorities on any legislation regarding refunds/exchanges/warranties relevant to your industry, as there will be laws and legislation you&rsquo;ll need to abide by. Remember that these can differ depending on your location.</p>\r\n\r\n<p><strong>Tip: </strong>It&rsquo;s also a good idea to check with your bank to see what kind of payment methods are available and will work best for you. Shop around. Banks are pretty competitive. Make sure you get the best rates and account fees possible. Less money for them means more money for you!</p>\r\n\r\n<p>Here are some business policies to think about:</p>\r\n\r\n<ul>\r\n	<li>Credit card policy, e.g., 1.5% surcharge on all credit card payments</li>\r\n	<li>Refund/return/exchange policy, e.g., a seven day refund or exchange policy</li>\r\n	<li>Warranty policy: what will you provide if your product is faulty, breaks down or needs repair?</li>\r\n</ul>\r\n', '2015-10-18 05:50:23', '2015-10-18 05:50:23', 54),
(13, '<p>Have a think about all inventories you&rsquo;ll need to run your business (the items, their costs, the number you need etc.). For instance, if you&rsquo;re a baker, think about all of the ingredients you need for each variety of cakes you sell.</p>\r\n\r\n<p>List them so you&rsquo;re clear on all costs associated to produce each item you intend to sell and how many you&rsquo;ll need to have on hand. This list will change as you develop your business and start purchasing inventory items.</p>\r\n', '2015-10-18 05:50:32', '2015-10-18 05:50:32', 55),
(14, '<p>Understanding how your product/service gets to your end customer is very important. Having the correct distribution channels from the start ensures your business maximises its profit. You need to think about how you&rsquo;ll be selling your products/services, i.e., via a shopfront, email, website, wholesale or a combination of all.</p>\r\n\r\n<p>Each channel has its advantages and disadvantages. Here are a few things to consider:</p>\r\n\r\n<ul>\r\n	<li>The time and costs associated for each channel</li>\r\n	<li>Are the channels suited to your product/service and end goals?</li>\r\n	<li>What channels do your potential customers use? What are their buying habits? E.g., an online store may be better for the youth market</li>\r\n	<li>Can you use multiple channels? Of course you can, just make sure they&rsquo;ll achieve the results you&rsquo;re chasing</li>\r\n</ul>\r\n\r\n<p>During this phase, it&rsquo;s also worth considering what you&rsquo;ll be using to get your business out there. Are you pounding the pavement or hiring a sales team to sell on your behalf? Have a think, and nail the process from the start.</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Will you have a sales team? Will you provide your sales team with a handbook/guide to ensure they&rsquo;re selling your vision correctly?</li>\r\n	<li>Who will make up your sales team? How many sales people will you have?</li>\r\n	<li>Will your sales team have clear goals/KPI&rsquo;s based on the number of leads/sales they achieve?</li>\r\n</ul>\r\n', '2015-10-18 05:50:47', '2015-10-18 05:50:47', 56),
(15, '<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>The time and costs associated for each channel</li>\r\n	<li>Are the channels suited to your product/service and end goals?</li>\r\n	<li>What channels do your potential customers use? What are their buying habits? E.g., an online store may be better for the youth market</li>\r\n	<li>Can you use multiple channels? Of course you can, just make sure they&rsquo;ll achieve the results you&rsquo;re chasing</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>During this phase, it&rsquo;s also worth considering what you&rsquo;ll be using to get your business out there. Are you pounding the pavement or hiring a sales team to sell on your behalf? Have a think, and nail the process from the start.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Will you have a sales team? Will you provide your sales team with a handbook/guide to ensure they&rsquo;re selling your vision correctly?</li>\r\n	<li>Who will make up your sales team? How many sales people will you have?</li>\r\n	<li>Will your sales team have clear goals/KPI&rsquo;s based on the number of leads/sales they achieve?</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0">\r\n	<tbody>\r\n		<tr>\r\n			<td style="width:160px">\r\n			<p>Product/Service</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Distribution Channel</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Reason for Chosen Channel</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Method of Sale</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Strengths</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>Weaknesses</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="width:160px">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>List the product/service</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>Where your customers will be able to purchase your product/service, .e.g., shopfront, online</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>Why you chose this channel, &nbsp;e.g., because it&rsquo;s where your target market is based</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>How you&rsquo;ll reach customers, e.g., salespeople actively selling door to door</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>List all strengths</p>\r\n			</td>\r\n			<td style="width:160px">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>List all weaknesses</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp; 3.3.4 Location</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&lsquo;Location, Location, Location.&rsquo; Ensuring your business is in a prime location is one of the most important business decisions you can make. If people can&rsquo;t find you, whether it&rsquo;s a physical shopfront or an online portal, there&rsquo;s no point being in business.</p>\r\n\r\n<p><strong>Tips: </strong></p>\r\n\r\n<ul>\r\n	<li>Think about how much space you&rsquo;ll need, whether you&rsquo;ll need additional storage, and can the space you&rsquo;ve chosen grow with you?</li>\r\n	<li>Are you looking to rent, buy or lease?</li>\r\n	<li>Think about the location from your customers&rsquo; point of view to understand what works for them</li>\r\n	<li>Get to know the community and assess the foot traffic and accessibility of the area</li>\r\n	<li>Look at other businesses and competition in the area</li>\r\n	<li>Assess the building to ensure it suits: how old is the building, will it require maintenance or a complete fit-out and what are the costs?&nbsp;</li>\r\n</ul>\r\n', '2015-10-18 05:51:05', '2015-10-18 05:51:05', 57),
(16, '<p>It doesn&rsquo;t matter how big or small you think your business is going to be, understanding your legal requirements is vital. Starting your business with the right structures and procedures will avoid unnecessary legal headaches down the road.</p>\r\n\r\n<p>There are many legal requirements to consider when starting a business, and a qualified lawyer&mdash;preferably with experience in your industry&mdash;will help guide you through the process. Speaking with your relevant government bodies will also assist here.&nbsp;</p>\r\n\r\n<p><strong>Tip: </strong>It&rsquo;s always a good idea to conduct your own research. Knowing the right questions to ask will not only help you, but also the professionals you seek because they&rsquo;ll be able to gain a better understanding of your requirements. Remember the old saying: there&rsquo;s no such thing as a stupid question!</p>\r\n\r\n<p>Things to consider:</p>\r\n\r\n<ul>\r\n	<li>What kind of business do you intend to create and what business structure is right for you, i.e., sole trader, trust, partnership, company (<strong>Hot Tip: </strong>accountants can also assist with this process)</li>\r\n	<li>Will you need any contracts drafted (employee contracts, consultant contracts, distribution contracts, confidentiality agreements, shareholder agreements, lease/purchase contracts etc.)?</li>\r\n	<li>Will you need protection on your IP (intellectual property), trademarks, copyright and patents?</li>\r\n	<li>Will you require any licenses to operate your business? Note: these will differ depending on what state/territory you&rsquo;ll be operating in.</li>\r\n</ul>\r\n', '2015-10-18 05:51:25', '2015-10-18 05:51:25', 48),
(17, '<p>The boring numbers are so important and a trusted accounting adviser is MUST! Do your research. Having the appropriate taxation and accounting procedures from the outset will put your business in good stead for the future, and can avoid expensive mistakes and hassles with your local taxation department. An accountant will also help set up your business with the correct structure and offer advice on how to maximise your tax and accounting benefits.</p>\r\n\r\n<p>Once set up, we recommend investing in a &lsquo;cloud based&rsquo; accounting system like Xero, MYOB, Quickbooks or Freshbooks. Everyone loves a good excel spread sheet, but they can be limiting, so utilising a specialised accounting/bookkeeping system will be more efficient (they&rsquo;ll do the hard work for you!). This system will allow you to track and manage your cash flow as you grow, and will also allow your accountant to access your chosen platform without you needing to supply anything.</p>\r\n\r\n<p>It&rsquo;s important to document the accounting system you&rsquo;ll be using, including who&rsquo;ll be responsible for it, how you&rsquo;ll manage your client/customer payments to give your accountant a better understanding of your business, and what they&rsquo;ll need to complete for you. &nbsp;</p>\r\n\r\n<p><strong>Tips: </strong>When choosing a system, consider the following.</p>\r\n\r\n<ul>\r\n	<li>Can you set up and manage multiple bank accounts in the system?</li>\r\n	<li>Can the system calculate and track items like PAYG, annual and long service leave?</li>\r\n	<li>Will the system track stock, purchase orders and other task management requirements?</li>\r\n	<li>Will you be dealing with foreign currency?</li>\r\n	<li>Can the system link with your other online computer systems, such as online payments?</li>\r\n	<li>Can you enter, record and maintain customer contacts in the system, and be able to track what, how and how often they purchase?&nbsp;</li>\r\n</ul>\r\n', '2015-10-18 05:52:08', '2015-10-18 05:52:08', 49),
(18, '<p>You can&rsquo;t predict the future but you can plan and put practices in place for any &lsquo;what if&rsquo; scenario that may arise. Think about the needs of your business, any hazards you may come across and the steps you can employ to reduce risks and threats. While business insurance is typically a costly exercise, it&rsquo;s unfortunately a necessary evil. Every business needs protection.</p>\r\n\r\n<p>Insurances you should consider can be broken down into asset and revenue insurance, personal and workers insurance, and liability insurance.</p>\r\n\r\n<p>Here&rsquo;s a list of insurances you may need to consider for you business:</p>\r\n\r\n<ul>\r\n	<li>Public liability insurance</li>\r\n	<li>Professional indemnity insurance</li>\r\n	<li>Worker&rsquo;s compensation insurance</li>\r\n	<li>Third party personal injury insurance</li>\r\n	<li>Business equipment insurance</li>\r\n	<li>Fire and perils insurance</li>\r\n	<li>Product liability insurance</li>\r\n	<li>Motor vehicle insurance</li>\r\n	<li>Income protection insurance</li>\r\n	<li>Cash insurance</li>\r\n	<li>Life insurance</li>\r\n</ul>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Check with your local authorities or an insurance broker regarding appropriate insurances</li>\r\n	<li>Complete a risk assessment analysis to identify any potential liabilities in your business</li>\r\n	<li>Discuss your insurance with experts in your field to get the best and most suitable coverage</li>\r\n	<li>Try and purchase from one company; it will be easier for you to manage and negotiate price</li>\r\n</ul>\r\n', '2015-10-18 05:52:51', '2015-10-18 05:52:51', 50),
(19, '<p>A risk management strategy is a way for you to identify and assess potential hazards that could affect the success of your business.</p>\r\n\r\n<p>Having the foresight to create a risk management plan ensures you not only understand your business but you can predict potential threats and develop strategies to reduce any damage. &nbsp;</p>\r\n\r\n<p>Possible risk management examples include:</p>\r\n\r\n<ul>\r\n	<li>Staff performance or staff turnover</li>\r\n	<li>Introduction of a new product or service</li>\r\n	<li>Customer dissatisfaction</li>\r\n	<li>Instability in the market</li>\r\n	<li>Financial risk including a decrease in turnover, productivity or cash-flow</li>\r\n	<li>Power failure impacting IT systems or machinery</li>\r\n	<li>Natural disaster and hazards resulting in property damage</li>\r\n	<li>New competitor introduced to the market</li>\r\n	<li>Change of distributors/vendors</li>\r\n</ul>\r\n', '2015-10-18 05:53:06', '2015-10-18 05:53:06', 51),
(20, '<p>List all software requirements for your business. This is a great way to track all your systems and keep them in one place.</p>\r\n\r\n<p>Examples include:</p>\r\n\r\n<ul>\r\n	<li>Accounting and reporting systems/programs</li>\r\n	<li>Email/network systems (e.g., Microsoft Office)</li>\r\n	<li>Database/File sharing such as Office 365 or Google Drive</li>\r\n	<li>CRM relationship systems like Infusionsoft and Salesforce</li>\r\n	<li>Online payment systems for online businesses (e.g., Shopify or PayPal)</li>\r\n	<li>Inventory tracking systems</li>\r\n</ul>\r\n\r\n<p>List your IT/maintenance solutions. Be sure to detail who&rsquo;ll be responsible for updates, when your updates are required and any ongoing maintenance.</p>\r\n\r\n<p><strong>Tip: </strong>Try and use systems that talk to each other. Plug-ins between systems will make your life easier.&nbsp;</p>\r\n', '2015-10-18 05:53:31', '2015-10-18 05:53:31', 52),
(21, '<p>List all equipment needed to operate your business. Examples could include:</p>\r\n\r\n<ul>\r\n	<li>Coffee Machines</li>\r\n	<li>Eftpos Machine</li>\r\n	<li>Phones/printers</li>\r\n</ul>\r\n', '2015-10-18 05:53:41', '2015-10-18 05:53:41', 53),
(22, '<p>The Market Summary is where you can detail the market you&rsquo;ll be doing business in. It&rsquo;s a good idea to include answers to the following questions:</p>\r\n\r\n<ul>\r\n	<li>What is it?</li>\r\n	<li>What&rsquo;s happening in it?</li>\r\n	<li>Who&rsquo;s in it?</li>\r\n	<li>Where is it?</li>\r\n	<li>What are the gaps in the market?</li>\r\n	<li>What improvements are needed?</li>\r\n	<li>What&rsquo;s the future of the market?</li>\r\n	<li>Are there any trends in the market?</li>\r\n	<li>How do you see your business fitting in the market?</li>\r\n</ul>\r\n', '2015-10-18 05:53:54', '2015-10-18 05:53:54', 34),
(23, '<p>Your target market is the actual customer group you&rsquo;ll be targeting to sell your product/service. Which demographics will it appeal to? It&rsquo;s also a good idea to include how you intend to maintain your customer relationships and ensure they keep coming back.</p>\r\n\r\n<p>This section is all about identifying and building your customer profile. When thinking of your customer, think of the:</p>\r\n\r\n<ul>\r\n	<li>Demographics (age, sex, income, education, occupation, location, marital status)</li>\r\n	<li>Psychographics (customers attitudes, tastes, lifestyles, disposable income)</li>\r\n</ul>\r\n\r\n<p>If you&rsquo;re targeting a business, think of the:</p>\r\n\r\n<ul>\r\n	<li>Business size (locations, number of employees etc.)</li>\r\n	<li>Annual sales</li>\r\n	<li>Years in business</li>\r\n	<li>Competitors</li>\r\n</ul>\r\n\r\n<p><strong>Tips: </strong></p>\r\n\r\n<ul>\r\n	<li>You can have one clear target market or segment your target market</li>\r\n	<li>Think about whether your target market or segment has long-term/ongoing potential</li>\r\n	<li>Analyse your product/service and how it fulfils the requirements of your target market</li>\r\n	<li>Conduct a little market research on your competitors&mdash;this will help you position your target market and look for potential opportunities</li>\r\n</ul>\r\n', '2015-10-18 05:54:09', '2015-10-18 05:54:09', 35),
(24, '<p>What methods did you use to establish your target market? Did you conduct a focus group or get people to complete an online survey? This section is all about how you arrived at your target market and your use of relevant research to back it up.&nbsp;</p>\r\n\r\n<p>Different sources can include:</p>\r\n\r\n<ul>\r\n	<li>Online surveys like: <a href="https://www.surveymonkey.com/">Survey Monkey</a></li>\r\n	<li>Phone surveys</li>\r\n	<li>Focus groups such as your local chamber of commerce</li>\r\n</ul>\r\n\r\n<p>Local and state government resources (they&rsquo;re a great way to find census info about your market)&nbsp;</p>\r\n', '2015-10-18 05:54:23', '2015-10-18 05:54:23', 45),
(25, '<p>It&rsquo;s always a good idea to have one eye on your competitors (and not in a creepy way!)</p>\r\n\r\n<p>Take a look at what they&rsquo;re selling, their pricing structure and any marketing or advertising plans. This way you can not only compare your business, but also ensure your business has its own point of difference or unique selling point.&nbsp;</p>\r\n', '2015-10-18 05:54:34', '2015-10-18 05:54:34', 46),
(26, '<p>Let&rsquo;s have a look at your business objectively through a SWOT analysis, which is a proven method of exploring your strengths, weaknesses, opportunities and threats.</p>\r\n\r\n<p>The SWOT analysis is an easy way to breakdown and understand your business. It enables you to not only see the good, but also highlight potential obstacles. Knowing your weaknesses will help you avoid mistakes, while your strengths and opportunities will ensure you maximise your potential.</p>\r\n\r\n<p>Generally, strengths and weaknesses are written for the present and internal structures, whereas opportunities and threats focus more on the future and external factors.</p>\r\n\r\n<p><strong>Tips</strong></p>\r\n\r\n<ul>\r\n	<li>Strengths and weaknesses include prices, business costs, business reputation and quality</li>\r\n	<li>Opportunities and threats include competition, laws, politics and seasons, plus economic and cultural landscapes&nbsp;</li>\r\n	<li>Keep it short and concise</li>\r\n	<li>Detail each item from highest to lowest priority</li>\r\n	<li>Be honest and complete the analysis thoroughly and carefully</li>\r\n	<li>Look at your business from an objective observer perspective and ask people for input</li>\r\n</ul>\r\n', '2015-10-18 05:54:47', '2015-10-18 05:54:47', 36),
(27, '<p>Unique Selling Position (USP) is all about differentiating yourself from your competition. What makes you so special?</p>\r\n\r\n<p>A USP is about setting yourself apart. We recommend not being &lsquo;all things to all people.&rsquo; Specialise in one product or service first and multiply the learning&rsquo;s and success for future products or services you release.</p>\r\n\r\n<p>Questions to ask yourself when determining your USP:</p>\r\n\r\n<ul>\r\n	<li>What do you offer that your competitors don&rsquo;t?</li>\r\n	<li>Does your product/service have added benefits and features?</li>\r\n	<li>What value and/or added value do you offer?</li>\r\n</ul>\r\n', '2015-10-18 05:55:02', '2015-10-18 05:55:02', 47),
(28, '<p>Whether your budget is large or small, you need to think about how you&rsquo;re going to tell people that you&rsquo;re open for business. Having the knowledge and know-how around basic marketing and media channels can be the difference between success and failure.</p>\r\n\r\n<p>There&rsquo;s no &lsquo;one size fits all&rsquo; method and nor should there be.</p>\r\n\r\n<p>Here are some media channels to think about:</p>\r\n\r\n<ul>\r\n	<li>Social media (e.g., Facebook, Twitter, Linkedin, Instagram)</li>\r\n	<li>Google Adwords</li>\r\n	<li>Digital advertising (web banners)</li>\r\n	<li>Email marketing</li>\r\n	<li>Traditional media like print, television and radio</li>\r\n	<li>Direct mail</li>\r\n	<li>Outdoor advertising</li>\r\n	<li>Directories (e.g., Yellow Pages)</li>\r\n	<li>Launch events</li>\r\n	<li>Sponsorships&nbsp;</li>\r\n	<li>Public relations (media releases)</li>\r\n</ul>\r\n\r\n<p>When putting together your plan, here are some <strong>tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Think about your profit margin and what percentage of that margin you&rsquo;d like to commit to marketing your business</li>\r\n	<li>Look at what media your target market is consuming</li>\r\n	<li>Incorporate several channels to create a fully integrated campaign in order to maximise your budget and exposure</li>\r\n	<li>You don&rsquo;t need to spend your entire life&rsquo;s savings on your marketing/advertising (a simple Facebook campaign can be just as effective as a TV campaign for the right audience)</li>\r\n	<li>Make sure you set measurable media objectives&mdash;there&rsquo;s no point putting it out there if you don&rsquo;t know it will work</li>\r\n</ul>\r\n\r\n<p>We can help give you more info via <strong>INSERT NEXT PACKAGE LINK</strong></p>\r\n', '2015-10-18 05:55:24', '2015-10-18 05:55:24', 37),
(29, '<p>While specific accounting software will be able to complete these documents for you, if you&rsquo;re not yet up and running with an accounting system, we&rsquo;ve created basic templates for you, using Microsoft Excel to help get you started.</p>\r\n\r\n<p>Download, edit and then once they&rsquo;re complete, upload to your existing Business Plan ready for exporting</p>\r\n\r\n<p>How much money do you want to make? What&rsquo;s your profit objective? Start to number-crunch so you not only know what you&rsquo;re spending, but also where you want to go (sales profits or targets).</p>\r\n\r\n<p>At this point you may have no idea how much money you&rsquo;re going to make, let alone how much you need in order to get your business off the ground. But financial documents are essential, especially if you&rsquo;re going to a bank or investor to seek backing. They&rsquo;ll need to know things like the projections, the numbers (coming in and going out), how liquid your business is and the potential sales opportunities.</p>\r\n\r\n<p>Ask yourself:</p>\r\n\r\n<ul>\r\n	<li>What upfront funds do you need to set up your business?</li>\r\n	<li>Where are the funds coming from?</li>\r\n	<li>Do you need to obtain finance from banks, investors, friends or relatives?</li>\r\n	<li>Will you personally be contributing any funds?</li>\r\n	<li>Are there factors that will influence financial success, like seasonal influences, staffing, distribution etc?</li>\r\n</ul>\r\n', '2015-10-18 05:55:50', '2015-10-18 05:55:50', 41),
(30, '<p>A profit and loss forecast allows you to track all money coming in (sales), less your expenses (losses) to give you your potential profit. It provides you with visibility on your business and an overall summary of your financial performance. The Profit &amp; Loss (P&amp;L) is the most common financial document for all small businesses and they can either be completed monthly, quarterly or annually, depending on how you prefer to operate.</p>\r\n\r\n<p>The profit and loss forecast should include any set-up costs and projected operational costs. The forecast includes, but is not limited to, wages, superannuation and taxes plus general business expenses including your marketing and media spend.</p>\r\n\r\n<p>The formula for a P&amp;L is pretty simple:</p>\r\n\r\n<p><strong>Gross Profit</strong> = sales &ndash; cost of goods sold</p>\r\n\r\n<p><strong>Net Profit</strong> = gross profit - expenses</p>\r\n\r\n<p>A good P&amp;L should be able to:</p>\r\n\r\n<ul>\r\n	<li>Show how much money is coming into your business (i.e. your sales/revenue)</li>\r\n	<li>Highlight the number of expenses you&rsquo;re incurring over time</li>\r\n	<li>Compare your expenses and performance over different periods (this&rsquo;ll show the financial trends in your business)</li>\r\n	<li>Prove your income/earning should you need evidence to apply for a mortgage/loan</li>\r\n</ul>\r\n\r\n<p>Start-up cost examples:</p>\r\n\r\n<ul>\r\n	<li>Business registration including business name, licenses, permits and domain name</li>\r\n	<li>General business: e.g., rent/lease fees, lawyer fees, accountant fees</li>\r\n	<li>Design and website build (logos, letterhead, website pages etc.)</li>\r\n	<li>Equipment such as computers, furniture, vehicles, phones and stationary/office supplies</li>\r\n</ul>\r\n\r\n<p>Insurances, e.g., public liability, workers compensation, business assets</p>\r\n', '2015-10-18 05:56:11', '2015-10-18 05:56:11', 42),
(31, '<p>A balance sheet has three main components to it:</p>\r\n\r\n<ol>\r\n	<li>Assets: what your business owns&mdash;i.e., cash, land, property&mdash;each divided into long and short term</li>\r\n	<li>Liabilities: what your business owes, such as loans and accounts payable&mdash;each divided into long and short term</li>\r\n	<li>Owner&rsquo;s equity: the remaining balance once your liabilities (i.e., financial obligations) are deducted&mdash;also known as capital</li>\r\n</ol>\r\n\r\n<p>The formula for a balance sheet is: The Owner&rsquo;s Equity = Assets &ndash; Liabilities</p>\r\n\r\n<p>The balance sheet is generally used by banks and investors because it highlights your overall financial position. What items are included in your balance sheet will differ depending on the nature of your business. They&rsquo;re not necessarily compulsory to your business plan but it&rsquo;s best to check with your local authorities to see whether your business particulars require a balance sheet.</p>\r\n\r\n<p>A good balance sheet should be able to:</p>\r\n\r\n<ul>\r\n	<li>Highlight all assets, liabilities and equity in your business</li>\r\n	<li>Be a quick reference showing your financial strengths and opportunities</li>\r\n	<li>Highlight your ability to be able to pay your bills on time (both short and long term)</li>\r\n</ul>\r\n', '2015-10-18 05:56:28', '2015-10-18 05:56:28', 43),
(32, '<p><strong>Cash flow</strong>&nbsp;is how much cash you expect to generate in your business and any outgoings (expenses). The whole point of cash flow is to make sure the business makes more money than it pays out. Your level of cash flow will most likely be a key indicator when potential investors are analysing your viability and ultimately deciding whether to invest&nbsp;or not.</p>\r\n\r\n<p>Being able to predict cash shortfalls over time will allow you to plan ahead, and ensure you&rsquo;re able to pay all financial obligations as they arise. These can be completed weekly, monthly or annually but are generally broken down by months. These documents are excellent methods of proving your businesses&rsquo; liquidity, and whether you&rsquo;re in surplus or deficit at the end of each period.</p>\r\n\r\n<p>The expected cash flow document is only as valuable as the data you enter. Sales forecasting can be hard, especially if you&rsquo;re not yet up and running, but if you&rsquo;re looking at your expected cash flow objectively, it may be wise to produce a couple of separate versions (i.e. realistic, pessimistic and optimistic versions). Once you&rsquo;re up and running, this will be a great way to measure what you actually achieved.</p>\r\n\r\n<p><strong>Tip: </strong>Chat to your accountant about this one; they&rsquo;ll be able to guide you and offer advice about the process.</p>\r\n\r\n<p>It&rsquo;s a pretty simple formula: <strong>Net Cash Position </strong>= cash incoming &ndash; cash outgoing&nbsp;</p>\r\n', '2015-10-18 05:56:53', '2015-10-18 05:56:53', 44),
(33, '<p>Research and development (R&amp;D) can lead to great innovation, increased productivity and overall success in your business.</p>\r\n\r\n<p>Creating a strong R&amp;D plan can help you grow and improve your business, giving you an advantage over your competitors. Whether it&rsquo;s researching new technologies, analysing a product/service in your market, or examining your market in general, R&amp;D should be conducted for each and every business.</p>\r\n\r\n<p><strong>Tip:</strong> Check with your local authorities to see if you&rsquo;re eligible for any R&amp;D tax Incentives or grants.&nbsp;</p>\r\n', '2015-10-18 05:57:09', '2015-10-18 05:57:09', 15);

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE `revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `revisionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `revisions`
--

INSERT INTO `revisions` (`id`, `revisionable_type`, `revisionable_id`, `user_id`, `key`, `old_value`, `new_value`, `created_at`, `updated_at`) VALUES
(1, 'App\\BusinessPlanContent', 1, NULL, 'content', 'Tester tester tester', 'New Version!', '2015-10-29 03:28:24', '2015-10-29 03:28:24'),
(2, 'App\\BusinessPlanContent', 1, NULL, 'user_id', '0', '1', '2015-10-29 03:31:01', '2015-10-29 03:31:01'),
(3, 'App\\BusinessPlanContent', 1, NULL, 'business_plan_id', '0', '1', '2015-10-29 03:31:01', '2015-10-29 03:31:01'),
(4, 'App\\BusinessPlanContent', 1, NULL, 'content', 'New Version!', 'New contents...', '2015-10-29 03:31:19', '2015-10-29 03:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referral` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_info` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_address_id` int(11) NOT NULL,
  `upgraded_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mailchimp_member_hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `briefed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `company`, `contact_number`, `referral`, `other_info`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `user_address_id`, `upgraded_at`, `mailchimp_member_hash`, `briefed`) VALUES
(1, 'Tremaine Breitenberg', 'Gabriel Lind', 'Rodriguez-Erdman', '1-670-558-1515', 'Larry Emard MD', 'Consequatur eum nihil facilis ex temporibus. Rerum error iure quis aut est dolorem voluptatibus. Provident vel dolorem quae sit fugiat.', 'mickey.ftw@gmail.com', '$2y$10$0N6GYMyGgBO6uwN1Hjg4yOvArk9vDWMTGPdPl38s.DYNoGIvBkdXi', 'XPbZm4FyFPDpjk1kKLp1X7vNLjWxT3nclDqgs8Vy9eGryoiUvpqKQD8EXTMN', '2015-10-14 17:38:09', '2015-10-17 05:16:14', 1, '0000-00-00 00:00:00', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `premise` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thoroughfare` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `administrative_area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `premise`, `thoroughfare`, `administrative_area`, `locality`, `postal_code`, `country`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '3/44', 'Sunrise Bvld', 'Queensland', 'Surfers Paradise, Gold Coast', '4217', 'AU', 1, '2015-10-23 14:00:00', '2015-10-23 14:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `briefing_form_documents`
--
ALTER TABLE `briefing_form_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `briefing_form_questions`
--
ALTER TABLE `briefing_form_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `briefing_form_submissions`
--
ALTER TABLE `briefing_form_submissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `briefing_question_answers`
--
ALTER TABLE `briefing_question_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buckets`
--
ALTER TABLE `buckets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bucket_user`
--
ALTER TABLE `bucket_user`
  ADD PRIMARY KEY (`bucket_id`,`user_id`),
  ADD KEY `bucket_user_bucket_id_index` (`bucket_id`),
  ADD KEY `bucket_user_user_id_index` (`user_id`);

--
-- Indexes for table `business_plan_documents`
--
ALTER TABLE `business_plan_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_contents`
--
ALTER TABLE `default_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_from_id_index` (`from_id`),
  ADD KEY `notifications_from_type_index` (`from_type`),
  ADD KEY `notifications_to_id_index` (`to_id`),
  ADD KEY `notifications_to_type_index` (`to_type`),
  ADD KEY `notifications_category_id_index` (`category_id`);

--
-- Indexes for table `notifications_categories_in_groups`
--
ALTER TABLE `notifications_categories_in_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_categories_in_groups_category_id_index` (`category_id`),
  ADD KEY `notifications_categories_in_groups_group_id_index` (`group_id`);

--
-- Indexes for table `notification_categories`
--
ALTER TABLE `notification_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `notification_categories_name_unique` (`name`),
  ADD KEY `notification_categories_name_index` (`name`);

--
-- Indexes for table `notification_groups`
--
ALTER TABLE `notification_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `notification_groups_name_unique` (`name`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_user`
--
ALTER TABLE `package_user`
  ADD PRIMARY KEY (`package_id`,`user_id`),
  ADD KEY `package_user_package_id_index` (`package_id`),
  ADD KEY `package_user_user_id_index` (`user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `page_closure`
--
ALTER TABLE `page_closure`
  ADD PRIMARY KEY (`closure_id`),
  ADD KEY `page_closure_ancestor_foreign` (`ancestor`),
  ADD KEY `page_closure_descendant_foreign` (`descendant`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_categories`
--
ALTER TABLE `plan_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_plan_categories_parent_id_index` (`parent_id`),
  ADD KEY `business_plan_categories_lft_index` (`lft`),
  ADD KEY `business_plan_categories_rgt_index` (`rgt`);

--
-- Indexes for table `plan_contents`
--
ALTER TABLE `plan_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_tips`
--
ALTER TABLE `plan_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `briefing_form_documents`
--
ALTER TABLE `briefing_form_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `briefing_form_questions`
--
ALTER TABLE `briefing_form_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `briefing_form_submissions`
--
ALTER TABLE `briefing_form_submissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `briefing_question_answers`
--
ALTER TABLE `briefing_question_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buckets`
--
ALTER TABLE `buckets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `business_plan_documents`
--
ALTER TABLE `business_plan_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_contents`
--
ALTER TABLE `default_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notifications_categories_in_groups`
--
ALTER TABLE `notifications_categories_in_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_categories`
--
ALTER TABLE `notification_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_groups`
--
ALTER TABLE `notification_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `page_closure`
--
ALTER TABLE `page_closure`
  MODIFY `closure_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `plan_categories`
--
ALTER TABLE `plan_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `plan_contents`
--
ALTER TABLE `plan_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `plan_tips`
--
ALTER TABLE `plan_tips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;
--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bucket_user`
--
ALTER TABLE `bucket_user`
  ADD CONSTRAINT `bucket_user_bucket_id_foreign` FOREIGN KEY (`bucket_id`) REFERENCES `buckets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bucket_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `notification_categories` (`id`);

--
-- Constraints for table `notifications_categories_in_groups`
--
ALTER TABLE `notifications_categories_in_groups`
  ADD CONSTRAINT `notifications_categories_in_groups_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `notification_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_categories_in_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `notification_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_user`
--
ALTER TABLE `package_user`
  ADD CONSTRAINT `package_user_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `package_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `pages` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `page_closure`
--
ALTER TABLE `page_closure`
  ADD CONSTRAINT `page_closure_ancestor_foreign` FOREIGN KEY (`ancestor`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `page_closure_descendant_foreign` FOREIGN KEY (`descendant`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
