/*
SQLyog Community v12.15 (64 bit)
MySQL - 5.6.16 : Database - gjm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `appointments` */

DROP TABLE IF EXISTS `appointments`;

CREATE TABLE `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `availability` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `appointments` */

insert  into `appointments`(`id`,`type`,`start_time`,`end_time`,`status`,`user_id`,`created_at`,`updated_at`,`availability`) values 
(1,2,'2015-10-31 08:00:00','2015-10-31 10:00:00',1,1,'0000-00-00 00:00:00','2015-11-01 23:44:54',0),
(2,0,'2015-10-31 10:00:00','2015-10-31 12:00:00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),
(3,2,'2015-10-30 15:24:53','2015-10-30 17:25:03',1,1,'0000-00-00 00:00:00','2015-11-01 23:44:26',0),
(4,1,'2015-11-05 13:00:00','2015-11-05 15:00:00',0,1,'0000-00-00 00:00:00','2015-11-02 00:03:12',0),
(5,2,'2015-11-05 10:00:00','2015-11-05 12:00:00',0,1,'0000-00-00 00:00:00','2015-11-01 23:42:43',1),
(6,1,'2015-11-06 08:00:00','2015-11-06 10:00:00',0,1,'0000-00-00 00:00:00','2015-11-01 23:37:25',0),
(7,0,'2016-01-06 15:59:52','2016-01-06 17:59:59',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00',0),
(8,2,'2016-01-08 10:48:59','2016-01-08 12:48:59',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(9,1,'2016-02-12 18:01:02','2016-02-12 20:01:02',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(10,1,'2016-01-28 12:10:49','2016-01-28 14:10:49',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(11,2,'2016-02-18 09:57:51','2016-02-18 11:57:51',0,1,'2015-11-02 00:51:18','2015-11-02 01:05:55',0),
(12,2,'2015-11-25 08:51:18','2015-11-25 10:51:18',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(13,1,'2016-01-08 19:08:08','2016-01-08 21:08:08',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(14,2,'2016-02-06 20:02:01','2016-02-06 22:02:01',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(15,2,'2015-11-30 14:29:42','2015-11-30 16:29:42',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(16,2,'2016-02-29 02:31:24','2016-02-29 04:31:24',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(17,2,'2015-11-10 17:26:26','2015-11-10 19:26:26',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(18,1,'2015-12-05 13:46:10','2015-12-05 15:46:10',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(19,2,'2016-01-08 13:32:17','2016-01-08 15:32:17',0,0,'2015-11-02 00:51:18','2015-11-02 00:51:18',1),
(20,2,'2015-12-26 20:22:12','2015-12-26 22:22:12',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(21,1,'2015-12-04 00:55:08','2015-12-04 02:55:08',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(22,2,'2015-12-05 03:11:22','2015-12-05 05:11:22',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(23,1,'2015-12-27 15:53:23','2015-12-27 17:53:23',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(24,1,'2015-11-10 08:04:08','2015-11-10 10:04:08',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(25,2,'2016-01-26 12:01:17','2016-01-26 14:01:17',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(26,1,'2015-12-19 00:56:25','2015-12-19 02:56:25',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(27,1,'2016-01-11 08:23:50','2016-01-11 10:23:50',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(28,2,'2016-02-18 15:18:35','2016-02-18 17:18:35',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(29,1,'2015-12-30 02:56:23','2015-12-30 04:56:23',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(30,1,'2016-01-22 11:15:24','2016-01-22 13:15:24',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(31,2,'2016-01-16 13:55:22','2016-01-16 15:55:22',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(32,2,'2016-01-19 00:13:44','2016-01-19 02:13:44',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(33,2,'2015-11-23 19:59:40','2015-11-23 21:59:40',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(34,2,'2015-11-25 13:39:33','2015-11-25 15:39:33',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(35,1,'2016-02-05 04:36:05','2016-02-05 06:36:05',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(36,2,'2015-12-02 15:31:06','2015-12-02 17:31:06',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(37,1,'2015-12-10 11:05:38','2015-12-10 13:05:38',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(38,1,'2015-11-17 15:45:42','2015-11-17 17:45:42',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(39,1,'2016-01-07 10:18:47','2016-01-07 12:18:47',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(40,2,'2015-11-11 21:18:58','2015-11-11 23:18:58',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(41,1,'2015-12-17 14:14:09','2015-12-17 16:14:09',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(42,2,'2015-11-20 21:24:04','2015-11-20 23:24:04',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(43,2,'2015-11-03 09:50:26','2015-11-03 11:50:26',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(44,2,'2016-02-12 02:01:31','2016-02-12 04:01:31',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(45,1,'2015-11-04 22:16:08','2015-11-05 00:16:08',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(46,1,'2015-11-27 03:19:30','2015-11-27 05:19:30',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(47,2,'2015-11-29 16:16:11','2015-11-29 18:16:11',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(48,1,'2015-11-03 14:12:30','2015-11-03 16:12:30',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(49,2,'2016-01-28 12:59:26','2016-01-28 14:59:26',0,1,'2015-11-02 00:51:19','2015-11-02 00:59:09',0),
(50,1,'2015-12-18 11:37:26','2015-12-18 13:37:26',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(51,2,'2016-01-05 23:53:42','2016-01-06 01:53:42',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(52,1,'2015-12-13 05:30:17','2015-12-13 07:30:17',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(53,2,'2015-12-09 15:15:59','2015-12-09 17:15:59',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(54,2,'2016-01-21 01:21:55','2016-01-21 03:21:55',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(55,2,'2015-11-17 15:31:13','2015-11-17 17:31:13',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(56,1,'2016-01-26 22:35:46','2016-01-27 00:35:46',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(57,1,'2016-01-23 06:06:50','2016-01-23 08:06:50',0,0,'2015-11-02 00:51:19','2015-11-02 00:51:19',1),
(58,1,'2016-02-03 09:40:13','2016-02-03 11:40:13',0,0,'2015-11-18 06:22:55','2015-11-18 06:22:55',1),
(59,2,'2016-02-21 01:46:40','2016-02-21 03:46:40',0,0,'2015-11-18 06:24:29','2015-11-18 06:24:29',1),
(60,2,'2015-12-20 06:41:11','2015-12-20 08:41:11',0,0,'2015-11-18 06:25:06','2015-11-18 06:25:06',1),
(61,1,'2015-11-23 23:19:30','2015-11-24 01:19:30',0,0,'2015-11-18 06:26:37','2015-11-18 06:26:37',1),
(62,2,'2016-02-12 05:47:23','2016-02-12 07:47:23',0,0,'2015-11-18 06:33:37','2015-11-18 06:33:37',1),
(63,2,'2016-01-23 08:21:33','2016-01-23 10:21:33',0,0,'2015-11-18 06:34:20','2015-11-18 06:34:20',1),
(64,2,'2016-02-06 08:49:01','2016-02-06 10:49:01',0,0,'2015-11-18 06:35:12','2015-11-18 06:35:12',1),
(65,1,'2016-02-03 06:36:51','2016-02-03 08:36:51',0,0,'2015-11-18 06:37:48','2015-11-18 06:37:48',1);

/*Table structure for table `briefing_form_documents` */

DROP TABLE IF EXISTS `briefing_form_documents`;

CREATE TABLE `briefing_form_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `briefing_form_submission_id` int(11) NOT NULL,
  `briefing_question_answer_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `doc_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `briefing_form_documents` */

insert  into `briefing_form_documents`(`id`,`briefing_form_submission_id`,`briefing_question_answer_id`,`created_at`,`updated_at`,`filename`,`doc_type`) values 
(1,1,26,'2015-12-01 16:14:39','2015-12-01 16:14:39','Top Menu Bar - Email Reply Notification.png','image/png'),
(2,1,26,'2015-12-02 10:18:19','2015-12-02 10:18:19','Medical Certificate.pdf','application/pdf'),
(3,1,27,'2015-12-02 10:18:19','2015-12-02 10:18:19','Outlook Web App.png','image/png');

/*Table structure for table `briefing_form_questions` */

DROP TABLE IF EXISTS `briefing_form_questions`;

CREATE TABLE `briefing_form_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `briefing_form_questions` */

insert  into `briefing_form_questions`(`id`,`question_text`,`question_name`,`question_type`,`created_at`,`updated_at`) values 
(1,'Business name','business_name','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(2,'Business contact name and title','contact_title','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(3,'Email Address','email','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(4,'Tell us about your business (what do you sell? How long have you been in buisiness? Just some background information on your business)','about_your_business','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(5,'What’s your annual turnover?','turnover','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(6,'How do you sell your product or service? (wholesale, retail, online, offline). And for how much (price range)?','selling_method','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(7,'Who is your target audience? Your customer, their age, income and occupation etc','target_audience','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(8,'Who is your ideal customer?  What do you know about them?','ideal_customer','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(9,'What are your vision and mission statements (core messages)','core_messages','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(10,'What’s your current brand position? Look and feel. What do you stand for?','current_brand_position','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(11,'Please provide your logo, slogan, website address and social media accounts (handles and hashtags you use)','logo','file','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(12,'Do you know who your biggest competitors are?\rIf yes, please list them and what you know about them','competitor','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(13,'Unique selling position (what are you currently offering that competitors aren’t). \r\rHow do you differentiate yourself from your competitors?','unique_selling_position','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(14,'Have you conducted any market research before (i.e. surveys, focus groups, your industry research). What were the results?','market_research','file','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(15,'How do you manage your customers feedback? Positive or negative','customer_feedback','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(16,'How do your customers hear about you, what you offer and where you’re located?','hear_about_you','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(17,'How do you stay in contact with your customers? i.e. newsletters etc','contact_method','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(18,'Have you done any other marketing, PR or advertising in the past? \r\rIf yes please describe and show us.','marketing_material','file','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(19,'Tell us what you want to market? Is it the business in general or a specific product/service, launch, event or campaign?\r\rPlease give us a description of what you’d like to focus on.','what_to_market','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(20,'What are your media and objectives? Do you have clear marketing KPI’s?\r\r(Do you want to generate inquiries, create awareness (brand), provide information, build your image,  sell a product or announce a new service)?','media_and_objectives','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(21,'What is your media and marketing budget? And how did you come to this number?','budget','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(22,'What are your campaign timings for the budget outlined?','campaign_timings','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(23,'What markets would you like to focus your marketing activity on?','market_focus','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(24,'What media platforms are you interested in exploring? And how much do you know about each? \r\rFor example; social media, TV, radio, digital, print, outdoor, PR, sponsorships, events or direct mail.','media_platforms','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(25,'Do you have a social media plan for your business?\r\rAnd who maintains your website and social media accounts','social_media_plan','textarea','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(26,'Do you have a loyalty program in place?','loyalty_program','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(27,'How will you be measuring the success of the marketing activity you select?','measuring_criteria','text','0000-00-00 00:00:00','0000-00-00 00:00:00'),
(28,'Are there any factors that could Influence the success of your marketing effort (ie holidays, cost or availability of products and services across the year, peak – off peak times or staffing issues?)','influential_factors','text','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `briefing_form_submissions` */

DROP TABLE IF EXISTS `briefing_form_submissions`;

CREATE TABLE `briefing_form_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `emailed_submission` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `briefing_form_submissions` */

insert  into `briefing_form_submissions`(`id`,`user_id`,`created_at`,`updated_at`,`emailed_submission`) values 
(1,1,'2015-11-23 05:20:34','2015-11-23 05:20:34',NULL);

/*Table structure for table `briefing_question_answers` */

DROP TABLE IF EXISTS `briefing_question_answers`;

CREATE TABLE `briefing_question_answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `briefing_form_submission_id` int(11) NOT NULL,
  `briefing_question_id` int(11) NOT NULL,
  `briefing_question_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `briefing_question_answers` */

insert  into `briefing_question_answers`(`id`,`briefing_form_submission_id`,`briefing_question_id`,`briefing_question_answer`,`created_at`,`updated_at`) values 
(1,1,1,'GJM','2015-11-23 05:20:34','2015-11-23 05:20:34'),
(2,1,2,'GJM','2015-11-23 05:20:34','2015-11-23 05:20:34'),
(3,1,3,'asai.vishal@yahoo.com','2015-11-23 05:20:34','2015-11-23 05:20:34'),
(4,1,4,'saved_answer','2015-11-23 05:20:34','2015-11-23 05:20:34'),
(5,1,5,'saved_answer','2015-11-23 05:20:34','2015-11-23 05:20:34'),
(6,1,6,'saved_answer','2015-11-23 05:20:34','2015-11-23 05:20:34'),
(7,1,7,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(8,1,8,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(9,1,9,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(10,1,10,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(11,1,12,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(12,1,13,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(13,1,15,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(14,1,16,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(15,1,17,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(16,1,19,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(17,1,20,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(18,1,21,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(19,1,22,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(20,1,23,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(21,1,24,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(22,1,25,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(23,1,26,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(24,1,27,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(25,1,28,'saved_answer','2015-11-23 05:20:35','2015-11-23 05:20:35'),
(26,1,11,'uploads/Medical Certificate.pdf','2015-12-01 16:14:39','2015-12-02 10:18:19'),
(27,1,18,'uploads/Outlook Web App.png','2015-12-02 10:18:19','2015-12-02 10:18:19');

/*Table structure for table `bucket_user` */

DROP TABLE IF EXISTS `bucket_user`;

CREATE TABLE `bucket_user` (
  `bucket_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`bucket_id`,`user_id`),
  KEY `bucket_user_bucket_id_index` (`bucket_id`),
  KEY `bucket_user_user_id_index` (`user_id`),
  CONSTRAINT `bucket_user_bucket_id_foreign` FOREIGN KEY (`bucket_id`) REFERENCES `buckets` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bucket_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `bucket_user` */

insert  into `bucket_user`(`bucket_id`,`user_id`) values 
(6,130),
(7,130),
(8,130),
(9,130),
(12,1),
(13,1);

/*Table structure for table `buckets` */

DROP TABLE IF EXISTS `buckets`;

CREATE TABLE `buckets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mailchimp_list_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `buckets` */

insert  into `buckets`(`id`,`name`,`created_at`,`updated_at`,`mailchimp_list_id`) values 
(1,'Testing Mailchimp','2015-11-13 05:24:00','2015-11-13 05:24:00',''),
(2,'Test','2015-11-13 05:28:38','2015-11-13 05:28:38',''),
(3,'Testing','2015-11-13 05:30:31','2015-11-13 05:30:31',''),
(4,'Testing Test','2015-11-13 05:39:25','2015-11-13 05:39:25',''),
(5,'Name','2015-11-17 04:24:04','2015-11-17 04:24:04','7d382659ea'),
(6,'New Sign Ups','2015-11-18 00:27:54','2015-11-18 00:27:54','33fee68800'),
(7,'Make It Happen','2015-11-18 02:58:09','2015-11-18 02:58:09','c3820ca3e6'),
(8,'Market It','2015-11-18 03:06:41','2015-11-18 03:06:41','51abcb5ed6'),
(9,'Just Tell It','2015-11-18 03:07:00','2015-11-18 03:07:00','221442e726'),
(10,'Has Not Upgraded For Six Months','2015-11-19 04:18:28','2015-11-19 04:18:28','a5e4719647'),
(11,'This is just a test','2015-12-09 11:13:17','2015-12-09 11:13:17','bb3f22f789'),
(12,'Artisan Bucket','2015-12-09 11:18:54','2015-12-09 11:18:54','9a5df018ca'),
(13,'Artisan Bucket Again','2015-12-09 11:19:17','2015-12-09 11:19:17','8edd167200'),
(14,'Artisan Bucket Not a Package','2015-12-09 11:25:19','2015-12-09 11:25:19','9623e7d5bb');

/*Table structure for table `business_plan_documents` */

DROP TABLE IF EXISTS `business_plan_documents`;

CREATE TABLE `business_plan_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `doc_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `business_plan_id` int(11) NOT NULL,
  `business_plan_category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `filepath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `business_plan_documents` */

insert  into `business_plan_documents`(`id`,`filename`,`doc_type`,`business_plan_id`,`business_plan_category_id`,`created_at`,`updated_at`,`user_id`,`filepath`) values 
(1,'javamagazine20140304-dl.pdf','application/pdf',1,16,'2015-11-23 00:45:13','2015-11-23 00:45:13',1,'uploads/javamagazine20140304-dl.pdf'),
(2,'Resource-Guru-Overview.pdf','application/pdf',1,16,'2015-11-24 02:04:26','2015-11-24 02:04:26',1,'uploads/Resource-Guru-Overview.pdf'),
(3,'Resource-Guru-Overview.pdf','application/pdf',8,16,'2015-11-24 03:09:24','2015-11-24 03:09:24',1,'uploads/Resource-Guru-Overview.pdf');

/*Table structure for table `default_contents` */

DROP TABLE IF EXISTS `default_contents`;

CREATE TABLE `default_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `cat_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `default_contents` */

insert  into `default_contents`(`id`,`content`,`cat_id`,`created_at`,`updated_at`) values 
(9,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th>\r\n			<p>Goals</p>\r\n			</th>\r\n			<th>\r\n			<p>What steps will you take to achieve your goals/objectives</p>\r\n			</th>\r\n			<th>\r\n			<p>When do you expect to achieve your set goals/objectives?</p>\r\n			</th>\r\n			<th>\r\n			<p>Person responsible</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>What are the business goals/objectives?</p>\r\n			</td>\r\n			<td>\r\n			<p>Action steps. Step 1,2,3, etc.</p>\r\n			</td>\r\n			<td>\r\n			<p>Add dates</p>\r\n			</td>\r\n			<td>\r\n			<p>Who&rsquo;ll be responsible to make sure your goal is achieved?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',33,'2015-10-19 04:10:38','2015-10-19 04:10:38'),
(10,'<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>General Business Details</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Description</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business name:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Trading name:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Registered date:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Registered location:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business structure:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Your business number/s:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Location of premises:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Buy/lease agreements</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Relevant business taxes: are you registered? Yes or no?</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Licences and permits:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Domain/website names:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Memberships and affiliations:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business trading hours:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Add more&hellip;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Products/Services</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Products/services:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Add more&hellip;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Owner Details</strong></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Business owner(s):</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Relevant owner experience:</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Add more&hellip;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:14:07','2015-10-19 04:14:07'),
(11,'<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:99%\" summary=\"A table with space to enter details of current staff under the headings: Job Title (e.g. Marketing/ Sales Manager), Name (e.g. Mr Chris Brantley), Expected staff turnover (e.g. 12-18 months) and Skills or strengths (e.g. Relevant qualifications in Sales/Marketing. At least 5 years experience in the industry. Award in marketing excellence).\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:135px\">\r\n			<p>Name</p>\r\n			</th>\r\n			<th style=\"width:184px\">\r\n			<p>Job Title</p>\r\n			</th>\r\n			<th style=\"width:293px\">\r\n			<p>Skills/Qualifications or Strengths/Responsibilities</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:135px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:184px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:293px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:14:51','2015-10-19 04:14:51'),
(12,'<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:99%\" summary=\"A table with space to enter details of required staff under the headings: Job Title (e.g. Office Manager), Quantity (e.g. 1), Expected staff turnover (e.g. 2-3 years), Skills necessary (e.g. Relevant qualifications in Office Management. At least 2 years experience.), and date required (e.g. Month/Year).\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:97px\">\r\n			<p>Job Title</p>\r\n			</th>\r\n			<th style=\"width:97px\">\r\n			<p>Quantity</p>\r\n			</th>\r\n			<th style=\"width:267px\">\r\n			<p>Necessary Skills</p>\r\n			</th>\r\n			<th style=\"width:151px\">\r\n			<p>Date Required</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:97px\">\r\n			<p>What is the position you&rsquo;ll be looking to fill?</p>\r\n			</td>\r\n			<td style=\"width:97px\">\r\n			<p>How many of these positions will you need?</p>\r\n			</td>\r\n			<td style=\"width:267px\">\r\n			<p>List the experience or qualifications required</p>\r\n			</td>\r\n			<td style=\"width:151px\">\r\n			<p>When do you expect to need this position?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:15:07','2015-10-19 04:15:07'),
(13,'<table align=\"left\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th scope=\"col\">\r\n			<p>Product/Service</p>\r\n			</th>\r\n			<th scope=\"col\">\r\n			<p>Description</p>\r\n			</th>\r\n			<th scope=\"col\">\r\n			<p>Market Demand</p>\r\n			</th>\r\n			<th scope=\"col\">\r\n			<p>Distributor/Supplier</p>\r\n			</th>\r\n			<th scope=\"col\">\r\n			<p>Cost for Distributor/Supplier</p>\r\n			</th>\r\n			<th scope=\"col\">\r\n			<p>Price</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>Product/Service &ndash; include all necessary codes</p>\r\n			</td>\r\n			<td>\r\n			<p>Brief description</p>\r\n			</td>\r\n			<td>\r\n			<p>Anticipated demand from the market</p>\r\n			</td>\r\n			<td>\r\n			<p>Supplier/distributor details</p>\r\n			</td>\r\n			<td>\r\n			<p>Costs associated from Distributor/supplier</p>\r\n			</td>\r\n			<td>\r\n			<p>Include final retail price and all taxes</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:16:42','2015-10-19 04:25:02'),
(14,'<table align=\"left\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th scope=\"col\" style=\"width:250px\">\r\n			<p>Inventory Item</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:250px\">\r\n			<p>Cost Per Unit</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:250px\">\r\n			<p>Quantity on Hand</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:250px\">\r\n			<p>Total</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:250px\">\r\n			<p>List your inventory &ndash; include any necessary internal codes</p>\r\n			</td>\r\n			<td style=\"width:250px\">\r\n			<p>How much does each unit cost?</p>\r\n			</td>\r\n			<td style=\"width:250px\">\r\n			<p>How many units do you have on hand?</p>\r\n			</td>\r\n			<td style=\"width:250px\">\r\n			<p>What is the total cost?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:17:03','2015-10-19 04:25:25'),
(15,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th scope=\"col\" style=\"width: 160px;\">\r\n			<p>Product/Service</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 160px;\">\r\n			<p>Distribution Channel</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 160px;\">\r\n			<p>Reason for Chosen Channel</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 160px;\">\r\n			<p>Method of Sale</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 160px;\">\r\n			<p>Strengths</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 160px;\">\r\n			<p>Weaknesses</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:160px\">\r\n			<p>List the product/service</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Where your customers will be able to purchase your product/service, .e.g., shopfront, online</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Why you chose this channel, &nbsp;e.g., because it&rsquo;s where your target market is based</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>How you&rsquo;ll reach customers, e.g., salespeople actively selling door to door</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>List all strengths</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>List all weaknesses</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:17:16','2015-10-19 04:25:55'),
(16,'<table align=\"left\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:1031px\" summary=\"A table with space to enter details of products and services, including the product/service name, a brief description, and price (including GST).\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:217px\">\r\n			<p>Legal Requirement</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</th>\r\n			<th style=\"width:345px\">\r\n			<p>Description</p>\r\n			</th>\r\n			<th style=\"width:266px\">\r\n			<p>Date Completed</p>\r\n			</th>\r\n			<th style=\"width:203px\">\r\n			<p>Cost</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:217px\">\r\n			<p>What is the legal requirement, e.g., trademark application</p>\r\n			</td>\r\n			<td style=\"width:345px\">\r\n			<p>Give a brief description of your legal requirement. For example, do you need assistance from a lawyer to officially trademark your business name?</p>\r\n			</td>\r\n			<td style=\"width:266px\">\r\n			<p>What date do you intend to have this completed?</p>\r\n			</td>\r\n			<td style=\"width:203px\">\r\n			<p>Detail any costs associated, such as lawyer fees</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:17:34','2015-10-19 04:17:34'),
(17,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th scope=\"col\" style=\"width: 200px;\">\r\n			<p>Insurance Type</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 200px;\">\r\n			<p>Description/Coverage</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 200px;\">\r\n			<p>Insurance Broker/Company/Contact Details</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 200px;\">\r\n			<p>Premiums/Cost</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width: 200px;\">\r\n			<p>Renewal Date</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:200px\">\r\n			<p>Example: professional indemnity insurance</p>\r\n			</td>\r\n			<td style=\"width:200px\">\r\n			<p>Give a brief description of the cover</p>\r\n			</td>\r\n			<td style=\"width:200px\">\r\n			<p>Who is your policy with?</p>\r\n			</td>\r\n			<td style=\"width:200px\">\r\n			<p>What are the costs associated with the policy?</p>\r\n			</td>\r\n			<td style=\"width:200px\">\r\n			<p>When is your policy due for renewal?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:17:50','2015-10-19 04:26:19'),
(18,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th>\r\n			<p>Potential Risk</p>\r\n			</th>\r\n			<th>\r\n			<p>Level of Severity</p>\r\n			</th>\r\n			<th>\r\n			<p>Likelihood</p>\r\n			</th>\r\n			<th>\r\n			<p>Impact</p>\r\n			</th>\r\n			<th>\r\n			<p>Strategy to Avoid/Manage</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>What is the potential risk to your business?</p>\r\n			</td>\r\n			<td>\r\n			<p>How severe could the damage be (minor, moderate, significant)?</p>\r\n			</td>\r\n			<td>\r\n			<p>What is the likelihood this risk could occur (low, medium, high)?</p>\r\n			</td>\r\n			<td>\r\n			<p>What is the impact of the risk to your business (minor, moderate, significant)?</p>\r\n			</td>\r\n			<td>\r\n			<p>What strategies will you put in place to try and avoid the risk?</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:18:46','2015-10-19 04:24:00'),
(19,'<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:277px\">\r\n			<p>Software and Programs</p>\r\n			</th>\r\n			<th style=\"width:126px\">\r\n			<p>Purchase Date</p>\r\n			</th>\r\n			<th style=\"width:119px\">\r\n			<p>Purchase Price</p>\r\n			</th>\r\n			<th style=\"width:97px\">\r\n			<p>Updating or Running Costs</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:277px\">\r\n			<p>File sharing programs/email systems</p>\r\n			</td>\r\n			<td style=\"width:126px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:119px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:97px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:277px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:126px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:119px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:97px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:267px\">\r\n			<p>Potential IT Issues/Required Updates or Maintenance</p>\r\n			</th>\r\n			<th style=\"width:158px\">\r\n			<p>Person Responsible</p>\r\n			</th>\r\n			<th style=\"width:176px\">\r\n			<p>Dates of Scheduled Updates/Maintenance</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:267px\">\r\n			<p>Example: update all anti-virus software</p>\r\n			</td>\r\n			<td style=\"width:158px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:176px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:267px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:158px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:176px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:19:40','2015-10-19 04:26:54'),
(20,'<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\" summary=\"A table with space to enter details of plant and equipment purchases under the headings: Equipment (e.g Personal Computer), purchase date (e.g. 20/03/2010), purchase price (e.g $2100) and running cost (e.g $100 a month).\">\r\n	<thead>\r\n		<tr>\r\n			<th style=\"width:269px\">\r\n			<p>Equipment</p>\r\n			</th>\r\n			<th style=\"width:122px\">\r\n			<p>Purchase Date</p>\r\n			</th>\r\n			<th style=\"width:115px\">\r\n			<p>Purchase Price</p>\r\n			</th>\r\n			<th style=\"width:95px\">\r\n			<p>Running Cost</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:269px\">\r\n			<p>Examples: machinery/printers/computers</p>\r\n			</td>\r\n			<td style=\"width:122px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:115px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:95px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:19:56','2015-10-19 04:19:56'),
(21,'<table align=\"left\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Name</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Date Established</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Business Size </strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Estimated Turnover/Market Share</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Value to Customer</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Business Strengths</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Business Weaknesses</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Competitor name</p>\r\n			</td>\r\n			<td>\r\n			<p>When did they enter the market?</p>\r\n			</td>\r\n			<td>\r\n			<p>Staff numbers, sites, offices, stores etc.</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>Estimated revenue and market share of competitor</p>\r\n			</td>\r\n			<td>\r\n			<p>Niche product/service, time in market etc.</p>\r\n			</td>\r\n			<td>\r\n			<p>Example: convenience to customer</p>\r\n			</td>\r\n			<td>\r\n			<p>Limited offering, cost of product/service etc.</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:22:31','2015-10-19 04:22:31'),
(22,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"height:30px; width:497px\">\r\n			<p><strong>STRENGTHS</strong></p>\r\n			</td>\r\n			<td style=\"height:30px; width:497px\">\r\n			<p><strong>WEAKNESSES</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:34px; width:497px\">\r\n			<p>What you&rsquo;re good at</p>\r\n			</td>\r\n			<td style=\"height:34px; width:497px\">\r\n			<p>What you&rsquo;re not so good at</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:29px; width:497px\">\r\n			<p><strong>OPPORTUNITIES</strong></p>\r\n			</td>\r\n			<td style=\"height:29px; width:497px\">\r\n			<p><strong>THREATS</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:36px; width:497px\">\r\n			<p>Areas of growth for your business that will give you an edge over competitors</p>\r\n			</td>\r\n			<td style=\"height:36px; width:497px\">\r\n			<p>What will influence your business growth/success?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:23:09','2015-10-19 04:23:09'),
(23,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>Marketing and Media Activity</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>Target Product/Service</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>Reason for Chosen Channel</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>Measurement of Success</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>Total Budget Allocation ($)</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>When</p>\r\n			</th>\r\n			<th scope=\"col\" style=\"width:140px\">\r\n			<p>Person Responsible</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:140px\">\r\n			<p>AdWords, radio, giveaways, launch, media release etc.</p>\r\n			</td>\r\n			<td style=\"width:140px\">\r\n			<p>&nbsp;</p>\r\n			</td>\r\n			<td style=\"width:140px\">\r\n			<p>E.g. product launch, sales, brand awareness</p>\r\n			</td>\r\n			<td style=\"width:140px\">\r\n			<p>E.g. Increase sales by 20%</p>\r\n			</td>\r\n			<td style=\"width:140px\">\r\n			<p>$</p>\r\n			</td>\r\n			<td style=\"width:140px\">\r\n			<p>Insert campaign dates</p>\r\n			</td>\r\n			<td style=\"width:140px\">\r\n			<p>Who will be responsible for carrying out the campaign?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',0,'2015-10-19 04:23:38','2015-10-19 04:24:36');

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `jobs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values 
('2014_10_12_000000_create_users_table',1),
('2014_10_12_100000_create_password_resets_table',1),
('2015_10_15_045303_create_business_plan_categories_table',2),
('2015_10_15_054230_create_business_plan_contents_table',3),
('2015_10_15_054849_CreateBusinessContentCategoriesTable',4),
('2015_10_15_064134_AddStatusColumnToBusinessPlanContentTable',4),
('2015_10_15_070308_AddUserIdToBusinessPlanContentsTable',5),
('2015_10_15_071014_create_business_plan_tips_table',6),
('2015_10_15_071140_AddTipIdToBusinessPlanCategoryTable',7),
('2015_10_15_114545_AddCatIdToTipsTable',8),
('2015_10_16_010434_create_business_plans_table',9),
('2015_10_16_022752_AddCatIdToContentsTable',9),
('2015_10_16_134046_AddNameToBusinessPlansTable',9),
('2015_10_16_134234_AddBusinessPlanIdToContentTable',9),
('2015_10_16_160141_create_business_plan_documents_table',10),
('2015_10_17_135105_AddTypeColumnToBusinessCategoriesTable',11),
('2015_10_17_144519_AddUserIdToFileUpload',12),
('2015_10_17_154239_create_business_plan_default_contents_table',13),
('2015_10_19_033916_AddDefaultContentIdToCategoryTable',14),
('2015_10_19_040156_ChangeContentColumnOnDefaultContentTable',15),
('2015_10_23_054503_create_packages_table',16),
('2015_10_24_091052_create_user_addresses_table',16),
('2015_10_24_092210_AddAddressColumnToUserTable',16),
('2015_10_25_065824_create_package_user_pivot_table',16),
('2015_10_25_081301_create_buckets_table',16),
('2015_10_25_081701_create_bucket_user_pivot_table',16),
('2013_04_09_062329_create_revision_table',17),
('2015_10_26_123813_ChangePackagesDescriptionColumn',17),
('2015_10_26_132115_create_appointments_table',17),
('2015_10_30_043243_add_availability_to_appointments_table',18),
('2015_10_30_135044_add_appointment_type_to_appointments',19),
('2015_11_04_011121_create_marketing_plans_table',20),
('2015_11_04_011704_AddPolyRelationToContentsTable',20),
('2015_11_04_021504_RenameKeyTables',20),
('2015_11_04_023611_ClearBPIDFromContentsTable',20),
('2015_11_13_043957_AddFieldMailchimpListId',21),
('2015_11_18_015701_create_jobs_table',22),
('2015_11_18_033424_create_failed_jobs_table',22),
('2015_11_19_010213_AddColumnUpgradedAtToUsersTable',23),
('2015_11_19_051630_AddColumnMailchimpMemberHashToUsers',24),
('2015_11_18_094057_AddPlanTypeToCategoryTable',25),
('2015_11_18_114438_create_pages_table',25),
('2015_11_18_114439_create_page_closures_table',25),
('2015_11_18_120022_add_slug_to_pages_table',25),
('2015_11_18_144754_AddFilepathToMarketingPlansTable',25),
('2015_11_19_164912_CreateTableBriefingFormQuestions',26),
('2015_11_19_182209_AddFieldBriefedToUsersTable',26),
('2015_11_19_190313_AddFilePathToBPDocs',26),
('2015_11_20_045655_CreateTableBriefingFormSubmission',26),
('2015_11_20_045729_CreateTableBriefingQuestionAnswers',26),
('2015_11_20_045759_CreateTableBriefingFormDocuments',26),
('2015_11_23_015057_AddFileFieldsToBriefingFormDocumentsTable',27),
('2015_11_23_061823_CreatePlansTable',28),
('2015_11_23_062003_AddPlanTypeToPlansTable',29),
('2015_11_23_071046_AddFilepathToPlansTable',30),
('2015_11_24_033012_DropTableBusinessPlan',31),
('2015_11_24_033034_DropTableMarketingPlan',31),
('2015_11_24_034531_AddPlanIDToPlanContentsTable',32),
('2015_11_24_053301_DropColumnsContentablesFromPlanContentsTable',33),
('2014_02_10_145728_notification_categories',34),
('2014_08_01_210813_create_notification_groups_table',34),
('2014_08_01_211045_create_notification_category_notification_group_table',34),
('2015_05_05_212549_create_notifications_table',34),
('2015_06_06_211555_add_expire_time_column_to_notification_table',34),
('2015_06_06_211555_change_type_to_extra_in_notifications_table',34),
('2015_06_07_211555_alter_category_name_to_unique',34),
('2015_12_03_232639_SuperAdminFlagToUserTable',35),
('2015_12_06_182714_DashboardRouteNameForPackages',35),
('2015_12_08_112300_ChangeDashesToUnderscores',36),
('2015_12_08_225039_AddEmailedFlagToBriefingFormSubmission',37);

/*Table structure for table `notification_categories` */

DROP TABLE IF EXISTS `notification_categories`;

CREATE TABLE `notification_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notification_categories_name_unique` (`name`),
  KEY `notification_categories_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notification_categories` */

insert  into `notification_categories`(`id`,`name`,`text`) values 
(1,'user.pdf','Your PDF has been generated'),
(2,'user.brief','Briefing Session booked'),
(3,'user.review','Review Session booked'),
(4,'user.created','Signed up new user'),
(5,'user.upgrade','User has upgraded to a new package');

/*Table structure for table `notification_groups` */

DROP TABLE IF EXISTS `notification_groups`;

CREATE TABLE `notification_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notification_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notification_groups` */

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_id` bigint(20) unsigned NOT NULL,
  `from_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_id` bigint(20) unsigned NOT NULL,
  `to_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expire_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_from_id_index` (`from_id`),
  KEY `notifications_from_type_index` (`from_type`),
  KEY `notifications_to_id_index` (`to_id`),
  KEY `notifications_to_type_index` (`to_type`),
  KEY `notifications_category_id_index` (`category_id`),
  CONSTRAINT `notifications_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `notification_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`from_id`,`from_type`,`to_id`,`to_type`,`category_id`,`url`,`extra`,`read`,`created_at`,`updated_at`,`expire_time`) values 
(1,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 01:28:58','2015-11-25 01:28:58',NULL),
(2,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 03:39:22','2015-11-25 03:39:22',NULL),
(3,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 03:41:06','2015-11-25 03:41:06',NULL),
(4,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 03:41:13','2015-11-25 03:41:13',NULL),
(5,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 03:52:47','2015-11-25 03:52:47',NULL),
(6,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 03:54:18','2015-11-25 03:54:18',NULL),
(7,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 03:54:29','2015-11-25 03:54:29',NULL),
(8,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:01:49','2015-11-25 04:01:49',NULL),
(9,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:02:30','2015-11-25 04:02:30',NULL),
(10,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:06:21','2015-11-25 04:06:21',NULL),
(11,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:08:42','2015-11-25 04:08:42',NULL),
(12,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:18:00','2015-11-25 04:18:00',NULL),
(13,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:18:29','2015-11-25 04:18:29',NULL),
(14,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:24:53','2015-11-25 04:24:53',NULL),
(15,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:32:22','2015-11-25 04:32:22',NULL),
(16,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:33:07','2015-11-25 04:33:07',NULL),
(17,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:34:16','2015-11-25 04:34:16',NULL),
(18,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:34:46','2015-11-25 04:34:46',NULL),
(19,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:47:34','2015-11-25 04:47:34',NULL),
(20,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 04:48:12','2015-11-25 04:48:12',NULL),
(21,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 05:51:46','2015-11-25 05:51:46',NULL),
(22,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 06:01:38','2015-11-25 06:01:38',NULL),
(23,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 06:02:26','2015-11-25 06:02:26',NULL),
(24,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 06:04:27','2015-11-25 06:04:27',NULL),
(25,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-25 06:58:12','2015-11-25 06:58:12',NULL),
(26,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-26 23:56:43','2015-11-26 23:56:43',NULL),
(27,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-26 23:57:19','2015-11-26 23:57:19',NULL),
(28,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 01:28:26','2015-11-27 01:28:26',NULL),
(29,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 02:46:55','2015-11-27 02:46:55',NULL),
(30,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 02:46:57','2015-11-27 02:46:57',NULL),
(31,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 02:47:08','2015-11-27 02:47:08',NULL),
(32,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 03:04:22','2015-11-27 03:04:22',NULL),
(33,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 03:15:09','2015-11-27 03:15:09',NULL),
(34,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 03:16:52','2015-11-27 03:16:52',NULL),
(35,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 03:20:53','2015-11-27 03:20:53',NULL),
(36,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 03:21:52','2015-11-27 03:21:52',NULL),
(37,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 03:34:46','2015-11-27 03:34:46',NULL),
(38,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 04:11:35','2015-11-27 04:11:35',NULL),
(39,1,NULL,1,NULL,1,'http://gjm.dev',NULL,0,'2015-11-27 04:11:56','2015-11-27 04:11:56',NULL),
(40,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-11-30 03:08:35','2015-11-30 03:08:35',NULL),
(41,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-11-30 03:20:16','2015-11-30 03:20:16',NULL),
(42,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-11-30 03:54:56','2015-11-30 03:54:56',NULL),
(43,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-11-30 03:58:33','2015-11-30 03:58:33',NULL),
(44,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-11-30 03:58:49','2015-11-30 03:58:49',NULL),
(45,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-12-08 15:21:08','2015-12-08 15:21:08',NULL),
(46,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-12-08 15:26:58','2015-12-08 15:26:58',NULL),
(47,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-12-08 15:27:32','2015-12-08 15:27:32',NULL),
(48,1,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 14:06:25','2015-12-09 14:06:25',NULL),
(49,1,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 14:09:05','2015-12-09 14:09:05',NULL),
(50,1,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 14:29:27','2015-12-09 14:29:27',NULL),
(51,1,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 14:31:07','2015-12-09 14:31:07',NULL),
(52,122,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 14:52:24','2015-12-09 14:52:24',NULL),
(53,123,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 14:57:18','2015-12-09 14:57:18',NULL),
(54,125,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 15:01:38','2015-12-09 15:01:38',NULL),
(55,125,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:01:38','2015-12-09 15:01:38',NULL),
(56,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-12-09 15:02:35','2015-12-09 15:02:35',NULL),
(57,1,NULL,1,NULL,1,'Pdf generated',NULL,0,'2015-12-09 15:02:43','2015-12-09 15:02:43',NULL),
(58,126,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 15:05:51','2015-12-09 15:05:51',NULL),
(59,126,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:05:51','2015-12-09 15:05:51',NULL),
(60,128,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 15:11:12','2015-12-09 15:11:12',NULL),
(61,128,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:11:12','2015-12-09 15:11:12',NULL),
(62,129,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 15:14:40','2015-12-09 15:14:40',NULL),
(63,129,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:14:40','2015-12-09 15:14:40',NULL),
(64,130,NULL,1,NULL,4,'New user created',NULL,0,'2015-12-09 15:20:12','2015-12-09 15:20:12',NULL),
(65,130,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:20:13','2015-12-09 15:20:13',NULL),
(66,130,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:33:39','2015-12-09 15:33:39',NULL),
(67,130,NULL,1,NULL,5,'User upgraded to a new package',NULL,0,'2015-12-09 15:35:11','2015-12-09 15:35:11',NULL);

/*Table structure for table `notifications_categories_in_groups` */

DROP TABLE IF EXISTS `notifications_categories_in_groups`;

CREATE TABLE `notifications_categories_in_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_categories_in_groups_category_id_index` (`category_id`),
  KEY `notifications_categories_in_groups_group_id_index` (`group_id`),
  CONSTRAINT `notifications_categories_in_groups_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `notification_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `notifications_categories_in_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `notification_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notifications_categories_in_groups` */

/*Table structure for table `package_user` */

DROP TABLE IF EXISTS `package_user`;

CREATE TABLE `package_user` (
  `package_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`package_id`,`user_id`),
  KEY `package_user_package_id_index` (`package_id`),
  KEY `package_user_user_id_index` (`user_id`),
  CONSTRAINT `package_user_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `package_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `package_user` */

insert  into `package_user`(`package_id`,`user_id`) values 
(1,1),
(1,130),
(2,1),
(2,130),
(3,1),
(3,130);

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dashboard_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `packages` */

insert  into `packages`(`id`,`name`,`price`,`description`,`created_at`,`updated_at`,`dashboard_url`) values 
(1,'Make It Happen','250.00','','2015-11-18 02:58:07','2015-11-18 02:58:07','business-plan-dash'),
(2,'Market It','250.00','','2015-11-18 03:06:40','2015-11-18 03:06:40','marketing-channel-dashboard'),
(3,'Just Tell It','250.00','','2015-11-18 03:06:59','2015-11-18 03:06:59','marketing-plan-dash'),
(5,'Artisan Bucket','350.00','','2015-12-09 11:18:52','2015-12-09 11:18:52','marketing-plan-dash'),
(6,'Artisan Bucket Again','450.00','','2015-12-09 11:19:15','2015-12-09 11:19:15','marketing-plan-dash');

/*Table structure for table `page_closure` */

DROP TABLE IF EXISTS `page_closure`;

CREATE TABLE `page_closure` (
  `closure_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ancestor` int(10) unsigned NOT NULL,
  `descendant` int(10) unsigned NOT NULL,
  `depth` int(10) unsigned NOT NULL,
  PRIMARY KEY (`closure_id`),
  KEY `page_closure_ancestor_foreign` (`ancestor`),
  KEY `page_closure_descendant_foreign` (`descendant`),
  CONSTRAINT `page_closure_ancestor_foreign` FOREIGN KEY (`ancestor`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `page_closure_descendant_foreign` FOREIGN KEY (`descendant`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `page_closure` */

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL,
  `real_depth` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `package_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_parent_id_foreign` (`parent_id`),
  CONSTRAINT `pages_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `pages` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pages` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `plan_categories` */

DROP TABLE IF EXISTS `plan_categories`;

CREATE TABLE `plan_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tip_id` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_content_id` int(11) NOT NULL,
  `plan_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_plan_categories_parent_id_index` (`parent_id`),
  KEY `business_plan_categories_lft_index` (`lft`),
  KEY `business_plan_categories_rgt_index` (`rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `plan_categories` */

insert  into `plan_categories`(`id`,`parent_id`,`lft`,`rgt`,`depth`,`name`,`created_at`,`updated_at`,`tip_id`,`content_type`,`default_content_id`,`plan_type`) values 
(9,NULL,1,98,0,'Business Plan Root','2015-10-15 05:07:53','2015-10-19 03:55:40',0,'text',5,'business'),
(10,9,2,3,1,'Executive Summary','2015-10-15 05:09:22','2015-10-17 11:15:13',1,'text',1,'business'),
(11,9,4,27,1,'The Business Summary','2015-10-15 05:10:42','2015-10-15 12:11:59',0,'text',0,'business'),
(12,9,28,67,1,'The Business Operations','2015-10-15 05:10:47','2015-10-15 12:38:14',6,'text',0,'business'),
(13,9,68,83,1,'The Market','2015-10-15 05:10:57','2015-10-15 10:32:59',0,'text',0,'business'),
(14,9,84,93,1,'The Financials','2015-10-15 05:11:03','2015-10-15 10:32:59',0,'text',0,'business'),
(15,9,94,95,1,'Research and Development(R&D)/Innovation Activities','2015-10-15 05:11:16','2015-10-18 15:57:09',33,'text',0,'business'),
(16,9,96,97,1,'Appendix/Supporting Documents','2015-10-15 05:11:26','2015-10-15 10:32:59',0,'document',0,'business'),
(24,12,29,30,2,'Business and Registration Details','2015-10-15 09:22:19','2015-10-19 04:14:07',6,'text',10,'business'),
(25,12,31,38,2,'Organisational Structure','2015-10-15 09:22:30','2015-10-18 15:49:02',7,'text',0,'business'),
(26,12,39,48,2,'Product and Service','2015-10-15 09:22:37','2015-10-18 15:50:00',11,'text',0,'business'),
(27,12,49,58,2,'Legal and Accounting','2015-10-15 09:23:09','2015-10-15 10:32:59',0,'text',0,'business'),
(29,12,61,66,2,'Technology and Equipment','2015-10-15 09:23:22','2015-10-15 10:32:59',0,'text',0,'business'),
(30,11,19,20,2,'What is your Business?','2015-10-15 09:25:02','2015-10-18 15:47:27',2,'text',0,'business'),
(31,11,21,22,2,'Vision','2015-10-15 09:25:08','2015-10-18 15:47:50',3,'text',0,'business'),
(32,11,23,24,2,'Mission','2015-10-15 09:25:13','2015-10-18 15:48:06',4,'text',0,'business'),
(33,11,25,26,2,'Goals and Action Plan','2015-10-15 09:25:17','2015-10-19 04:10:38',5,'text',9,'business'),
(34,13,69,70,2,'Market Summary','2015-10-15 09:26:10','2015-10-18 15:53:54',22,'text',0,'business'),
(35,13,71,78,2,'Target Market','2015-10-15 09:26:16','2015-10-18 15:54:09',23,'text',0,'business'),
(36,13,79,80,2,'SWOT Analysis','2015-10-15 09:26:22','2015-10-19 04:23:10',26,'text',22,'business'),
(37,13,81,82,2,'Marketing and Media Plan','2015-10-15 09:26:27','2015-10-19 04:23:39',28,'text',23,'business'),
(38,25,32,33,3,'Key Personnel','2015-10-15 09:27:08','2015-10-19 04:14:51',8,'text',11,'business'),
(39,25,34,35,3,'Required Personnel','2015-10-15 09:27:16','2015-10-19 04:15:07',9,'text',12,'business'),
(40,25,36,37,3,'Recruitment and Training','2015-10-15 09:27:24','2015-10-19 05:03:53',10,'text',0,'business'),
(41,14,85,92,2,'Key Objectives and Financial Review','2015-10-15 10:09:33','2015-10-18 15:55:51',29,'text',0,'business'),
(42,41,86,87,3,'Profit and Loss Forecast','2015-10-15 10:09:59','2015-10-18 15:56:11',30,'text',0,'business'),
(43,41,88,89,3,'Balance Sheet Forecast','2015-10-15 10:10:07','2015-10-18 15:56:29',31,'text',0,'business'),
(44,41,90,91,3,'Expected Cash Flow','2015-10-15 10:10:14','2015-10-18 15:56:53',32,'text',0,'business'),
(45,35,72,73,3,'Market Research','2015-10-15 10:11:00','2015-10-18 15:54:24',24,'text',0,'business'),
(46,35,74,75,3,'Competitors','2015-10-15 10:11:07','2015-10-19 04:22:31',25,'text',21,'business'),
(47,35,76,77,3,'Unique Selling Position','2015-10-15 10:11:14','2015-10-18 15:55:02',27,'text',0,'business'),
(48,27,50,51,3,'Legal','2015-10-15 10:28:20','2015-10-19 04:17:35',16,'text',16,'business'),
(49,27,52,53,3,'Accounting','2015-10-15 10:31:41','2015-10-18 15:52:08',17,'text',0,'business'),
(50,27,54,55,3,'Insurance','2015-10-15 10:31:49','2015-10-19 04:17:50',18,'text',17,'business'),
(51,27,56,57,3,'Risk Management and Security','2015-10-15 10:32:05','2015-10-19 04:18:46',19,'text',18,'business'),
(52,29,62,63,3,'IT Infrastructure','2015-10-15 10:32:14','2015-10-19 04:19:40',20,'text',19,'business'),
(53,29,64,65,3,'Equipment','2015-10-15 10:32:21','2015-10-19 04:19:56',21,'text',20,'business'),
(54,26,40,41,3,'Product/Service and Pricing','2015-10-15 10:32:32','2015-10-19 04:16:42',12,'text',13,'business'),
(55,26,42,43,3,'Inventory','2015-10-15 10:32:46','2015-10-19 04:17:03',13,'text',14,'business'),
(56,26,44,45,3,'Distribution','2015-10-15 10:32:53','2015-10-19 04:17:16',14,'text',15,'business'),
(57,26,46,47,3,'Location','2015-10-15 10:32:59','2015-10-18 15:51:05',15,'text',0,'business'),
(58,NULL,99,140,0,'Marketing Plan Root','2015-11-18 09:04:09','2015-11-18 10:44:27',0,'text',0,'marketing'),
(59,58,100,101,1,'Executive Summary','2015-11-18 09:36:55','2015-11-18 10:27:27',0,'text',0,'marketing'),
(61,58,104,105,1,'Vision Statement','2015-11-18 10:40:39','2015-11-18 10:40:40',0,'text',0,'marketing'),
(62,58,106,107,1,'Mission Statement','2015-11-18 10:40:48','2015-11-18 10:40:48',0,'text',0,'marketing'),
(63,58,108,123,1,'The Market','2015-11-18 10:40:54','2015-11-18 10:43:24',0,'text',0,'marketing'),
(64,58,124,125,1,'Sales and Distribution Channels','2015-11-18 10:41:02','2015-11-18 10:43:24',0,'text',0,'marketing'),
(65,58,126,127,1,'Marketing and Media Goals & Objectives','2015-11-18 10:41:13','2015-11-18 10:43:24',0,'text',0,'marketing'),
(66,58,128,129,1,'Marketing and Media Plans','2015-11-18 10:41:21','2015-11-18 10:43:24',0,'text',0,'marketing'),
(67,58,130,131,1,'Marketing and Media Budget','2015-11-18 10:41:28','2015-11-18 10:43:24',0,'text',0,'marketing'),
(68,58,132,137,1,'Timeline of Activity','2015-11-18 10:41:55','2015-11-18 10:44:27',0,'text',0,'marketing'),
(69,58,138,139,1,'Monitoring and Measurement of Marketing and Media Activities','2015-11-18 10:42:11','2015-11-18 10:44:27',0,'text',0,'marketing'),
(70,63,109,110,2,'Market Summary','2015-11-18 10:42:41','2015-11-18 10:42:42',0,'text',0,'marketing'),
(71,63,111,112,2,'Target Market','2015-11-18 10:42:47','2015-11-18 10:42:48',0,'text',0,'marketing'),
(72,63,113,114,2,'Market Research','2015-11-18 10:42:55','2015-11-18 10:42:56',0,'text',0,'marketing'),
(73,63,115,116,2,'Competitors','2015-11-18 10:43:02','2015-11-18 10:43:03',0,'text',0,'marketing'),
(74,63,117,118,2,'SWOT Analysis','2015-11-18 10:43:08','2015-11-18 10:43:09',0,'text',0,'marketing'),
(75,63,119,120,2,'Unique Selling Position','2015-11-18 10:43:16','2015-11-18 10:43:16',0,'text',0,'marketing'),
(76,63,121,122,2,'Brand Position/Statement','2015-11-18 10:43:24','2015-11-18 10:43:25',0,'text',0,'marketing'),
(77,68,133,134,2,'Marketing and Media Plan','2015-11-18 10:44:18','2015-11-18 10:44:18',0,'text',0,'marketing'),
(78,68,135,136,2,'Digital/Social Media Content Plan','2015-11-18 10:44:27','2015-11-18 10:44:28',0,'text',0,'marketing');

/*Table structure for table `plan_contents` */

DROP TABLE IF EXISTS `plan_contents`;

CREATE TABLE `plan_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `plan_contents` */

insert  into `plan_contents`(`id`,`content`,`created_at`,`updated_at`,`status`,`user_id`,`cat_id`,`plan_id`) values 
(24,'This is a test section.','0000-00-00 00:00:00','0000-00-00 00:00:00',1,1,30,0),
(25,'<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<thead>\r\n		<tr>\r\n			<th>\r\n			<p>Goals</p>\r\n			</th>\r\n			<th>\r\n			<p>What steps will you take to achieve your goals/objectives</p>\r\n			</th>\r\n			<th>\r\n			<p>When do you expect to achieve your set goals/objectives?</p>\r\n			</th>\r\n			<th>\r\n			<p>Person responsible</p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>What are the business goals/objectives?</p>\r\n			</td>\r\n			<td>\r\n			<p>Action steps. Step 1,2,3, etc.</p>\r\n			</td>\r\n			<td>\r\n			<p>Add dates</p>\r\n			</td>\r\n			<td>\r\n			<p>Who&rsquo;ll be responsible to make sure your goal is achieved?</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n','2015-10-19 05:43:51','2015-10-19 05:43:51',1,1,33,0),
(26,'<p>This is my executive summary.</p>\r\n','2015-10-19 07:20:47','2015-10-19 07:20:47',1,1,10,0),
(32,'<p>test</p>\r\n','2015-11-17 02:33:06','2015-11-17 02:33:06',0,1,0,0),
(38,'<p>testing</p>\r\n','2015-11-24 02:00:24','2015-11-24 04:08:35',0,1,59,3),
(39,'<p>Students</p>\r\n','2015-11-24 02:00:48','2015-11-24 02:01:20',1,1,71,3),
(40,'<p>test</p>\r\n','2015-11-24 02:02:45','2015-11-24 02:02:45',0,1,59,10),
(41,'<p>ffadsf</p>\r\n','2015-11-24 03:34:58','2015-11-24 03:34:58',0,1,72,4),
(42,'<p>adsfdas</p>\r\n','2015-11-24 03:35:03','2015-11-24 03:35:03',0,1,59,4),
(45,'<p>vision statement for 3</p>\r\n','2015-11-24 04:09:22','2015-11-24 04:09:22',0,1,61,3),
(48,'<p>Fhhjdsfghccdghh</p>\r\n','2015-11-25 07:01:10','2015-11-25 07:01:10',0,1,62,3),
(54,'<p>test</p>\r\n','2015-11-27 01:32:25','2015-11-27 01:32:25',1,1,30,75),
(55,'<p>test</p>\r\n','2015-11-27 01:32:38','2015-11-27 01:32:38',1,1,31,75),
(56,'<p>test</p>\r\n','2015-11-27 01:50:25','2015-11-27 01:50:25',1,1,30,78),
(57,'<p>test</p>\r\n','2015-11-27 01:50:25','2015-11-27 01:50:25',1,1,31,78),
(58,'<p>test</p>\r\n','2015-11-27 01:51:16','2015-11-27 01:51:16',1,1,30,79),
(59,'<p>test</p>\r\n','2015-11-27 01:51:16','2015-11-27 01:51:16',1,1,31,79),
(60,'<p>asdf dasfdasfdasfdasfas</p>\r\n','2015-11-27 01:52:26','2015-11-27 01:52:26',1,1,30,80),
(61,'<p>asasd fdas das</p>\r\n','2015-11-27 01:52:35','2015-11-27 01:52:35',1,1,32,80),
(62,'<p>adsfdasf dasf das</p>\r\n','2015-11-27 01:52:41','2015-11-27 01:52:41',1,1,31,80),
(63,'<p>adsadsf dasf</p>\r\n','2015-11-27 01:52:47','2015-11-27 01:52:47',1,1,30,80),
(64,'<p>asdsdfs</p>\r\n','2015-11-27 01:53:50','2015-11-27 01:53:50',0,1,25,80);

/*Table structure for table `plan_tips` */

DROP TABLE IF EXISTS `plan_tips`;

CREATE TABLE `plan_tips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tip` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `plan_tips` */

insert  into `plan_tips`(`id`,`tip`,`created_at`,`updated_at`,`cat_id`) values 
(1,'<p>Don&rsquo;t write this now. The Executive Summary should be written last as it&rsquo;s the most important part of your plan. It should summarise your business and is designed to capture the reader&rsquo;s attention in a few sharp, pointed and perfectly shaped sentences, getting them excited about your business.</p>\r\n\r\n<p>Think about:</p>\r\n\r\n<ul>\r\n	<li>Who you are</li>\r\n	<li>What you do</li>\r\n	<li>What your business aims to achieve and the steps you&rsquo;ll take to get there</li>\r\n</ul>\r\n\r\n<p>If you&rsquo;re seeking financing or an investor, this is your first opportunity to grab interest. It should highlight the strengths of your business. If you&rsquo;re just starting out, you won&rsquo;t have as much info as an established company, so focus on your own experience.</p>\r\n\r\n<p>You want to prove how and why your business will be successful and how you&rsquo;ll get there. Remember to think of location; the current market and opportunities; your customer base; and who&rsquo;s involved in your business. The simple who, what, when, where, why and how come into play here.&nbsp;</p>\r\n','2015-10-17 11:15:13','2015-10-17 11:15:13',10),
(2,'<p>Okay, lets describe what your business is.</p>\r\n\r\n<p>Look to provide:</p>\r\n\r\n<ul>\r\n	<li>An overview of your business</li>\r\n	<li>What you&rsquo;re selling, i.e., product, service, or combination of the two?</li>\r\n	<li>Will you have a physical store/online store or wholesale?</li>\r\n	<li>Will you have an office/warehouse?</li>\r\n	<li>Will you need staff/contractors/consultants?</li>\r\n</ul>\r\n\r\n<p>Provide a description of what your idea of success is. Will your product/service be offering your customers/clients with:</p>\r\n\r\n<ul>\r\n	<li>A niche product/service not currently in your market/industry?</li>\r\n	<li>Something price driven, i.e., cheaper than your competitors?</li>\r\n	<li>A premium product or service that isn&rsquo;t currently available?</li>\r\n</ul>\r\n','2015-10-18 15:47:27','2015-10-19 05:31:56',30),
(3,'<p>Now it&rsquo;s time to describe your business potential and your goals for now and the future. It must be clear, concise and leave everyone inspired, wanting to know more. It doesn&rsquo;t matter how many words you use; it&rsquo;s the quality of the words that count. &nbsp;</p>\r\n\r\n<p>Think about how you want others to perceive your business and the reasons why you&rsquo;re creating the business. Look to include your customers, suppliers, competitors and employees.</p>\r\n\r\n<p>The vision must be a long-term view&mdash;you should never have to change your vision statement, no matter how much you grow.</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Keep it short and concise</li>\r\n	<li>Make sure it&rsquo;s specific to your business</li>\r\n	<li>Make sure people internally and externally understand it</li>\r\n	<li>Use passion and emotion&mdash;it should excite the reader</li>\r\n</ul>\r\n\r\n<p><strong>Some examples for you:</strong></p>\r\n\r\n<ul>\r\n	<li>Nike: (source: <a href=\"http://about.nike.com/\">http://about.nike.com/</a>)</li>\r\n	<li>Avon (source: <a href=\"http://www.avon.com.ph/PRSuite/our_vision.page\">http://www.avon.com.ph/PRSuite/our_vision.page</a>)</li>\r\n</ul>\r\n','2015-10-18 15:47:49','2015-10-18 15:47:49',31),
(4,'<p>How are you going to accomplish your vision? This is where the Mission Statement comes in.</p>\r\n\r\n<p>You should describe the company&rsquo;s function, the markets you&rsquo;re targeting and your competitive advantage. The mission should describe your business, its customers, employees and the community. How will you achieve your vision statement? It&rsquo;s time to clearly state your objectives.</p>\r\n\r\n<p><strong>Tip: </strong>Your vision and mission statements will often be visible to your customers, potential customers, suppliers and investors (e.g., published on your website) so they need to stand out and give people an insight into your company&rsquo;s ethos.</p>\r\n\r\n<p><strong>Some examples for you:</strong></p>\r\n\r\n<ul>\r\n	<li>Disney (source: <a href=\"http://disneycompanyprofile.weebly.com/\">http://disneycompanyprofile.weebly.com/</a>)</li>\r\n	<li>Amazon (source: <a href=\"https://www.facebook.com/Amazon/info?tab=page_info\">https://www.facebook.com/Amazon/info?tab=page_info</a>)</li>\r\n</ul>\r\n','2015-10-18 15:48:06','2015-10-18 15:48:06',32),
(5,'<p>Setting clear goals/objectives and including the necessary steps to make them happen is A MUST, both when starting your business and to ensure future success.</p>\r\n\r\n<p>List your business goals. Detail how, when and who&rsquo;ll be responsible to achieve them.</p>\r\n\r\n<p><strong>Examples of goals/objectives:</strong></p>\r\n\r\n<ul>\r\n	<li>Finding a good product supplier</li>\r\n	<li>Looking for a good distribution deal</li>\r\n	<li>Making your first sale</li>\r\n	<li>Hiring your first staff member</li>\r\n</ul>\r\n\r\n<p>Each goal/objective should be specific to your business and will change and grow as your business does.</p>\r\n\r\n<p><strong>Tip</strong>: Make them measurable and achievable.&nbsp;</p>\r\n','2015-10-18 15:48:21','2015-10-18 15:48:21',33),
(6,'<p>Time to collate! This section should house all major business details. Keeping all key information in the one place will ensure you and the reader (i.e. investor) can quickly access your essential business information.</p>\r\n\r\n<p>When looking at the basics of setting up a business (your business/trading names and overall business structure) a great starting point is to speak with your local government authorities and your accountant. They&rsquo;ll be able to guide you through the right business structure for you, i.e., sole trader, a trust, partnership, business or company. Each structure has its own advantages and disadvantages, so choosing the right structure for your business is key. Making the wrong choice at the start may lead to large and costly changes down the track.</p>\r\n\r\n<p><strong>Tip: </strong>Do your research. Even scrolling through your government websites will offer advice/information you may not previously have known. It&rsquo;s also a good idea to do a website domain search before settling on names to make sure the website address you want is available. It&rsquo;s as simple as typing your ideal website address into Google to see if it&rsquo;s free, or you can head to specific websites where you&rsquo;ll be able to run a search for your ideal website address, e.g., <a href=\"https://au.godaddy.com/?ci\">https://au.godaddy.com/?ci</a>=&nbsp;</p>\r\n','2015-10-18 15:48:49','2015-10-18 15:48:49',24),
(7,'<p>An organisational chart is a great way to show how you see your business operating on a daily basis, and it also makes future planning really easy and efficient.</p>\r\n\r\n<p>A good organisational chart can:</p>\r\n\r\n<ul>\r\n	<li>Guide employees in understanding their own responsibilities</li>\r\n	<li>Help manage the workloads of all employees</li>\r\n	<li>Clearly show employees responsibilities and promotion opportunities</li>\r\n	<li>Identify potential gaps and what you may need to plan for</li>\r\n</ul>\r\n','2015-10-18 15:49:02','2015-10-18 15:49:02',25),
(8,'<p>It&rsquo;s always a good idea to detail your key personnel. That way you can highlight who you&rsquo;ve currently got in your business and what skills/strengths they bring to the table.</p>\r\n\r\n<p>While you may not even be considering staff at this point, detailing your key personnel is also a great way to see what possible gaps or skills you may need in the future.</p>\r\n\r\n<p><strong>Tip:</strong> If you&rsquo;re looking for funding/investment, highlighting the skills and strengths of your personnel will not only showcase the potential of your business but also back-up why they should invest in your future.</p>\r\n\r\n<p>List your current staff in the table, including all owners.</p>\r\n','2015-10-18 15:49:20','2015-10-18 15:49:20',38),
(9,'<p>Hopefully you see your business getting bigger and bigger over time. But in getting bigger, you may find you need more employees/staff to operate your business and achieve your goals. It&rsquo;s time to future-proof your business and think about the positions you may need.</p>\r\n\r\n<p>Detail the positions, what skills are required and the date you think you&rsquo;ll need someone by. This section will change as your business grows.</p>\r\n','2015-10-18 15:49:30','2015-10-18 15:49:30',39),
(10,'<p>People can make or break a business, so specific recruiting and training systems for your business are a MUST.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The hiring and firing process can be a timely and costly exercise so ensuring your team has the right qualifications/experience from the start is key. Not only that, your business should have the appropriate systems and training manuals/processes in place to ensure your staff have clarity in their positions and align their daily activities with your company&rsquo;s vision and &lsquo;tone of voice&rsquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Investing in the right people not only improves productivity but also boosts team morale.</p>\r\n\r\n<p>All costs associated need to be considered during the recruitment process (monetary and otherwise). Things to think about include advertising for the required position, current staff covering the vacant role, or purely the time it takes to hire someone new (screening, interviewing, testing etc.). For instance, it&rsquo;s a standard procedure to not only interview web developers, but also design specific tests to prove their proficiency in languages and systems.</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Online Advertising services (i.e., Seek, CareerOne, LinkedIn) and even credible referrals are a great way to find staff</li>\r\n	<li>Create clear procedural manuals/documents that can be given to staff</li>\r\n	<li>Put checklists/important information in high traffic areas for staff so they can read them</li>\r\n	<li>Make sure you have written job descriptions for each staff member so there&rsquo;s no confusion on tasks</li>\r\n</ul>\r\n\r\n<p>Put in place review processes to make sure staff are held accountable</p>\r\n','2015-10-18 15:49:47','2015-10-18 15:49:47',40),
(11,'<p>Here&rsquo;s where you detail each product/service you&rsquo;ll be providing to your clients/customers.</p>\r\n\r\n<p>Consider how you&rsquo;ll be selling them, where they&rsquo;re coming from and all costs associated with each product/service.</p>\r\n','2015-10-18 15:50:00','2015-10-18 15:50:00',26),
(12,'<p>Describe how your products/services will be sold, and the payment method you&rsquo;ll have available, e.g., direct debit/credit card/merchant facilities./gift cards/PayPal.</p>\r\n\r\n<p>Will you have credit card, refund and warranty policies in place? We recommend you check with your local government authorities on any legislation regarding refunds/exchanges/warranties relevant to your industry, as there will be laws and legislation you&rsquo;ll need to abide by. Remember that these can differ depending on your location.</p>\r\n\r\n<p><strong>Tip: </strong>It&rsquo;s also a good idea to check with your bank to see what kind of payment methods are available and will work best for you. Shop around. Banks are pretty competitive. Make sure you get the best rates and account fees possible. Less money for them means more money for you!</p>\r\n\r\n<p>Here are some business policies to think about:</p>\r\n\r\n<ul>\r\n	<li>Credit card policy, e.g., 1.5% surcharge on all credit card payments</li>\r\n	<li>Refund/return/exchange policy, e.g., a seven day refund or exchange policy</li>\r\n	<li>Warranty policy: what will you provide if your product is faulty, breaks down or needs repair?</li>\r\n</ul>\r\n','2015-10-18 15:50:23','2015-10-18 15:50:23',54),
(13,'<p>Have a think about all inventories you&rsquo;ll need to run your business (the items, their costs, the number you need etc.). For instance, if you&rsquo;re a baker, think about all of the ingredients you need for each variety of cakes you sell.</p>\r\n\r\n<p>List them so you&rsquo;re clear on all costs associated to produce each item you intend to sell and how many you&rsquo;ll need to have on hand. This list will change as you develop your business and start purchasing inventory items.</p>\r\n','2015-10-18 15:50:32','2015-10-18 15:50:32',55),
(14,'<p>Understanding how your product/service gets to your end customer is very important. Having the correct distribution channels from the start ensures your business maximises its profit. You need to think about how you&rsquo;ll be selling your products/services, i.e., via a shopfront, email, website, wholesale or a combination of all.</p>\r\n\r\n<p>Each channel has its advantages and disadvantages. Here are a few things to consider:</p>\r\n\r\n<ul>\r\n	<li>The time and costs associated for each channel</li>\r\n	<li>Are the channels suited to your product/service and end goals?</li>\r\n	<li>What channels do your potential customers use? What are their buying habits? E.g., an online store may be better for the youth market</li>\r\n	<li>Can you use multiple channels? Of course you can, just make sure they&rsquo;ll achieve the results you&rsquo;re chasing</li>\r\n</ul>\r\n\r\n<p>During this phase, it&rsquo;s also worth considering what you&rsquo;ll be using to get your business out there. Are you pounding the pavement or hiring a sales team to sell on your behalf? Have a think, and nail the process from the start.</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Will you have a sales team? Will you provide your sales team with a handbook/guide to ensure they&rsquo;re selling your vision correctly?</li>\r\n	<li>Who will make up your sales team? How many sales people will you have?</li>\r\n	<li>Will your sales team have clear goals/KPI&rsquo;s based on the number of leads/sales they achieve?</li>\r\n</ul>\r\n','2015-10-18 15:50:47','2015-10-18 15:50:47',56),
(15,'<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>The time and costs associated for each channel</li>\r\n	<li>Are the channels suited to your product/service and end goals?</li>\r\n	<li>What channels do your potential customers use? What are their buying habits? E.g., an online store may be better for the youth market</li>\r\n	<li>Can you use multiple channels? Of course you can, just make sure they&rsquo;ll achieve the results you&rsquo;re chasing</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>During this phase, it&rsquo;s also worth considering what you&rsquo;ll be using to get your business out there. Are you pounding the pavement or hiring a sales team to sell on your behalf? Have a think, and nail the process from the start.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Will you have a sales team? Will you provide your sales team with a handbook/guide to ensure they&rsquo;re selling your vision correctly?</li>\r\n	<li>Who will make up your sales team? How many sales people will you have?</li>\r\n	<li>Will your sales team have clear goals/KPI&rsquo;s based on the number of leads/sales they achieve?</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width:160px\">\r\n			<p>Product/Service</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Distribution Channel</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Reason for Chosen Channel</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Method of Sale</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Strengths</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>Weaknesses</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width:160px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>List the product/service</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>Where your customers will be able to purchase your product/service, .e.g., shopfront, online</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>Why you chose this channel, &nbsp;e.g., because it&rsquo;s where your target market is based</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>How you&rsquo;ll reach customers, e.g., salespeople actively selling door to door</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>List all strengths</p>\r\n			</td>\r\n			<td style=\"width:160px\">\r\n			<p>&nbsp;</p>\r\n\r\n			<p>List all weaknesses</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp; 3.3.4 Location</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&lsquo;Location, Location, Location.&rsquo; Ensuring your business is in a prime location is one of the most important business decisions you can make. If people can&rsquo;t find you, whether it&rsquo;s a physical shopfront or an online portal, there&rsquo;s no point being in business.</p>\r\n\r\n<p><strong>Tips: </strong></p>\r\n\r\n<ul>\r\n	<li>Think about how much space you&rsquo;ll need, whether you&rsquo;ll need additional storage, and can the space you&rsquo;ve chosen grow with you?</li>\r\n	<li>Are you looking to rent, buy or lease?</li>\r\n	<li>Think about the location from your customers&rsquo; point of view to understand what works for them</li>\r\n	<li>Get to know the community and assess the foot traffic and accessibility of the area</li>\r\n	<li>Look at other businesses and competition in the area</li>\r\n	<li>Assess the building to ensure it suits: how old is the building, will it require maintenance or a complete fit-out and what are the costs?&nbsp;</li>\r\n</ul>\r\n','2015-10-18 15:51:05','2015-10-18 15:51:05',57),
(16,'<p>It doesn&rsquo;t matter how big or small you think your business is going to be, understanding your legal requirements is vital. Starting your business with the right structures and procedures will avoid unnecessary legal headaches down the road.</p>\r\n\r\n<p>There are many legal requirements to consider when starting a business, and a qualified lawyer&mdash;preferably with experience in your industry&mdash;will help guide you through the process. Speaking with your relevant government bodies will also assist here.&nbsp;</p>\r\n\r\n<p><strong>Tip: </strong>It&rsquo;s always a good idea to conduct your own research. Knowing the right questions to ask will not only help you, but also the professionals you seek because they&rsquo;ll be able to gain a better understanding of your requirements. Remember the old saying: there&rsquo;s no such thing as a stupid question!</p>\r\n\r\n<p>Things to consider:</p>\r\n\r\n<ul>\r\n	<li>What kind of business do you intend to create and what business structure is right for you, i.e., sole trader, trust, partnership, company (<strong>Hot Tip: </strong>accountants can also assist with this process)</li>\r\n	<li>Will you need any contracts drafted (employee contracts, consultant contracts, distribution contracts, confidentiality agreements, shareholder agreements, lease/purchase contracts etc.)?</li>\r\n	<li>Will you need protection on your IP (intellectual property), trademarks, copyright and patents?</li>\r\n	<li>Will you require any licenses to operate your business? Note: these will differ depending on what state/territory you&rsquo;ll be operating in.</li>\r\n</ul>\r\n','2015-10-18 15:51:25','2015-10-18 15:51:25',48),
(17,'<p>The boring numbers are so important and a trusted accounting adviser is MUST! Do your research. Having the appropriate taxation and accounting procedures from the outset will put your business in good stead for the future, and can avoid expensive mistakes and hassles with your local taxation department. An accountant will also help set up your business with the correct structure and offer advice on how to maximise your tax and accounting benefits.</p>\r\n\r\n<p>Once set up, we recommend investing in a &lsquo;cloud based&rsquo; accounting system like Xero, MYOB, Quickbooks or Freshbooks. Everyone loves a good excel spread sheet, but they can be limiting, so utilising a specialised accounting/bookkeeping system will be more efficient (they&rsquo;ll do the hard work for you!). This system will allow you to track and manage your cash flow as you grow, and will also allow your accountant to access your chosen platform without you needing to supply anything.</p>\r\n\r\n<p>It&rsquo;s important to document the accounting system you&rsquo;ll be using, including who&rsquo;ll be responsible for it, how you&rsquo;ll manage your client/customer payments to give your accountant a better understanding of your business, and what they&rsquo;ll need to complete for you. &nbsp;</p>\r\n\r\n<p><strong>Tips: </strong>When choosing a system, consider the following.</p>\r\n\r\n<ul>\r\n	<li>Can you set up and manage multiple bank accounts in the system?</li>\r\n	<li>Can the system calculate and track items like PAYG, annual and long service leave?</li>\r\n	<li>Will the system track stock, purchase orders and other task management requirements?</li>\r\n	<li>Will you be dealing with foreign currency?</li>\r\n	<li>Can the system link with your other online computer systems, such as online payments?</li>\r\n	<li>Can you enter, record and maintain customer contacts in the system, and be able to track what, how and how often they purchase?&nbsp;</li>\r\n</ul>\r\n','2015-10-18 15:52:08','2015-10-18 15:52:08',49),
(18,'<p>You can&rsquo;t predict the future but you can plan and put practices in place for any &lsquo;what if&rsquo; scenario that may arise. Think about the needs of your business, any hazards you may come across and the steps you can employ to reduce risks and threats. While business insurance is typically a costly exercise, it&rsquo;s unfortunately a necessary evil. Every business needs protection.</p>\r\n\r\n<p>Insurances you should consider can be broken down into asset and revenue insurance, personal and workers insurance, and liability insurance.</p>\r\n\r\n<p>Here&rsquo;s a list of insurances you may need to consider for you business:</p>\r\n\r\n<ul>\r\n	<li>Public liability insurance</li>\r\n	<li>Professional indemnity insurance</li>\r\n	<li>Worker&rsquo;s compensation insurance</li>\r\n	<li>Third party personal injury insurance</li>\r\n	<li>Business equipment insurance</li>\r\n	<li>Fire and perils insurance</li>\r\n	<li>Product liability insurance</li>\r\n	<li>Motor vehicle insurance</li>\r\n	<li>Income protection insurance</li>\r\n	<li>Cash insurance</li>\r\n	<li>Life insurance</li>\r\n</ul>\r\n\r\n<p><strong>Tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Check with your local authorities or an insurance broker regarding appropriate insurances</li>\r\n	<li>Complete a risk assessment analysis to identify any potential liabilities in your business</li>\r\n	<li>Discuss your insurance with experts in your field to get the best and most suitable coverage</li>\r\n	<li>Try and purchase from one company; it will be easier for you to manage and negotiate price</li>\r\n</ul>\r\n','2015-10-18 15:52:51','2015-10-18 15:52:51',50),
(19,'<p>A risk management strategy is a way for you to identify and assess potential hazards that could affect the success of your business.</p>\r\n\r\n<p>Having the foresight to create a risk management plan ensures you not only understand your business but you can predict potential threats and develop strategies to reduce any damage. &nbsp;</p>\r\n\r\n<p>Possible risk management examples include:</p>\r\n\r\n<ul>\r\n	<li>Staff performance or staff turnover</li>\r\n	<li>Introduction of a new product or service</li>\r\n	<li>Customer dissatisfaction</li>\r\n	<li>Instability in the market</li>\r\n	<li>Financial risk including a decrease in turnover, productivity or cash-flow</li>\r\n	<li>Power failure impacting IT systems or machinery</li>\r\n	<li>Natural disaster and hazards resulting in property damage</li>\r\n	<li>New competitor introduced to the market</li>\r\n	<li>Change of distributors/vendors</li>\r\n</ul>\r\n','2015-10-18 15:53:06','2015-10-18 15:53:06',51),
(20,'<p>List all software requirements for your business. This is a great way to track all your systems and keep them in one place.</p>\r\n\r\n<p>Examples include:</p>\r\n\r\n<ul>\r\n	<li>Accounting and reporting systems/programs</li>\r\n	<li>Email/network systems (e.g., Microsoft Office)</li>\r\n	<li>Database/File sharing such as Office 365 or Google Drive</li>\r\n	<li>CRM relationship systems like Infusionsoft and Salesforce</li>\r\n	<li>Online payment systems for online businesses (e.g., Shopify or PayPal)</li>\r\n	<li>Inventory tracking systems</li>\r\n</ul>\r\n\r\n<p>List your IT/maintenance solutions. Be sure to detail who&rsquo;ll be responsible for updates, when your updates are required and any ongoing maintenance.</p>\r\n\r\n<p><strong>Tip: </strong>Try and use systems that talk to each other. Plug-ins between systems will make your life easier.&nbsp;</p>\r\n','2015-10-18 15:53:31','2015-10-18 15:53:31',52),
(21,'<p>List all equipment needed to operate your business. Examples could include:</p>\r\n\r\n<ul>\r\n	<li>Coffee Machines</li>\r\n	<li>Eftpos Machine</li>\r\n	<li>Phones/printers</li>\r\n</ul>\r\n','2015-10-18 15:53:41','2015-10-18 15:53:41',53),
(22,'<p>The Market Summary is where you can detail the market you&rsquo;ll be doing business in. It&rsquo;s a good idea to include answers to the following questions:</p>\r\n\r\n<ul>\r\n	<li>What is it?</li>\r\n	<li>What&rsquo;s happening in it?</li>\r\n	<li>Who&rsquo;s in it?</li>\r\n	<li>Where is it?</li>\r\n	<li>What are the gaps in the market?</li>\r\n	<li>What improvements are needed?</li>\r\n	<li>What&rsquo;s the future of the market?</li>\r\n	<li>Are there any trends in the market?</li>\r\n	<li>How do you see your business fitting in the market?</li>\r\n</ul>\r\n','2015-10-18 15:53:54','2015-10-18 15:53:54',34),
(23,'<p>Your target market is the actual customer group you&rsquo;ll be targeting to sell your product/service. Which demographics will it appeal to? It&rsquo;s also a good idea to include how you intend to maintain your customer relationships and ensure they keep coming back.</p>\r\n\r\n<p>This section is all about identifying and building your customer profile. When thinking of your customer, think of the:</p>\r\n\r\n<ul>\r\n	<li>Demographics (age, sex, income, education, occupation, location, marital status)</li>\r\n	<li>Psychographics (customers attitudes, tastes, lifestyles, disposable income)</li>\r\n</ul>\r\n\r\n<p>If you&rsquo;re targeting a business, think of the:</p>\r\n\r\n<ul>\r\n	<li>Business size (locations, number of employees etc.)</li>\r\n	<li>Annual sales</li>\r\n	<li>Years in business</li>\r\n	<li>Competitors</li>\r\n</ul>\r\n\r\n<p><strong>Tips: </strong></p>\r\n\r\n<ul>\r\n	<li>You can have one clear target market or segment your target market</li>\r\n	<li>Think about whether your target market or segment has long-term/ongoing potential</li>\r\n	<li>Analyse your product/service and how it fulfils the requirements of your target market</li>\r\n	<li>Conduct a little market research on your competitors&mdash;this will help you position your target market and look for potential opportunities</li>\r\n</ul>\r\n','2015-10-18 15:54:09','2015-10-18 15:54:09',35),
(24,'<p>What methods did you use to establish your target market? Did you conduct a focus group or get people to complete an online survey? This section is all about how you arrived at your target market and your use of relevant research to back it up.&nbsp;</p>\r\n\r\n<p>Different sources can include:</p>\r\n\r\n<ul>\r\n	<li>Online surveys like: <a href=\"https://www.surveymonkey.com/\">Survey Monkey</a></li>\r\n	<li>Phone surveys</li>\r\n	<li>Focus groups such as your local chamber of commerce</li>\r\n</ul>\r\n\r\n<p>Local and state government resources (they&rsquo;re a great way to find census info about your market)&nbsp;</p>\r\n','2015-10-18 15:54:23','2015-10-18 15:54:23',45),
(25,'<p>It&rsquo;s always a good idea to have one eye on your competitors (and not in a creepy way!)</p>\r\n\r\n<p>Take a look at what they&rsquo;re selling, their pricing structure and any marketing or advertising plans. This way you can not only compare your business, but also ensure your business has its own point of difference or unique selling point.&nbsp;</p>\r\n','2015-10-18 15:54:34','2015-10-18 15:54:34',46),
(26,'<p>Let&rsquo;s have a look at your business objectively through a SWOT analysis, which is a proven method of exploring your strengths, weaknesses, opportunities and threats.</p>\r\n\r\n<p>The SWOT analysis is an easy way to breakdown and understand your business. It enables you to not only see the good, but also highlight potential obstacles. Knowing your weaknesses will help you avoid mistakes, while your strengths and opportunities will ensure you maximise your potential.</p>\r\n\r\n<p>Generally, strengths and weaknesses are written for the present and internal structures, whereas opportunities and threats focus more on the future and external factors.</p>\r\n\r\n<p><strong>Tips</strong></p>\r\n\r\n<ul>\r\n	<li>Strengths and weaknesses include prices, business costs, business reputation and quality</li>\r\n	<li>Opportunities and threats include competition, laws, politics and seasons, plus economic and cultural landscapes&nbsp;</li>\r\n	<li>Keep it short and concise</li>\r\n	<li>Detail each item from highest to lowest priority</li>\r\n	<li>Be honest and complete the analysis thoroughly and carefully</li>\r\n	<li>Look at your business from an objective observer perspective and ask people for input</li>\r\n</ul>\r\n','2015-10-18 15:54:47','2015-10-18 15:54:47',36),
(27,'<p>Unique Selling Position (USP) is all about differentiating yourself from your competition. What makes you so special?</p>\r\n\r\n<p>A USP is about setting yourself apart. We recommend not being &lsquo;all things to all people.&rsquo; Specialise in one product or service first and multiply the learning&rsquo;s and success for future products or services you release.</p>\r\n\r\n<p>Questions to ask yourself when determining your USP:</p>\r\n\r\n<ul>\r\n	<li>What do you offer that your competitors don&rsquo;t?</li>\r\n	<li>Does your product/service have added benefits and features?</li>\r\n	<li>What value and/or added value do you offer?</li>\r\n</ul>\r\n','2015-10-18 15:55:02','2015-10-18 15:55:02',47),
(28,'<p>Whether your budget is large or small, you need to think about how you&rsquo;re going to tell people that you&rsquo;re open for business. Having the knowledge and know-how around basic marketing and media channels can be the difference between success and failure.</p>\r\n\r\n<p>There&rsquo;s no &lsquo;one size fits all&rsquo; method and nor should there be.</p>\r\n\r\n<p>Here are some media channels to think about:</p>\r\n\r\n<ul>\r\n	<li>Social media (e.g., Facebook, Twitter, Linkedin, Instagram)</li>\r\n	<li>Google Adwords</li>\r\n	<li>Digital advertising (web banners)</li>\r\n	<li>Email marketing</li>\r\n	<li>Traditional media like print, television and radio</li>\r\n	<li>Direct mail</li>\r\n	<li>Outdoor advertising</li>\r\n	<li>Directories (e.g., Yellow Pages)</li>\r\n	<li>Launch events</li>\r\n	<li>Sponsorships&nbsp;</li>\r\n	<li>Public relations (media releases)</li>\r\n</ul>\r\n\r\n<p>When putting together your plan, here are some <strong>tips:</strong></p>\r\n\r\n<ul>\r\n	<li>Think about your profit margin and what percentage of that margin you&rsquo;d like to commit to marketing your business</li>\r\n	<li>Look at what media your target market is consuming</li>\r\n	<li>Incorporate several channels to create a fully integrated campaign in order to maximise your budget and exposure</li>\r\n	<li>You don&rsquo;t need to spend your entire life&rsquo;s savings on your marketing/advertising (a simple Facebook campaign can be just as effective as a TV campaign for the right audience)</li>\r\n	<li>Make sure you set measurable media objectives&mdash;there&rsquo;s no point putting it out there if you don&rsquo;t know it will work</li>\r\n</ul>\r\n\r\n<p>We can help give you more info via <strong>INSERT NEXT PACKAGE LINK</strong></p>\r\n','2015-10-18 15:55:24','2015-10-18 15:55:24',37),
(29,'<p>While specific accounting software will be able to complete these documents for you, if you&rsquo;re not yet up and running with an accounting system, we&rsquo;ve created basic templates for you, using Microsoft Excel to help get you started.</p>\r\n\r\n<p>Download, edit and then once they&rsquo;re complete, upload to your existing Business Plan ready for exporting</p>\r\n\r\n<p>How much money do you want to make? What&rsquo;s your profit objective? Start to number-crunch so you not only know what you&rsquo;re spending, but also where you want to go (sales profits or targets).</p>\r\n\r\n<p>At this point you may have no idea how much money you&rsquo;re going to make, let alone how much you need in order to get your business off the ground. But financial documents are essential, especially if you&rsquo;re going to a bank or investor to seek backing. They&rsquo;ll need to know things like the projections, the numbers (coming in and going out), how liquid your business is and the potential sales opportunities.</p>\r\n\r\n<p>Ask yourself:</p>\r\n\r\n<ul>\r\n	<li>What upfront funds do you need to set up your business?</li>\r\n	<li>Where are the funds coming from?</li>\r\n	<li>Do you need to obtain finance from banks, investors, friends or relatives?</li>\r\n	<li>Will you personally be contributing any funds?</li>\r\n	<li>Are there factors that will influence financial success, like seasonal influences, staffing, distribution etc?</li>\r\n</ul>\r\n','2015-10-18 15:55:50','2015-10-18 15:55:50',41),
(30,'<p>A profit and loss forecast allows you to track all money coming in (sales), less your expenses (losses) to give you your potential profit. It provides you with visibility on your business and an overall summary of your financial performance. The Profit &amp; Loss (P&amp;L) is the most common financial document for all small businesses and they can either be completed monthly, quarterly or annually, depending on how you prefer to operate.</p>\r\n\r\n<p>The profit and loss forecast should include any set-up costs and projected operational costs. The forecast includes, but is not limited to, wages, superannuation and taxes plus general business expenses including your marketing and media spend.</p>\r\n\r\n<p>The formula for a P&amp;L is pretty simple:</p>\r\n\r\n<p><strong>Gross Profit</strong> = sales &ndash; cost of goods sold</p>\r\n\r\n<p><strong>Net Profit</strong> = gross profit - expenses</p>\r\n\r\n<p>A good P&amp;L should be able to:</p>\r\n\r\n<ul>\r\n	<li>Show how much money is coming into your business (i.e. your sales/revenue)</li>\r\n	<li>Highlight the number of expenses you&rsquo;re incurring over time</li>\r\n	<li>Compare your expenses and performance over different periods (this&rsquo;ll show the financial trends in your business)</li>\r\n	<li>Prove your income/earning should you need evidence to apply for a mortgage/loan</li>\r\n</ul>\r\n\r\n<p>Start-up cost examples:</p>\r\n\r\n<ul>\r\n	<li>Business registration including business name, licenses, permits and domain name</li>\r\n	<li>General business: e.g., rent/lease fees, lawyer fees, accountant fees</li>\r\n	<li>Design and website build (logos, letterhead, website pages etc.)</li>\r\n	<li>Equipment such as computers, furniture, vehicles, phones and stationary/office supplies</li>\r\n</ul>\r\n\r\n<p>Insurances, e.g., public liability, workers compensation, business assets</p>\r\n','2015-10-18 15:56:11','2015-10-18 15:56:11',42),
(31,'<p>A balance sheet has three main components to it:</p>\r\n\r\n<ol>\r\n	<li>Assets: what your business owns&mdash;i.e., cash, land, property&mdash;each divided into long and short term</li>\r\n	<li>Liabilities: what your business owes, such as loans and accounts payable&mdash;each divided into long and short term</li>\r\n	<li>Owner&rsquo;s equity: the remaining balance once your liabilities (i.e., financial obligations) are deducted&mdash;also known as capital</li>\r\n</ol>\r\n\r\n<p>The formula for a balance sheet is: The Owner&rsquo;s Equity = Assets &ndash; Liabilities</p>\r\n\r\n<p>The balance sheet is generally used by banks and investors because it highlights your overall financial position. What items are included in your balance sheet will differ depending on the nature of your business. They&rsquo;re not necessarily compulsory to your business plan but it&rsquo;s best to check with your local authorities to see whether your business particulars require a balance sheet.</p>\r\n\r\n<p>A good balance sheet should be able to:</p>\r\n\r\n<ul>\r\n	<li>Highlight all assets, liabilities and equity in your business</li>\r\n	<li>Be a quick reference showing your financial strengths and opportunities</li>\r\n	<li>Highlight your ability to be able to pay your bills on time (both short and long term)</li>\r\n</ul>\r\n','2015-10-18 15:56:28','2015-10-18 15:56:28',43),
(32,'<p><strong>Cash flow</strong>&nbsp;is how much cash you expect to generate in your business and any outgoings (expenses). The whole point of cash flow is to make sure the business makes more money than it pays out. Your level of cash flow will most likely be a key indicator when potential investors are analysing your viability and ultimately deciding whether to invest&nbsp;or not.</p>\r\n\r\n<p>Being able to predict cash shortfalls over time will allow you to plan ahead, and ensure you&rsquo;re able to pay all financial obligations as they arise. These can be completed weekly, monthly or annually but are generally broken down by months. These documents are excellent methods of proving your businesses&rsquo; liquidity, and whether you&rsquo;re in surplus or deficit at the end of each period.</p>\r\n\r\n<p>The expected cash flow document is only as valuable as the data you enter. Sales forecasting can be hard, especially if you&rsquo;re not yet up and running, but if you&rsquo;re looking at your expected cash flow objectively, it may be wise to produce a couple of separate versions (i.e. realistic, pessimistic and optimistic versions). Once you&rsquo;re up and running, this will be a great way to measure what you actually achieved.</p>\r\n\r\n<p><strong>Tip: </strong>Chat to your accountant about this one; they&rsquo;ll be able to guide you and offer advice about the process.</p>\r\n\r\n<p>It&rsquo;s a pretty simple formula: <strong>Net Cash Position </strong>= cash incoming &ndash; cash outgoing&nbsp;</p>\r\n','2015-10-18 15:56:53','2015-10-18 15:56:53',44),
(33,'<p>Research and development (R&amp;D) can lead to great innovation, increased productivity and overall success in your business.</p>\r\n\r\n<p>Creating a strong R&amp;D plan can help you grow and improve your business, giving you an advantage over your competitors. Whether it&rsquo;s researching new technologies, analysing a product/service in your market, or examining your market in general, R&amp;D should be conducted for each and every business.</p>\r\n\r\n<p><strong>Tip:</strong> Check with your local authorities to see if you&rsquo;re eligible for any R&amp;D tax Incentives or grants.&nbsp;</p>\r\n','2015-10-18 15:57:09','2015-10-18 15:57:09',15);

/*Table structure for table `plans` */

DROP TABLE IF EXISTS `plans`;

CREATE TABLE `plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filepath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `plans` */

insert  into `plans`(`id`,`name`,`user_id`,`created_at`,`updated_at`,`type`,`filepath`) values 
(3,'Marketing Plan',1,'2015-11-23 17:09:34','2015-11-24 01:59:39','marketing','1448330379--3-marketing-plan.pdf'),
(4,'Test Marketing',1,'2015-11-23 17:09:36','2015-11-24 01:59:32','marketing','1448330372--4-marketing-plan.pdf'),
(9,'New Marketing Plan',1,'2015-11-24 01:56:03','2015-12-08 15:26:58','marketing','1449552418--9-marketing-plan.pdf'),
(10,'Any Marketing Plan',1,'2015-11-24 02:01:52','2015-12-08 15:27:31','marketing','1449552451--10-marketing-plan.pdf'),
(75,'Business Plan',1,'2015-11-27 01:30:04','2015-11-30 03:58:34','business','__final-1448855912--75-business-plan.pdf'),
(77,'Business Plan',1,'2015-11-27 01:42:19','2015-11-30 03:20:17','business','__final-1448853615--77-business-plan.pdf'),
(78,'Business Plan',1,'2015-11-27 01:50:25','2015-11-30 03:58:50','business','__final-1448855928--78-business-plan.pdf'),
(79,'Business Plan',1,'2015-11-27 01:51:16','2015-12-09 15:02:32','business','1449637352--79-business-plan.pdf'),
(80,'test',1,'2015-11-27 01:52:16','2015-12-09 15:02:42','business','1449637362--80-business-plan.pdf');

/*Table structure for table `revisions` */

DROP TABLE IF EXISTS `revisions`;

CREATE TABLE `revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `revisionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `revisions` */

insert  into `revisions`(`id`,`revisionable_type`,`revisionable_id`,`user_id`,`key`,`old_value`,`new_value`,`created_at`,`updated_at`) values 
(1,'App\\PlanContent',27,1,'content','<p>asdfasfdas</p>\r\n','<p>asdfasfdas adsfs ads</p>\r\n','2015-11-17 00:16:07','2015-11-17 00:16:07'),
(2,'App\\PlanContent',28,1,'content','<p>Saving new content for this category</p>\r\n','<p>Saving new content for this category</p>\r\n\r\n<p>Saving already created content with the same endpoint.</p>\r\n','2015-11-17 00:17:30','2015-11-17 00:17:30'),
(3,'App\\PlanContent',28,1,'content','<p>Saving new content for this category</p>\r\n\r\n<p>Saving already created content with the same endpoint.</p>\r\n','<p>Saving new content for this category</p>\r\n\r\n<p>Saving already created content with the same endpoint.</p>\r\n\r\n<p>Saving again</p>\r\n','2015-11-17 00:31:43','2015-11-17 00:31:43'),
(4,'App\\PlanContent',27,1,'content','<p>asdfasfdas adsfs ads</p>\r\n','<p>Test</p>\r\n','2015-11-17 01:39:00','2015-11-17 01:39:00'),
(5,'App\\PlanContent',27,1,'content','<p>Test</p>\r\n','<p>Test test</p>\r\n','2015-11-17 01:39:28','2015-11-17 01:39:28');

/*Table structure for table `user_addresses` */

DROP TABLE IF EXISTS `user_addresses`;

CREATE TABLE `user_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `premise` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thoroughfare` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `administrative_area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_addresses` */

insert  into `user_addresses`(`id`,`premise`,`thoroughfare`,`administrative_area`,`locality`,`postal_code`,`country`,`user_id`,`created_at`,`updated_at`) values 
(1,'U89/2311','Logan Road','Queensland','EIGHT MILE PLAINS','4113','Australia',1,'2015-11-02 05:23:46','2015-11-02 05:23:46'),
(2,'','6 computer road','Tennessee','Knoxville','00100','',9,'2015-11-03 05:06:11','2015-11-03 05:06:11'),
(3,'','','','','','',10,'2015-11-03 05:08:18','2015-11-03 05:08:18'),
(4,'asdf','asdf','adsf','das','fasdf','dsf',12,'2015-11-03 05:11:19','2015-11-03 05:11:19'),
(5,'asdf','asdf','adsf','das','fasdf','dsf',14,'2015-11-03 05:12:07','2015-11-03 05:12:07'),
(6,'23','dasf','df','adf','dfas','dasf',15,'2015-11-03 05:13:00','2015-11-03 05:13:00'),
(7,'','computer road','QLD','yatala','4207','Australia',8,'2015-11-17 05:35:18','2015-11-17 05:35:18'),
(8,'','street','QLD','akjf','4207','Australia',9,'2015-11-17 06:06:25','2015-11-17 06:06:25'),
(9,'','street','QLD','akjf','4207','Australia',10,'2015-11-17 06:08:49','2015-11-17 06:08:49'),
(10,'','street','QLD','akjf','4207','Australia',11,'2015-11-17 06:39:55','2015-11-17 06:39:55'),
(11,'','6 computer road','Tennessee','Knoxville','00100','',12,'2015-11-18 00:30:04','2015-11-18 00:30:04'),
(12,'','6 computer road','Queensland','Yatala','4207','Australia',13,'2015-11-18 00:31:16','2015-11-18 00:31:16'),
(13,'','test','Northern Territory','Darwin','800','',14,'2015-11-18 00:48:32','2015-11-18 00:48:32'),
(14,'','street','QLD','akjf','4207','Australia',15,'2015-11-18 00:59:10','2015-11-18 00:59:10'),
(15,'','street','QLD','akjf','4207','Australia',16,'2015-11-18 01:03:48','2015-11-18 01:03:48'),
(16,'','6 computer road','Queensland','Yatala','4207','Australia',17,'2015-11-18 01:10:05','2015-11-18 01:10:05'),
(17,'','street','QLD','akjf','4207','Australia',18,'2015-11-18 01:35:03','2015-11-18 01:35:03'),
(18,'','street','QLD','akjf','4207','Australia',19,'2015-11-18 01:54:27','2015-11-18 01:54:27'),
(19,'','street','QLD','akjf','4207','Australia',20,'2015-11-18 01:55:31','2015-11-18 01:55:31'),
(20,'','6 computer road','Queensland','Yatala','4207','Australia',21,'2015-11-18 01:56:49','2015-11-18 01:56:49'),
(21,'','computer road','QLD','yatala','4207','Australia',22,'2015-11-18 03:18:25','2015-11-18 03:18:25'),
(22,'','computer road','QLD','yatala','4207','Australia',23,'2015-11-18 03:21:25','2015-11-18 03:21:25'),
(23,'','computer road','QLD','yatala','4207','Australia',24,'2015-11-18 03:34:24','2015-11-18 03:34:24'),
(24,'','street','QLD','akjf','4207','Australia',118,'2015-11-19 05:28:21','2015-11-19 05:28:21'),
(25,'3','erica court','SA','parafield gardens','5107','Australia',119,'2015-12-09 14:42:20','2015-12-09 14:42:20'),
(26,'3','erica court','SA','parafield gardens','5107','Australia',121,'2015-12-09 14:45:18','2015-12-09 14:45:18'),
(27,'3','erica court','SA','parafield gardens','5107','Australia',122,'2015-12-09 14:52:24','2015-12-09 14:52:24'),
(28,'3','erica court','SA','parafield gardens','5107','Australia',123,'2015-12-09 14:57:17','2015-12-09 14:57:17'),
(29,'','','','','','',124,'2015-12-09 14:59:17','2015-12-09 14:59:17'),
(30,'','','','','','',125,'2015-12-09 15:01:37','2015-12-09 15:01:37'),
(31,'3','','SA','parafield gardens','5107','Australia',126,'2015-12-09 15:05:50','2015-12-09 15:05:50'),
(32,'','','','','','',127,'2015-12-09 15:09:46','2015-12-09 15:09:46'),
(33,'','','','','','',128,'2015-12-09 15:11:11','2015-12-09 15:11:11'),
(34,'','','','','','',129,'2015-12-09 15:14:40','2015-12-09 15:14:40'),
(35,'','','','','','',130,'2015-12-09 15:20:12','2015-12-09 15:20:12');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referral` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_info` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_address_id` int(11) NOT NULL,
  `upgraded_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mailchimp_member_hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `briefed` tinyint(1) NOT NULL,
  `is_super` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`company`,`contact_number`,`referral`,`other_info`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`user_address_id`,`upgraded_at`,`mailchimp_member_hash`,`briefed`,`is_super`,`is_active`) values 
(1,'Tremaine Breitenberg','Gabriel Lind','Rodriguez-Erdman','1-670-558-1515','Larry Emard MD','Consequatur eum nihil facilis ex temporibus. Rerum error iure quis aut est dolorem voluptatibus. Provident vel dolorem quae sit fugiat.','mickey.ftw@gmail.com','$2y$10$0N6GYMyGgBO6uwN1Hjg4yOvArk9vDWMTGPdPl38s.DYNoGIvBkdXi','SqENMT2YyopzQcmKetXoUWSPLvfTvu4vlq9vluDTX5OGF7PQSN9JGg4HKLny','2015-10-15 03:38:09','2015-12-09 15:02:54',0,'2015-12-09 14:31:07','9cb19d7741392e92d54a83ba53158c47',1,1,0),
(2,'Icie Russel','Ms. Roberta Huels','Rath-Lowe','395.215.6305','Omari Stanton','Recusandae asperiores pariatur repellendus ab labore. Veniam deleniti nihil sit nam voluptas et in. Officiis mollitia dolorem vel accusamus iusto quia. Quisquam molestias ipsam consequatur cum.','sunnyw@sunnyinteractive.com.au','$2y$10$5rtisjcgdRKhIQUe.sCxk.KRT.Fstp06AX9UpdPubEXSi07rEcH7K','QGxIU5zZx0','2015-10-15 03:38:09','2015-10-15 03:38:09',0,'0000-00-00 00:00:00','',0,0,0),
(3,'Murray Jacobi','Percy Botsford','Kuvalis-Weimann','(851)385-6862x398','Angelina Parisian PhD','Aut veniam officia est excepturi. Aspernatur dicta veniam voluptatibus ut tenetur laboriosam. Adipisci error dolores tenetur qui sed qui. Reiciendis quo dignissimos non.','amy@getjackdmedia.com','$2y$10$065UlH5WYcLLH.hXxP0JuuOfYr/mwcuiWyOD5Eca1a6GwdsvbpLdK','8S2OEkKnsV','2015-10-15 03:38:10','2015-10-15 03:38:10',0,'0000-00-00 00:00:00','',0,0,0),
(4,'Miss Fanny Rogahn PhD','Lou Kutch','Reilly, Lesch and Paucek','443.640.0508x161','Montana Lemke Jr.','Similique facilis debitis non corporis quisquam labore. Ex cum corporis dignissimos voluptatibus sint ab. Quis natus porro voluptatibus ad.','kirsty@getjackdmedia.com','$2y$10$97nwo.G744BgdkntN3XKW.i6R6IH6OHBL/bcscqh.4ty4DRnoeREa','xwzwfpOv2b','2015-10-15 03:38:10','2015-10-15 03:38:10',0,'0000-00-00 00:00:00','',0,0,0),
(130,'Vishal','Asai','GJM','232343','','','vishal.asai@gmail.com','$2y$10$ykaKu/vZpz5twGtwWamivusQAmDTHfhG94UQKkKcMsl97ZVOG.zda',NULL,'2015-12-09 15:20:12','2015-12-09 15:35:11',0,'2015-12-09 15:35:11','2828b2d9eb6805a9ed70eb1fbd5dcb07',0,0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
