-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2015 at 04:17 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gjroot`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_content_categories`
--

CREATE TABLE IF NOT EXISTS `business_content_categories` (
  `category_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `business_content_categories`
--

INSERT INTO `business_content_categories` (`category_id`, `content_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `business_plan_categories`
--

CREATE TABLE IF NOT EXISTS `business_plan_categories` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tip_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `business_plan_categories`
--

INSERT INTO `business_plan_categories` (`id`, `parent_id`, `lft`, `rgt`, `depth`, `name`, `created_at`, `updated_at`, `tip_id`) VALUES
(9, NULL, 1, 98, 0, 'Business Plan Root', '2015-10-14 19:07:53', '2015-10-15 00:32:59', 0),
(10, 9, 2, 3, 1, 'Executive Summary', '2015-10-14 19:09:22', '2015-10-15 02:38:49', 7),
(11, 9, 4, 27, 1, 'The Business Summary', '2015-10-14 19:10:42', '2015-10-15 02:11:59', 0),
(12, 9, 28, 67, 1, 'The Business Operations', '2015-10-14 19:10:47', '2015-10-15 02:38:14', 6),
(13, 9, 68, 83, 1, 'The Market', '2015-10-14 19:10:57', '2015-10-15 00:32:59', 0),
(14, 9, 84, 93, 1, 'The Financials', '2015-10-14 19:11:03', '2015-10-15 00:32:59', 0),
(15, 9, 94, 95, 1, 'Research and Development(R&D)/Innovation Activities', '2015-10-14 19:11:16', '2015-10-15 00:32:59', 0),
(16, 9, 96, 97, 1, 'Appendix/Supporting Documents', '2015-10-14 19:11:26', '2015-10-15 00:32:59', 0),
(24, 12, 29, 30, 2, 'Business and Registration Details', '2015-10-14 23:22:19', '2015-10-14 23:25:17', 0),
(25, 12, 31, 38, 2, 'Organisational Structure', '2015-10-14 23:22:30', '2015-10-14 23:27:24', 0),
(26, 12, 39, 48, 2, 'Product and Service', '2015-10-14 23:22:37', '2015-10-15 00:32:59', 0),
(27, 12, 49, 58, 2, 'Legal and Accounting', '2015-10-14 23:23:09', '2015-10-15 00:32:59', 0),
(29, 12, 61, 66, 2, 'Technology and Equipment', '2015-10-14 23:23:22', '2015-10-15 00:32:59', 0),
(30, 11, 19, 20, 2, 'What is your Business?', '2015-10-14 23:25:02', '2015-10-15 02:12:59', 0),
(31, 11, 21, 22, 2, 'Vision', '2015-10-14 23:25:08', '2015-10-15 02:35:23', 0),
(32, 11, 23, 24, 2, 'Mission', '2015-10-14 23:25:13', '2015-10-14 23:25:13', 0),
(33, 11, 25, 26, 2, 'Goals and Action Plan', '2015-10-14 23:25:17', '2015-10-14 23:25:18', 0),
(34, 13, 69, 70, 2, 'Market Summary', '2015-10-14 23:26:10', '2015-10-15 00:32:59', 0),
(35, 13, 71, 78, 2, 'Target Market', '2015-10-14 23:26:16', '2015-10-15 00:32:59', 0),
(36, 13, 79, 80, 2, 'SWOT Analysis', '2015-10-14 23:26:22', '2015-10-15 00:32:59', 0),
(37, 13, 81, 82, 2, 'Marketing and Media Plan', '2015-10-14 23:26:27', '2015-10-15 00:32:59', 0),
(38, 25, 32, 33, 3, 'Key Personnel', '2015-10-14 23:27:08', '2015-10-14 23:27:08', 0),
(39, 25, 34, 35, 3, 'Required Personnel', '2015-10-14 23:27:16', '2015-10-14 23:27:16', 0),
(40, 25, 36, 37, 3, 'Recruitment and Training', '2015-10-14 23:27:24', '2015-10-14 23:27:24', 0),
(41, 14, 85, 92, 2, 'Key Objectives and Financial Review', '2015-10-15 00:09:33', '2015-10-15 00:32:59', 0),
(42, 41, 86, 87, 3, 'Profit and Loss Forecast', '2015-10-15 00:09:59', '2015-10-15 00:32:59', 0),
(43, 41, 88, 89, 3, 'Balance Sheet Forecast', '2015-10-15 00:10:07', '2015-10-15 00:32:59', 0),
(44, 41, 90, 91, 3, 'Expected Cash Flow', '2015-10-15 00:10:14', '2015-10-15 00:32:59', 0),
(45, 35, 72, 73, 3, 'Market Research', '2015-10-15 00:11:00', '2015-10-15 00:32:59', 0),
(46, 35, 74, 75, 3, 'Competitors', '2015-10-15 00:11:07', '2015-10-15 00:32:59', 0),
(47, 35, 76, 77, 3, 'Unique Selling Position', '2015-10-15 00:11:14', '2015-10-15 00:32:59', 0),
(48, 27, 50, 51, 3, 'Legal', '2015-10-15 00:28:20', '2015-10-15 00:32:59', 0),
(49, 27, 52, 53, 3, 'Accounting', '2015-10-15 00:31:41', '2015-10-15 00:32:59', 0),
(50, 27, 54, 55, 3, 'Insurance', '2015-10-15 00:31:49', '2015-10-15 00:32:59', 0),
(51, 27, 56, 57, 3, 'Risk Management and Security', '2015-10-15 00:32:05', '2015-10-15 00:32:59', 0),
(52, 29, 62, 63, 3, 'IT Infrastructure', '2015-10-15 00:32:14', '2015-10-15 00:32:59', 0),
(53, 29, 64, 65, 3, 'Equipment', '2015-10-15 00:32:21', '2015-10-15 00:32:59', 0),
(54, 26, 40, 41, 3, 'Product/Service and Pricing', '2015-10-15 00:32:32', '2015-10-15 00:32:32', 0),
(55, 26, 42, 43, 3, 'Inventory', '2015-10-15 00:32:46', '2015-10-15 00:32:46', 0),
(56, 26, 44, 45, 3, 'Distribution', '2015-10-15 00:32:53', '2015-10-15 00:32:53', 0),
(57, 26, 46, 47, 3, 'Location', '2015-10-15 00:32:59', '2015-10-15 00:32:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `business_plan_contents`
--

CREATE TABLE IF NOT EXISTS `business_plan_contents` (
  `id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_plan_tips`
--

CREATE TABLE IF NOT EXISTS `business_plan_tips` (
  `id` int(10) unsigned NOT NULL,
  `tip` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `business_plan_tips`
--

INSERT INTO `business_plan_tips` (`id`, `tip`, `created_at`, `updated_at`, `cat_id`) VALUES
(6, 'Fillable?', '2015-10-15 02:38:14', '2015-10-15 02:38:14', 12),
(7, 'Tpzzz', '2015-10-15 02:38:49', '2015-10-15 02:38:49', 10);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_15_045303_create_business_plan_categories_table', 2),
('2015_10_15_054230_create_business_plan_contents_table', 3),
('2015_10_15_054849_CreateBusinessContentCategoriesTable', 4),
('2015_10_15_064134_AddStatusColumnToBusinessPlanContentTable', 4),
('2015_10_15_070308_AddUserIdToBusinessPlanContentsTable', 5),
('2015_10_15_071014_create_business_plan_tips_table', 6),
('2015_10_15_071140_AddTipIdToBusinessPlanCategoryTable', 7),
('2015_10_15_114545_AddCatIdToTipsTable', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `referral` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_info` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `company`, `contact_number`, `referral`, `other_info`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tremaine Breitenberg', 'Gabriel Lind', 'Rodriguez-Erdman', '1-670-558-1515', 'Larry Emard MD', 'Consequatur eum nihil facilis ex temporibus. Rerum error iure quis aut est dolorem voluptatibus. Provident vel dolorem quae sit fugiat.', 'mickey.ftw@gmail.com', '$2y$10$0N6GYMyGgBO6uwN1Hjg4yOvArk9vDWMTGPdPl38s.DYNoGIvBkdXi', 'v21PAXjYoi', '2015-10-14 17:38:09', '2015-10-14 17:38:09'),
(2, 'Icie Russel', 'Ms. Roberta Huels', 'Rath-Lowe', '395.215.6305', 'Omari Stanton', 'Recusandae asperiores pariatur repellendus ab labore. Veniam deleniti nihil sit nam voluptas et in. Officiis mollitia dolorem vel accusamus iusto quia. Quisquam molestias ipsam consequatur cum.', 'sunnyw@sunnyinteractive.com.au', '$2y$10$5rtisjcgdRKhIQUe.sCxk.KRT.Fstp06AX9UpdPubEXSi07rEcH7K', 'QGxIU5zZx0', '2015-10-14 17:38:09', '2015-10-14 17:38:09'),
(3, 'Murray Jacobi', 'Percy Botsford', 'Kuvalis-Weimann', '(851)385-6862x398', 'Angelina Parisian PhD', 'Aut veniam officia est excepturi. Aspernatur dicta veniam voluptatibus ut tenetur laboriosam. Adipisci error dolores tenetur qui sed qui. Reiciendis quo dignissimos non.', 'amy@getjackdmedia.com', '$2y$10$065UlH5WYcLLH.hXxP0JuuOfYr/mwcuiWyOD5Eca1a6GwdsvbpLdK', '8S2OEkKnsV', '2015-10-14 17:38:10', '2015-10-14 17:38:10'),
(4, 'Miss Fanny Rogahn PhD', 'Lou Kutch', 'Reilly, Lesch and Paucek', '443.640.0508x161', 'Montana Lemke Jr.', 'Similique facilis debitis non corporis quisquam labore. Ex cum corporis dignissimos voluptatibus sint ab. Quis natus porro voluptatibus ad.', 'kirsty@getjackdmedia.com', '$2y$10$97nwo.G744BgdkntN3XKW.i6R6IH6OHBL/bcscqh.4ty4DRnoeREa', 'xwzwfpOv2b', '2015-10-14 17:38:10', '2015-10-14 17:38:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_content_categories`
--
ALTER TABLE `business_content_categories`
  ADD PRIMARY KEY (`category_id`,`content_id`);

--
-- Indexes for table `business_plan_categories`
--
ALTER TABLE `business_plan_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_plan_categories_parent_id_index` (`parent_id`),
  ADD KEY `business_plan_categories_lft_index` (`lft`),
  ADD KEY `business_plan_categories_rgt_index` (`rgt`);

--
-- Indexes for table `business_plan_contents`
--
ALTER TABLE `business_plan_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_plan_tips`
--
ALTER TABLE `business_plan_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_plan_categories`
--
ALTER TABLE `business_plan_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `business_plan_contents`
--
ALTER TABLE `business_plan_contents`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `business_plan_tips`
--
ALTER TABLE `business_plan_tips`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
