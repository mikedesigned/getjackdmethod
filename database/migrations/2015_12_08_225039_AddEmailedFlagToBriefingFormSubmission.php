<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailedFlagToBriefingFormSubmission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('briefing_form_submissions', function($table) {
           $table->boolean('emailed_submission')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('briefing_form_submissions', function($table) {
            $table->dropColumn('emailed_submission');
        });
    }
}
