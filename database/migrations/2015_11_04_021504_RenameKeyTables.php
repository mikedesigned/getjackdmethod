<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameKeyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('business_plan_categories', 'plan_categories');
        Schema::rename('business_plan_contents', 'plan_contents');
        Schema::rename('business_plan_tips', 'plan_tips');
        Schema::rename('business_plan_default_contents', 'default_contents');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('plan_categories', 'business_plan_categories');
        Schema::rename('plan_contents', 'business_plan_contents');
        Schema::rename('plan_tips', 'business_plan_tips');
        Schema::rename('default_contents', 'business_plan_default_contents');
    }
}
