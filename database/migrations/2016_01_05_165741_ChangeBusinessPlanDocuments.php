<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBusinessPlanDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_plan_documents', function($table){
            $table->renameColumn('business_plan_id', 'plan_id');
            $table->renameColumn('business_plan_category_id', 'plan_category_id');
        });

        Schema::rename('business_plan_documents', 'plan_documents');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
