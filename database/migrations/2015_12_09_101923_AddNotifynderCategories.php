<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotifynderCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('notifynder:create:category', ['name' => 'user.pdf', 'text' => 'Your PDF has been generated']);
        Artisan::call('notifynder:create:category', ['name' => 'user.brief', 'text' => 'Briefing Session booked']);
        Artisan::call('notifynder:create:category', ['name' => 'user.review', 'text' => 'Review Session booked']);
        Artisan::call('notifynder:create:category', ['name' => 'user.created', 'text' => 'Signed up new user']);
        Artisan::call('notifynder:create:category', ['name' => 'user.upgrade', 'text' => 'User has upgraded to a new package']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
