<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBucketsAndMailchimpList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::queue('bucket:new', ['name' => 'New Sign Ups']);
        Artisan::queue('bucket:new', ['name' => 'Has Not Upgraded For Six Months']);
        Artisan::queue('bucket:package', ['name' => 'Just Tell It', 'price' => 250.00, 'url_slug' => 'marketing-plan-dash']);
        Artisan::queue('bucket:package', ['name' => 'Market It', 'price' => 250.00, 'url_slug' => 'marketing-channel-dashboard']);
        Artisan::queue('bucket:package', ['name' => 'Make It Happen', 'price' => 250.00, 'url_slug' => 'business-plan-dash']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
