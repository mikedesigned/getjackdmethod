<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBriefingQuestionAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('briefing_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('briefing_form_submission_id');
            $table->integer('briefing_question_id');
            $table->text('briefing_question_answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('briefing_question_answers');
    }
}
