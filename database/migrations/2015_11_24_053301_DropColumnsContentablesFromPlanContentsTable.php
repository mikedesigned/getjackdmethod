<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsContentablesFromPlanContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_contents', function (Blueprint $table) {
            $table->dropColumn('contentable_type');
            $table->dropColumn('contentable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_contents', function (Blueprint $table) {
            $table->string('contentable_type');
            $table->integer('contentable_id');
        });
    }
}
