<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessPlanDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_plan_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('doc_type');
            $table->integer('business_plan_id');
            $table->integer('business_plan_category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('business_plan_documents');
    }
}
