<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileFieldsToBriefingFormDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('briefing_form_documents', function (Blueprint $table) {
            $table->string('filename');
            $table->string('doc_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('briefing_form_documents', function (Blueprint $table) {
            $table->dropColumn('filename');
            $table->dropColumn('doc_type');
        });
    }
}
