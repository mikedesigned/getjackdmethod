<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'company' => $faker->company,
        'contact_number' => $faker->phoneNumber,
        'referral' => $faker->name,
        'other_info' => $faker->paragraph,
        'email' => $faker->email,
        'password' => Hash::make('admin'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\UserAddress::class, function (Faker\Generator $faker) {
   return [
       'premise' => $faker->buildingNumber,
       'thoroughfare' => $faker->streetName,
       'administrative_area' => $faker->stateAbbr,
       'locality' => $faker->city,
       'postal_code' => $faker->postcode
   ];
});

$factory->define(App\BusinessPlan::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->company,
    ];
});

$factory->define(App\PlanContent::class, function(Faker\Generator $faker){
    return [
        'content' => $faker->paragraphs(2)
    ];
});

$factory->define(App\Package::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->words(3),
        'price' => $faker->randomFloat(2, 50, 2500),
        'description' => $faker->paragraphs(2),
    ];
});

$factory->define(App\Appointment::class, function(Faker\Generator $faker) {
    $start = $faker->dateTimeBetween('now', '+4 months');
    $start_time = $start->format('Y-m-d H:i:s');
    $start->modify('+2 hours');

        return [
            'type' => $faker->numberBetween(1,2),
            'status' => 0,
            'start_time' => $start_time,
            'end_time' => $start->format('Y-m-d H:i:s'),
            'availability' => 1,
        ];
});