<?php

namespace App;

use DB;
use Auth;
use App\User;
use App\PlanContent;
use App\PlanCategory;
use App\Events\PDFDocumentCreated;
use Illuminate\Database\Eloquent\Model;
use App\Services\NotifynderNotifications as Notification;

abstract class Plan extends Model
{
    protected $table = 'plans';
    protected $fillable = ['name'];

    protected $casts = [
        'published' => 'boolean'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function content()
    {
        return $this->hasMany(PlanContent::class, 'plan_id');
    }

    public function categories()
    {
        return $this->hasMany(PlanCategory::class);
    }

    public function scopeBusiness($query)
    {
        return $query->where('type', 'business');
    }

    public function scopeMarketing($query)
    {
        return $query->where('type', 'marketing');
    }

    public function scopeCurrentUser($query, $type)
    {
        return $query->where('user_id', Auth::user()->id)->where('type', $type);
    }

    public function detachPlanContent()
    {
        foreach ($this->content as $key => $content) {
            $content->delete();
        }
    }

    public function notifyPDFCreated() {
        $message = Notification::plan_created();
        event(new PDFDocumentCreated($this, $message));
    }
    
    abstract public function getType();
    abstract public function getPlanCategoryRoot();
}
