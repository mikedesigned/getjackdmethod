<?php

namespace App\Traits;

use App\Package;
use Paypalpayment;
use URL;
use Session;
use Redirect;

trait ProcessesPayments
{
	/**
	 * Prepare a Paypal payment object to send to paypal API
	 * @param  Package $package
	 * @return PayPal\Api\Payment      
	 */
	public function prepare(Package $package) {
		$apiContext = $this->getApiContext();

		$gst = $package->price * 0.1;
        $package_actual_price = $package->price - $gst;

        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("paypal");

        $item1 = Paypalpayment::item();
        $item1->setName($package->name)
                ->setDescription($package->description)
                ->setCurrency('AUD')
                ->setQuantity(1)
                ->setTax($gst)
                ->setPrice($package_actual_price);

        $itemList = Paypalpayment::itemList();
        $itemList->setItems([$item1]);

        $details = Paypalpayment::details();
        $details->setTax($gst)
                ->setSubtotal($package_actual_price);

        //Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("AUD")
                ->setTotal($package_actual_price + $gst)
                ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'

        $redirect_urls = Paypalpayment::redirectUrls();
        $redirect_urls->setReturnUrl(URL::route('success'))->setCancelUrl(URL::route('signup'));

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        return $payment;
	}

    public function pay(Package $package) {
        /**
         * Prepare payment object
         */
        $payment = $this->prepare($package);
        
        try {
            // ### Create Payment
            $payment->create($this->getApiContext());
        } catch (\PPConnectionException $ex) {
            return  "Exception: " . $ex->getMessage() . PHP_EOL;
            exit(1);
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
     
        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());
     
        if(isset($redirect_url)) {
            // redirect to paypal
            return Redirect::away($redirect_url);
        }
     
        return Redirect::route('original.route')
            ->with('error', 'Unknown error occurred');
    }
}