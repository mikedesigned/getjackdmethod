<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Bucket extends Model
{
    protected $fillable = ['name'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', $name);
    }
}
