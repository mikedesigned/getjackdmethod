<?php

namespace App\Console\Commands;

use App\Package;
use Illuminate\Console\Command;

class CreateNewPackageWithMailchimpList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bucket:package {name} {price} {url_slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new bucket with package information and mailchimp list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Package::create(['name' => $this->argument('name'), 'price' => $this->argument('price'), 'dashboard_url' => $this->argument('url_slug')]);
    }
}
