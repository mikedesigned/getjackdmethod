<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use App\Bucket;
use App\User;
use Mail;
use Carbon\Carbon;

class HasNotUpgradedFor6Months extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bucket:noupgrade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds users to mailchimp mailing list who has not upgraded for six months';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Running command to add users to mailing list who has not upgraded for six months');
        $users = User::all();
        $bucket = Bucket::withName('Has Not Upgraded For Six Months')->first();
        $before_six_month = new Carbon('-6 months');
        $listOrganiser = app('App\Contracts\ListOrganiserContract');

        foreach($users as $user) {
            if (! $user->isInBucket($bucket) && $user->upgraded_at->timestamp > 0) {
                if($user->upgraded_at < $before_six_month) {
                    $listOrganiser->subscribeUser($user, $bucket);
                    Log::info('--- Added user : ' . $user->email);
                    // Send a no activity email and let them know they are messing up.
                    Mail::send('emails.no-activity', ['user' => $user], function ($mail) use ($user) {
                        $mail->from('noreply@getjackdmethod.com', 'GetJackD Method Team');
                        $mail->to($user->email, $user->first_name)->subject('Where have you been? It\'s been awhile');
                    });
                }
            }
        }
    }
}
