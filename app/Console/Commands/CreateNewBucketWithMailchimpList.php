<?php

namespace App\Console\Commands;

use App\Bucket;
use Illuminate\Console\Command;

class CreateNewBucketWithMailchimpList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bucket:new {name : Name of Mailchimp List}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new bucket and associated mailchimp list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Bucket::create(['name' => $this->argument('name')]);
    }
}
