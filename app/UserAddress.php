<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserAddress extends Model
{

    protected $fillable = ['address_1', 'address_2', 'country', 'administrative_area', 'postal_code'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function full() {
        return $this->address_1 . ' ' . $this->address_1 . ' ' . $this->administrative_area . ' ' . $this->country . ' ' . $this->postal_code;
    }
}
