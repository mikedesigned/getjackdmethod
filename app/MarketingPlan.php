<?php

namespace App;

use App\PlanContent;
use App\PlanDocument;

class MarketingPlan extends Plan
{
    protected $table = 'plans';

    public function getType()
    {
        return 'marketing';
    }

    public function documents()
    {
        return $this->hasMany(PlanDocument::class, 'plan_id');
    }

    public function getPlanCategoryRoot()
    {
        return 'Marketing Plan Root';
    }

    public function contents()
    {
        return $this->hasMany(PlanContent::class, 'plan_id');
    }
}
