<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Appointment extends Model
{
    protected $fillable = ['start_time', 'end_time'];

    protected $casts = ['type' => 'int'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function scopeTimeBlock($query, $date)
    {
        $start = new Carbon($date);
        $end = new Carbon($date);
        return $query->where('start_time', '>', $start->startOfDay())->where('end_time', '<', $end->endOfDay());
    }

    public function scopeAvailableInMonth($query, $month) {
        $carbon = new Carbon($month);
        return $query->where('start_time', '>=', $carbon->startOfMonth())->where('availability', (int) 1)->distinct();
    }
    /**
     * Date mutators.  Added appointments start and end time to be automatically converted to Carbon object
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'start_time', 'end_time'];
    }
}
