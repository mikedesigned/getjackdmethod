<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewAppointmentBooking' => [
            'App\Listeners\NewAppointmentBookingEmail',
        ],
        'App\Events\NewUserRegistration' => [
            'App\Listeners\RegisterNewUser'
        ],
        'App\Events\NewUserRegistered' => [
            'App\Listeners\NewUserRegistrationEmail'
        ],
        'App\Events\NewPackagePurchased' => [
            'App\Listeners\BindPackageToUser'
        ],
        'App\Events\PDFDocumentCreated' => [
            'App\Listeners\MergeAppendixJobDispatcher'
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
