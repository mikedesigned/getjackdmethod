<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // for chapter navigation on the page show view.
        view()->composer(
            'pages.show-marketing',
                'App\Http\ViewComposers\ChapterNavigationComposer'
        );
        // for chapter navigation on the page show view.
        view()->composer(
            'pages.show-brand',
                'App\Http\ViewComposers\ChapterNavigationComposer'
        );
        // for the package select form.
        view()->composer(
            ['pages.create', 'pages.edit'],
                'App\Http\ViewComposers\PackageSelectionComposer'
        );
    }

    public function register()
    {
        //
    }
}