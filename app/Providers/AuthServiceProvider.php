<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        $gate->define('view-business-plan', function($user, $business_plan) {
            return $user->id === $business_plan->user_id;
        });

        $gate->define('view-marketing-plan', function($user, $marketing_plan) {
            return $user->id === $marketing_plan->user_id;
        });

        $gate->define('view-plan', function($user, $plan) {
            return $user->id === $plan->user_id;
        });

        $gate->define('user-owns-package', function($user, $packageId) {
            return $user->packages()->contains($packageId);
        });
    }
}
