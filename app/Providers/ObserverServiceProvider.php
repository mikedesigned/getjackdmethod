<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Mailchimp;

/**
 * ObserverServiceProvider class to boot any necessary observers
 *
 * @author Vishal.Asai
 */
class ObserverServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any necessary services.
     *
     * @return void
     */
    public function boot() {
        \App\Bucket::observe(new \App\Listeners\BucketObserver);
        \App\PlanContent::observe(new \App\Listeners\PlanContentObserver(new \Illuminate\Http\Request));
        \App\Package::observe(new \App\Listeners\PackageObserver);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {

    }

}
