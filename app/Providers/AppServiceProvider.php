<?php

namespace App\Providers;

use Validator;
use Hash;
use Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('current_user_email', function($attribute, $value, $parameters, $validator) {
            return $value == Auth::user()->email;
        });
        Validator::extend('current_user_password', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }

        $this->app->bind('App\Contracts\ListOrganiserContract', 'App\Services\MailchimpListOrganiser');
    }
}
