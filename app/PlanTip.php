<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PlanCategory;

class PlanTip extends Model
{

    protected $fillable = ['tip', 'cat_id'];

    public function parentCategory()
    {
        return $this->belongsTo(PlanCategory::class, 'cat_id');
    }

}
