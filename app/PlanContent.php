<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PlanCategory;
use App\BusinessPlan;
use App\MarketingPlan;

class PlanContent extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;

    // Only keep 50 revisions of a content shard.
    protected $historyLimit = 10;
    protected $revisionCleanup = true;
    protected $revisionEnabled = true;
    protected $keepRevisionOf = ['content', 'plan_id'];

    public static function boot()
    {
        parent::boot();
    }

    protected $fillable = ['content'];

    public function categories()
    {
        return $this->belongsTo(PlanCategory::class, 'cat_id');
    }

    public function serializedData()
    {
        $unserialized_sheet = unserialize($this->content);
        $decoded_sheet = json_decode($unserialized_sheet);
        return $decoded_sheet;
    }

    /**
     * Get all of the owning contentable models.
     */
    public function contentable()
    {
        return $this->morphTo();
    }

    public function plan($type)
    {
        $class = 'App\\' . ucfirst($type) . 'Plan';
        return $this->belongsTo($class, 'plan_id');
    }

    public function scopePlanContainer($query, $plan_id)
    {
        return $query->where('plan_id', $plan_id);
    }
}
