<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BriefingFormSubmission;
use App\BriefingQuestionAnswer;

class BriefingFormDocument extends Model
{
    public function submission()
    {
        return $this->belongsTo(BriefingFormSubmission::class, 'briefing_form_submission_id');
    }

    public function answer()
    {
        return $this->belongsTo(BriefingQuestionAnswer::class, 'briefing_question_answer_id');
    }
}
