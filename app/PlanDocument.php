<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PlanCategory;
use App\BusinessPlan;
use App\MarketingPlan;
use App\User;

class PlanDocument extends Model
{
    public function category()
    {
        return $this->belongsTo(PlanCategory::class, 'plan_category_id');
    }

    public function plan($type)
    {
        $class = 'App\\' . ucfirst($type) . 'Plan';
        return $this->belongsTo($class, 'plan_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
