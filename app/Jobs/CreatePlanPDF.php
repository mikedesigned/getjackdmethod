<?php

namespace App\Jobs;

use Carbon;
use Storage;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\MergeAppendixToPlan;
use App\PlanCategory;
use App\User;
use App\Plan;
use App;

class CreatePlanPDF extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $plan;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Plan $plan, User $user)
    {
        $this->plan = $plan;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $plan = $this->plan;
        $user = $this->user;

        $sections = PlanCategory::with(['contents' => function ($query) use ($plan, $user) {
            $query->where('user_id', $user->id)
                ->where('plan_id', $plan->id)
                ->where('status', 1);
        }])->where('plan_type', $plan->getType())->where('depth', '>', 0)->get()->toHierarchy();

        $categories = PlanCategory::where('name', $plan->getPlanCategoryRoot())
            ->first()
            ->getDescendants()
            ->toHierarchy();

        $pdf = App::make('snappy.pdf.wrapper');

        $this->plan->filepath = Carbon::now()->timestamp . '--' . $this->plan->id . '-' . $this->plan->getType() . '-plan.pdf';
        $this->plan->generated_date = Carbon::now();
        $this->plan->save();

        $pdf->setOption('toc', true);
        $pdf->setOption('xsl-style-sheet', base_path('resources/views/pdf/toc.xsl'));
        $pdf->setOption('default-header', false);
        $pdf->setOption('margin-bottom', '1.9cm');
        $pdf->setOption('margin-left', '1.6cm');
        $pdf->setOption('margin-right', '1.6cm');
        $pdf->setOption('margin-top', '1.9cm');
        $pdf->setOption('enable-internal-links', true);
        $pdf->setOption('enable-external-links', true);
        $pdf->setOption('print-media-type', true);
        $pdf->setOption('footer-center', '[page] of [topage]');

        $coverView = view('pdf.coverpage', ['user' => $this->user, 'plan' => $this->plan]);
        $pdf->setOption('cover', $coverView->renderSections()['cover-page']);

        $output = $pdf->loadView(
            'pdf.'. $this->plan->getType() . 'plan',
            ['plan' => $this->plan, 'user' => $this->user, 'sections' => $sections, 'categories' => $categories]
        )->output();

        Storage::put($this->plan->filepath, $output);
        $plan->notifyPDFCreated();
    }
}
