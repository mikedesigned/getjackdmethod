<?php

namespace App\Jobs;

use Storage;
use App\Jobs\Job;
use App\BusinessPlan;
use Clegginabox\PDFMerger\PDFMerger;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Plan;

class MergeAppendixToPlan extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $plan;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $appendix = $this->plan->documents;
        $pdfMerger = new PDFMerger;
        $pdfMerger->addPdf(storage_path() . '/app/' . $this->plan->filepath);
        foreach($appendix as $document) {
            $pdfMerger->addPdf(storage_path() . '/app/' . $document->filepath, "all", "landscape");
        }
        $path = storage_path() . '/app/__final-' . $this->plan->filepath;
        $this->plan->filepath = '__final-' . $this->plan->filepath;
        $this->plan->save();
        $pdfMerger->merge('file', $path); // replace with merged one
    }
}
