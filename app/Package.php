<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Help;

class Package extends Model
{

    protected $fillable = ['name', 'price', 'description'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function help()
    {
    	return $this->hasOne(Help::class);
    }
}
