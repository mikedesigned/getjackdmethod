<?php
namespace App\Listeners;

class GJMEventListener {
    protected $listOrganiser;

    public function __construct() {
        $this->listOrganiser = app('App\Contracts\ListOrganiserContract');
    }

    public function getListOrganiser() {
        return $this->listOrganiser;
    }
}