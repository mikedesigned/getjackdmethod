<?php
namespace App\Listeners;

use App\Bucket;

class PackageObserver {
    public function created($package) {
        $bucket = Bucket::create(['name' => $package->name]);
        $bucket->save();
    }
}