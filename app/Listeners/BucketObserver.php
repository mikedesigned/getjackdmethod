<?php

namespace App\Listeners;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Contracts\ListOrganiserContract;

class BucketObserver extends GJMEventListener {
    
    public function creating($bucket) {
        $list = $this->getListOrganiser()->createList($bucket->name);
    	$new_list = $list->toArray();
    	$bucket->mailchimp_list_id = $new_list['id'];
    }

}
