<?php

namespace App\Listeners;

use App\PlanContent;
use App\PlanCategory;
use App\BusinessPlan;
use App\MarketingPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanContentObserver
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function saving(PlanContent $content)
    {
        if ($this->request->input('category_type')) {
            $data = $this->request->all();
            $content->content = serialize($data['spreadsheet']);
        } else {
            $content->content = clean($this->request->input('section_text'));
        }

        $content->user_id = Auth::user()->id; // current user id.

        $category = PlanCategory::find($this->request->input('category_id'));

        if ($this->request->input('plan_type') == "marketing") {
            $plan = MarketingPlan::find($this->request->input('plan_id'));
        } else {
            $plan = BusinessPlan::find($this->request->input('plan_id'));
        }
        $content->plan($plan->getType())->associate($plan);
        $content->categories()->associate($category);
    }
}
