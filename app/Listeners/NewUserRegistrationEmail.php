<?php

namespace App\Listeners;

use App\Events\NewUserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NewUserRegistrationEmail
{

    public function handle(NewUserRegistered $event)
    {
    	if($event->package->sku == "pimisi") {
	        Mail::send('emails.method5-email', ['user' => $event->user, 'package' => $event->package], function ($m) use ($event) {
	            $m->to($event->user->email, $event->user->first_name)->subject('Welcome to GetJackD! Method. Are you ready to take flight?');
	        });
    	} else {
    		Mail::send('emails.method-signup-email', ['user' => $event->user, 'package' => $event->package], function ($m) use ($event) {
            	$m->to($event->user->email, $event->user->first_name)->subject('Welcome to GetJackD! Method. Are you ready to take flight?');
        	});
    	}
        return $event;
    }
}
