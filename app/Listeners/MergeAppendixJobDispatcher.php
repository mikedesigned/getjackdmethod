<?php

namespace App\Listeners;

use App\Jobs\MergeAppendixToPlan;
use App\Events\PDFDocumentCreated;
use Illuminate\Queue\InteractsWithQueue;
use App\BusinessPlan;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;

class MergeAppendixJobDispatcher
{
    use DispatchesJobs;
    
    public function handle(PDFDocumentCreated $event)
    {
        $this->dispatch(new MergeAppendixToPlan($event->plan));
    }
}
