<?php

namespace App\Listeners;

use DB;
use Hash;
use Auth;
use App\User;
use App\Bucket;
use App\Package;
use App\Events\NewUserRegistered;
use App\Events\NewUserRegistration;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\NotifynderNotifications as Notification;

class RegisterNewUser extends GJMEventListener
{

    public function handle(NewUserRegistration $event)
    {
        $request = $event->request->session()->all();
        $form_data = $request['_old_input'];
        $package = Package::find($form_data['package_id']);

        $signup = DB::transaction(function () use ($form_data) {
            
            $user = User::create([
                'first_name'        => $form_data['first_name'],
                'last_name'         => $form_data['last_name'],
                'company'           => $form_data['company'],
                'contact_number'    => $form_data['contact_number'],
                'email'             => $form_data['email'],
                'is_active'         => true, // set user account to active pls.
                'password'          => Hash::make($form_data['password']),
            ]);
            $user->save();
            $address = $user->address()->create([
                'address_1'             => $form_data['address_1'],
                'address_2'             => $form_data['address_2'],
                'postal_code'           => $form_data['postal_code'],
                'administrative_area'   => $form_data['administrative_area'],
                'country'               => $form_data['country'],
            ]);

            return $user;
        });

        $this->getListOrganiser()->subscribeUser($signup, Bucket::withName('New Sign Ups')->first());
        $message = Notification::new_user($signup);
        return event(new NewUserRegistered($signup, $package, $message));
    }
}
