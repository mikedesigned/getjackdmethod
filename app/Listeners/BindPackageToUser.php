<?php

namespace App\Listeners;

use App\Events\NewPackagePurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Bucket;

class BindPackageToUser extends GJMEventListener
{
    /**
     * Handle the event.
     *
     * @param  NewPackagePurchased  $event
     * @return void
     */
    public function handle(NewPackagePurchased $event)
    {
        $event->user->packages()->attach($event->package->id);

        Mail::send($event->email_template, ['user' => $event->user, 'package' => $event->package], function ($m) use ($event) {
            $m->to($event->user->email, $event->user->first_name)->subject('Signed Up New Package');
        });

        $this->getListOrganiser()->subscribeUser($event->user, Bucket::withName($event->package->name)->first());
        $this->unsubscribeFromListNotUpgradedForSixMonths($event);
        return;
    }

    public function unsubscribeFromListNotUpgradedForSixMonths(NewPackagePurchased $event) {
        $bucket = Bucket::withName('Has Not Upgraded For Six Months')->first();
        if ($event->user->isInBucket($bucket)) {
            $this->getListOrganiser()->unsubscribeUser($event->user, $bucket);
        }
    }
}
