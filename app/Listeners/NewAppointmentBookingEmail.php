<?php

namespace App\Listeners;

use App\Events\NewAppointmentBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NewAppointmentBookingEmail
{

    public function handle(NewAppointmentBooking $event)
    {
    	if($event->appointment->type == 1) { // if it's type 1 then it's a briefing session that's been booked by the user.
	    	Mail::send('emails.briefing-email', ['user' => $event->user, 'appointment' => $event->appointment], function ($m) use ($event) {
	            $m->to($event->user->email, $event->user->first_name)->subject('Your Briefing Session is Booked!');
	        });
    	} else { // then it's a review session if it isn't the type == 1...
    		Mail::send('emails.review-email', ['user' => $event->user, 'appointment' => $event->appointment], function ($m) use ($event) {
            	$m->to($event->user->email, $event->user->first_name)->subject('Your Review Session is Booked!');
        	});
    	}

    }
}
