<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PlanCategory;

class PlanDefaultContent extends Model
{

    protected $table = 'default_contents';

    protected $fillable = ['content'];

    public function parent_category()
    {
        return $this->belongsTo(PlanCategory::class, 'cat_id', 'id');
    }

}
