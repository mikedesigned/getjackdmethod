<?php
namespace App;

use App\PlanContent;
use App\PlanDocument;

class BusinessPlan extends Plan
{
    protected $table = 'plans';

    public function getType()
    {
        return 'business';
    }

    public function getPlanCategoryRoot()
    {
        return 'Business Plan Root';
    }

    public function documents()
    {
        return $this->hasMany(PlanDocument::class, 'plan_id');
    }

    public function contents()
    {
        return $this->hasMany(PlanContent::class, 'plan_id');
    }
}
