<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\BriefingQuestionAnswer;
use App\BriefingFormDocument;
use App\Http\Requests\BriefingRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Jobs\SaveBriefingFormDocuments;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Storage;
use File;

class BriefingFormSubmission extends Model
{
    use DispatchesJobs;

    protected $casts = [
        'emailed_submission' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany(BriefingQuestionAnswer::class)->orderBy('briefing_question_id');
    }

    public function documents()
    {
        return $this->hasMany(BriefingFormDocument::class);
    }

    public function saveAnswers(BriefingRequest $request)
    {
        $docs = array();
        $answers = $request->all();
        foreach ($answers as $question_name => $answer) {
            if (!empty($question_name) && !empty($answer) && !is_object($answer)) {
                $this->saveAnswer($question_name, $answer);
            } elseif ($answer instanceof UploadedFile) {
                $this->saveDocument($question_name, $answer);
            }
        }
    }

    public function saveAnswer($question_name, $form_answer)
    {
        $question = BriefingFormQuestion::where('question_name', $question_name)->first();
        if (!empty($question)) {
            $saved_answer = BriefingQuestionAnswer::where('briefing_form_submission_id', $this->id)->where('briefing_question_id', $question->id)->get(['id'])->pluck('id')->toArray();
            $answer_id = !empty($saved_answer) ? $saved_answer[0] : null;
            $answer = BriefingQuestionAnswer::findOrNew($answer_id);
            $answer->briefing_question_answer = $form_answer;
            $answer->briefing_question_id = $question->id;
            $answer->briefing_form_submission_id = $this->id;
            $answer->save();
            return $answer;
        }
    }

    public function saveDocument($question_name, $doc) {
        // Drop the file on disk.
        Storage::disk('local')->put('uploads/' . $doc->getClientOriginalName(), File::get($doc));

        //  Save filepath as a question answer
        $answer = $this->saveAnswer($question_name, 'uploads/' . $doc->getClientOriginalName());

        //  Save document and associate it with submittion and answer
        $document = new BriefingFormDocument;
        $document->filename = $doc->getClientOriginalName();
        $document->doc_type = $doc->getclientMimeType();
        $document->submission()->associate($this->id);
        $document->answer()->associate($answer->id);
        $document->save();
    }
}
