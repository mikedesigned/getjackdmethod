<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use DB;

class BriefingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $questions = DB::table('briefing_form_questions')->where('question_type', '<>', 'file')->lists('question_name');
        return array_fill_keys($questions, 'required');
    }
}
