<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class UpgradeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email|current_user_email',
            'password' => 'required|current_user_password',
            'password_confirm' => 'same:password'
        ];
    }

    public function messages() {
        return [
            'email.current_user_email' => 'Entered email address does not match with your current email',
            'password.current_user_password' => 'Entered password does not match with your current password'
        ];
    }
}
