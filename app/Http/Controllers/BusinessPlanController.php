<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PlanController;
use App\Jobs\CreatePlanPDF;
use App\Jobs\MergeAppendixToPlan;
use App;
use App\BusinessPlan;

class BusinessPlanController extends PlanController
{

    public function planType() {
        return 'business';
    }

    public function createPlan($name) {
        return BusinessPlan::create(['name' => $name]);
    }

    public function findPlan($id) {
        return BusinessPlan::findOrFail($id);
    }

    public function dispatchCreatePDFJob(App\Plan $plan, App\User $user) {
        $this->dispatch(new CreatePlanPDF($plan, $user));
    }
}
