<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\PlanCategory;
use App\Plan;
use Barryvdh\DomPDF\Facade as PDF;
use App;
use App\Jobs\CreateMarketingPlanPDF;
use Storage;

abstract class PlanController extends Controller
{
    /**
     * Define what type of plan it deals with
     *
     * @return string Type of plan
     */
    abstract public function planType();
    
    public function index()
    {
        // used to call in list of plans for the modal window.
        $plans = Plan::currentUser($this->planType())->get();
        return response($plans, 200);
    }

    /**
     * Create plan (depending on which controller is calling Business/Marketing)
     *
     * @param  Request $request
     * @return JSON response
     */
    public function store(Request $request)
    {
        $plan = $this->createPlan($request->input('plan_name'));
        $plan->user()->associate(Auth::user());
        $plan->type = $this->planType();
        $plan->save();
        return response()->json($plan);
    }

    /**
     * @param Marketing Plan $id
     *
     * @return PDF Document.
     */
    public function show(Request $request, $id)
    {
        $plan = $this->findPlan($id);
        if ($request->user()->cannot('view-plan', $plan)) {
            return response('Not allowed.', 403);
        }
        $this->dispatchCreatePDFJob($plan, Auth::user());
    }

    public function destroy($id)
    {
        $plan = $this->findPlan($id);
        $plan->delete();
        $plan->detachPlanContent();
        return response("Deleted successfully!", 200);
    }

    public function download($id)
    {
        $plan = $this->findPlan($id);
        $file = Storage::get($plan->filepath);
        return response($file, 200)->header('Content-Type', 'application/pdf');
    }

    public function replicate(Request $request, $id)
    {
        $plan = $this->findPlan($id);
        $replicatedPlan = $plan->replicate();
        $replicatedPlan->filepath = '';
        $replicatedPlan->name = $request->get('planNewName');
        $replicatedPlan->save();

        $plan->contents->each(function ($item, $key) use ($replicatedPlan) {
            $cloned = $item->replicate();
            App\PlanContent::flushEventListeners();
            $cloned->plan($replicatedPlan->getType())->associate($replicatedPlan);
            $cloned->save();
        });

        return response()->json($replicatedPlan);

    }

    abstract public function dispatchCreatePDFJob(App\Plan $plan, App\User $user);
}
