<?php

namespace App\Http\Controllers;

use App\PlanCategory;
use App\PlanContent;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BusinessPlan;
use App\MarketingPlan;
use Auth;

class PlanContentController extends Controller
{
    public function show($id)
    {
        $content = PlanContent::findOrFail($id);
        return response($content, 200);
    }

    public function loadBusinessPlanBuilder(Request $request, $id)
    {
        $business_plan = BusinessPlan::find($id);
        return view('plan-contents.create', ['plan' => $business_plan,'plan_type' => 'business']);
    }

    public function loadMarketingPlanBuilder($id)
    {
        $marketing_plan = MarketingPlan::find($id);
        return view('plan-contents.create', ['plan' => $marketing_plan, 'plan_type' => 'marketing']);
    }

    /**
     * @internal: Do a check since a business plan can only have
     *          one piece of content per category per individual plan.
     *
     *          If it returns null then we can add new content into the category,
     *          but if it contains data, we can overwrite the existing data and save
     *          the old data into a revision table.
     * @todo:   Implement revisions for plan-contents shards.
     */
    public function store(Request $request)
    {
        $content = PlanContent::findOrNew($request->input('content_id'));
        $content->status = $request->get('status');
        $content->save();
        return response()->json($content);
    }

    public function update($id)
    {

    }

    public function destroy($id)
    {
        $content = PlanContent::find($id);
        $content->delete();
        return response("Your content was deleted!", 200);
    }
}
