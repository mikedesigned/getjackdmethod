<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\PlanCategory;
use Illuminate\Support\Collection;

class PlanCategoriesController extends Controller
{
    public function getPlanCategories($plan_id)
    {
        list($plan_type) = DB::table('plans')->where('id', $plan_id)->get(['type']);
        $categories = PlanCategory::with(['default_content', 'contents' => function ($query) use ($plan_id) {
            $query->where('user_id', Auth::user()->id)
                ->where('plan_id', $plan_id);
        }])->where('plan_type', $plan_type->type)->where('depth', '>', 0)->get()->toHierarchy();
        return response()->json($categories);
    }

    public function index()
    {
        $categories = PlanCategory::all();
        return view('plan-categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PlanCategory::getNestedList('name');
        return view('plan-categories.create', ['categories' => $categories]);
    }

    public function move($category, $target_category)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parent = PlanCategory::find($request->input('parent_category'));
        $cat = $parent->children()->create(['name' => $request->input('name'), 'content_type' => $request->input('content_type')]);
        $request->session()->flash('success', 'Category was created as' . $cat->name);
        return redirect()->back();
    }

    /**
     * Show an individual category and it's associated data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /**
         * @internal: try to grab the descendants of a category. and if the value is truthy...
         * then this is a leaf or the end of a branch so just return the category.
         * Otherwise... this is a branch and doesn't have a content field, so grab the first
         * element of the collection and return that instead.
         */
        $category = PlanCategory::with('contents', 'default_content')->findOrFail($id);

        if ($category->getDescendants()->isEmpty()) {
            return response($category, 200);
        } else {
            $content_branch = $category->getDescendants()->first();
            return response($content_branch, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
