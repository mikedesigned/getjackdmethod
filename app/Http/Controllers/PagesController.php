<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;

class PagesController extends Controller
{
    public function adminListPages()
    {
        $pages = Page::getTree();
        return view('pages.index', ['pages' => $pages]);
    }

    public function adminCreatePages()
    {
        $pages = Page::all();
        return view('pages.create', ['roots' => $pages]);
    }

    public function edit($id)
    {
        $page = Page::findOrFail($id);
        $roots = Page::all();
        return view('pages.edit', ['page' => $page, 'roots' => $roots]);
    }

    public function moveUp($id) {
    }

    public function moveDown($id) {
    }

    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        if ($page->title != $request->input('page_title')) {

            $page->title = $request->input('page_title');
            $page->resluggify(); // incase the title changes

        }

        $page->position = $request->input('position');
        $page->content = $request->input('page_content');

        if ($request->parent_page != null) {

            $root = Page::find($request->parent_page);
            $root->addChild($page);
            $page->save();
            return redirect()->back()->with('success_message', 'The page updated successfully.');

        } else {
            $page->save();
            return redirect()->back()->with('success_message', 'The page updated successfully.');
        }

    }

    public function store(Request $request)
    {
        $package = Package::find($request->input('package'));
        $page = Page::create();
        $page->title = $request->input('page_title');
        $page->content = $request->input('page_content');
        $page->package()->associate($package);

        if ($request->parent_page != null) {
            $root = Page::find($request->parent_page);
            $root->addChild($page);
            return redirect()->back()->with('success_message', 'pages created successfully.');
        } else {
            $page->save();
            return redirect()->back()->with('success_message', 'pages created successfully.');
        }

    }

    /**
     * Find the page by it's slug and display it in the view.
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMarketingPages($slug)
    {
        $page = Page::findBySlugOrFail($slug);
        return view('pages.show-marketing', ['page' => $page]);
    }

    /**
     * Find the page by it's slug and display it in the view.
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showBrandItPages()
    {
        $page = Page::findBySlugOrFail('introduction');
        return view('pages.show-brand', ['page' => $page]);
    }

    public function showBrandItSubPages($slug)
    {
        $page = Page::findBySlugOrFail($slug)->where('package_id', '=', '4');
        return view('pages.show-brand', ['page' => $page]);
    }

    public function delete($id) 
    {
        $page = Page::find($id);
        $page->delete();

        return redirect()->back();
    }
}
