<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PlanTip;
use App\PlanCategory;

class PlanTipsController extends Controller
{

    public function index()
    {
        $categories = PlanCategory::paginate(15);
        return view('plan-tips.index', ['categories' => $categories]);
    }

    public function create($cat_id)
    {
        $category = PlanCategory::find($cat_id);
        return view('plan-tips.create', ['category' => $category]);
    }

    public function edit($cat_id, $id)
    {
        $tip = PlanTip::find($id);
        $category = PlanCategory::find($cat_id);
        return view('plan-tips.edit', ['tip' => $tip, 'category' => $category]);
    }

    public function update(Request $request, $cat_id, $id)
    {
        $tip = PlanTip::find($id);
        $tip->tip = $request->input('tip');
        $tip->save();
        return response("Tip Updated!", 200);
    }

    public function store(Request $request, $cat_id)
    {
        $tip = PlanTip::create(['tip' => $request->input('tip')]);
        $category = PlanCategory::find($cat_id);
        $tip->parentCategory()->associate($category);
        $tip->save();
        $category->tip_id = $tip->id;
        $category->save();

        return redirect()->action('PlanTipsController@index');
    }

    public function show($id)
    {
        $tip = PlanTip::find($id);
        return response($tip, 200);
    }
}
