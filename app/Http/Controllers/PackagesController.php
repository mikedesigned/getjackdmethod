<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use Carbon;
use App\User;
use App\Package;
use Illuminate\Http\Request;
use App\Events\NewUserRegistration;
use App\Events\NewPackagePurchased;
use App\Http\Controllers\Controller;
use App\Services\NotifynderNotifications as Notification;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $user = User::find(Auth::user()->id);
            $packages = $user->packages;
            $actualPackages = Package::all();
            // diff the two collections to find users unowned packages.
            $unownedPackages = $actualPackages->diff($packages);
            return response()->json(['user' => $user, 'packages' => $packages, 'unowned_packages' => $unownedPackages]);
        }
        $packages = Package::all();
        return view('packages.index', ['packages' => $packages]);
    }

    public function purchasePackage()
    {
        $packages = Package::all();
        return view('packages.purchase', ['packages' => $packages->sortBy('price')]);
    }

    public function upgradePackage()
    {
        $packages = Package::all();
        return view('packages.upgrade', ['packages' => $packages->sortBy('price')]);
    }

    public function checkout(Request $request, $packageId)
    {
        $package = Package::find($packageId);

        if (Auth::guest()) {
            return view('packages.checkout', ['package' => $package]);
        }

        // incase the user tries to navigate here but already owns package
        // just send them back where they came from..
        // YOU SHALL NOT PASS!
        if ($request->user()->packages->contains($packageId)) {
            return redirect()->back();
        }

        return view('packages.checkout', ['package' => $package]);
    }

    public function showSuccessPage(Request $request)
    {
        if (!Auth::user()) {
            list($registration_event) = event(new NewUserRegistration($request));
            $message = Notification::upgrade_user($registration_event[0]->user);
            event(new NewPackagePurchased($registration_event[0]->user, $registration_event[0]->package, 'emails.signup', $message));
            return redirect('/')->with('message', 'Purchase Successful!');            
        } else {
            $request = $request->session()->all();
            $form_data = $request['_old_input'];
            $package = Package::find($form_data['package_id']);

            $user = Auth::user();
            $user->upgraded_at = Carbon::now();
            $user->save();

            $message = Notification::upgrade_user($user);
            event(new NewPackagePurchased($user, $package, 'emails.upgrade', $message));
            return redirect('/')->with('message', 'Purchase Successful');
        }
    }

    public function edit($id) 
    {
        $package = Package::findOrFail($id);
        return view('packages.edit', ['package' => $package]);
    }

    public function update(Request $request, $id) 
    {
        $package = Package::findOrFail($id);
        dd($package, $request);
    }
}
