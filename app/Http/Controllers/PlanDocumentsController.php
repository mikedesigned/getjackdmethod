<?php

namespace App\Http\Controllers;

use App\PlanDocument;
use App\Http\Controllers\Controller;
use Storage;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use File;

class PlanDocumentsController extends Controller
{
    public function getPlanDocuments(Request $request, $plan_id)
    {
        $documents = PlanDocument::where('plan_id', '=', $plan_id)->get();
       return response()->json($documents);
    }

    public function destroy($plan_id, $document_id)
    {
        $document = PlanDocument::find($document_id);
        $document->delete();
        return response(200);
    }

    public function store(Request $request)
    {
        // Grab user id for assignment.
        $user_id = Auth::user()->id;

        // pull in file details from the POST array.
        $file = $request->file('file');

        // Drop the file on disk.
        Storage::disk('local')->put('uploads/' . $file->getClientOriginalName(), File::get($file));

        // Init new DOC object.
        $document = new PlanDocument;

        /**
         * Set properties of the document.
         */
        $document->filename = $file->getClientOriginalName();
        $document->doc_type = $file->getClientMimeType();
        $document->filepath = 'uploads/' . $file->getClientOriginalName();

        /**
         * Associate with User and Category.
         */
        $document->category()->associate($request->input('category_id'));
        $document->plan($request->input('plan_type'))->associate($request->input('plan_id'));
        $document->owner()->associate($user_id); // owner

        $document->save();

        return response()->json($document);
    }
}
