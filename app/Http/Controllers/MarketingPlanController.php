<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PlanController;
use App\Jobs\CreatePlanPDF;
use App\MarketingPlan;
use App;

class MarketingPlanController extends PlanController
{
    public function planType() {
        return 'marketing';
    }
    
    public function createPlan($name) {
        return MarketingPlan::create(['name' => $name]);
    }

    public function findPlan($id) {
        return MarketingPlan::findOrFail($id);
    }

    public function dispatchCreatePDFJob(App\Plan $plan, App\User $user) {
        $this->dispatch(new CreatePlanPDF($plan, $user));        
    }
}
