<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Paypalpayment;
use App\Package;
use App\Http\Requests\SignUpRequest;
use App\Http\Requests\UpgradeRequest;
use App\Traits\ProcessesPayments;

/**
* Paypal Payment controller to accept payments from Credit Card
*/
class PaymentController extends Controller
{
    use ProcessesPayments;

    public function getApiContext()
    {
        return Paypalpayment::ApiContext(config('paypal_payment.Account.ClientId'), config('paypal_payment.Account.ClientSecret'));
    }

	public function signup(SignUpRequest $request)
	{
        /**
         * Find a package to be subscribed
         */
        $package = Package::find($request->package_id);

        /**
         * Store request data to session
         */
        $request->flash();

        return $this->pay($package);
    }

    public function upgrade(UpgradeRequest $request)
    {
        /**
         * Find a package to be subscribed
         */
        $request->flashOnly('package_id');
        $package = Package::find($request->package_id);
        return $this->pay($package);
    }
}