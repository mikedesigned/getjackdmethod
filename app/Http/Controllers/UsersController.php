<?php

namespace App\Http\Controllers;

use App\BriefingFormDocument;
use App\BriefingFormSubmission;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BriefingRequest;
use App\User;
use Auth;
use Input;
use File;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Kris\LaravelFormBuilder\FormBuilder;

class UsersController extends Controller
{

    public function index()
    {
        $users = User::paginate(15);
        return view('users.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    public function brief(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create('App\Forms\BriefingForm', ['method' => 'POST', 'route' => 'submit.briefing']);
        return view('users.briefing', ['form' => $form]);
    }

    public function submitBriefing(BriefingRequest $request)
    {
        $user = Auth::user();
        $user->save();

        $submission = BriefingFormSubmission::findOrNew($request->get('id'));
        $submission->user()->associate(Auth::user());
        $submission->save();
        $submission->saveAnswers($request);
        return redirect('dashboard/market')->with('success', 'Thanks for submitting Brief');
    }

    public function notifications(Request $request)
    {
        $list = DB::table('notifications')->where('to_id', Auth::user()->id)->orWhere('to_id', 0)->orderBy('id', 'desc')->get();
        return response()->json($list);
    }

    public function profile()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('users.profile', compact('user'));
    }

    /**
     * @todo CREATE USER CANCELLATIONS
     * Delete user content, anything tied to the user account ID.
     * Address, Plans, Plan Documents, Briefing Forms, Appointments, User Account Inactive.
     * User accounts need to be cancelled voluntarily.
     */
    public function cancel(Request $request, $user_id)
    {

    }

    public function downloadBriefingDocument(Request $request)
    {
        $document = BriefingFormDocument::where('filename', '=', $request->input('name'))->where('briefing_form_submission_id', Auth::user()->brief->id)->firstOrFail();
        $file = Storage::disk('local')->get('uploads/' . $document->filename);
        return (new Response($file, 200))->header('Content-Type', $document->doc_type );
    }

    public function sendBriefingEmail()
    {
        // @todo: send email to the girls.
        $brief = Auth::user()->brief;
        $brief->emailed_submission = true;
        $user = Auth::user();
        $user->briefed = 1;
        $user->save();
        $brief->save();

        return redirect('/dashboard/market')->with('status', 'Email sent, you can now book your briefing session!');
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->abn = $request->get('abn');
        $user->contact_number = $request->get('contact_number');
        $user->mobile_number = $request->get('mobile_number');
        $user->other_info = $request->get('other_info');
        $user->address->address_1 = $request->get('address_1');
        $user->address->address_2 = $request->get('address_2');
        $user->address->administrative_area = $request->get('state');
        $user->address->postal_code = $request->get('postal_code');

        if($request->file('logo')) {
            $image = $request->file('logo');
            $filename = Auth::user()->company . '_' . time() . '_logo.' . $image->getClientOriginalExtension();
            $path = public_path('logos/' . $filename);
            Storage::put($filename, File::get($image));
            $user->logo_path = $path;
        }

        $user->save();
        $user->address->save();
        return redirect()->back();
    }
}
