<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Event;
use App\Http\Controllers\Controller;
use Auth;
use App\Package;
use Cookie;

class DashboardController extends Controller
{

    /**
     * Display the users main application dashboard.
     *
     * @return Response
     */
    public function showMainAppDashboard(Request $request)
    {
        if(!$request->cookie('first_login')) {
          Cookie::queue(Cookie::forever('first_login', 'show alert for video'));
        }

        $user = Auth::user();
        $actualPackages = Package::all();
        // diff the two collections to find users unowned packages.
        $unownedPackages = $actualPackages->diff($user->packages);

        return view('dashboards.app', ['user' => $user, 'unowned_packages' => $unownedPackages]);
    }

    public function settingsPage()
    {
      return view('dashboards.settings');
    }

    /**
     * Display the dashboard for the Business Plan Package
     * @internal: Plan's name is "Make It Happen!"
     * @return Response
     */
    public function showBusinessPlanDashboard()
    {
        $user = Auth::user();
        $package = Package::where('sku', 'make')->first();
        return view('dashboards.business', ['user' => $user, 'package' => $package]);
    }

    /**
     * Display the content package dashboard
     * @internal: Plan's name is "Just Tell It".
     * @return Response
     */
    public function showPlanItShoutItDashboard()
    {
        $user = Auth::user();
        
        if($user->packages->contains('sku', 'pimisi')) {
            $package = Package::where('sku', 'pimisi')->first();
        } else {
            $package = Package::where('sku', 'shout')->first();
        }

        return view('dashboards.shout', ['user' => $user, 'package' => $package]);
    }

    /**
     * Display the Scheduling/Plan Builder Dashboard.
     * @internal: Plan's name is "Plan It, Market It, Now Sell It!".
     * @return Response
     */
    public function showMarketItDashboard()
    {
        $user = Auth::user();
        $package = Package::where('sku', 'pimisi')->first();
        return view('dashboards.market', ['user' => $user, 'package' => $package]);
    }

    /**
     * Display the admin dashboard for managing the resources within the application.
     * @return \Illuminate\View\View
     */
    public function showAdminDashboard()
    {
        $user = Auth::user();
        return view('dashboards.admin', ['user' => $user]);
    }
}
