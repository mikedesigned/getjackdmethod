<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Event;
use Carbon;
use Collection;
use App\Appointment;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\NewAppointmentBooking;
use App\Services\NotifynderNotifications as Notification;

class AppointmentsController extends Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showCalendar()
    {
        $appointments = Appointment::all();
        return view('appointments.calendar', ['appointments' => $appointments]);
    }

    public function adminGetBookings()
    {
        $appointments = DB::table('appointments')
                        ->join('users', 'appointments.user_id', '=', 'users.id')
                        ->select('users.email as title', 'users.first_name', 'users.id as user_id', 'start_time as start', 'end_time as end')
                        ->get();

        return response()->json($appointments);
    }

    public function adminGetFreeSlots()
    {
        $freeslots = DB::table('appointments')
                        ->select('start_time as start', 'end_time as end')
                        ->where('user_id', '=', '0')
                        ->get();
        return response()->json($freeslots);
    }

    public function adminCreateAppointment(Request $request)
    {
        $appointment = Appointment::create();
        $appointment->start_time = Carbon::parse($request->day . $request->time);
        $appointment->end_time = Carbon::parse($request->day . $request->time)->addHours(2);
        $appointment->status = 1;
        $appointment->availability = 1;
        $appointment->save();

        return redirect()->back()->with('success', 'Appointment created!');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         *  @todo: Check if request is AJAX
         *  @todo: Check if user is authenticated
         */

        $appointment = Appointment::create($request->all());
        $appointment->user_id = $request->user()->id;
        $appointment->save();
    }

       /**
     * Get appointments for given date
     *
     * @param string $date
     */
    public function forDate($date)
    {
        $appointments = Appointment::timeBlock($date)->get();
        return response()->json(['appointments' => $appointments]);
    }

    public function forMonth($month)
    {
        $appointments = Appointment::availableInMonth($month)->get();
        $available_days = collect($appointments)->map(function ($app, $key) {
            $date = new Carbon($app->start_time);
            return $date->format('Y-m-d');
        });
        return response()->json(['dates' =>  $available_days]);
    }

    /**
     * Book selected appointments
     * @param  int
     * @return json
     * @todo Send email notification to client and management about appointments booking
     */
    public function book(Request $request)
    {
        $user = $request->user();
        $appointment = Appointment::find($request->appointment_id);
        $appointment->type = $request->type;
        $appointment->user_id = $user->id;
        $appointment->availability = 0;
        $appointment->save();

        $message = Notification::new_appointment($appointment);
        event(new NewAppointmentBooking($request->user(), $appointment, $message));
        return response("Booking Successful!", 200);
    }
}
