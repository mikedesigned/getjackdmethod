<?php

namespace App\Http\Controllers;

use App\BusinessPlan;
use App\PlanContent;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PlanDefaultContent;
use App\PlanCategory;

class PlanDefaultContentsController extends Controller
{

    public function index()
    {
        $categories = PlanCategory::paginate(15);
        return view('plan-default-contents.index', ['categories' => $categories]);
    }

    public function create($cat_id)
    {
        $category = PlanCategory::find($cat_id);
        return view('plan-default-contents.create', ['category' => $category]);
    }

    public function store(Request $request, $cat_id)
    {
        $default_content = PlanDefaultContent::create(['content' => clean($request->input('default_content'))]);
        $category = PlanCategory::find($cat_id);
        $category->default_content_id = $default_content->id;
        $default_content->parent_category()->associate($category);
        $default_content->save();
        $category->save();
        return redirect()->back();
    }

    function show($id)
    {
        $default_content = PlanDefaultContent::find($id);
        return view('plan-default-contents.edit', ['default_content' => $default_content]);
    }

    public function edit($cat_id, $id)
    {
        $default_content = PlanDefaultContent::find($id);
        $category = PlanCategory::find($cat_id);
        return view('plan-default-contents.edit', ['default_content' => $default_content, 'category' => $category]);
    }

    public function update($cat_id, Request $request, $id)
    {
        $default_content = PlanDefaultContent::find($request->input('id'));
        $default_content->content = $request->input('default_content');
        $default_content->save();
        return response("Content Updated Successfully", 200);
    }

    public function destroy($id)
    {
        //
    }
}
