<?php

namespace App\Http\Middleware;

use Closure;

class PackageCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $package_sku)
    {
        /**
        *   Check if the user has the package associated with their account.
        */
        if ($request->user()->packages->contains('sku', $package_sku)) {
            return $next($request);
        }
        // If the user owns the big package, they can also access this PISI package too.
        if ($package_sku == "shout" && $request->user()->packages->contains('sku', 'pimisi')){
            return $next($request);
        }

        return response("You don't own this package", 403);
    }
}
