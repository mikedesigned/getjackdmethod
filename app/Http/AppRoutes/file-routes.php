<?php

/**
* PROFIT LOSS SPREADSHEET XLS
*/
Route::get('/api/spreadsheet/profitloss', function () {
    $file = Storage::get('ProfitLoss.xls');
    return response($file, 200)->header('Content-Type', 'application/vnd.ms-excel');
});

/**
* PROFIT LOSS SPREADSHEET XLS
*/
Route::get('/api/spreadsheet/digitalsocial', function () {
    $file = Storage::get('DigitalSocialMediaContentPlan.xlsx');
    return response($file, 200)->header('Content-Type', 'application/vnd.ms-excel');
});

/**
* PROFIT LOSS SPREADSHEET XLS
*/
Route::get('/api/spreadsheet/activitytimeline', function () {
    $file = Storage::get('TimelineOfActivity.xlsx');
    return response($file, 200)->header('Content-Type', 'application/vnd.ms-excel');
});

/**
* EXPECTED CASHFLOW SPREADSHEET XLS
* @internal get the spreadsheet data to display
* @todo perhaps refactor these into a json file for direct loading...
*/
Route::get('/api/spreadsheet/expected', function () {
    $file = Storage::get('ProjectedCashflow.xls');       
    return response($file, 200)->header('Content-Type', 'application/vnd.ms-excel');
});

/**
* BALANCE SHEET XLS
* @internal get the spreadsheet data to display
* @todo perhaps refactor these into a json file for direct loading...
*/
Route::get('/api/spreadsheet/balancesheet', function () {
    $file = Storage::get('BalanceSheet.xls');
    return response($file, 200)->header('Content-Type', 'application/vnd.ms-excel');
});

Route::get('/api/plans/{type}', function($type) {
    $user = Auth::user();

    if($type == "marketing") {
        return response()->json($user->marketing_plans);
    } else {
        return response()->json($user->business_plans);
    }

});