<?php

Route::group(['prefix' => 'marketing-channels', 'middleware' => 'auth'], function () {
    Route::get('/{slug}', ['uses' => 'PagesController@showMarketingPages'])->name('marketing');
});

Route::group(['prefix' => 'brand-it', 'middleware' => 'package:brand'], function () {
    Route::get('/{slug}', ['uses' => 'PagesController@showBrandItPages'])->name('brand-it');
});