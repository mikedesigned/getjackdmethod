<?php

/**
 * API ENDPOINTS
 * @internal VUEJS API ENDPOINTS
 */
Route::group(['prefix' => 'api', 'middleware' => 'auth'], function () {
    Route::get('/categories/{plan_id}', ['uses' => 'PlanCategoriesController@getPlanCategories']);
    Route::get('/tips/{id}', ['uses' => 'PlanTipsController@show']);
    Route::get('/packages', ['uses' => 'PackagesController@index']);
});