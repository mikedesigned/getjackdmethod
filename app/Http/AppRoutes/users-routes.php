<?php
/**
 * Users route group.
 * @interal: users related actions.
 */
Route::group(['prefix' => 'users', 'middleware' => 'auth'], function () {
    Route::get('/briefing', ['middleware' => 'auth', 'as' => 'briefing', 'uses' => 'UsersController@brief']);
    Route::post('/briefing', ['middleware' => 'auth', 'as' => 'submit.briefing', 'uses' => 'UsersController@submitBriefing']);
    Route::get('/profile', ['middleware' => 'auth', 'as' => 'profile', 'uses' => 'UsersController@profile']);
    Route::put('/{id}', ['middleware' => 'auth', 'uses' => 'UsersController@update']);
    Route::get('/briefing/email', ['middleware' => 'auth', 'as' => 'email.briefing', 'uses' => 'UsersController@sendBriefingEmail']);
});

Route::get('/notifications', [
	'middleware' => 'auth', 
	'as' => 'notifications', 
	'uses' => 'UsersController@notifications'
]);
