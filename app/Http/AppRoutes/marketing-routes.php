<?php

/**
 * @internal: Plan It, Market It, Now Sell It - Marketing Plan Routes.
 *            These control the content, file upload and PDF
 */
Route::group(['prefix' => 'plan', 'middleware' => ['auth', 'package:pimisi']], function () {
    Route::resource('marketing', 'MarketingPlanController');
    Route::get('marketing/{id}/download/', ['uses' => 'MarketingPlanController@download']);
    Route::get('marketing/{id}/content/', ['uses' => 'PlanContentController@loadMarketingPlanBuilder']);
    Route::post('marketing/content', ['uses' => 'PlanContentController@store']);
    Route::delete('marketing/content/{id}', ['uses' => 'PlanContentController@destroy']);
    Route::post('marketing/{id}/replicate', ['uses' => 'MarketingPlanController@replicate']);\
    Route::post('marketing/upload', ['uses' => 'PlanDocumentsController@store']);
    Route::get('marketing/{id}/documents', ['uses' => 'PlanDocumentsController@getPlanDocuments']);
    Route::delete('marketing/{id}/documents/{document_id}', ['uses' => 'PlanDocumentsController@destroy']);
});

/**
 * appointments controller endpoints
 * @internal APPOINTMENTS AND BOOKINGS
 */
Route::group(['prefix' => 'appointments'], function () {
    Route::get('/forDate/{date}', ['uses' => 'AppointmentsController@forDate']);
    Route::get('/forMonth/{month}', ['uses' => 'AppointmentsController@forMonth']);
    Route::get('/get_bookings', ['uses' => 'AppointmentsController@adminGetBookings']);
    Route::get('/get_free_slots', ['uses' => 'AppointmentsController@adminGetFreeSlots']);
    Route::post('/book', ['uses' => 'AppointmentsController@book']);
});