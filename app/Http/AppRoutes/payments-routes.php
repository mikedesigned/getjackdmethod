<?php

Route::get('/signup/{package}', ['uses' => 'PackagesController@checkout']);
Route::post('/processNew', 'PaymentController@signup');
Route::post('/processUpgrade', 'PaymentController@upgrade');
Route::get('/signup', ['as' => 'signup', 'uses' => 'PackagesController@purchasePackage']);
Route::get('/success', ['as' => 'success', 'uses' => 'PackagesController@showSuccessPage']);

/**
* @internal upgrade endpoint routes, links to the package controller.
*/
Route::group(['prefix' => 'upgrade'], function () {
    Route::get('/', ['uses' => 'PackagesController@upgradePackage']);
    Route::get('/{package}', ['uses' => 'PackagesController@checkout']);
});