<?php
// Main Dashboard Route...
Route::get('/', [
	'middleware' => 'auth', 
	'as' => 'home', 
	'uses' => 'DashboardController@showMainAppDashboard'
]);

/**
 * Dashboard Controllers & Routing.
 * @internal: For now I'm passing through a static package ID as a middleware parameter.
 */
Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    // Main App Dash.
    Route::get('/', ['uses' => 'DashboardController@showMainAppDashboard'])->name('main-app-dash');
    // Make It Happen Dashboard.
    Route::get('/business', ['middleware' => 'package:make', 'uses' => 'DashboardController@showBusinessPlanDashboard'])->name('business-plan-dash');
    // Just Tell It Dashboard.
    Route::get('/research', ['middleware' => 'package:shout', 'uses' => 'DashboardController@showPlanItShoutItDashboard'])->name('marketing-channel-dashboard');
    // Market It Dashboard.
    Route::get('/market', ['middleware' => 'package:pimisi', 'uses' => 'DashboardController@showMarketItDashboard'])->name('marketing-plan-dash');
});