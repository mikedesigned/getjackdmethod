<?php

/**
 * @internal: Make It Happen - Business Plan Routes.
 *            These control the content, file upload and PDF
 */
Route::group(['prefix' => 'plan', 'middleware' => ['auth', 'package:make']], function () {
    Route::resource('business', 'BusinessPlanController');
    Route::get('business/{id}/download/', ['uses' => 'BusinessPlanController@download']);
    Route::get('business/{id}/content/', ['uses' => 'PlanContentController@loadBusinessPlanBuilder']);
    Route::post('business/content', ['uses' => 'PlanContentController@store']);
    Route::delete('business/content/{id}', ['uses' => 'PlanContentController@destroy']);
    Route::post('business/upload', ['uses' => 'PlanDocumentsController@store']);
    Route::get('business/{id}/documents', ['uses' => 'PlanDocumentsController@getPlanDocuments']);
    Route::delete('business/{id}/documents/{document_id}', ['uses' => 'PlanDocumentsController@destroy']);
    Route::post('business/{id}/replicate', ['uses' => 'BusinessPlanController@replicate']);
});