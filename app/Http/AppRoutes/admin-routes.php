<?php
/**
 * Backend Admin Interface.
 */
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'superadmin']], function () {

    /**
     * Load Admin Dashboard
     * @internal ADMIN AREA
     */
    Route::get('/', ['uses' => 'DashboardController@showAdminDashboard']);
    Route::get('settings', ['uses' => 'DashboardController@settingsPage']);
    Route::post('settings', ['uses' => 'DashbaordController@settingsSave']);

    /**
     * Packages Administration
     * @internal ADMIN AREA
     */
    Route::get('packages', ['uses' => 'PackagesController@index']);
    Route::get('packages/{id}/edit', ['uses' => 'PackagesController@edit']);
    Route::put('packages/{id}', ['uses' => 'PackagesController@update']);

    /**
     * Manage Pages
     * @internal ADMIN AREA
     */
    Route::get('pages', ['uses' => 'PagesController@adminListPages']);
    Route::get('pages/create', ['uses' => 'PagesController@adminCreatePages']);
    Route::post('pages', ['uses' => 'PagesController@store']);
    Route::get('pages/{id}/edit', ['uses' => 'PagesController@edit']);
    Route::delete('pages/{id}', ['uses' => 'PagesController@delete']);
    Route::put('pages/{id}', ['uses' => 'PagesController@update']);

    // Backend Appointments Management
    Route::get('appointments', ['uses' => 'AppointmentsController@showCalendar']);
    Route::post('appointments', ['uses' => 'AppointmentsController@adminCreateAppointment']);

    /**
     * Manage Help Text.
     * @internal ADMIN AREA
     */
    Route::get('help', ['uses' => 'HelpController@index']);
    Route::post('help', ['uses' => 'HelpController@store']);
    Route::get('help/create', ['uses' => 'HelpController@create']);
    Route::get('help/{id}/edit', ['uses' => 'HelpController@edit', 'as' => 'help.edit']);
    Route::put('help/{id}', ['uses' => 'HelpController@update']);

    /**
     * Users Administration
     * @internal ADMIN AREA
     */
    Route::get('users', ['uses' => 'UsersController@index']);
    Route::get('users/{id}', ['uses' => 'UsersController@show']);

    /**
     * Plan Categories Admin
     * @internal ADMIN AREA
     */
    Route::get('categories/create', ['uses' => 'PlanCategoriesController@create']);
    Route::get('categories', ['uses' => 'PlanCategoriesController@index']);
    Route::post('categories', ['uses' => 'PlanCategoriesController@store']);
    Route::get('categories/{id}/edit}', ['uses' => 'PlanCategoriesController@edit']);
    Route::put('categories/{id}', ['uses' => 'PlanCategoriesController@show']);
    Route::post('categories/{id}/move', ['uses' => 'PlanCategoriesController@move']);

    /**
     * Plan Tips Admin
     * @internal ADMIN AREA
     */
    Route::get('categories/tips', ['uses' => 'PlanTipsController@index']);
    Route::get('categories/{cat_id}/tips/create', ['uses' => 'PlanTipsController@create']);
    Route::get('categories/{cat_id}/tips/{id}/edit', ['uses' => 'PlanTipsController@edit']);
    Route::post('categories/{cat_id}/tips', ['uses' => 'PlanTipsController@store']);
    Route::put('categories/{cat_id}/tips/{id}', ['uses' => 'PlanTipsController@update']);

    /**
     * Default Content Templates Admin
     * @internal ADMIN AREA
     */
    Route::get('categories/templates', ['uses' => 'PlanDefaultContentsController@index']);
    Route::get('categories/{cat_id}/templates/create', ['uses' => 'PlanDefaultContentsController@create']);
    Route::get('categories/{cat_id}/templates/{id}/edit', ['uses' => 'PlanDefaultContentsController@edit']);
    Route::post('categories/{cat_id}/templates', ['uses' => 'PlanDefaultContentsController@store']);
    Route::put('categories/{cat_id}/templates/{id}', ['uses' => 'PlanDefaultContentsController@update']);
});