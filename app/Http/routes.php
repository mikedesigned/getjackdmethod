<?php
// Routes relating to checkouts/upgrade and purchasing.
include('AppRoutes/payments-routes.php');
// Include JS API Endpoints
include('AppRoutes/api-endpoints.php');
//Include the users briefing form and other editing routes.
include('AppRoutes/users-routes.php');
// Include the Dashboard Routes
include('AppRoutes/auth-routes.php');
// Include the Dashboard Routes
include('AppRoutes/dashboard-routes.php');
// Include the Business Plan package routes.
include('AppRoutes/business-plan-routes.php');
// Include Marketing Plan package routes.
include('AppRoutes/marketing-routes.php');
// Include the admin routes.
include('AppRoutes/admin-routes.php');
// Include the rtoues pertaining to general file downloads and such.
include('AppRoutes/file-routes.php');
// Content consumption sections routing.
include('AppRoutes/content-routes.php');

/**
*
* @internal testing routes for api endpoints or templates.
*/

Route::get('/test/email/signup', ['middleware' => 'auth', function () {
    $user = Auth::user();
   return view('emails.method-signup-email', ['user' => $user]);
}]);
Route::get('/test/email/noactivity', ['middleware' => 'auth', function () {
    $user = Auth::user();
   return view('emails.no-activity', ['user' => $user]);
}]);
Route::get('/test/email/method5', ['middleware' => 'auth', function () {
    $user = Auth::user();
   return view('emails.method5-email', ['user' => $user]);
}]);
Route::get('/test/email/expired', ['middleware' => 'auth', function () {
    $user = Auth::user();
   return view('emails.expired-account', ['user' => $user]);
}]);
Route::get('/test/email/briefing', ['middleware' => 'auth', function () {
    $user = Auth::user();
   return view('emails.briefing-email', ['user' => $user]);
}]);