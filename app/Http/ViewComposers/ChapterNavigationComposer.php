<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Page;

/**
 * @internal This view composer creates
 * the list of chapters seen on the side
 * of the content packages.
 * It's wicked!
 *
 * Class ChapterNavigationComposer
 * @package App\Http\ViewComposers
 */
class ChapterNavigationComposer
{
    /**
     * @var Page
     */
    protected $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Some crazy complicated view logic here.
     * @param  View   $view [description]
     * @return [type]       [description]
     */
    public function compose(View $view)
    {
        // if it's a root, find itself and return it's children into the view.
        if($view->page->isRoot()) {
            $page = Page::find($view->page->id);
            $view->with('chapters', $page->getDescendants()->sortBy('position'))->with('main_section', $page);
        } else {
            // or if it isn't, get it's parent and return the children.
            if ($view->page->real_depth > 1) {
              $page = $view->page->getAncestors()->where("real_depth", '0')->first();
            } else {
              $page = $view->page->getAncestors()->where("real_depth", '0')->first();
            }

            $rootPage = $page->getDescendants()->sortBy('position');
            $view->with('chapters', $rootPage)->with('main_section', $page);
        }
    }
}
