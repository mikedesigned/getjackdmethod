<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Package;

/**
 * Class PackageSelectionComposer
 * @package App\Http\ViewComposers
 */
class PackageSelectionComposer
{

    /**
     * @var Page
     */
    protected $package;

    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    public function compose(View $view)
    {
        $packages = Package::all();
        $view->with('packages', $packages);
    }
}
