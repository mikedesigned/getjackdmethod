<?php
namespace App\Services;
use App\User;
use App\Contracts\ListOrganiserContract;
use App\Bucket;

class MailchimpListOrganiser implements ListOrganiserContract {
    protected $mailchimp;

    public function __construct() {
        $this->mailchimp = app('Mailchimp\Mailchimp');
    }

    public function createList($listName) {
        /*
         * All fields are required to create a mailing list
         */
        $list = $this->mailchimp->post('lists', [
         'name' => $listName,
         'permission_reminder' => 'You signed up for updates on Greeks economy.',
            'email_type_option' => false,
            'contact' => [
                'company' => 'GJM',
                'address1' => 'Test Street',
                'address2' => '',
                'city' => 'Brisbane',
                'state' => 'QLD',
                'zip' => '4000',
                'country' => 'AU',
                'phone' => '0411222333'
            ],
            'campaign_defaults' => [
                'from_name' => 'GJM',
                'from_email' => 'admin@getjackedmethod.com.au',
                'subject' => 'New GJM campaign!',
                'language' => 'AU'
            ]
        ]);
        return $list;
    }

    public function subscribeUser(User $user, Bucket $bucket) {
        $member = $this->mailchimp->post('lists/' . $bucket->mailchimp_list_id . '/members', ['status' => 'subscribed', 'email_address' => $user->email]);
        $user->buckets()->attach($bucket);
        $subscribed_member = $member->toArray();
        $user->mailchimp_member_hash = $member['id'];
        $user->save();
        return true;
    }

    public function unsubscribeUser(User $user, Bucket $bucket) {
        $member = $this->mailchimp->patch("lists/{$bucket->mailchimp_list_id}/members/{$user->mailchimp_member_hash}", ['status' => 'unsubscribed']);
        $user->buckets()->detach($bucket);
        return true;
    }

}