<?php
namespace App\Services;

use Auth;
use Fenos\Notifynder\Facades\Notifynder;

class NotifynderNotifications {
    public static function new_appointment($appointment) {        
        return Notifynder::category($appointment->type == 1 ? 'user.brief' : 'user.review')
            ->from(Auth::user()->id)
            ->to(Auth::user()->id)
            ->url('New ' . ($appointment->type == 1 ? 'Briefing' : 'Review') . ' Session booked')
            ->send();
    }

    public static function new_user($user) {
        return Notifynder::category('user.created')
            ->from($user->id)
            ->to(1)
            ->url('New user created')
            ->send();
    }

    public static function plan_created() {
        return Notifynder::category('user.pdf')
            ->from(Auth::user()->id)
            ->to(Auth::user()->id)
            ->url('Pdf generated')
            ->send();
    }

    public static function upgrade_user($user) {
        return Notifynder::category('user.upgrade')
            ->from($user->id)
            ->to(1)
            ->url('User upgraded to a new package')
            ->send();        
    }

    public static function broadcast($message_title) {
        return Notifynder::category('user.broadcast')
            ->from(Auth::user()->id)
            ->to(0)
            ->url('<a href="/">' . $message_title . '</a>')
            ->send();
    }
}