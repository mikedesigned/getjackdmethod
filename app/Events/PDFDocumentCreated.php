<?php

namespace App\Events;

use Auth;
use App\Events\Event;
use App\Plan;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PDFDocumentCreated extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $plan;
    public $message;

    public function __construct(Plan $plan, $message)
    {
        $this->plan = $plan;
        $this->message = $message;
    }

    public function broadcastOn()
    {
        return [Auth::user()->email];
    }
}
