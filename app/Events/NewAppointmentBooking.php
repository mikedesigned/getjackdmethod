<?php

namespace App\Events;

use Auth;
use App\User;
use App\Appointment;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewAppointmentBooking extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $appointment;
    public $user;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Appointment $appointment, $message)
    {
        $this->appointment = $appointment;
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['admin'];
    }
}
