<?php

namespace App\Events;

use App\User;
use App\Package;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewPackagePurchased extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $user;
    public $package;
    public $email_template;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Package $package, $template, $message)
    {
        $this->user = $user;
        $this->package = $package;
        $this->email_template = $template;
        $this->message = $message;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['admin'];
    }
}
