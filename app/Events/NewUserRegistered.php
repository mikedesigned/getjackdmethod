<?php

namespace App\Events;

use App\User;
use App\Package;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewUserRegistered extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $user;
    public $package;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Package $package, $message)
    {
        $this->user = $user;
        $this->package = $package;
        $this->message = $message;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['admin'];
    }
}
