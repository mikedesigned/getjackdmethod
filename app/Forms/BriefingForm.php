<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\BriefingFormQuestion;
use App\BriefingQuestionAnswer;
use Auth;
class BriefingForm extends Form
{

    public function buildForm()
    {
        $questions = BriefingFormQuestion::all();
        $this->addQuestions($questions);
        $this->add('save', 'submit', ['attr' => ['class' => 'btn btn-success']]);
    }

    public function addQuestions($questions) {
        $answers = array();
        if (Auth::user()->brief) {
            BriefingQuestionAnswer::where('briefing_form_submission_id', Auth::user()->brief->id)->get(['briefing_question_id', 'briefing_question_answer'])->map(function ($answer) use (&$answers) {
                $answers[$answer['briefing_question_id']] = $answer['briefing_question_answer'];
            });
            $this->add('id', 'hidden', ['value' => Auth::user()->brief->id]);
        }

        foreach ($questions as $key => $question) {
            $this->add($question->question_name, $question->question_type, [
                'label' => $question->question_text,
                'class' => 'field',
                'value' => !empty($answers) && isset($answers[$question->id]) ? $answers[$question->id] : '',
                // 'help_block' => $question->question_type == 'file' ? ['text' => $answers[$question->id], 'tag' => 'a', 'attr' => ['href' => '#', 'class' => 'help-block', 'style' => 'color: white; cursor: pointer;']] : []
            ]);
        }
    }
}
