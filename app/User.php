<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Package;
use App\BusinessPlan;
use App\PlanContent;
use App\UserAddress;
use App\Bucket;
use App\Appointment;
use App\MarketingPlan;
use Fenos\Notifynder\Notifable;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
* @var array
*/
    protected $fillable = ['first_name', 'last_name',
        'company',  'contact_number', 'referral', 'other_info', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['upgraded_at'];

    protected $casts = [
        'is_super' => 'boolean',
        'is_active' => 'boolean'
    ];

    public function packages()
    {
        return $this->belongsToMany(Package::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function business_plans()
    {
        return $this->hasMany(BusinessPlan::class)->where('type', 'business');
    }

    public function marketing_plans()
    {
        return $this->hasMany(MarketingPlan::class)->where('type', 'marketing');
    }

    public function plans()
    {
        return $this->hasMany(Plan::class);
    }
    
    public function business_plan_content()
    {
        return $this->hasMany(PlanContent::class);
    }

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }

    public function buckets()
    {
        return $this->belongsToMany(Bucket::class);
    }

    public function brief() {
        return $this->hasOne(BriefingFormSubmission::class);
    }

    public function getBriefingSessionTime()
    {
        $appointments = $this->appointments;
        $briefingSession = $appointments->where('type', (int) 1)->first();
        return $briefingSession->start_time;
    }

    public function getReviewSessionTime()
    {
        $appointments = $this->appointments;
        $reviewSession = $appointments->where('type', (int) 2)->first();
        return $reviewSession->start_time;
    }

    public function ableToBookReview()
    {
        if ($this->hasBriefingSession() && $this->getBriefingSessionTime()->isPast()) {
            return true;
        } else {
            return false;
        }
    }

    public function hasBriefingSession()
    {
        foreach($this->appointments as $appointment) {
            if($appointment->type == (int) 1)
            {
                return true;
            }
        }

        return false;
    }

    public function hasReviewSession()
    {
        foreach($this->appointments as $appointment) {
            if($appointment->type == (int) 2)
            {
                return true;
            }
        }

        return false;
    }
    
    public function isInBucket(Bucket $bucket) {
        foreach($this->buckets as $subscribed_bucket) {
            if ($subscribed_bucket->pivot->bucket_id == $bucket->id) {
                return true;
            }
        }

        return false;
    }
}
