<?php
namespace App\Contracts;

use App\User;
use App\Bucket;

interface ListOrganiserContract {
    public function createList($list);
    public function subscribeUser(User $user, Bucket $bucket);
    public function unsubscribeUser(User $user, Bucket $bucket);
}