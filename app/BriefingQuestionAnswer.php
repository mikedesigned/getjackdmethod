<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BriefingQuestionAnswer extends Model
{
    protected $fillable = ['briefing_question_answer'];

    public function question()
    {
        return $this->belongsTo( BriefingFormQuestion::class, 'briefing_question_id', 'id');
    }

    public function documents()
    {
        return $this->hasMany(BriefingFormDocument::class);
    }
}
