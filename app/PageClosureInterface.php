<?php
namespace App;

use Franzose\ClosureTable\Contracts\ClosureTableInterface;

interface PageClosureInterface extends ClosureTableInterface
{
}
