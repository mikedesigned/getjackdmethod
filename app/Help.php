<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Package;

class Help extends Model
{
    protected $fillable = ['content'];
    protected $table = "help";

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }
}
