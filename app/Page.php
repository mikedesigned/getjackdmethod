<?php
namespace App;

use Franzose\ClosureTable\Models\Entity;
use App\Package;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Page extends Entity implements PageInterface, SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'slug',
    ];

    protected $fillable = ['title', 'content', 'slug'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * ClosureTable model instance.
     *
     * @var PageClosure
     */
    protected $closure = 'App\PageClosure';

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }
}
