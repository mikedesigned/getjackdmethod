<?php

namespace App;

use Baum\Node;
use App\PlanContent;
use App\PlanTip;
use App\PlanDefaultContent;

class PlanCategory extends Node
{

    public function contents()
    {
        return $this->hasMany(PlanContent::class, 'cat_id');
    }

    public function tip()
    {
        return $this->hasOne(PlanTip::class, 'id', 'tip_id');
    }

    public function hasNoContent()
    {
        if (count($this->contents) > 0)
        {
            return true;
        } else {
            return false;
        }
    }

    public function default_content()
    {
        return $this->hasOne(PlanDefaultContent::class, 'id', 'default_content_id');
    }
}
