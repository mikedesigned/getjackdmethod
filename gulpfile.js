var elixir = require('laravel-elixir');
var gulp = require('gulp');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');

elixir.config.js.browserify.transformers.push({
    name: 'coffeeify',
    options: {}
});

elixir.config.js.browserify.transformers.push({
    name: 'hbsfy',
    options: {}
});

elixir(function(mix) {
    mix.sass('app.scss');
    mix.sass('admin.scss');
    mix.browserify('app.coffee');
    mix.scripts(['libs/pushy.min.js', 'libs/sticky.js']);
    mix.browserify('plan-builder.js');
    mix.browserify('dashboards.js');
    mix.browserify('admin.js');
});
