CKEDITOR.plugins.add( 'getjackd_contenteditor', {
	requires: 'widget',
	icons: 'getjackd_contenteditor',
	init: function ( editor ) {
		editor.widgets.add ( 'getjackd_contenteditor', {
			button: 'Create an hot tips box.',
			template:
				'<div class="getjackd_hot-tips">' +
					'<strong>Hot Tips</strong>' +
		            '<ul class="hot-tips"><li></li>' +
					'</ul>' +
		        '</div>',
		    editables: {
		    	content: {
		    		selector: '.getjackd_hot-tips'
		    	}
		    },
		    requiredContent: 'div(getjackd_hot-tips)',
            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'getjackd_hot-tips' );
            }
		});
	}
});
