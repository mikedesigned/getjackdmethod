var Vue = require('vue');
var Router = require('vue-router');
var VueResource = require('vue-resource');
Vue.use(Router);
Vue.use(VueResource);
Vue.config.debug = true;
var App = Vue.extend({});
var router = new Router();

    router.map({
    '/': {
        component: require('./components/plan-builder/components/PlanView.js'),
        subRoutes: {
            '/edit/:category_id': {
                name: 'edit-view',
                component: require('./components/plan-builder/components/Editor.js')
            },
            '/upload/:category_id': {
                name: 'upload-view',
                component: require('./components/plan-builder/components/Upload.js')
            },
            '/spreadsheet/:category_id': {
                name: 'spreadsheet-view',
                component: require('./components/plan-builder/components/Spreadsheet.js')
            }
        }
    }
});
router.beforeEach(function(transition) {
    transition.next();
});
router.start(App, '#app');