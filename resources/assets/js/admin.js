var $ = require('jquery');

var Vue = require('vue');
var moment = require('moment');
var Promise = require('es6-promise').Promise;
var VueResource = require('vue-resource');
var VueAsyncData = require('vue-async-data');
var Pikaday = require('pikaday');
var request = require('superagent');
Vue.use(VueResource);
Vue.use(VueAsyncData);
    
require('./libs/fullcalendar');

$(document).ready(function() {
    var day = new Pikaday({
        field: document.getElementById("day"),
        format: 'D MMM YYYY',
        onSelect: function() {
            console.log(this.getMoment().format('Do MMMM YYYY'));
        }
    });    

    $('form[name=editDefaultContent]').on('submit', function(event) {
        event.preventDefault();

        var contents = CKEDITOR.instances.default_content.getData();
        var category_id = $("input[name=category_id]").val();
        var default_content_id = $("input[name=default_content_id]").val();

        $("input[type=submit").val("Submitting...");

        request.put('/admin/categories/' + category_id + "/templates/" + default_content_id)
            .send({
                id: default_content_id,
                cat_id: category_id,
                default_content: contents
            })
            .end(function(err, res) {
                console.log(res);
                location.reload();
            });
    });

    $('form[name=editBusinessPlanTips]').on('submit', function(event) {
        event.preventDefault();

        var contents = CKEDITOR.instances.tip.getData();
        var category_id = $("input[name=category_id]").val();
        var tip_id = $("input[name=tip_id]").val();

        $("input[type=submit").val("Submitting...");

        request.put('/admin/categories/' + category_id + "/tips/" + tip_id)
            .send({
                id: tip_id,
                cat_id: category_id,
                tip: contents
            })
            .end(function(err, res) {
                console.log(res);
                location.reload();
            });
    });
    $(".delete-button").on('click', function(event){ 
        event.preventDefault();

        var acceptance = confirm("Do you wish to delete this page?");
        var page_id = $(this).data('page-id');

        if (acceptance == true) {  
            request.delete('/admin/pages/' + page_id).send({
                id: page_id
            }).end(function(err, res) {
                alert("Page deleted successfully.");
            });
        }

    });

    /*
    *   Capture the CKEDITOR data and submit via ajax so we don't lose data.
    */
    $("#edit-page").on('submit', function(event) {
        event.preventDefault();

        var content = CKEDITOR.instances.page_content.getData();
        var id = parseInt($("input[name='page_id']").val());
        var title = $("input[name='page_title']").val();
        var page_position = $('input[name="position"]').val();

        var formSubmit = $("#submit-button").addClass('disabled').html('Submitting...');

        request.put('/admin/pages/' + id).send({ 
            page_content: content, 
            page_title: title,
            position: page_position
        }).end(function(err, res){
            console.log(res);
            location.reload();
        }.bind(this));

    });

    $("#edit-package-help").on('submit', function(event) {
        event.preventDefault();

        var help_content = CKEDITOR.instances.content.getData();
        var id = parseInt($("input[name='help_id']").val());
        var package_id = parseInt($("input[name='package_id']").val());

        var formSubmit = $("#submit-button").addClass('disabled').val('Submitting...');

        request.put('/admin/help/' + id).send({ 
            content: help_content, 
            id: id,
            pid: package_id 
        }).end(function(err, res){
            console.log(res);
            location.reload();
        }.bind(this));

    });
});

var Calendar = Vue.extend({
    name: 'Calendar',
    data: function() {
        return { appointments: '' }
    },
    template: require('./templates/admin/calendar.html'),
    ready: function() {
        this.$nextTick(function() {
            $("#fullcalendar").fullCalendar({
                eventSources: [
                    {
                        url:'/appointments/get_bookings',
                        color: "#EC2C54",
                        textColor: "#FFFFFF"
                    },
                    {
                        url: '/appointments/get_free_slots',
                        color: "#E8E8E8",
                        textColor: "#000000"
                    }
                ],
                eventOrder: "start",
                defaultView:'month',
                theme: 'true',
                height: 500,
                width: 500
            })
        })
    }
});

Vue.component('Calendar', Calendar);

var vm = new Vue({
    el: "#calendar",
    props: ['appointments']
});