moment = require('moment')
clndr = require('clndr')
$ = require('jquery')
swal = require('sweetalert')

class Appointment

    constructor: (@element, @appointmentType) ->
        # Get instances for jQuery/CLNDR
        $el = $(@element)
        @calendarInstance = @initCalendar($el)

        # Bind Events
        $(document).on('click', '#bookAppointment', @bookAppointment)

        # Set the events for the current month on init.
        @setMonthEvents(moment(), @calendarInstance)

    destroy: () ->
        @calendarInstance.destroy()

    setMonthEvents: (month = moment(), calendarInstance) ->
        $.get '/appointments/forMonth/' + month.format('YYYY-MM-01'), (data) =>
            events = []
            $.each data.dates, (key, val) ->
                events.push({'date': val})
            calendarInstance.setEvents(events)

    getAppointmentsForDay: (day) ->
        $.get '/appointments/forDate/' + day.dateObj.format('YYYY-MM-DD'), (data) =>
            template = require '../../views/templates/modal.pickTime.hbs'
            html = template({
                'pickedDay': day, 
                'appointments': data.appointments, 
                'appointment_type': @appointmentType
            })
            @renderTemplate("#pickTime", html)

    bookAppointment: () ->
        appointmentId = $("input[name='appointment_selected']:checked").data('appointment-id')
        appointmentType = $("#bookAppointment").data('appointment-type')
        $.post "/appointments/book",
            appointment_id: appointmentId
            type: appointmentType
            (data) ->
                swal({
                    title: "Awesome!"
                    text: data,
                    type: "success"
                    }, () -> 
                        location.reload();
                    )
            
    renderTemplate: (selector, templateName) ->
        $("#{selector}").html(templateName)

    initCalendar: (element) ->
        _this = @
        $(element).clndr
            clickEvents:
                click: (target) ->
                    pickedDay =
                        picked: moment(target.date._i).format('MMMM Do, YYYY'),
                        dateObj: moment(target.date._i)
                    _this.getAppointmentsForDay(pickedDay)
                onMonthChange: (month) ->
                    _this.setMonthEvents(month, _this)
                    @.render()
            render: (data) ->
                template = require '../../views/templates/calendars.appointment.hbs'
                return template(data)

module.exports = Appointment