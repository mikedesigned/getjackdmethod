window.$ = window.jQuery = require 'jquery'
Appointment = require './Appointment.coffee'
swal = require 'sweetalert'
moment = require 'moment'
Handlebars = require 'hbsfy/runtime'
Boostrap = require 'bootstrap'

Handlebars.registerHelper "formatDate", (datetime, format) ->
        if moment
            DateFormats =
                short: "ha"
            format = DateFormats[format] or format
            moment(datetime).format(format)
        else
            datetime

Handlebars.registerHelper "equal", (lvalue, rvalue, options) ->
        if arguments.length < 3
            throw new Error "Handlebars Helper equal needs 2 parameters"
        if lvalue isnt rvalue
            options.inverse(this)
        else
            options.fn(this)

$(document).ready ->

    $('[data-toggle="tooltip"]').tooltip()

    $(".modal-body a").attr("target","_blank")
    # book briefing button on the market it plan.
    $(".bookAppointment").on "click", (event) ->
        event.preventDefault()
        appointmentType = $(this).data('appointment-type');
        template = require '../../views/templates/modal.appointment.hbs'
        modal = $(template({'appointment_type': appointmentType})).modal
            backdrop: true

        appointmentCalendar = {}
        # load the calendar once the modal has fully loaded.
        modal.on 'shown.bs.modal', () ->
            appointmentCalendar = new Appointment "#appointmentCalendar", appointmentType
