var Vue = require('vue')

export default Vue.extend({
    name: 'Modal',
    template: require('../templates/modal-template.html'),
    props: {
        show: {
            type: Boolean,
            required: true,
            twoWay: true
        }
    }
});
