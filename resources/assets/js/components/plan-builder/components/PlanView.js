var Vue = require('vue');
var cookies = require('cookies');

export default Vue.extend({
    name: 'PlanView',
    template: require('../templates/planview-template.html'),
    components: {
        'category': require('./Category.js')
    },
    props: {
        plan: Number,
        type: String,
        edited: Number
    },
    computed: {
        apiUrl: function() {
            console.log("Testing 123.");
            return '/plan/' + this.type + '/'
        }
    },
    data: function() {
        return {
            categories: '',
            active: '',
            safe: true
        }
    },
    methods: {
        publishToPDF: function() {
            this.$http.get(this.apiURL + this.plan.id, function (data) {
                swal("PDF Generating.", "It should appear in the 'download my plan' section within a few minutes!", "success")
            })
        }
    },
    events: {
        'set-active': function(active) {
            this.active = active;
        },
        'bootstrap': function() {
            console.log(this.$data.edited);
        },
        'draft-created': function() {
            console.log("Draft Created Event Thrown...");
            this.$broadcast('update-tags');
        },
        'content-deleted': function() {
            console.log("An item was deleted");
            this.$broadcast('deleted-item');
        },
        'content-published': function() {
            console.log("Published Event Thrown...");
            this.$broadcast('update-tags');
        }
    },
    created: function() {
        this.$http.get('/api/categories/' + this.$data.plan, function(categories) {
            this.$set('categories', categories)
            this.$emit('bootstrap')
        }.bind(this));
    }
});