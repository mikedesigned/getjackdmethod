var Vue = require('vue');
var swal = require('sweetalert');

export default Vue.extend({
    name: 'Spreadsheet',
    template: require('../templates/spreadsheet-template.html'),
    components: {
        'modal': require('./HintModal.js')
    },
    props: ['active', 'plan', 'type'],
    data: function() {
        return {
            showModal: false,
            hot: Object
        };
    },
    computed: {
        apiEndpoint: function() {
            if (this.$data.active.category.id == 42) {
                return '/api/spreadsheet/profitloss'
            }
            if (this.$data.active.category.id == 43) {
                return '/api/spreadsheet/balancesheet'
            }
            if (this.$data.active.category.id == 44) {
                return '/api/spreadsheet/expected'
            }
        }
    },
    methods: {
        submitSpreadsheet: function(spreadsheet) {
            var spreadsheetInfo = {
                spreadsheet: JSON.stringify({
                    data: spreadsheet
                }),
                category_id: this.$data.active.category.id,
                plan_id: this.$data.plan,
                plan_type: this.$data.type,
                status: 1,
                category_type: this.$data.active.category.content_type
            };
            this.$http.post('/plan/business/content', spreadsheetInfo, function() {
                swal("Spreadsheet Saved!", "success");
            });
        }
    },
    route: {
        canReuse: false, // if i don't do this, it doesn't reinitalize the ckeditor and transfers the data over to the next component load.
        deactivate: function(transition) {
            console.log("Deactivating component...", this);
            this.$remove(function() {
                transition.next();
            });
        }
    },
    created: function() {
    }
})