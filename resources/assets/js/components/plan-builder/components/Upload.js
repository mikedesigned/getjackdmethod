var Vue = require('vue')
var $ = require('jquery')
var Dropzone = require('dropzone')

export
default Vue.extend({
    name: 'Upload',
    template: require('../templates/upload-template.html'),
    components: {
        'modal': require('./HintModal.js')
    },
    props: ['active', 'plan', 'type'],
    data: function () {
        return {
            showModal: false,
            documents: []
        }
    },
    computed: {
        apiUrl: function() {
            var url;
            if (this.$data.type == "marketing") {
                url = '/plan/marketing/'
            } else {
                url = '/plan/business/'
            }

            return url;
        }
    },
    route: {
        canReuse: true,
        deactivate({
            next
            }) {
            console.log("Deactivating component...", this)
            this.$remove(function () {
                next()
            })
        }
    },
    methods: {
        grabUploadedDocuments: function () {
            // grab all the uploaded documents.
            this.$http.get(this.apiUrl + this.plan + '/documents', function (documents) {
                this.$data.documents = documents;
            }.bind(this));
        },
        destroyDocument: function (document) {
            console.log(document);
            this.$http.delete(this.apiUrl + this.plan + '/documents/' + document.id, function() {
                this.documents.$remove(document);
            }.bind(this));
        }
    },
    created: function () {
        this.grabUploadedDocuments();

        this.$nextTick(function () {
            var myDropzone = new Dropzone("div#dz", {
                url: this.apiUrl + "upload",
                acceptedFiles: 'application/pdf',
                dictDefaultMessage: 'Drag your PDF files here to upload.'
            });

            // while it's sending, attach a few extra fields to the file array.
            myDropzone.on('sending', function (event, xhr, formData) {
                formData.append('plan_id', this.$data.plan);
                formData.append('plan_type', this.$data.type);
                formData.append('category_id', this.$data.active.category.id);
            }.bind(this));

            // if it's successful, add it into the documents array.
            myDropzone.on('success', function (event, document) {
                console.log("Document uploaded successfully");
                this.$data.documents.push(document); // push the plan into the uploaded documents array
            }.bind(this));

        }.bind(this));

    }
})