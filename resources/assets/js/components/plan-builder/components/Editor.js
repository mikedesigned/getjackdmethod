var Vue = require('vue')
var $ = require('jquery')
var swal = require('sweetalert')

export default Vue.extend({
    name: 'Editor',
    template: require('../templates/editor-template.html'),
    components: {
        'modal': require('./HintModal.js')
    },
    props: ['active', 'plan', 'type', 'safe'],
    data: function() {
        return {
            section_text: '',
            showModal: false
        }
    },
    computed: {
        hasContent: function() {
            return this.active.content !== undefined;
        },
        hasDefault: function() {
            return this.active.default_content !== null;
        },
        isDraft: function() {
            return this.active.content !== undefined && this.active.content.status == 0;
        },
        editorContent: function() {
            if (this.hasContent) {
                return this.active.content.content;
            } else {
                if (this.active.default_content == null) {
                    return "";
                }
                return this.active.default_content.content;
            }
        },
        apiUrl: function() {
            var url;
            if (this.$data.type == "marketing") {
                url = '/plan/marketing/content'
            } else {
                url = '/plan/business/content'
            }

            return url;
        }
    },
    methods: {
        create: function(content) {
            this.$http.post(this.apiUrl, content, function(data) {
                var newUserContent = {
                    id: data.id,
                    cat_id: data.cat_id,
                    status: data.status,
                    content: data.content
                };
                this.$set('active.content', newUserContent);
                if (!this.hasContent) {
                    swal("Draft Saved!", "Your draft has been saved.", "success");
                    this.$dispatch('draft-created');
                } else {
                    if (content.status == 0) {
                        swal("Draft Saved!", "Your draft has been saved.", "success");
                        this.$dispatch('draft-created');
                    } else {
                        swal("Published!", "Your content has been published to your plan.", "success");
                        this.$dispatch('content-published');
                    }
                }
                return true;
            }.bind(this)).error(function(data) {
                swal("Error!", data, "error");
                return false;
            });
        },
        setContent: function(draftStatus) {
            // build up the plan object and return it
            return {
                content_id: this.hasContent ? this.$data.active.content.id : null,
                section_text: this.section_text,
                plan_id: this.$data.plan,
                plan_type: this.$data.type,
                category_id: this.$data.active.category.id,
                status: draftStatus ? 0 : 1
            };
        },
        submitDraft: function() {
            // ckeditor does some iframe bullshit
            // so gotta update the element into the textarea
            // before we send it off otherwise it gets nothing.
            var ckinstance = "richText_" + this.$data.active.category.id;
            this.$data.section_text = CKEDITOR.instances[ckinstance].getData();

            var content = this.setContent(true);

            if (this.hasContent) {
                swal({
                    title: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#016A97",
                    confirmButtonText: "Save",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                }, function() {
                    this.create(content);
                }.bind(this))
            } else {
                this.create(content);
            }
        },
        publishContent: function() {
            // ckeditor does some iframe bullshit
            // so gotta update the element into the textarea
            // before we send it off otherwise it gets nothing.
            var ckinstance = "richText_" + this.$data.active.category.id;
            this.$data.section_text = CKEDITOR.instances[ckinstance].getData();

            var content = this.setContent(false);

            swal({
                title: "Are you sure?",
                text: "Would you like to publish this content?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#016A97",
                confirmButtonText: "Publish",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                this.create(content);
            }.bind(this))
        },
        deleteContent: function() {
            swal({
                title: "Are you sure?",
                text: "This will delete your content.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "red",
                confirmButtonText: "Delete",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                this.$http.delete('/plan/business/content/' + this.$data.active.content.id, function(data) {
                    this.$dispatch('content-deleted')
                    var ckinstance = "richText_" + this.$data.active.category.id;
                    this.$set('active.content', undefined);
                    swal("Deleted!", data, "success");
                    CKEDITOR.instances[ckinstance].setData(this.editorContent);
                    return true
                }.bind(this))
            }.bind(this))
        }
    },
    route: {
        canReuse: false, // if i don't do this, it doesn't reinitalize the ckeditor and transfers the data over to the next component load.
        deactivate: function(transition) {
            console.log("Deactivating component...", this);
            this.$remove(function() {
                transition.next();
            });
        },
        canDeactivate: function(transition) {
            transition.next();
        }
    },
    beforeDestroy: function() {
        console.log("Destroying...");
    },
    attached: function() {
        this.$nextTick(function() {
            $('#editor').find('textarea').each(function() {
                CKEDITOR.replace(this.id, {
                    uiColor: '#FFFFFF',
                    height: '400px'
                });
                CKEDITOR.config.skin = 'office2013';
                CKEDITOR.config.fillEmptyBlocks = true;
                CKEDITOR.config.extraPlugins = 'autocorrect,divarea';
                CKEDITOR.config.removePlugins = 'image,sourcedialog';
                CKEDITOR.config.filebrowserBrowseUrl = '/elfinder/ckeditor';
                CKEDITOR.config.scayt_defLan = 'en_GB';
                CKEDITOR.config.extraAllowedContent = 'div(*)';
                CKEDITOR.config.scayt_sLang = 'en_GB';
                CKEDITOR.config.scayt_autoStartup = true;
            });
        });
    }
})
