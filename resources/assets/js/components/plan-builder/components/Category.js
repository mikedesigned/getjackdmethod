var Vue = require('vue');

export default Vue.extend({
    name: "Category",
    template: require('../templates/category-template.html'),
    props: ['category', 'plan', 'type', 'content', 'default', 'active', 'safe'],
    methods: {
        setActive: function() {
            var active = {
                category: this.category,
                content: this.content,
                default_content: this.default
            };
            /*
            if (this.active) {
                var ckinstance = "richText_" + this.$data.active.category.id;
                console.log(ckinstance);
                if(CKEDITOR.instances[ckinstance].checkDirty()) {
                    swal({
                        title: "You have unsaved changes.",
                        text: "Did you want to keep what you were working on?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "red",
                        confirmButtonText: "No, keep going.",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: false
                    }, function() {
                         this.$dispatch('set-active', active);
                    }.bind(this))
                } else {
                    this.$dispatch('set-active', active);
                }
            } else {
                this.$dispatch('set-active', active);
            }*/
            this.$dispatch('set-active', active);
        }
    },
    events: {
        'testing': function() {
            console.log("testing");
        },
        'update-tags': function() {
            if (this.$data.category.id == this.$data.active.category.id) {
                this.$set('content', this.$data.active.content);
            }
            this.$broadcast('update-tags');
        },
        'deleted-item': function() {
            if (this.$data.category.id == this.$data.active.category.id) {
                console.log("Deleting content...", this);
                this.$set('content', undefined);
            }
            this.$broadcast('deleted-item');
        }
    },
    computed: {
        hasChildren: function() {
            return this.category.children && this.category.children.length;
        },
        isDraft: function() {
            if (this.$data.content.status == 1) {
                return false;
            } else {
                return true;
            }
        },
        hasContent: function() {
            return this.content !== undefined;
        }
    }
})
