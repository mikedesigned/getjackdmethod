var Vue = require('vue')

export
default Vue.extend({
    name: 'HintsModal',
    template: require('../templates/hint_modal-template.html'),
    props: {
        tip: '',
        show: {
            type: Boolean,
            required: true,
            twoWay: true
        }
    },
    data: function() {
        return {
            helpfultip: ''
        }
    },
    attached: function() {
        this.$http.get('/api/tips/' + this.tip, function(data, status, request) {
            this.helpfultip = data.tip
        })
    }
})