var Vue = require('vue')
var moment = require('moment')
var swal = require('sweetalert')

export default Vue.extend({
    name: 'DownloadModal',
    template: require('../templates/modal-download-window.html'),
    props: {
        plans: [],
        type: String,
        show: {
            type: Boolean,
            required: true,
            twoWay: true
        }
    },
    components: {
        'plan': {
            template: require('../templates/modal-download-template.html'),
            props: ['plan', 'type'],
            computed: {
                hasFile: function() {
                    return this.plan.filepath !== null
                },
                formattedTime: function() {
                    return moment(this.plan.generated_date).fromNow()
                },
                apiUrl: function() {
                    return "/plan/" + this.$data.type + "/";

                },
                contentUrl: function() {
                    return "/plan/" + this.$data.type + "/" + this.$data.plan.id;
                }
            },
        }
    }
})
