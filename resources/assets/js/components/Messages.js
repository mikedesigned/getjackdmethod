var Vue = require('vue')
var toastr = require('toastr')
var _ = require('underscore')

window.messages = []

export
default Vue.extend({
    name: 'Messages',
    template: require('../templates/messages.html'),
    components: {
        'notification-popover': require('./NotificationPopover')
    },
    data: function() {
        return {
            messages: messages ,
            showMessagePopover: false
        }
    },
    computed: {
        count: function() {
            return this.$data.messages.length
        }
    },
    props: ['channelName', 'isAdmin'],
    created: function() {
        this.startPusher();
        this.fetchNotifications();
    },
    methods: {
        notify: function(notification_message) {
            toastr.success(notification_message, 'GJM');
        },
        startPusher: function() {
            var pusher = new Pusher('ca133eca8a6be9c83899', {
                encrypted: true
            });
            console.log(this.$data.channelName);
            var channel = pusher.subscribe(this.$data.channelName);
            var $_this = this;
            if (this.$data.isAdmin == '1') {
                var admin_channel = pusher.subscribe('admin');
                admin_channel.bind('App\\Events\\NewAppointmentBooking', function(data) {
                    $_this.$data.messages.unshift(data.message);
                    $_this.notify('New ' + (data.appointment.type == 1 ? 'Briefing' : 'Review') + ' session booked by: ' + data.user.first_name + ' ' + data.user.last_name);
                });
                admin_channel.bind('App\\Events\\NewPackagePurchased', function(data) {
                    $_this.$data.messages.unshift(data.message);
                    $_this.notify('New package ' + data.package.name + ' purchased by: ' + data.user.first_name + ' ' + data.user.last_name);
                });
                admin_channel.bind('App\\Events\\NewUserRegistration', function(data) {
                    $_this.$data.messages.unshift(data.message);
                    $_this.notify('New user signed up: ' + data.user.first_name + ' ' + data.user.last_name);
                });
            }
            channel.bind('App\\Events\\PDFDocumentCreated', function(data) {
                $_this.notify('PDF document for your plan: ' + data.plan.name + ' created');
            });
        },
        fetchNotifications: function() {
            this.$http.get('/notifications', function (data) {
                this.$data.messages = data;
            });
        }
    }
});