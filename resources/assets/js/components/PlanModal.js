var Vue = require('vue');
var moment = require('moment');
var swal = require('sweetalert');

export default Vue.extend({
    name: 'PlanModal',
    template: require('../templates/modal-plans-window.html'),
    props: {
        plans: [],
        type: String,
        show: {
            type: Boolean,
            required: true,
            twoWay: true
        }
    },
    computed: {
        apiUrl: function () {
            return "/plan/" + this.$data.type;
        }
    },
    data: function () {
        return {
            plan_name: ''
        }
    },
    events: {
        'new-plan': function (plan) {
            this.$data.plans.push(plan);
            swal("Success!", "Your plan was cloned, including it's content.", "success");
        }
    },
    methods: {
        createPlan: function () {

            var content = {
                plan_name: this.$data.plan_name
            };

            this.$http.post(this.apiUrl, content, function (data) {

                var plan = {
                    id: data.id,
                    updated_at: data.updated_at,
                    created_at: data.created_at,
                    name: data.name,
                    filepath: null
                };

                this.$data.plans.push(plan);

            }.bind(this));

        }
    },
    components: {
        'plan': {
            template: require('../templates/modal-plans-template.html'),
            props: ['plan', 'type'],
            computed: {
                hasFile: function () {
                    return this.$data.plan.filepath !== null
                },
                formattedTime: function () {
                    var createdDate = moment(this.plan.created_at);
                    console.log(createdDate);
                    return moment(createdDate).fromNow();
                },
                apiUrl: function () {
                    return "/plan/" + this.$data.type + "/";

                },
                contentUrl: function () {
                    return "/plan/" + this.$data.type + "/" + this.$data.plan.id;
                }
            },
            methods: {
                dispatchPDFCreation: function (event) {
                    event.preventDefault();
                    this.plan.filepath = "pending";
                    this.plan.generated_date = moment();
                    this.$http.get(this.apiUrl + this.plan.id, function (data) {
                        console.log(data);
                    });
                    swal("Your PDF is being generated.", "It should appear in the 'download my plan' section within a few minutes!", "success");
                },
                clonePlan: function (plan) {

                    swal({
                        title: "Clone Plan",
                        text: "Select a name for the new plan",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonColor: "#016A97",
                        showLoaderOnConfirm: true,
                        animation: "slide-from-top",
                        inputPlaceholder: "New Plan Name"
                    }, function (planName) {
                        if (planName === false) return false;
                        if (planName === "") {
                            swal.showInputError("You need to select a name!");
                            return false;
                        }
                        var postData = {
                            planNewName: planName
                        };
                        this.$http.post(this.apiUrl + plan.id + '/replicate', postData, function (plan) {
                            console.log(plan);
                            this.$dispatch('new-plan', plan)
                        }.bind(this));
                    }.bind(this));

                },
                deletePlan: function (event) {
                    event.preventDefault();
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this plan.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    }, function () {
                        this.$remove();
                        this.$http.delete(this.apiUrl + this.plan.id, function (data) {
                            swal("Deleted!", "Your plan file has been deleted.", "success");
                            console.log(data);
                        });
                    }.bind(this));
                }
            }
        }
    }
})
