var Vue = require('vue')

export default Vue.extend({
    name: 'Message',
    template: require('../templates/message.html'),
    props: ['message']
});
