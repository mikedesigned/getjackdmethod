var Vue = require('vue')

window.messages = [
    { message: "something" },
    { message: "something else" }
]

export default Vue.extend({
    name: 'NotificationPopover',
    template: require('../templates/notification-window.html'),
    props: {
        messages: [],
        show: {
            type: Boolean,
            required: true,
            twoWay: true
        }
    },
    components: {
        'message': require('./Message')
    }
});
