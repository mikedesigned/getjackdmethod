var Vue = require('vue')
var VueResource = require('vue-resource')
var $ = require('jquery')

Vue.use(VueResource)

var dashboard = new Vue({
    el: "#app-dashboard",
    name: "Packages",
    components: {
        'modal': require('./components/Modal')
    },
    data: function() {
        return {
            showModal: false,
            userPackages: Object
        }
    },
    created: function() {
        this.$http.get('/api/packages', function(data, status, request) {
            this.$set('userPackages', data)
        })
    }
})

var businessDashboard = new Vue({
    el: "#business-dashboard",
    name: "BusinessDash",
    components: {
        'plan-modal': require('./components/PlanModal'),
        'download-modal': require('./components/DownloadModal')
    },
    data: function() {
        return {
            showModal: false,
            showDownloadModal: false,
            plans: []
        }
    },
    created: function() {
        this.$http.get('/api/plans/business', function(business_plans) {
            this.$set('plans', business_plans);
        });
    }
});

var marketingDashboard = new Vue({
    el: "#marketing-dashboard",
    name: "MarketingDash",
    components: {
        'plan-modal': require('./components/PlanModal'),
        'download-modal': require('./components/DownloadModal')
    },
    data: function() {
        return {
            showModal: false,
            showDownloadModal: false,
            plans: []
        }
    },
    created: function() {
        this.$http.get('/api/plans/marketing', function(marketing_plans) {
            this.$set('plans', marketing_plans);
        });
    }
});
