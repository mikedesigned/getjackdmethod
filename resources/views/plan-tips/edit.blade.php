@extends('layouts.admin')
@section('sectionName', 'Create Tip ')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Editing Helpful Hint</div>
    <div class="panel-body">
        <p class="lead">Category Name: {{ $category->name }}</p>
        <div class="alert alert-info"><p>Please write the information you'd like to appear on this particular section of the Plan here. Each section can only have one instance of a "tip" so just come back and edit this to change whatever appears.</p></div>
        <form action="/admin/categories/{{$category->id}}/tips/{{$tip->id}}" method="post" name="editBusinessPlanTips">
            {!! csrf_field() !!}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="category_id" value="{{$category->id}}" />
            <input type="hidden" name="tip_id" value="{{$tip->id}}" />
            <div class="form-group">
                <textarea class="ckeditor form-control" rows="25" name="tip">{!! $tip->tip !!}</textarea>
            </div>
            <input type="submit" class="btn btn-success" value="Update Tip">
        </form>
    </div>
</div>
@stop