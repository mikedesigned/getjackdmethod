@extends('layouts.admin')
@section('sectionName', 'Create Tip ')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Creating Helpful Hint</div>
    <div class="panel-body">
        <p class="lead">Category Name: {{$category->name}}</p>
        <div class="alert alert-info"><p>Please write the information you'd like to appear on this particular section of the Business Plan here. Each section can only have one instance of a "tip" so just come back and edit this to change whatever appears.</p></div>
        <form action="/admin/categories/{{$category->id}}/tips" method="post" name="createBusinessPlanTips">
            {!! csrf_field() !!}
            <div class="form-group">
                <textarea id="tipEditor" class="ckeditor form-control" rows="25" name="tip"></textarea>
            </div>
            <input type="submit" class="btn btn-success" value="Save Tip">
        </form>
    </div>
</div>
@stop