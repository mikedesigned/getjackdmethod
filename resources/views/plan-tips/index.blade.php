@extends('layouts.admin')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Viewing All Helpful Hints</div>
        <table class="table">
            <tr>
                <th>Category</th>
                <th>Tip Text</th>
                <th>Actions</th>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>(<strong>ID: </strong>{{$category->id}}) {{$category->name}}</td>
                    <td>{!! $category->tip['tip'] ? $category->tip['tip'] : 'No tip created.' !!}</td>
                    @if(empty($category->tip['tip']))
                        <td valign="center"><a class="btn btn-primary btn-block" href="/admin/categories/{{$category->id}}/tips/create">Create Tip</a></p></td>
                    @else
                        <td><a class="btn btn-success btn-block" href="/admin/categories/{{$category->id}}/tips/{{$category->tip['id']}}/edit">Edit Tip</a></td>
                    @endif
                </tr>
            @endforeach
        </table>
        <div class="panel-footer">{!! $categories->render() !!}</div>
    </div>
@stop
