@extends('layouts.master')
@section('title', 'Your Plan Editor')
@section('content')

<div id="app">
        <router-view v-ref:router :plan="{{ $plan->id }}" type="{{$plan_type}}" :edited="{{ count($plan->contents) }}"></router-view>
</div>
@stop

@section('javascripts')
    <script type="text/javascript" src="/js/plan-builder.js"></script>
@stop