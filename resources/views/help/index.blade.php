@extends('layouts.admin')
@section('sectionName', 'Manage Help')
@section('sidebar')
    @parent
@stop

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Package Help</h3>
		</div>

		<div class="panel-body">
		<table class="table table-condensed">
			@foreach($helpItems as $help)

				<tr>
					<td>{{$help->package->name}}</td>
					<td><a href="{{route('help.edit', $help->id)}}" class="btn btn-success">Edit</a></td>
				</tr>
				
			@endforeach	
		</table>

		</div>
	</div>

@stop