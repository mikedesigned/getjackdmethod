@extends('layouts.admin')
@section('sectionName', 'Manage Help')
@section('sidebar')
    @parent
@stop

@section('content')

        <div class="panel panel-default">
    	<div class="panel-heading">
    	<h3 class="panel-title">Edit <strong>{{$help->package->name}}</strong> Help</h3>
    	</div>

    	<div class="panel-body">
    		<form class="form" method="POST" action="/admin/help/{{$help->id}}" id="edit-package-help">
				<input type="hidden" name="help_id" value="{{$help->id}}">
				<input type="hidden" name="package_id" value="{{$help->package->id}}">
                <input name="_method" type="hidden" value="PUT">
    			<div class="form-group">
    				<textarea name="content" id="content" class="ckeditor form-control" >{{$help->content}}</textarea>
    			</div>

    			<div class="form-group">
    				<input type="submit" id="submit-button" value="Update Help" class="btn btn-success" />		
    			</div>
    		</form>
    	</div>
    </div>

@stop