@extends('layouts.admin')
@section('sectionName', 'Manage Help')
@section('sidebar')
    @parent
@stop

@section('content')

    <div class="panel panel-default">
    	<div class="panel-heading">
    	<h3 class="panel-title">Create Package Help</h3>
    	</div>

    	<div class="panel-body">
    		<form class="form" method="POST" action="/admin/help" id="create-package-help">

			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Package</h3></div>
				<div class="panel-body">
					<div class="form-group">
		                <label for="page_title" class="col-sm-2 control-label"><strong>Package</strong></label>
		                <div class="col-sm-10">

		                    <select name="package" id="package_select" class="form-control">
		                        @foreach($packages as $package)
		                            <option value="{{$package->id}}">{{$package->name}}</option>
		                        @endforeach
		                    </select>
		                    
		                </div>
		            </div>
				</div>
			</div>

			<div class="panel panel-info">
				<div class="panel-heading"><h3 class="panel-title">Editor</h3></div>
				<div class="panel-body">
					<div class="form-group">
    				<textarea name="content" id="content" class="ckeditor form-control" ></textarea>
    			</div>

    			<div class="form-group">
    				<input type="submit" value="Create Help" class="btn btn-success" />		
    			</div>
				</div>
			</div>
    			
    		</form>
    	</div>
    </div>

@stop