@extends('layouts.admin')

@section('sectionName', 'View Business Plan Templates')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Viewing All Content Templates</div>
        <table class="table">
            <thead>
            <tr>
                <th>Category</th>
                <th>Has Template?</th>
                <th>Action</th>
            </tr>
            </thead>
            @foreach($categories as $cat)
                <tr>
                    <td>{{$cat->name}}</td>
                    @if($cat->default_content)
                        <td><span class="text-success text-center">Yup!</span></td>
                        <td><a class="btn btn-success btn-block" href="/admin/categories/{{$cat->id}}/templates/{{$cat->default_content->id}}/edit">Edit</a></td>
                    @else
                        <td><span class="text-danger text-center">None defined.</span></td>
                        <td><a href="/admin/categories/{{$cat->id}}/templates/create" class="btn btn-primary btn-block">Create</a></td>
                    @endif
                </tr>
            @endforeach
        </table>

        <div class="panel-footer">{!! $categories->render() !!}</div>
    </div>

@stop
