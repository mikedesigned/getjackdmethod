@extends('layouts.admin')
@section('sectionName', 'Edit Template')
@section('content')
<div class="alert alert-info"><p>Please provide a default content (if any) that the user needs to fill out in this particular section.</p></div>
<form action="/admin/categories/{{$category->id}}/templates/{{$default_content->id}}" method="post" name="editDefaultContent">
    {!! csrf_field() !!}
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Edit Content Template</h3></div>
        <input type="hidden" name="category_id" value="{{$category->id}}" />
        <input type="hidden" name="default_content_id" value="{{$default_content->id}}">
        <div class="panel-body">
            <input type="hidden" name="_method" value="PUT">
            <textarea class="form-control ckeditor" rows="25" name="default_content">{!! $default_content->content !!}</textarea>
        </div>
        <div class="panel-footer">
            <input type="submit" class="btn btn-success" value="Update Template">
        </div>
    </div>
</form>
@stop