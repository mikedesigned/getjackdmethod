@extends('layouts.admin')
@section('sectionName', 'Create Template ')
@section('content')
<form action="/admin/categories/{{$category->id}}/templates" method="post" name="createDefaultContent">
{!! csrf_field() !!}
<div class="alert alert-info"><p>Please provide a default content (if any) that the user needs to fill out in this particular section.</p></div>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Create Content Template</h3></div>
        <div class="panel-body">
            <input type="hidden" name="category_id" value="{{$category->id}}">
            <textarea class="ckeditor form-control" rows="25" name="default_content"></textarea>
    </div>

        <div class="panel-footer">
            <input type="submit" class="btn btn-success" value="Save Template">
        </div>
    </div>
    </form>
@stop