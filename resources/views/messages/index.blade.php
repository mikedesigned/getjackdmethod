@extends('layouts.admin')
@section('sectionName', 'Create Pages')
@section('content')
    <form id="broadcast-message" method="post" action="/admin/message" class="form-horizontal">
        {!! csrf_field() !!}
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Broadcast Message</h3></div>
            <div class="panel-body">
                @if (Session::has('success_message'))
                    <div class="alert alert-success">{{ Session::get('success_message') }}</div>
                @endif

                <p class="text-muted">From here, you can broadcast out messages to the entire app userbase. Notify them of a weblink to a new EDM or wish everyone a Merry Christmas.</p>
                <div class="form-group">
                    <label for="subject" class="col-sm-2 control-label">Subject</label>
                    <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject">
                    </div>
                </div>
                <div class="form-group">
                    <label for="body" class="col-sm-2 control-label">Message</label>
                    <div class="col-sm-10">
                        <textarea rows="10" name="body" class="form-control" id="body" placeholder="Message Body"></textarea>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Broadcast</button>
            </div>
        </div>
    </form>

    <div class="panel panel-info">

        <div class="panel-heading"><h3 class="panel-title">Sent Messages</h3></div>

        <table class="table table-condensed">

            <thead>
                <th>Date Sent</th>
                <th>Subject</th>
                <th>Body</th>
            </thead>

            <tbody>
                @foreach ($messages as $message)
                    <tr>
                        <td>{{ $message->created_at->toDayDateTimeString() }}</td>
                        <td>{{ $message->subject }}</td>
                        <td>{{$message->body}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="panel-footer">
        </div>
    </div>
@stop