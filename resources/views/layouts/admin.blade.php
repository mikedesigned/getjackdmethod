<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GJM | Back of House</title>
    <!-- Custom styles for this template -->
    <!-- Optional theme -->
    <link href="/css/jquery-ui.min.css" rel="stylesheet">
    <link href="/css/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/css/jquery-ui.theme.min.css" rel="stylesheet">
    <link href="/css/admin.css" rel="stylesheet">

</head>
<body id="admin">
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">GetJack'D Portal Back of House</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/auth/logout">Logout</a></li>
            </ul>

        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            @section('sidebar')
                @include('partials.admin.sidebar')
            @show
        </div>
        <div class="col-md-9">
            @yield('content')
        </div>
    </div>
</div>
<script src="/js/all.js"></script>
<script src="/vendor/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    CKEDITOR.config.height = "600px";
    CKEDITOR.config.skin = 'moono';
    CKEDITOR.config.fillEmptyBlocks = true;
    CKEDITOR.config.extraPlugins = 'autocorrect,divarea,getjackd_contenteditor';
    CKEDITOR.config.filebrowserBrowseUrl = '/elfinder/ckeditor';
    CKEDITOR.config.scayt_defLan = 'en_GB';
    CKEDITOR.config.extraAllowedContent = 'div(*)';
    CKEDITOR.config.scayt_sLang = 'en_GB';
    CKEDITOR.config.scayt_autoStartup = true;
</script>
<script src="/js/admin.js"></script>
</body>
</html>