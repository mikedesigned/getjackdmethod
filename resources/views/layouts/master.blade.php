<!DOCTYPE html>
<html class="@yield('bodyClass')">
    {{-- HEAD SECTION PARTIAL --}}
    @include('partials.html.head')
    <body class="body">
        {{-- CONTENT CANVAS MENU --}}
        @include('partials.html.canvasMenu')
        {{-- SITE OVERLAY --}}
        <div class="site-overlay"></div>
        <div class="container-fluid">
            {{-- HEADER INC MENUS --}}
            @include ('partials.html.header')
            <div class="row">
                {{-- MAIN CONTENT SECTION--}}
                @yield('content')
            </div>
            <div class="row">
                {{-- FOOTER PARTIAL --}}
                @include ('partials.html.footer')
            </div>
        </div>
        {{-- FOOTER JAVASCRIPTS WITH YIELD FOR EXTRA --}}
        @include('partials.html.javascripts')
        @yield('javascripts')
    </body>
</html>