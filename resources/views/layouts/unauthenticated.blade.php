<!DOCTYPE html>
<html>
    @include('partials.html.head')

    <body class="unauthenticated @yield('bodyClass')">

        @include('partials.html.canvasMenu')
        <!-- Site Overlay -->
        <div class="site-overlay"></div>
        <div class="container-fluid">
            <header class="row clearfix headerSection">
                <div id="logo" class="col-xs-6 col-md-4">
                    <a href="/dashboard"><h1><img src="/img/svg/getjackd-logo.svg" alt="GetJack'D Method!" /></h1></a>
                </div>
                <div class="col-xs-6 col-md-8" id="signInPrompt">
                    <p class="login-text pull-right">Have an account already? <a href="/auth/login" class="btn btn-link">Sign In.</a></p>
                </div>
            </header>
            <div class="row">
                @yield('content')
            </div>
            @include ('partials.html.footer')
        </div>

    @include ('partials.html.javascripts')
    </body>
</html>
