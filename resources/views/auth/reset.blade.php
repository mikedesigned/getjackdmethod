@extends('layouts.unauthenticated')
@section('content')
<div class="row profile-wrap">
    <div class="col-md-12">
        <div class="edit-profile-wrap">
            <div class="profile-edit-header">
                <h2 class="text-center">Reset Your Password</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-edit-form">
                        <form method="POST" class="form-horizontal" action="/password/reset">
                            {!! csrf_field() !!}
                            <input type="hidden" name="token" value="{{ $token }}">
                            @if (count($errors) > 0)
                            <div class="alert alert-error">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="form-group">
                                Email
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                Password
                                <input class="form-control" type="password" name="password">
                            </div>
                            <div class="form-group">
                                Confirm Password
                                <input class="form-control" type="password" name="password_confirmation">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-danger" type="submit">
                                Reset Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop