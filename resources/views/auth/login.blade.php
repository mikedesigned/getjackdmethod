@extends('layouts.bare')
@section('title', 'Login')
@section('content')
<div class="flat-form">
    <h1 id="loginLogo"><img src="/img/svg/getjackd-logo.svg" alt="GetJack'D Method!" /></h1>
    <div id="login" class="form-action show">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form method="POST" action="/auth/login">
            <h1 class="text-center">Login</h1>
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" name="email" placeholder="Email Address" />
            </div>
            <div class="form-group">
                <input type="password" name="password" placeholder="Password" />
            </div>
            <div class="form-group">
                <p class="text-center">Forgot your password? <a href="#" data-toggle="modal" data-target="#forgotPassword">Click here.</a></p>
                <input type="submit" class="btn btn-info btn-lg btn-block" value="Login" class="button" />
            </div>
        </form>
        </div>
        <!--/#login.form-action-->

    <!-- Modal -->
    <div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                        <br>
                            <form method="POST" class="form form-horizontal" action="/password/email">
                                {!! csrf_field() !!}
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="control-label">Email Address: </label>
                                    <input type="email" class="form-control" placeholder="your@email.com" name="email" value="{{ old('email') }}">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-block btn-pink" type="submit">
                                        Send Link
                                    </button>
                                    <p class="text-muted">To reset your password, please enter your email address and a link will be sent to you.</p>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop