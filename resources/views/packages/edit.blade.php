@extends('layouts.admin')

@section('content')
	<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Edit <strong>{{$package->name}}</strong></h3>
	</div>
		<div class="panel-body">
		  <form class="form" method="POST" action="/admin/packages/{{$package->id}}">
		  		<div class="form-group">
		  			<label class="control-label"><strong>Package Name</strong></label>
		  			<input type="text" placeholder="Package Name" class="form-control" name="name" id="name" value="{{$package->name}}">
		  		</div>
		  		<hr />
		  		<div class="form-group">
		  		<label class="control-label"><strong>Package Price</strong></label>
					<div class="input-group">
					  <span class="input-group-addon">$</span>
					  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" value="{{$package->price}}">
					</div>
		  		</div>

		  		<hr />
		  		<div class="form-group">
		  			<label class="control-label"><strong>Description</strong></label>
		  			<p class="text-muted">This appears on the table for the upgrade section. Also in PayPal checkout.</p>
		  			<textarea name="description" class="form-control ckeditor">{{$package->description}}</textarea>
		  		</div>
		  </form>
		</div>
	</div>
@stop