@extends('layouts.admin')
@section('title', 'packages')
@section('content')
    <p class="text-muted"><strong>Note: </strong> The SKU is used for permissions checking and access rights for users that buy that particular package.</p>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">List Packages</h3></div>
        <table class="table table-bordered">
        <tr>
            <th><p class="text-center">SKU</p></th>
            <th>Name</th>
            <th>Cost</th>
            <th>Description</th>
            <th>Subscribers</th>
            <th>Actions</th>
        </tr>

        @foreach($packages as $package)
            <tr>
                <td><p class="text-center">{{$package->sku}}</p></td>
                <td>{{$package->name}}</td>
                <td>${{$package->price}}</td>
                <td><p class="text-muted">{{$package->description}}</p></td>
                <td><p class="text-info">{{$package->users->count()}} people own this package</p></td>
                <td><a href="/admin/packages/{{$package->id}}/edit" class="btn btn-success">Edit</a></td>
            </tr>
        @endforeach
    </table>
    </div>

    
@stop