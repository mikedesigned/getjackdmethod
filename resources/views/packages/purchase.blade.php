@extends('layouts.unauthenticated')
@section('title', 'Purchase Package')

@section('content')

    <div class="signup-wrap">

        <header class="signup-header">
            <h2 class="signup-title">Select a Package!</h2>
        </header>

        <section class="signup-packages">

            @foreach($packages as $package)

                <div class="package">
                   <div class="upper-package">
                       <h4>{{ $package->name }}</h4>
                       <span class="package-tagline">The GetJackD! Business Plan</span>

                       <div class="price-section">
                           <span class="price-figure">${{$package->price}}</span>
                       </div>
                   </div>
                   <div class="middle-package">
                       <p>A step-by-step guide to building your Business Plan with real-world examples,. tips and advice to get your business moving - a plan that grows as your business.</p>
                   </div>
                   <div class="lower-package">
                       <a href="/signup/{{$package->id}}" class="btn btn-block btn-pink">Buy Now</a>
                   </div>
                </div>

            @endforeach

        </section>


    </div>

@stop