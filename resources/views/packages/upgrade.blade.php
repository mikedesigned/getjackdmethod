@extends('layouts.master')
@section('title', 'Upgrade Package')
@section('content')
<div class="signup-wrap">
    <header class="signup-header">
        <h2 class="signup-title">Upgrade!</h2>
    </header>
    <section class="signup-packages">
        @foreach($packages as $package)
        <div class="package">
            <div class="upper-package">
                <h4>{{ $package->name }}</h4>
                <span class="package-tagline">The GetJackD! Business Plan</span>
                <div class="price-section">
                    <span class="price-figure">
                        @if(Auth::user()->packages->contains($package->id))
                            <i class="fa fa-check fa-3x text-success"></i>
                        @else
                            ${{$package->price}}
                        @endif
                    </span>
                </div>
            </div>
            <div class="middle-package">
                <p>A step-by-step guide to building your Business Plan with real-world examples,. tips and advice to get your business moving - a plan that grows as your business.</p>
            </div>
            <div class="lower-package">
                @if(Auth::user()->packages->contains($package->id))
                    <a href="#" class="disabled btn btn-block btn-success">
                    You Own This!
                    </a>
                @else
                    <a href="/upgrade/{{$package->id}}" class="btn btn-block btn-pink">Buy Now</a>
                @endif
            </div>
        </div>
        @endforeach
    </section>
</div>
@stop