@extends((( Request::is('upgrade/*')) ? 'layouts.master' : 'layouts.unauthenticated' ))
@section('title', 'Checkout')
@section('bodyClass', 'checkout')
@section('content')
<div class="signup-form-wrap">
    {{-- Basically, here we are switching based on the URI whether they are upgrading or just purchasing from the Sign Up page. --}}
    @if(Request::is('upgrade/*'))
        @include ('partials.Packages.forms.upgrade', $package)
    @else
        @include ('partials.Packages.forms.fresh', $package)
    @endif
</div>
@stop