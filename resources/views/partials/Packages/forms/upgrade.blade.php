 <header class="checkout-header">
        <h1 class="checkout-title">Upgrade To Package</h1>
    </header>

    <form action="/processUpgrade" method="POST" class="form-horizontal" role="form">
    
        {!! csrf_field() !!}

        <div class="field">
            <h4 class="userName"> Hey again, {{Auth::user()->first_name}}.</h4>
            <p class="lead">We just need to confirm your email and password to proceed with the transaction. You'll be redirected to PayPal and then back to your dashboard where the package will show up under your <strong>"My Stuff"</strong> tile.</p>
        </div>

        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        
        <div id="emailAddress">
            <div class="field">
                <label for="email">Email Address</label>
                <input type="email" name="email" value="{{ Auth::user()->email }}">
                <small>Please confirm your email</small>
            </div>
        </div> 

        <div id="password" class="field field-half">
            <label for="password">Password</label>
            <input type="password" name="password" value="" placeholder="">
        </div>       

        <div id="passwordConfirm" class="field field-half">
            <label for="password_confirm">Confirm</label>
            <input type="password" name="password_confirm" value="" placeholder="">
        </div>

        <div id="purchase">
            <input type="hidden" name="package_id" value="{{$package->id}}" >
            <input type="submit" name="submit" class="btn btn-lg btn-success" value="Upgrade To &rarr; {{$package->name}} (${{$package->price}})" placeholder="">
        </div>

    </form>