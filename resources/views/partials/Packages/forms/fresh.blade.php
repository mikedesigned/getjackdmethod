 <header class="checkout-header">
        <h1 class="checkout-title">Sign Up</h1>
</header>

    <form action="/processNew" method="POST" class="form-horizontal" role="form">
    
        {!! csrf_field() !!}
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        <div class="field">
            <p class="lead"><strong>Chosen Package: </strong> {{ $package->name }} <span class="price">${{ $package->price }}</span></p>
        </div>

        <div id="firstName">
            <div class="field">
                <label for="first_name">First Name</label>
                <input type="text" name="first_name" value="">
            </div>
        </div>

        <div id="lastName">
            <div class="field">
                <label for="last_name">Last Name</label>
                <input type="text" name="last_name" value="">
            </div>
        </div>   

        <div id="companyName">
            <div class="field">
                <label for="company">Company Name</label>
                <input type="text" name="company" value="">
            </div>
        </div>        

        <div id="contact_number">
            <div class="field">
                <label for="contact_number">Contact Number</label>
                <input type="text" name="contact_number" value="">
            </div>
        </div>

        <div class="field">
            <p class="lead">User Account</p>
        </div>

        <div id="emailAddress">
            <div class="field">
                <label for="email">Email Address</label>
                <input type="email" name="email" value="">
                <small>This will be your sign-in name.</small>
            </div>
        </div> 

        <div id="password" class="field field-half">
            <label for="password">Password</label>
            <input type="password" name="password" value="" placeholder="">
        </div>       

        <div id="passwordConfirm" class="field field-half">
            <label for="password_confirm">Password Confirm</label>
            <input type="password" name="password_confirm" value="" placeholder="">
        </div>

        <div class="field">
            <p class="lead">Address Information</p>
        </div>

        <div id="houseNumber" class="field">
            <label for="address_1">Address 1</label>
            <input type="text" name="address_1" value="" placeholder="">
            <small>Address Line 1</small>
        </div>        

        <div id="streetAddress" class="field">
            <label for="address_2">Address 2</label>
            <input type="text" name="address_2" value="" placeholder="">
            <small>Address Line 2</small>
        </div>         

        <div id="postal_code" class="field field-half">
            <label for="postal_code">Postcode</label>
            <input type="text" name="postal_code" value="" placeholder="">
            <small>E.g: Gold Coast, Newcastle, etc.</small>
        </div>

        <div id="administrative_area" class="field field-half">
            <label for="administrative_area">State</label>
            <input type="text" name="administrative_area" value="" placeholder="">
            <small>QLD, NSW, etc.</small>
        </div>        

        <div id="country" class="field">
            <label for="country">Country</label>
            <input type="text" name="country" value="" placeholder="">
            <small>Australia</small>
        </div>

        <div id="purchase">
            <input type="hidden" name="package_id" value="{{$package->id}}" >
            <input type="submit" name="submit" class="btn btn-lg btn-success" value="Purchase Plan" placeholder="">
        </div>

    </form>