<div class="row">
@if($user->business_plans)
       <div class="col-md-6">
            <ul class="list-group ">
        <li class="title list-group-item">
            <h2 class="text-center">Business Plans</h2>
        </li>
            <li class="list-group-item plans-list">
                <dl>
                     @foreach($user->business_plans as $plan)
                    <dt>
                        <h4><strong>{{$plan->name}}</strong></h4>
                    </dt>
                    <dd>
                        <strong>Created: </strong> <span class="text-muted">{{$plan->created_at->diffForHumans()}}</span> <br />
                        <a href="/plan/marketing/{{$plan->id}}/download">Download It</a>
                    </dd>
                    <hr />
                    @endforeach
                </dl>
            </li>
    </ul>
</div>
@endif

@if($user->marketing_plans)
<div class="col-md-6">
    <ul class="list-group">
        <li class="title list-group-item">
            <h2 class="text-center">Marketing Plans</h2>
        </li>
            <li class="list-group-item plans-list">
                <dl>
                     @foreach($user->marketing_plans as $plan)
                    <dt>
                        <h4><strong>{{$plan->name}}</strong></h4>
                    </dt>
                    <dd>
                        <strong>Created: </strong> <span class="text-muted">{{$plan->created_at->diffForHumans()}}</span> <br />
                        <a href="/plan/business/{{$plan->id}}/download">Download It</a>
                    </dd>
                    <hr />
                    @endforeach
                </dl>
            </li>
    </ul>
</div>
@endif
</div>
