<div class="edit-profile-wrap">
  <div class="profile-edit-header">
    <h2 class="text-center">Edit My Profile</h2>
  </div>
  <div class="row">

      <div class="col-md-12">

        <div class="profile-edit-form">

          <form class="form-horizontal" action="/users/{{Auth::user()->id}}" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="_method" value="PUT">            
            <div class="col-md-6">
              <div class="form-group">
                <div class="col-sm-12">
                  <h2 class="text-center text-muted">Basic Details</h2>
                </div>
              </div>

              <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="first_name" name="first_name" value="{{Auth::user()->first_name}}">
                </div>
              </div>

              <div class="form-group">
                <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="last_name" name="last_name" value="{{Auth::user()->last_name}}">
                </div>
              </div>

              <div class="form-group">
                <label for="company" class="col-sm-2 control-label">Company</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="company" name="company" value="{{Auth::user()->company}}" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" name="email" placeholder="name@company.com" value={{ Auth::user()->email}}>
                </div>
              </div>

              <div class="form-group">
                <label for="abn" class="col-sm-2 control-label">ABN</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="abn" name="abn" value="{{Auth::user()->abn}}">
                </div>
              </div>

              <div class="form-group">
                <label for="contact_number" class="col-sm-2 control-label">Contact Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="555 789456123" value={{ Auth::user()->contact_number}}>
                </div>
              </div>

              <div class="form-group">
                <label for="mobile_number" class="col-sm-2 control-label">Mobile Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="555 789456123" value={{ Auth::user()->mobile_number}}>
                </div>
              </div>

              <div class="form-group">
                <label for="other_info" class="col-sm-2 control-label">Other Info</label>
                <div class="col-sm-10">
                  <textarea rows="10" type="other_info" class="form-control" name="other_info" id="other_info">{{ Auth::user()->other_info}}</textarea>
                </div>
              </div>
            </div>

            <div class="col-md-6">

              <div class="form-group">
                <h2 class="text-center text-muted">Company Logo</h2>
              </div>

              <div class="form-group">

                <div class="col-sm-12">
                  @if(Auth::user()->logo_path)
                    <img src="#" class="img-responsive" style="margin: 0 auto;" alt="{{Auth::user()->company}}" />
                    <hr>
                  @endif
                </div>

                <label for="logo" class="col-sm-2 control-label">Logo</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" name="logo" id="logo" /> <br />
                  <small class="text-muted">Please upload a version of your company logo.</small>
                </div>
              </div>

              <div class="form-group">
                <h2 class="text-center text-muted">Address Info</h2>
              </div>

              <div class="form-group">
                <label for="address_1" class="col-sm-2 control-label">Address 1</label>
                <div class="col-sm-10">
                  <input type="text" name="address_1" class="form-control" id="address_1" value="{{ Auth::user()->address->address_1}}">
                </div>
              </div>

              <div class="form-group">
                <label for="address_2" class="col-sm-2 control-label">Address 2</label>
                <div class="col-sm-10">
                  <input type="text" name="address_2" class="form-control" id="address_2" value="{{ Auth::user()->address->address_2}}">
                </div>
              </div>

             <div class="form-group">
                <label for="state" class="col-sm-2 control-label">State</label>
                <div class="col-sm-10">
                  <input type="text" name="state" class="form-control" id="state" value="{{ Auth::user()->address->administrative_area}}">
                </div>
              </div>

              <div class="form-group">
                <label for="postal_code" class="col-sm-2 control-label">Postcode</label>
                <div class="col-sm-10">
                  <input type="text" name="postal_code" class="form-control" id="postal_code" value="{{ Auth::user()->address->postal_code}}">
                </div>
              </div>
            </div>


            <div class="form-group">
              <div class="col-sm-12">
                <hr>
                <button type="submit" class="btn btn-lg btn-success">Update my Profile</button>
              </div>
            </div>

          </form>

        </div>

      </div>

  </div>

</div>
