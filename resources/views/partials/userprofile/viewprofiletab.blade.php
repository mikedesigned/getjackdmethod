<ul class="list-group">
    <li class="title list-group-item">
        <h2 class="text-center">My Deets</h2>
    </li>
    <li class="list-group-item">
        <span class="col-md-3 col-sm-2 profile-item-header">Name</span>
        <span class="profile-item">{{ $user->first_name }} {{ $user->last_name }}</span>
    </li>
    <li class="list-group-item">
        <span class="col-md-3 col-sm-2 profile-item-header">Contact Number</span>
        <span class="profile-item">{{ $user->contact_number }}</span>
    </li>
    <li class="list-group-item">
        <span class="col-md-3 col-sm-2 profile-item-header">Email</span>
        <span class="profile-item">{{ $user->email }}</span>
    </li>
    <li class="list-group-item">
        <span class="col-md-3 col-sm-2 profile-item-header">Address</span>
        <span class="profile-item">{{ $user->address ? $user->address->full() : 'N/A' }}</span>
    </li>
    <li class="list-group-item">
        <span class="col-md-3 col-sm-2 profile-item-header">Company Name</span>
        <span class="profile-item">{{ $user->company ? $user->company : 'N/A' }}</span>
    </li>
    <li class="list-group-item">
        <span class="col-md-3 col-sm-2 profile-item-header">Other Info</span>
        <span class="profile-item">{{ $user->other_info }}</span>
    </li>
</ul>
