<div class="edit-profile-wrap">

    <div class="profile-edit-header">
        <h2 class="text-center">Reset My Password</h2>
    </div>

    <div class="row">
        
        <div class="col-md-8 col-md-offset-2">
            <br>
            <p class="text-muted text-center lead">Here you can reset your password. This will send an email to your designed email address with a link so you can perform the reset.</p>
            <div class="col-md-6 col-md-offset-4">
                <form method="POST" class="form-horizontal" action="/auth/password/email">
                    {!! csrf_field() !!}
                    @if (count($errors) > 0)
                    <div class="alert alert-error">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" name="email" placeholder="Your Current Email" class="form-control" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-success" type="submit">
                        Send Password Reset Link
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>