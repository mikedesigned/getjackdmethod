@foreach ($category->children as $category)
        @if($category->isLeaf())
            <li>{{ $category->name }}</li>
        @else
            <li> {{$category->name}}
                <ol>
                	@include('partials.pdf.category', $category)
                </ol>
            </li>
        @endif
@endforeach
