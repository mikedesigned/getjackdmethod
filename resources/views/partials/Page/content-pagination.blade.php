<nav class="Page--pagination clearfix">

    @if($page->isRoot())

        @if($page->hasChildren())
            <a href="{{$page->getFirstChild()->slug}}" class="btn Page--paginator pull-right">
                <i class="fa fa-forward"></i>
                <em>{{$page->getFirstChild()->title}}</em>
            </a>
        @else
            <em>No more pages.</em>
        @endif

    @elseif($page->hasSiblings())

        @if($page->hasPrevSiblings())
            <a href="{{$page->getPrevSibling()->slug}}" class="btn Page--paginator pull-left">
                <i class="fa fa-backward"></i>
                &nbsp;&nbsp;&nbsp;<em>{{$page->getPrevSibling()->title}}</em>
            </a>
        @endif

        @if($page->hasNextSiblings())
            <a href="{{ $page->getNextSibling()->slug}}" class="btn Page--paginator pull-right">
                <em>{{ $page->getNextSibling()->title}}</em> &nbsp;&nbsp;&nbsp;<i class="fa fa-forward"></i>
            </a>
        @endif

    @endif
</nav>