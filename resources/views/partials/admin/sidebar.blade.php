<div class="well">
<p class="lead">Hello, {{ Auth::user()->first_name }} <br /><hr />
<small class="text-center">Logged in as: <span class="text-success">{{ Auth::user()->email }}</span></small></p>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Dashboard</h3>
    </div>
        <div class="list-group">
            <a href="/admin" class="list-group-item"> <i class="fa fa-hashtag"></i> Go To Dashboard</a>
            <a href="/admin/settings" class="list-group-item"> <i class="fa fa-hashtag"></i> Go To Settings</a>
            <a href="/admin/help" class="list-group-item"> <i class="fa fa-hashtag"></i> Go To Package Help</a>
            <a href="/admin/packages" class="list-group-item"> <i class="fa fa-hashtag"></i> View Packages</a>

        </div>
    <div class="panel-footer"></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Business & Marketing Plans</h3>
    </div>
        <div class="list-group">
            <a href="/admin/categories" class="list-group-item"> <i class="fa fa-hashtag"></i> All Plan Chapters</a>
            <a href="/admin/categories/templates" class="list-group-item"><i class="fa fa-hashtag"></i> Manage Default Templates</a>
            <a href="/admin/categories/tips" class="list-group-item"><i class="fa fa-hashtag"></i> See All Helpful Hints</a>
        </div>
    <div class="panel-footer"></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Pages</h3>
    </div>
    <div class="list-group">
        <a href="/admin/pages" class="list-group-item"> Manage Pages</a>
        <a href="/admin/pages/create" class="list-group-item">Create A New Page</a>
    </div>
    <div class="panel-footer"></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Appointments</h3>
    </div>
    <div class="list-group">
        <a href="/admin/appointments" class="list-group-item"> Manage Appointments</a>
    </div>
    <div class="panel-footer"></div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Users Administration</h3>
    </div>
    <div class="list-group">
        <a href="/admin/users" class="list-group-item">View All Users</a>
    </div>
    <div class="panel-footer"></div>
</div>

</div>
