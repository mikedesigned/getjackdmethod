@foreach($page->children as $page)
<tr>
    <td width="50%">&rarr; {{$page->title}}</td>

    <td>{{$page->getParent()->title}}</td>
    <td><em>{{$page->package->name}}</em></td>

    <td>{{$page->position}}</td>
    <td>
        <a href="/admin/pages/{{$page->id}}/edit" class="btn btn-sm btn-warning">Edit</a> &nbsp;
        <a href="#" data-page-id="{{$page->id}}" class="btn btn-sm btn-danger delete-button">Delete</a> &nbsp;
        <a href="/marketing-channels/{{$page->slug}}" class="btn btn-sm btn-success">View</a>
    </td>
</tr>
    @if($page->hasChildren())
        @include('partials.admin.page-children', $page)
    @endif
@endforeach