<header class="row clearfix headerSection">
    <div id="logo" class="col-xs-12 col-sm-6 col-md-4">
        <a href="/dashboard"><img src="/img/svg/getjackd-logo.svg" alt="GetJack'D Method!" /></a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8">
        <ul class="menu pull-right">
            <li>
                <a href="#" class="menu-block menu-btn">
                    <i class="fa fa-bars"></i>
                    <span class="menu-subtitle">Menu</span>
                </a>
            </li>
            <li>
                <a href="/auth/logout" class="menu-block">
                    <i class="fa fa-power-off"></i>
                    <span class="menu-subtitle">Logout</span>
                </a>
            </li>
        </ul>
    </div>
</header>