<!-- Pushy Menu -->
<nav class="pushy pushy-left">
    <a class="logo-link" href="/dashboard"><h1 class="text-center"><img src="/img/svg/getjackd-logo.svg" alt="GetJack'D Method!" /></h1></a>
    <h2 class="text-center">Main Menu</h2>
    <ul>
        <li><a href="/dashboard">Go To Dashboard</a></li>
        @if(Auth::check() && Auth::user()->is_super)<li><a href="/admin">Admin Area</a></li>@endunless
        <li><a href="/users/profile">My Deets</a></li>
        <li><a href="#">Help Me</a></li>
        <li><a href="#">Report an Issue</a></li>
        <li><a href="/auth/logout">Logout</a></li>
    </ul>
</nav>