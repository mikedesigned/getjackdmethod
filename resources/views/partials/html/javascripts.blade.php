<script type="text/javascript" src="//js.pusher.com/3.0/pusher.min.js"></script>
<script type="text/javascript" src="/vendor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tinyscrollbar/1.81/jquery.tinyscrollbar.min.js" defer></script>
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/all.js"></script>
<script type="text/javascript" src="/js/dashboards.js"></script>
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='https://www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-72351457-1','auto');ga('send','pageview');
</script>