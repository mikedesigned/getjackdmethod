@foreach ($category->children as $category)
    @if($category->isLeaf())
        <li>
            <a class="leaf" data-business-plan-id="{{$business_plan->id}}"
               data-parent-id="0" data-category-id="{{$category->id}}"
               href="#/?category={{urlencode($category->name)}}&business_plan_id={{urlencode($business_plan->id)}}">
                {{ $category->name }}
            </a>
        </li>
    @else
        <li><a data-business-plan-id="{{$business_plan->id}}" data-parent-id="0" data-category-id="{{$category->id}}" href="#/?category={{urlencode($category->name)}}&business_plan_id={{urlencode($business_plan->id)}}">{{ $category->name }}</a></li>
        <ul class="nth-child-list" >
            @include('partials.category')
        </ul>
        @endif
@endforeach