@foreach($sect->children as $sect)
	@if ($sect->content_type == "spreadsheet")
		@include('partials.pdf.spreadsheet', $sect)
	@else
        <div>
        <h2>{{$sect->name}}</h2>
            @foreach($sect->contents as $content)
                {!! $content->content !!}
            @endforeach
        </div>
        @include('partials.planlayout', $sect)
    @endif
@endforeach
