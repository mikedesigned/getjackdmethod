@section('cover-page')
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title></title>
        </head>

        <style type="text/css">

        html, body, #cover {
            height: 100%;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }

        .wrapper {
            width: 100%;
        }
        #contents, #cover {
            page-break-after: always;
            position: relative;
            width: 100%;
        }
        #logo {
            position: absolute;
            top: 0;
            left: 0;
        }
        #cover .wrapper {
            position: absolute;
            text-align: right;
            top: 30%;
            right: 0;
            margin-top: 20rem;
        }
        #cover h1 {
            margin: 0;
        }
        #cover h1#businessName {
            font-size: 36pt;
        }
        #cover #preparedBy {
            font-size: 10pt;
        }
        #cover h1#businessPlan {
            font-size: 24pt;
        }
        #contactDetails {
            font-size: 10pt;
        }
        </style>
        <body>

            <!-- COVER PAGE SECTION -->
            <div id="cover">
                <div id="logo">
                    <img src="{{ asset('img/getjackd-logo.jpg') }}" width="200px" height="200px" alt="">
                </div>

                <div class="wrapper">
                    <h1 id="businessName">{{ $user->company }}</h1>
                    <h1 id="businessPlan">{{ ucwords($plan->type) }} Plan</h1>
                    <span id="preparedBy"><strong>Prepared By Date: </strong> {{ Carbon::now()->toFormattedDateString() }}</span>
                    <br>
                    <br>
                    <br>
                    <div id="contactDetails">
                        <p>
                            <strong>Business Name: </strong> {{  $user->company }}<br />
                            <strong>Business Details: </strong> {{ $user->abn }} <br />
                            <strong>Business Address: </strong> {{ $user->address->address_1 }}, {{ $user->address->address_2 }} <br />
                            {{ $user->address->administrative_area }} <br />
                            {{ $user->address->postal_code }}, {{ $user->address->country }}
                            <br />
                            <strong>Website: </strong> www.getjackdmethod.com <br />
                            <br>
                            <br>
                            <br>
                            <strong>Contact Name: </strong> {{ $user->first_name }} {{ $user->last_name }} <br />
                            <strong>Contact Details: </strong> {{ $user->contact_number }}
                        </p>
                    </div>
                </div>
            </div>
        </body>
    </html>
@stop
