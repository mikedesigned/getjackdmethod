<!DOCTYPE html>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>{{$user->company}}</title>
    <style type="text/css">

        table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            vertical-align: baseline;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            margin: 2rem 0;
            width: 100%;
            table-layout: fixed;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }

        tr {
            page-break-inside: avoid;
        }

        thead, tfoot {display: table-row-group}
        
        th {
            font-weight: bold;
            text-transform: capitalize;
        }

        td, th {
            border: 1px solid gray;
            padding: 10px;
            word-wrap: break-word;
            font-size: 10pt !important;
        }

        td p {
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
            font-size: 10pt !important;
            display: block;
            word-wrap: break-word;
        }

        #plan h1 {
            font-size: 16pt;
            font-weight: bold;
            line-height: 18pt;
            text-transform: capitalize;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }
        #plan h2 {
            font-size: 16pt;
            font-weight: normal;
            line-height: 18pt;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }
        #plan p {
            font-size: 11pt;
            line-height: 12pt;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }
        #plan ul {
            font-size: 11pt;
            line-height: 12pt;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }
        #plan li {
            font-size: 11pt;
            line-height: 12pt;
            font-family: "Verdana", "Helvetica", "Arial", sans-serif;
        }
    </style>
</head>
<body>

<!-- PLAN SECTIONS -->
<div id="plan">
    @foreach($sections as $sect)
        @if(empty($sect->children))
            <div>
                <h1>{{$sect->name}}</h1>
                @foreach($sect->contents as $content)
                    {!! $content->content !!}
                @endforeach
            </div>
        @else
            <div>
                <h1>{{$sect->name}}</h1>
                @foreach($sect->contents as $content)
                    {!! $content->content !!}
                @endforeach
            @include('partials.planlayout', $sect)
            </div>
        @endif
    @endforeach
</div>
</body>
</html>
