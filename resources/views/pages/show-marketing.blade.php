@extends('layouts.master')
@section('title', $page->title)
@if($page->package->id == 4)
    @section('bodyClass', 'content package-brandit')
@else
    @section('bodyClass', 'content package-pisi')
@endif
@section('content')
<div class="Page--Wrapper">
    <div class="Page--section-wrapper">
        <div class="chapters-box">
            <div class="Page--header">
                <h2>{{ $main_section->title }}</h2>
            </div>
            <div class="Page--chapters">
                <ul class="Page--chapters-list">
                    <ul class="Page--children-chapters inner">
                        @foreach($chapters as $children)
                        @if($children->hasDescendants() && $children->real_depth == 1)
                        <li role="navigation" class="Page--chapter {{ Request::is('marketing-channels/' . $children->slug) ? 'active' : '' }}"><a href="{{$children->slug}}">{{$children->title}}</a></li>
                        <ul class="Page--children-chapters-descendants">
                            @foreach($children->getDescendants()->sortBy('position') as $child)
                            <li role="navigation" class="Page--chapter {{ Request::is('marketing-channels/' . $child->slug) ? 'active' : '' }}"><a href="{{$child->slug}}">{{$child->title}}</a></li>
                            @endforeach
                        </ul>
                        @elseif($children->real_depth == 1)
                        <li role="navigation" class="Page--chapter {{ Request::is('marketing-channels/' . $children->slug) ? 'active' : '' }}"><a href="{{$children->slug}}">{{$children->title}}</a></li>
                        @endif
                        @endforeach
                    </ul>
                </li>
                <li class="back-to-dashboard">
                    @if($page->package->sku == 'brand')
                        <a href="/dashboard">&larr; Back To Dashboard</a>
                    @else
                        <a href="{{ route('marketing-channel-dashboard') }}">&larr; Back To Dashboard</a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="Page--Content__wrapper">
    <div class="Page--header">
        <h2>{{ $page->title }}</h2>
    </div>
    <div class="Page--content">
        @if($page->isRoot())
        <div class="text-center">
            <h2>Welcome To <strong>{{$page->package->name}}</strong></h2>
            <p class="text-muted lead">Click the menu on the left to begin.</p>
        </div>
        @else
        {!! $page->content !!}
        @endif
    </div>
</div>
</div>
@stop