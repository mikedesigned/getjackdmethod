@extends('layouts.admin')
@section('sectionName', 'Create Pages')
@section('content')



        <form id="create-page" method="post" action="/admin/pages" class="form-horizontal">
            {!! csrf_field() !!}


        <div class="panel panel-default">
            
            <div class="panel-heading"><h3 class="panel-title">Meta Information</h3></div>

            <div class="panel-body">
                
                @if (Session::has('success_message'))
                    <div class="alert alert-success">{{ Session::get('success_message') }}</div>
                @endif

                <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" name="page_title" class="form-control" id="page_title" placeholder="Page Title">
                </div>
            </div>

            <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Package</label>
                <div class="col-sm-10">

                    <select name="package" id="package_select" class="form-control">
                        @foreach($packages as $package)
                            <option value="{{$package->id}}">{{$package->name}}</option>
                        @endforeach
                    </select>
                    
                </div>
            </div>

            <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Parent Page</label>
                <div class="col-sm-10">
                    <select name="parent_page" id="parent_page" class="form-control">
                        
                        <option value="">New Root Page</option>

                        @foreach($roots as $page)
                            <option value="{{$page->id}}">{{$page->title}}</option>
                        @endforeach

                    </select>
                    <small class="text-muted">Please select a parent page for this page to belong to. It will flow on sequentially with the other family of pages,
                    which will form nicely flowing chapters.</small>
                </div>
            </div>


            </div>

        </div>

        <div class="panel panel-default">

            <div class="panel-heading"><h3 class="panel-title">Editor</h3></div>
            <div class="panel-body">
               <textarea name="page_content" class="ckeditor form-control" id="page_content" cols="30" rows="10"></textarea>
            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Create Page</button>
            </div>
        </div>

        </form>


    
@stop