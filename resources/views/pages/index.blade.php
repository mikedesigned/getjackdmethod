@extends('layouts.admin')
@section('sectionName', 'Manage Pages')
@section('sidebar')
    @parent
@stop
@section('help')
    @parent
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">How do I use this page?</h3>
        </div>
        <div class="panel-body">
            <p>Use the table below to manage your pages. The "Parent" pages are the ones highlighted in blue on the content page and the children are the pages
                that appear with a white backgroud under the parent page in the contents section.</p>
            <p>The navigation is seemless between parent -> child, so it should work nicely.</p>
        </div>
    </div>
@stop

@section('content')

    <style type="text/css">
        td {
            vertical-align: middle;
        }
    </style>

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Managing Pages</h3></div>
        <table class="table" style="vertical-align: middle;">
            <thead>
                <th width="50%">Title</th>
                <th>Parent</th>
                <th>Package</th>
                <th>Action</th>
            </thead>
            <tbody>
            @foreach($pages as $page)
                <tr class="active" style="vertical-align: middle;">
                    <td width="50%"><p class="text-primary">{{$page->title}}</p></td>
                    <td><strong>Parent Page</strong></td>
                    <td><strong><em>{{$page->package->name}}</em></strong></td>
                    <td><a href="/admin/pages/{{$page->id}}/edit" class="btn btn-sm btn-warning">Edit</a> &nbsp; <a href="/marketing-channels/{{$page->slug}}" class="btn btn-sm btn-success">View</a></td>
                </tr>
                @if($page->hasChildren())
                    <tr>
                        <td colspan="4">
                            <div class="panel panel-default" style="margin: 0;">
                                <div class="panel-heading"><h3 class="panel-title">{{$page->title}} Subpages</h3></div>
                                <table class="table table-condensed table-striped" style="margin: 0 !important">
                                <thead>
                                    <th width="50%">Title</th>
                                    <th>Parent</th>
                                    <th>Package</th>
                                    <th>Position</th>
                                    <th>Action</th>
                                </thead>
                                    @include('partials.admin.page-children', $page)
                                </table>
                            </div>

                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
@stop