@extends('layouts.admin')
@section('sectionName', 'pages')
@section('content')
    @if (Session::has('success_message'))
        <div class="alert alert-success">{{ Session::get('success_message') }}</div>
    @endif

    <form id="edit-page" method="post" action="/admin/pages/{{$page->id}}" class="form-horizontal">
    {!! csrf_field() !!}

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-hashtag"></i> Meta Information</h3></div>

        <div class="panel-body">
            <input name="_method" type="hidden" value="PUT">
            <input name="page_id" type="hidden" value="{{$page->id}}" />
            <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" name="page_title" class="form-control" id="page_title" value="{{$page->title}}" placeholder="Page Title">
                </div>
            </div>
            <div class="form-group">
                <label for="position" class="col-sm-2 control-label">Position</label>
                <div class="col-sm-10">
                    <input type="number" name="position" class="form-control" id="position" value="{{$page->position}}" placeholder="Page Title">
                </div>
            </div>
            <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label text-success">Package</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$page->package->name}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Parent Page</label>
                <div class="col-sm-10">
                    <select class="form-control" name="parent_page" id="parent_page">
                        <optgroup label="Current Parent">
                            <option value="{{ $page->getParent()->id }}" selected><strong>Current: </strong>{{ $page->getParent()->title }}</option>
                        </optgroup>
                        <optgroup label="Choices">
                            @foreach($roots as $root)
                                <option value="{{ $root->id }}">{{$root->package->name}} : {{ $root->title }}</option>
                            @endforeach
                        </optgroup>

                    </select>
                </div>
            </div>
        </div>
    </div>
    
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-hashtag"></i> Editor</h3></div>

            <div class="panel-body">
                <textarea name="page_content" class="ckeditor form-control" id="page_content" cols="30" rows="10">{{$page->content}}</textarea>
            </div>

            <div class="panel-footer">
                <button type="submit" name="submit_page" id="submit-button" class="btn btn-success">Update Page</button>
            </div>
        </div>

    </form>

@stop