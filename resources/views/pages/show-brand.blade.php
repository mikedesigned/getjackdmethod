@extends('layouts.master')
@section('bodyClass', 'content')
@section('title', $page->title)

@section('content')
        <div class="Page--Wrapper">
            <div class="Page--section-wrapper">
                <div class="Page--header">
                    <h2>Just Tell It</h2>
                </div>
                <div class="Page--chapters">
                    <ul class="Page--chapters-list">
                        @foreach($chapters as $chapter)
                            <li class="Page--parent-chapter" role="navigation">
                                @if($chapter->hasChildren())
                                    <ul class="Page--children-chapters inner">
                                        @include('partials.Page.children-chapters-brand')
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="Page--Content__wrapper">
                <div class="Page--header">
                    <h2>{{ $page->title }}</h2>
                </div>
                <div class="Page--content">
                    @if($page->isRoot())
                        <h2>What's in this section?</h2>
                        <p class="text-muted">Listed below at the chapters available in this section.</p>
                        <ul>
                        @foreach($page->getChildren() as $children)
                            <li><a href="{{$children->slug}}">{{$children->title}}</a></li>
                        @endforeach
                        </ul>
                    @else
                        {!! $page->content !!}
                    @endif
                </div>
                <div class="Page--controls">
                    @include('partials.Page.content-pagination', $page)
                </div>
            </div>
        </div>
@stop