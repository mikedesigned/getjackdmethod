@extends('layouts.admin')

@section('content')
    <div id="calendar">
        
        <h1>Appointment Calendar Manager</h1>
        <p class="text-muted lead"><kbd>Gray</kbd> blocks denote unbooked slots, where <kbd>pink</kbd> slots denote booked appointments and their times. <br />You may use the arrow icons to navigate between months or click on a slot to see the users profile at a glance.</p>

        <p class="lead text-muted">Don't forget to add more slots to be available for booking using the form below.</p>

        <hr />

        <div id="calendarPanel" class="panel panel-default">
        	<div class="panel-heading"><h3 class="panel-title">Booking Calendar</h3></div>

        	<div class="panel-body">
        		<Calendar></Calendar>
        	</div>

        	<div class="panel-footer"></div>
        </div>

        <hr />

		<div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Add More Slots</h3></div>
            <div class="panel-body">
                <div class="alert alert-info">
                <p class="">In order to add more slots, just select the day you wish to create the free slot for and then specify a time in a format like "2pm." So if you selected the 3rd of December and type in "2pm," the code is smart enough to know what you mean and will create a free slot for that day available for booking. Cool, huh?</p>  
                </div>

            </div>
            <form method="post" action="/admin/appointments">
            {!! csrf_field() !!}
			
			<table class="table">
			<tr>
				<td><input type="text" id="day" name="day" class="form-control" placeholder="Day"></td>
                <td><input type="text" id="time" name="time" class="form-control" placeholder="Start Time"></td>
				<td><input type="submit" class="btn btn-success btn-block"></td>
			</tr>
			</table>
            </form>
		</div>	
        
    </div>
@stop