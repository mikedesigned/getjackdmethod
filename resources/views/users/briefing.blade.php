@extends('layouts.master')
@section('title', 'Briefing')
@section('bodyClass', 'briefing-form')
@section('content')
<main id="briefing-form" class="center-page">
    
<div class="briefing-form-wrap">

        @if(!empty(Auth::user()->brief) && Auth::user()->brief->emailed_submission)
        <div class="alert gj-alert">
            <div class="alert-body">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="text-success">Thank You!</h3>
                <p>Thanking you for your briefing form. It’s all received. We’re now checking you out ready for our session together. Chat soon.</p>
            </div>
        </div>
        @else
        <div class="alert gj-alert">
            <div class="alert-body">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Welcome to your first step of the Plan it, Market it, Now Sell it journey.</h3>
                <hr />
                <p>We’re about to head on our first date together, so we’d love to know a bit more about you. Please fill in the below form so we’re worded up on who you are and what makes you tick.</p>
                <p>Once you’ve completed your form, hit the ‘Briefing You Now’ (Button) and we’ll receive your deets, have a read and be in touch to arrange our one on one briefing session together. </p>
                <p>Not quite ready to brief us yet? That’s cool too. Just hit save and all your details will be waiting for you when you come back. Catch is, we can’t book in our one on one time together till we receive your completed form. So don’t keep us waiting too long, we can’t wait to get started! </p>
            </div>
        </div>
        @endif

    <header class="briefing-form-header">
        <h2 class="briefing-form-title">Your Briefing Form</h2>
    </header>
    {!! form_start($form) !!}
    <div class="briefing-form">
        {!! form_until($form, 'email') !!}
        <h2>Your business, market position, audience and competitors</h2>
        {!! form_until($form, 'influential_factors') !!}
    </div>
    <div class="briefing-form-actions">
        {!! form_end($form) !!}
        <a href="{{route('email.briefing')}}"
            class="btn btn-info {{!empty(Auth::user()->brief) && Auth::user()->brief->emailed_submission ? 'disabled':''}}">
            {{(!empty(Auth::user()->brief) && Auth::user()->brief->emailed_submission) ? 'Your email has been received, thank you!':'Briefing You Now'}}
        </a>
    </div>
</div>

</main>
@stop