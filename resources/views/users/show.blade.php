@extends('layouts.admin')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title">User Information</h3></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">User Details</h3>
					</div>
					<div class="panel-body">
						<dl class="dl-horizontal">
							<dt>First Name</dt>
							<dd>{{$user->first_name}}</dd>
							<dt>Last Name</dt>
							<dd>{{$user->last_name}}</dd>
							<dt>Email</dt>
							<dd>{{$user->email}}</dd>
							<dt>Company</dt>
							<dd>{{$user->company}}</dd>
							<dt>Contact Number</dt>
							<dd>{{$user->contact_number}}</dd>
							<dt>Referral</dt>
							<dd>{{$user->referral}}</dd>
							<dt>Other Info</dt>
							<dd>{{$user->other_info}}</dd>
							<dt>Member Since</dt>
							<dd>{{$user->created_at->diffForHumans(null, true)}}</dd>
							<dt>Owned Packages</dt>
							<dd>
								@foreach($user->packages as $package)
									<span class="label label-primary">{{$package->name}}</span>
								@endforeach
							</dd>
						</dl>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">Address Details</h3>
					</div>
					<div class="panel-body">
						<address>
							<strong>{{$user->company}}</strong><br>
							{{$user->address->address_1}} {{$user->address->address_2}}<br>
							{{$user->address->administrative_area}} {{$user->address->postal_code}}, {{$user->address->country}}<br>
						<abbr title="Phone">P:</abbr> {{$user->contact_number}}
					</address>
				</div>
				<div class="panel-footer"></div>
			</div>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-default">
				  <div class="panel-heading">
						<h3 class="panel-title">Briefing Form Answers</h3>
				  </div>
				  <div class="panel-body" style="max-height: 400px; overflow: hidden; overflow-y: scroll;">
						@if($user->brief)
						  <p class="text-info">Submitted @ {{$user->brief->created_at->toFormattedDateString()}}</p>
						  <dl class="horizontal-list">
							@foreach($user->brief->answers as $answer)
								<dt>{{$answer->question->question_text}}</dt>
								<dd>{{$answer->briefing_question_answer}}</dd>
								  <hr>
						  	@endforeach
						  </dl>
						@else
							<h4 class="text-muted text-center">No briefing form submission.</h4>
						@endif
				  </div>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Marketing Plans</h3>
				</div>
				<div class="panel-body">
					@if($user->marketing_plans)
						@foreach($user->marketing_plans as $plan)
						<dl class="dl-horizontal">
							<dt>Plan Name</dt>
							<dd>{{$plan->name}}</dd>
							<dt>Download</dt>
							<dd><a href="/plan/{{$plan->type}}/{{$plan->id}}/download" class="btn btn-link">Click Here</a></dd>
						</dl>
						@endforeach
					@else
						<h4 class="text-muted text-center">No Marketing Plans created.</h4>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Business Plans</h3>
				</div>
				<div class="panel-body">
				@if($user->business_plans)
						@foreach($user->business_plans as $plan)
						<dl class="dl-horizontal">
							<dt>Plan Name</dt>
							<dd>{{$plan->name}}</dd>
							<dt>Download</dt>
							<dd><a href="/plan/{{$plan->type}}/{{$plan->id}}/download" class="btn btn-link">Click Here</a></dd>
						</dl>
						@endforeach
					@else
						<h4 class="text-muted text-center">No Business Plans created.</h4>
					@endif
				</div>
			</div>
		</div>
	</div>

</div>
<div class="panel-footer"></div>
</div>
@stop
