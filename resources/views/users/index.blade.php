@extends('layouts.admin')
@section('content')
	<div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Users List</h3></div>
        <table class="table">
            <tr>
                <th>Email</th>
                <th>Company</th>
                <th>Name</th>
                <th>Member For</th>
                <th>Briefing Form</th>
                <th></th>
            </tr>
           	@foreach($users as $user)
                <tr>
	                <td>{{$user->email}}</td>
	                <td>{{$user->company}}</td>
	                <td>{{$user->first_name}} <strong>{{$user->last_name}}</strong></td>
	                <td>{{$user->created_at->diffForHumans(null, true) }}</td>
	                <td>{{$user->briefed ? 'Yes' : 'No'}}</td>
                    <td><a href="#" class="disabled btn btn-danger">Set to Inactive</a> &nbsp;<a href="/admin/users/{{$user->id}}" class="btn btn-primary">View Profile</a></td>
                </tr>

                <tr class="success">
                    <td colspan="6"><strong>User owns packages: </strong>

                        @foreach($user->packages as $package)
                            <span class="text-info">{{$package->name}}</span> <strong> > </strong>
                        @endforeach

                    </td>
                </tr>
            @endforeach
        </table>
        <div class="panel-footer">{!! $users->render() !!}</div>
    </div>
@stop