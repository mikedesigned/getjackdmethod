@extends('layouts.master')
@section('title', 'Briefing')
@section('content')

<div class="row profile-wrap">
    <div class="col-md-12">

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Edit Information</a></li>
        <li role="presentation"><a href="#plans" aria-controls="plans" role="tab" data-toggle="tab">My Plans</a></li>
     </ul>

      <div class="tab-content">

      <div role="tabpanel" class="tab-pane active" id="home">
        @include('partials.userprofile.viewprofiletab')
      </div>

      <div role="tabpanel" class="tab-pane " id="profile">
        @include('partials.userprofile.edittab')
      </div>

      <div role="tabpanel" class="tab-pane" id="plans">
        @include('partials.userprofile.planstab', $user)
      </div>

    </div>


    </div>


</div>

@stop
