@extends('layouts.admin')

@section('sectionName', 'Create Category Business Plan')

@section('content')
    <form action="/admin/business/categories" method="post" name="createBusinessPlanCategory">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" name="name" id="name" class="form-control" placeholder="Category Name"/>
        </div>
        <div class="form-group">
            <select class="form-control" id="parentCategory" name="parentCategory">
                @foreach($categories as $catId => $catName)
                    <option value="{{$catId}}">{{$catName}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" name="content_type" id="content_type">
                <option value="text">Text</option>
                <option value="document">Document</option>
            </select>
        </div>
        <input type="submit" class="btn btn-success" value="Save Category">
    </form>
@stop