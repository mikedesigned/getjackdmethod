@extends('layouts.admin')
@section('sectionName', 'View Business Plan Chapters')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">All Plan Chapters</div>
    @foreach($categories as $category)
    @if($category->isRoot())
    <div class="panel-body">
        <h3>{{$category->name}}</h3>
        <hr />
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Chapters</h3></div>
            <table class="table table-condensed">
                <thead>
                    <th>Category Name</th>
                    <th>Actions</th>
                </thead>
                @foreach($category->getDescendants() as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td><a href="#" class="btn btn-success">Edit</a></td>
                </tr>
                @endforeach
            </table>
        </div>
        @endif
        @endforeach
    </div>
    @stop