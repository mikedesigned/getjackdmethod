@extends('layouts.master')
@section('title', 'Plan It and Shout It!')
@section('bodyClass', 'package-pisi')
@section('content')
<main class="dash tell" id="dashboard">
<div class="row">
    <div class="col-md-12">
        <header class="page-heading">
            <h1 class="text-center welcome-line">
                {{$package->name}}
            </h1>
        </header>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="dashboard-tiles">
            <a href="/marketing-channels/tv" class="gj-tile">
                <div class="tile-content">
                    <i class="fa fa-television"></i>

                    <h3 class="tile-title">TV</h3>
                </div>
            </a>
            <a href="/marketing-channels/radio" class="gj-tile">
                <div class="tile-content">
                    <i class="fa fa-headphones"></i>
                    <h3 class="tile-title">Radio</h3>
                </div>
            </a>
            <a href="/marketing-channels/print" class="gj-tile">
                <div class="tile-content">
                    <i class="fa fa-newspaper-o"></i>
                    <h3 class="tile-title">Print</h3>
                </div>
            </a>
            <a href="/marketing-channels/digital" class="gj-tile">
                <div class="tile-content">
                    <i class="fa fa-laptop"></i>
                    <h3 class="tile-title">Digital</h3>
                </div>
            </a>
            <a href="/marketing-channels/outdoor" class="gj-tile">
                <div class="tile-content">
                    <i class="fa fa-map-signs"></i>
                    <h3 class="tile-title">Outdoor</h3>
                </div>
            </a>
            <a href="/marketing-channels/social" class="gj-tile">
                <div class="tile-content">
                    <i class="fa fa-comment"></i>
                    <h3 class="tile-title">Social</h3>
                </div>
            </a>
            </div>
        </div>
    </div>
</main>
@stop