@extends('layouts.master')
@section('title', 'Make It Happen')
@section('bodyClass', 'dashboard-view package-makeithappen')
@section('content')
    <main class="business dash" id="business-dashboard">

         <plan-modal v-cloak id="business-plans" :plans="plans" type="business"
                    :show.sync="showModal">
            <h3 slot="header">My Business Plan</h3>
        </plan-modal>

        <download-modal v-cloak id="business-plans" :plans="plans" type="business"
                    :show.sync="showDownloadModal">
            <h3 slot="header">Download My Business Plan</h3>
        </download-modal>
    
        <div class="row">
            <div class="col-md-12">
                <header class="page-heading">
                    <h2 class="text-center welcome-line">{{$package->name}}</h2>
                </header>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="dashboard-tiles">
                    <a id="viewPlans" href="#" @click="showModal = true" class="gj-tile">
                    <div class="tile-content">
                        <i class="fa fa-pencil-square"></i>

                        <h3 class="tile-title">My Business Plan</h3>
                    </div>
                    </a>
                    <a href="#" @click="showDownloadModal = true" class="gj-tile">
                        <div class="tile-content">
                            <i class="fa fa-download"></i>

                            <h3 class="tile-title">Download My Business Plan</h3>
                        </div>
                    </a>
                    <a href="#" class="gj-tile">
                        <div class="tile-content">
                            <i class="fa fa-question-circle"></i>

                            <h3 class="tile-title">Help Me</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </main>


@stop