@extends('layouts.admin')

@section('content')

  <h1>Settings</h1>

  <div class="settings-form">

    <div class="row">
      <div class="col-md-12">
        <p class="lead text-muted">
          Please update any settings appropriate and save the form.
        </p>


        <form class="form" action="#" method="post">
          <div class="form-group">
            <label for="ga">Google Analytics Property ID</label>
            <input type="text" name="google_analytics" value="" class="form-control" placeholder="UA-XXXXXX">
          </div>

          <div class="form-group">
            <label for="ga">Help Video URL</label>
            <input type="text" name="help_video" value="" class="form-control" placeholder="http://youtu.be/asdf90123123n">
          </div>

          <div class="form-group">
            <label for="ga">Public Website URL</label>
            <input type="text" name="public_website" value="" class="form-control" placeholder="http://www.getjackdmethod.com/">
          </div>

        </form>

      </div>
    </div>

  </div>

@stop
