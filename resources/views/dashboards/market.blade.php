@extends('layouts.master')
@section('title', 'Plan It, Market It, Now Sell It!')
@section('bodyClass', 'dashboard-view marketing-dash package-pmsi')
@section('content')

<main class="dash market" id="marketing-dashboard">

<plan-modal v-cloak id="marketing-plans" :plans="plans" type="marketing" :show.sync="showModal">
    <h3 slot="header">Your Marketing Plans</h3>
</plan-modal>

<download-modal v-cloak id="marketing-plans" :plans="plans" type="marketing"
            :show.sync="showDownloadModal">
    <h3 slot="header">Download My Marketing Plan</h3>
</download-modal>



<div class="row">
    <div class="col-md-12">

    <div class="alert gj-alert">
      <div class="alert-body">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3>Welcome to Plan It, Market It, Now Sell It</h3>
        <hr />
        <p>You've reached your Dashboard, aka Mission Control. This is where everything is housed. 
      Here you’ll learn more about the media channels available to you and start building you marketing plan. </p>
      <p>Ready to go? Cool. Hit the ‘Brief Us’ tile, fill in the form and then lock in your briefing session with us to get started. </p>
      </div>
    </div>
        <header class="page-heading">
            <h2 class="text-center welcome-line">{{$package->name}}</h2>
        </header>

        @if(session('status'))
          <div style="margin: 1rem 0;" class="alert alert-success">
            <strong>Success, your email has been sent!</strong>
            <p>{{ session('status') }}</p>
          </div>
        @endif
    </div>
</div>
<div class="row">
<div class="col-md-12">

<div class="dashboard-tiles">
    <a href="{{ route('briefing') }}" class="gj-tile">
        <div class="tile-content">
            <i class="fa fa-suitcase"></i>

            <h3 class="tile-title">
                Brief Us
            </h3>
        </div>
    </a>
    <a href="{{ route('marketing-channel-dashboard') }}" class="gj-tile">
        <div class="tile-content">
            <i class="fa fa-bullhorn"></i>
            <h3 class="tile-title">
                Media Channels
            </h3>
        </div>
    </a>
    <a href="#" @click="showModal = true" class="gj-tile">
        <div class="tile-content">
            <i class="fa fa-pencil"></i>

            <h3 class="tile-title">
                My Marketing Plan
            </h3>
        </div>
    </a>
    <a href="#" class="gj-tile" @click="showDownloadModal = true">
        <div class="tile-content">
            <i class="fa fa-download"></i>

            <h3 class="tile-title">
                Download my Plan
            </h3>
        </div>
    </a>
    <a href="#" class="gj-tile">
        <div class="tile-content">
            <i class="fa fa-graduation-cap"></i>

            <h3 class="tile-title">
                Learn Stuff
            </h3>
        </div>
    </a>
    <a href="#" class="gj-tile">
        <div class="tile-content">
            <i class="fa fa-question-circle"></i>
            <h3 class="tile-title">
                Help Me
            </h3>
        </div>
    </a>
    </div>
</div>
</div>
<div class="row">

    <div class="col-md-6">
        <a href="{{$user->briefed ? '#' : '/briefing'}}" 
        data-appointment-type="1" 
        class="{{$user->briefed ? '' : 'disabled'}} {{$user->hasBriefingSession() ? 'disabled' : ''}} btn btn-block btn-lg btn-black {{$user->briefed ? 'bookAppointment' : 'appointment'}}">
            {{$user->hasBriefingSession() ? 'Booked For ' . $user->getBriefingSessionTime()->format('jS \o\f F') . ' at ' . $user->getBriefingSessionTime()->format('h:m A') : 'Book 2hr Briefing Session' }}
        </a>
    </div>

    <div class="col-md-6">
        <a href="{{$user->briefed ? '#' : '/briefing'}}" data-appointment-type="2" class="{{$user->briefed ? '#' : 'disabled'}} {{$user->ableToBookReview() ? '' : 'disabled'}} {{$user->hasReviewSession() ? 'disabled' : ''}} btn btn-block btn-lg btn-black {{$user->briefed ? 'bookAppointment' : 'appointment'}}">
            {{$user->hasReviewSession() ? 'Booked For ' . $user->getReviewSessionTime()->format('jS \o\f F') . ' at ' . $user->getReviewSessionTime()->format('h:m A') : 'Book 2hr Review Session' }}
        </a>
    </div>
</div>
</main>
@stop
