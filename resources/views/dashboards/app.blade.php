@extends('layouts.master')
@section('title', 'Dashboard')
@section('bodyClass', 'dashboard-view')
@section('content')
<main class="dash home" id="app-dashboard">
<modal v-cloak id="my-packages" :show.sync="showModal">
<h3 slot="header">{{$user->first_name}}'s Stuff </h3>
<div slot="body">
  <div class="package-flex">
    @foreach($user->packages as $package)
    <div class="package-tile">
      <div class="tile-inner">
        <i class="fa fa-suitcase"></i>
        <h4>{{$package->name}}</h4>
        @if($package->sku == "brand")
        <a href="/marketing-channels/introduction" class="btn btn-block btn-black">Get Started</a>
        @else
        <a href="{{ route($package->dashboard_url) }}" class="btn btn-block btn-black">Get Started</a>
        @endif
      </div>
    </div>
    @endforeach
    @foreach($unowned_packages as $up)
    <div class="package-tile unowned">
      <div class="tile-inner">
        <i class="fa fa-arrow-circle-up"></i>
        <h4>{{$up->name}}</h4>
        <p class="upgrade-price"><strong>{{ $up->price }}</strong></p>
        <a href="/upgrade/{{$up->id}}" class="btn btn-block btn-success">Purchase</a>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div slot="footer"></div>
</modal>
<div class="row">
  <div class="col-md-12">
  
    <div class="alert gj-alert">
      <div class="alert-body">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3>Welcome to GetJackD!</h3>
        <hr />
        <p>You've reached your Dashboard, aka Mission Control. This is where everything is housed, feel free to look around or check out our 'how to' video to get started. Jack's got you covered. Take it away Jack.</p>
        <p>
         
          <a href="#" class="btn btn-success" data-toggle="modal" data-target="#welcomeMessage">Watch Video</a>
        </p>
      </div>
    </div>

    <div class="modal fade" tabindex="-1" id="welcomeMessage" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Welcome to your method!</h4>
          </div>
          <div class="modal-body">
            <div class="welcome-message-wrapper">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IzhMzY5avLI"></iframe>
              </div>
            </div>
            
          </div>
          <div class="modal-footer">
          </div>
          </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <header class="page-heading">
            <h2 class="text-center welcome-line">Hi <strong>{{ $user->first_name }}</strong>, Welcome to Your Method</h2>
          </header>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="dashboard-tiles">
            <a href="#" id="userPackages" @click="showModal = true" class="gj-tile">
              <div class="tile-content">
                <i class="fa fa-suitcase"></i>
                <h3 class="tile-title">My Stuff</h3>
              </div>
            </a>
            <a href="#" class="gj-tile">
              <div class="tile-content">
                <i class="fa fa-graduation-cap"></i>
                <h3 class="tile-title">Learn Stuff </h3>
              </div>
            </a>
            <a href="#" class="gj-tile">
              <div class="tile-content">
                <i class="fa fa-users"></i>
                <h3 class="tile-title">Help Me</h3>
              </div>
            </a>
            <a href="#" class="gj-tile">
              <div class="tile-content">
                <i class="fa fa-comment-o"></i>
                <h3 class="tile-title">Being Social </h3>
              </div>
            </a>
            <a href="/upgrade" class="gj-tile">
              <div class="tile-content">
                <i class="fa fa-arrow-circle-up"></i>
                <h3 class="tile-title">Upgrade Me</h3>
              </div>
            </a>
            <a href="{{ route('profile') }}" class="gj-tile">
              <div class="tile-content">
                <i class="fa fa-user"></i>
                <h3 class="tile-title">My Deets</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
      </main>
      @stop