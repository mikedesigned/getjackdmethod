<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body bgcolor="#f5f8fa" style="-webkit-text-size-adjust:none;margin:0;padding:0;">
        <p>Thank you for upgrading to GetJack'D Method - {{$package->name}}. Head over to the website <a href="http:///www.getjackdmethod.com/dashboard">here.</a> Once logged in, you'll be able to enjoy the full features of the plan that you've purchased.</p>
        <h4>Purchased Plan</h4>
        <table style="text-align: left;">
            <caption>Here is the plan and the details for your account.</caption>
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Package</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$user->email}}</td>
                    <td>{{$package->name}}</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>