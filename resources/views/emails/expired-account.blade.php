@extends('emails.email-template')
@section('email_body')
	<span style="font-size:16px"><font face="arial, helvetica neue, helvetica, sans-serif"><strong>Oh No, it&#39;s over... But it doesn&#39;t have to be.&nbsp;</strong></font></span><br />
	&nbsp;
	<hr /><br />
	<span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:11px"><font color="#696969">Thank you so much for playing with us over the last two years, we&#39;ve loved having you on board.<br />
		<br />
		We&#39;re so sorry to say that from <strong>11.59pm&nbsp;AEST today</strong> your GetJackD! Method account will be no longer. To safeguard your security and what you&#39;ve completed during our time together, we&#39;ve deleted all of your content, plans and templates from our system, Adios.&nbsp;<br />
		<br />
	We can&#39;t thank you enough for being a part of our team and we do hope to see you again soon. If you ever want to look us up again you can find us at </font><font color="#016a97"><strong>https://www.getjackdmethod.com.&nbsp;</strong></font></span></span><br />
	<br />
	<span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:11px"><span style="color:#696969">Team GetJackD! Method</span></span></span>
@stop