@extends('emails.email-template')
@section('email_body')
                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:20px">Your <strong>Briefing Session</strong> Agenda... Get Excited!</span></span><br />
                &nbsp;
                <hr />&nbsp;
                <h3 style="text-align:center;color:#016A97;">{{$appointment->start_time->toDayDateTimeString()}} <br> <em>to</em> <br> {{$appointment->end_time->toDayDateTimeString()}}</h3>
                <br>
                <table style="border-color: 1px solid lightgray;" width="100%" cellpadding="1" cellspacing="1" border="1">
                    <tr>
                        <td><strong>Business Name</strong></td>
                        <td>{{$user->company}}</td>
                    </tr>
                    <tr>
                        <td><strong>Business Contact Name & Title</strong></td>
                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Contact Details</strong>
                        </td>
                        <td>
                            {{$user->email}}
                        </td>
                    </tr>
                </table>
                <br style="line-height: 20.8px;" />
                <span style="font-size:11px"><span style="font-size:12px"><strong>Time for your Briefing Session:</strong></span><br />
                <br />
                <span style="color:#696969">Stoked you&rsquo;re ready&nbsp;to market your business &ndash; yee-ha! Here&rsquo;s your agenda and a few bits and pieces to have set aside for our call.</span><br />
                &nbsp;<br />
            <strong>Please Have Ready</strong></span>
            <ul>
                <li><span style="font-size:11px"><span style="color:#696969">Your completed briefing form</span></span></li>
                <li><span style="font-size:11px"><span style="color:#696969"><span style="color:#696969"><span style="color:#696969">Any past marketing and media examples</span>&nbsp;&nbsp;</span></span></span></li>
            </ul>
            <span style="color:rgb(105, 105, 105); font-size:11px; line-height:20.8px">Please note: Each section will be kept to time, so lets get started ll.</span><br />
            <span style="color:#696969">&nbsp;</span><br />
            <strong>Your Agenda</strong><br />
            &nbsp;<br />
            <span style="color:#696969"><span style="font-size:11px">During our chat, we&rsquo;ll being going through:</span></span><br />
            &nbsp;<br />
            <strong style="font-size:11px; line-height:20.8px">1. Your business and current marketing activity </strong><u style="font-size:11px; line-height:20.8px">15 mins</u><br />
            <span style="color:#696969">&nbsp;</span><br />
            <span style="color:#696969"><span style="font-size:11px">Business overview. We want to hear all about you<br />
                <br />
            Current media and marketing activity including:</span></span>
            <ul>
                <li><span style="color:#696969"><span style="font-size:11px">What your&rsquo;re doing and who&rsquo;s managing it</span></span></li>
                <li><span style="color:#696969"><span style="font-size:11px">Your budget</span></span></li>
                <li><span style="color:#696969"><span style="font-size:11px">The campaign&rsquo;s strengths and weaknesses</span></span></li>
                <li><span style="color:#696969"><span style="font-size:11px">The results and whether you were happy</span></span></li>
            </ul>
            <span style="font-size:11px"><strong>2. Time to find out what you now want to market &nbsp;</strong><u>90 mins max</u><br />
                &nbsp;<br />
                <span style="color:#696969">What are you trying to market and what&rsquo;s your budget?<br />
                    &nbsp;<br />
                    What do you want to achieve from your marketing and media activity?<br />
                    &nbsp;<br />
                Lets take a look at building your marketing plan and the media channels:</span></span>
                <ul>
                    <li><span style="color:#696969"><span style="font-size:11px">We&rsquo;ll run through the plan and how to complete it</span></span></li>
                    <li><span style="color:#696969"><span style="font-size:11px">The media platforms and how they&rsquo;re relevant to your business and budget</span></span></li>
                    <li><span style="color:#696969"><span style="font-size:11px">Get you thinking about your plan and what will work for your business</span></span></li>
                    <li><span style="color:#696969"><span style="font-size:11px">We&rsquo;ll help you think about setting clear KPIs</span></span></li>
                    <li><span style="font-size:11px"><span style="color:#696969">We&rsquo;ll offer some recommendations to get you started</span></span></li>
                </ul>
                <span style="font-size:11px"><strong>3. Next steps </strong><u>15 mins max</u><br />
                    &nbsp;<br />
                    <span style="color:#696969">Recap how to use the portal and your chance to ask any questions. Speak now or forever hold your peace!</span></span><br />
                    &nbsp;
    @stop