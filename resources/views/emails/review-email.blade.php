@extends('emails.email-template')
@section('email_body')
				<span style="font-size:16px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif">Your <strong>Review&nbsp;Session </strong>Agenda... Get Excited!</span></span><br />
				&nbsp;
				<hr />&nbsp;
                <table style="border-color: 1px solid lightgray;" width="100%" cellpadding="1" cellspacing="1" border="1">
                    <tr>
                        <td><strong>Business Name</strong></td>
                        <td>{{$user->company}}</td>
                    </tr>
                    <tr>
                        <td><strong>Business Contact Name & Title</strong></td>
                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Contact Details</strong>
                        </td>
                        <td>
                            {{$user->email}}
                        </td>
                    </tr>
                </table>
				<br />
				<span style="font-family:arial,helvetica neue,helvetica,sans-serif"><strong>Time for your Review Session</strong><br />
					&nbsp;<br />
					<span style="color:#696969">So your plan is all done? Look. At. You. Go! So awesome.<br />
						<br />
						We&rsquo;re excited to review your plan and make sure you&rsquo;re ready to take flight and market your business. Here&rsquo;s your agenda and a few bits and pieces to have set aside for our call.<br />
					&nbsp;</span><br />
				<strong>Please Have Ready</strong></span>
				<ul>
					<li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">Your draft media/marketing plan</span></span></li>
					<li><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">Any questions you have about what you&rsquo;ve drafted</span></span></li>
				</ul>
				<span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"> <span style="color:#696969">Please Note: Each section will be kept to time, so lets get started<br />
				&nbsp;</span><br />
				<strong>Your Agenda</strong><br />
				&nbsp;<br />
				<span style="color:#696969">During our chat, we&rsquo;ll being going through:</span><br />
				&nbsp;<br />
			<strong>1. Building you plan overview </strong><u>10 mins max</u></span></span>
			<ul>
				<li><span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">How you found the process of building the plan?</span></span></span></li>
				<li><span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">Did you find any obstacles in building the plan?</span></span></span></li>
				<li><span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">Did you learn/need to know more to help you build the plan?</span></span></span></li>
			</ul>
			<span style="font-size:11px"> <span style="font-family:arial,helvetica neue,helvetica,sans-serif"> <strong>2. Going through your Marketing Plan </strong><u>100 mins max</u></span></span>
			<ul>
				<li><span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">Lets go through all sections of your plan</span></span></span></li>
				<li><span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">We&rsquo;ll offer feedback and recommendations on the sections of the plan</span></span></span></li>
				<li><span style="font-size:11px"><font color="#696969" face="arial, helvetica neue, helvetica, sans-serif">We&rsquo;ll talk through how you&rsquo;re going to&nbsp;implement&nbsp;your&nbsp;plan</font></span></li>
			</ul>
			<span style="font-size:11px"> <span style="font-family:arial,helvetica neue,helvetica,sans-serif"> <strong>3. Next Steps </strong><u>10 mins max</u></span></span>
			<ul>
				<li><span style="font-size:11px"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="color:#696969">Your chance to ask any questions. Speak now or forever hold your peace!</span></span></span></li>
			</ul>
@stop