<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: 1 COLUMN -->
        <!--[if gte mso 15]>
        <xml>
        <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>*|MC:SUBJECT|*</title>
        <style type="text/css">
        p{
        margin:10px 0;
        padding:0;
        }
        table{
        border-collapse:collapse;
        }
        h1,h2,h3,h4,h5,h6{
        display:block;
        margin:0;
        padding:0;
        }
        img,a img{
        border:0;
        height:auto;
        outline:none;
        text-decoration:none;
        }
        body,#bodyTable,#bodyCell{
        height:100%;
        margin:0;
        padding:0;
        width:100%;
        }
        #outlook a{
        padding:0;
        }
        img{
        -ms-interpolation-mode:bicubic;
        }
        table{
        mso-table-lspace:0pt;
        mso-table-rspace:0pt;
        }
        .time {
        color: red;
        }
        .ReadMsgBody{
        width:100%;
        }
        .ExternalClass{
        width:100%;
        }
        p,a,li,td,blockquote{
        mso-line-height-rule:exactly;
        }
        a[href^=tel],a[href^=sms]{
        color:inherit;
        cursor:default;
        text-decoration:none;
        }
        p,a,li,td,body,table,blockquote{
        -ms-text-size-adjust:100%;
        -webkit-text-size-adjust:100%;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
        line-height:100%;
        }
        a[x-apple-data-detectors]{
        color:inherit !important;
        text-decoration:none !important;
        font-size:inherit !important;
        font-family:inherit !important;
        font-weight:inherit !important;
        line-height:inherit !important;
        }
        #bodyCell{
        padding:10px;
        }
        .templateContainer{
        max-width:600px !important;
        }
        a.mcnButton{
        display:block;
        }
        .mcnImage{
        vertical-align:bottom;
        }
        .mcnTextContent{
        word-break:break-word;
        }
        .mcnTextContent img{
        height:auto !important;
        }
        .mcnDividerBlock{
        table-layout:fixed !important;
        }
        @media only screen and (min-width:768px){
        .templateContainer{
        width:600px !important;
        }
            }   @media only screen and (max-width: 480px){
        body,table,td,p,a,li,blockquote{
        -webkit-text-size-adjust:none !important;
        }
            }   @media only screen and (max-width: 480px){
        body{
        width:100% !important;
        min-width:100% !important;
        }
            }   @media only screen and (max-width: 480px){
        #bodyCell{
        padding-top:10px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImage{
        width:100% !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
        max-width:100% !important;
        width:100% !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnBoxedTextContentContainer{
        min-width:100% !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImageGroupContent{
        padding:9px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
        padding-top:9px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
        padding-top:18px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImageCardBottomImageContent{
        padding-bottom:9px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImageGroupBlockInner{
        padding-top:0 !important;
        padding-bottom:0 !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImageGroupBlockOuter{
        padding-top:9px !important;
        padding-bottom:9px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnTextContent,.mcnBoxedTextContentColumn{
        padding-right:18px !important;
        padding-left:18px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
        padding-right:18px !important;
        padding-bottom:0 !important;
        padding-left:18px !important;
        }
            }   @media only screen and (max-width: 480px){
        .mcpreview-image-uploader{
        display:none !important;
        width:100% !important;
        }
        }
    }</style></head>
    <body>
        <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <tr>
                <td align="center" valign="top" id="bodyCell">
                    <!-- BEGIN TEMPLATE // -->
                    <!--[if gte mso 9]>
                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                            <td align="center" valign="top" width="600" style="width:600px;">
                                <![endif]-->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                    <tr>
                                        <td valign="top" id="templatePreheader"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" id="templateHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
                                            <tbody class="mcnImageBlockOuter">
                                                <tr>
                                                    <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                        <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer" style="min-width:100%;">
                                                            <tbody><tr>
                                                                <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
                                                                    <img align="center" alt="" src="{{ URL::to('/') }}/img/gj-email_header.png" width="564" style="max-width:1056px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                        <tbody class="mcnDividerBlockOuter">
                                            <tr>
                                                <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                                                    <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 0px;border-top-style: solid;border-top-color: #EAEAEA;">
                                                        <tbody><tr>
                                                            <td>
                                                                <div id="emailBody" style="font-family:'Verdana', 'arial', sans-serif; font-size:12px;">
                                                                    @yield('email_body')
                                                                    <div style="text-align:center;">
                                                                        <a style="color: #016A97;text-decoration:none;font-weight:700; text-transform: capitlize;font-size: 18px;display: block; width: 180px; padding: 15px 25px; border: 2px solid #016A97;margin:0 auto;" href="{{ url('/dashboard') }}" alt="Enter Portal Now" title="Portal Link">Enter Portal Now</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                                    <!--
                                                    <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                        <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                        -->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" id="templateBody"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" id="templateFooter"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
                                            <tbody class="mcnDividerBlockOuter">
                                                <tr>
                                                    <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                                                        <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EEEEEE;">
                                                            <tbody><tr>
                                                                <td>
                                                                    <span></span>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                        <!--
                                                        <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                            <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                            -->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                            <tbody class="mcnTextBlockOuter">
                                                <tr>
                                                    <td valign="top" class="mcnTextBlockInner">
                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                                                            <tbody><tr>
                                                                <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                                                    <div style="text-align: center;"><em style="font-family:tahoma,verdana,segoe,sans-serif; font-size:10px; line-height:20.8px; text-align:center">Copyright © 2015. GetJackD! Method. All rights reserved.</em><br style="font-family: tahoma, verdana, segoe, sans-serif; font-size: 10px; line-height: 20.8px; text-align: center;">
                                                                        <br style="font-family: tahoma, verdana, segoe, sans-serif; font-size: 10px; line-height: 20.8px; text-align: center;">
                                                                        <strong style="font-family:tahoma,verdana,segoe,sans-serif; font-size:10px; line-height:20.8px; text-align:center">Our mailing address is:</strong><br style="font-family: tahoma, verdana, segoe, sans-serif; font-size: 10px; line-height: 20.8px; text-align: center;">
                                                                        <span style="font-family:tahoma,verdana,segoe,sans-serif; font-size:10px; line-height:20.8px; text-align:center">PO Box 64 Broadbeach, Queensland 4218</span></div>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table></td>
                                        </tr>
                                    </table>
                                    <!--[if gte mso 9]>
                                </td>
                            </tr>
                        </table>
                        <![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
            </center>
        </body>
    </html>