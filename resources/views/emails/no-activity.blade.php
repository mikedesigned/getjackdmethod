@extends('emails.email-template')
@section('email_body')
				<span style="font-size:16px"><font face="arial, helvetica neue, helvetica, sans-serif"><strong>Where have you been?&nbsp;It&#39;s been a while.&nbsp;</strong></font></span><br />
				&nbsp;
				<hr /><br />
				<span style="font-size:11px"><span style="color:#696969">We&rsquo;ve noticed you haven&rsquo;t logged into your Method for a while. You&rsquo;re missing out on a heap of good stuff you know. If you&rsquo;re ready, simply click on the&nbsp;<strong>enter portal now&nbsp;</strong>button<strong>&nbsp;</strong>and we&rsquo;ll see you back in the portal.<br />
					&nbsp;<br />
				Team GetJackD! Method</span></span><br />
				&nbsp;
@stop