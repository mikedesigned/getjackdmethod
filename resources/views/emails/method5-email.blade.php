@extends('emails.email-template')
@section('email_body')
                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:18px">Welcome to <strong>GetJackD! Method</strong>. Are you ready to take flight?</span></span><br />
                &nbsp;
                <hr /><br />
                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:11px"><span style="color:#696969">We&#39;re excited you&#39;re joining us. Time to get started. Here&rsquo;s how you access your Method:</span><br />
                <br />
                <span style="color:#696969"><strong>Step one: </strong>click </span><span style="color:#016A97"><strong>here</strong> </span><span style="color:#696969">to log in to your portal</span><br />
                <span style="color:#696969"><strong>Step two:</strong> once logged in, head to &lsquo;My Deets&rsquo; to personalise your login</span></span></span><br />
                <strong style="color:rgb(105, 105, 105); font-family:arial,helvetica neue,helvetica,sans-serif; font-size:11px; line-height:20.8px">Step three:&nbsp;</strong><span style="color:rgb(105, 105, 105); font-family:arial,helvetica neue,helvetica,sans-serif; font-size:11px; line-height:20.8px">Ready to start? Click on &#39;My Stuff&#39; to access your &#39;Plan it, Market it, Now Sell it&#39; Method</span><br />
                <strong style="color:rgb(105, 105, 105); font-family:arial,helvetica neue,helvetica,sans-serif; font-size:11px; line-height:20.8px">Step four:</strong><span style="color:rgb(105, 105, 105); font-family:arial,helvetica neue,helvetica,sans-serif; font-size:11px; line-height:20.8px">&nbsp;Once in, please have a look around or if you&#39;re ready to go, hit &#39;Brief Us&#39; to get underway&nbsp;</span><br />
                <br />
                <span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:11px"><span style="color:#696969">And this is where we leave you, adios and good luck!</span><br />
                <br />
                <span style="color:#696969">Only joking! We&rsquo;ll be</span> <span style="color:#016A97"><strong>here </strong></span><span style="color:#696969">with useful info and advice about business and marketing, plus some fun stories and interviews. We love a pun or two so hopefully you laugh with us (and not at us). &nbsp;</span></span><br />
                <span style="font-size:11px"><span style="color:#696969">&nbsp;<br />
                    You can also give us a &lsquo;thumbs up&rsquo; on </span><a href="https://www.facebook.com/GetJackD-Method-1416495761987957/" target="_blank"><span style="color:#016A97"><strong>Facebook</strong></span></a> <span style="color:#696969">too.</span><br />
                    <br />
                    <span style="color:#696969">Happy Method-ing.&nbsp;<br />
                        <br />
                    Team GetJackD! Method</span></span></span>
    @stop